## 1.0.9 (2023-03-31)

### fixed (3 changes)

- [Update the swagger docs for items moving folders](subshq/server/api@7b3df181b78b6f99d8d83a6f1a801869a1ff38df) ([merge request](subshq/server/api!544))
- [Allow moving items in and out of folders](subshq/server/api@fdd5d9869e00fc309deaaf9ede009d4010b48617) ([merge request](subshq/server/api!542))
- [Use presence to select the display name or email](subshq/server/api@793d1e74e88a8bab648631f4a81826c289d26cee) ([merge request](subshq/server/api!537))

### added (1 change)

- [Add breaches to brands](subshq/server/api@7f6f2f5409bdc2d5097d2d5f05219ecc9f3ca286) ([merge request](subshq/server/api!532))

## 1.0.8 (2023-03-22)

### added (5 changes)

- [Deep merge user settings](subshq/server/api@ae9af14e5a80ac24eea949ae390a44ae8404f145) ([merge request](subshq/server/api!535))
- [Make CRUD endpoints for the folders](subshq/server/api@de65d1289939225809981e7968db8e157f68c261) ([merge request](subshq/server/api!528))
- [Track the auth provider](subshq/server/api@50874aa08f463b35bf77c6ef5c8f61af81e8eb77) ([merge request](subshq/server/api!527))
- [Add confirm referral mail](subshq/server/api@d2fd4c17d3a4ae6c014f6a62f91681b76f904ce2) ([merge request](subshq/server/api!530))
- [Track which platform did the request for items](subshq/server/api@a3e25df690cf93ee8274e102047d6cf12ed1596e) ([merge request](subshq/server/api!526))

### changed (1 change)

- [Remove Sentry error on contact duplicate](subshq/server/api@766c5c3de92dca42c2cd76cd462bfde05c58101d) ([merge request](subshq/server/api!529))

## 1.0.7 (2023-03-10)

### other (1 change)

- [Update gems](subshq/server/api@0160234cadba27de4de04ddad76a09951603ba5e) ([merge request](subshq/server/api!523))

### fixed (3 changes)

- [Update folder names when creating memberships](subshq/server/api@48ec1b26bc7bbc1b4269c93f94c3dc9ccc061598) ([merge request](subshq/server/api!520))
- [Scope accepted memberships to require an encrypted private key](subshq/server/api@0d68fc755bd775d6cbfc3043851cbd169e8400c6) ([merge request](subshq/server/api!521))
- [Invite button goes to signup page](subshq/server/api@064d90cd24f34082fa83b61aad80fd8784e98cf3) ([merge request](subshq/server/api!519))

### added (1 change)

- [Paginate the secrets endpoint](subshq/server/api@242788c049a6fc8c531fc3b3652d2d7476df4762) ([merge request](subshq/server/api!474))

### changed (1 change)

- [Added styling from figma to acceptance pages](subshq/server/api@5f2ac6bfcd8b2e78fd52bd5a8570838071a49bf5) ([merge request](subshq/server/api!482))

## 1.0.6 (2023-03-08)

### added (9 changes)

- [Fix email not being send after share](subshq/server/api@9da8ddc45ea42ec2f7238bfdee733b68c5e4399f) ([merge request](subshq/server/api!516))
- [Add track events](subshq/server/api@3a0dbc7346cec06982c1c85307be858576a9f8f6) ([merge request](subshq/server/api!515))
- [Add a eligable user to FolderMemberships after signup](subshq/server/api@823185860af53c81ae04ecdbf93d7d639574c063) ([merge request](subshq/server/api!509))
- [Endpoint for pending invitations](subshq/server/api@cc3bba043c25662644b4f0cb17380f7b82ca39f4) ([merge request](subshq/server/api!507))
- [[TurboSprint] Add user to membership on signup](subshq/server/api@db04d5b3f7a4e7674026cebd59b2984171e77950) ([merge request](subshq/server/api!502))
- [Add referrals](subshq/server/api@c74b954ee7017db578b16a8df74d81f8cd8a5a92) ([merge request](subshq/server/api!511))
- [[TurboSprint] Add email to folder_membership to create it without a user first](subshq/server/api@f3f141bbafbcc70931e909d27f35c363c03c9f03) ([merge request](subshq/server/api!501))
- [[TurboSprint] Add contact list](subshq/server/api@daaf8f7d771a261842c83edd2842b572cee7fb16) ([merge request](subshq/server/api!498))
- [Send follow up mail after user delete's profile](subshq/server/api@9dd80ff2edf84cbd850a8cabbf6d8dfbbbc2fe21) ([merge request](subshq/server/api!497))

### refactor (1 change)

- [Rename models and columns to new data model](subshq/server/api@a657eed0bc104fd041f03fd5759ae96976994610) ([merge request](subshq/server/api!500))

### fixed (1 change)

- [Use the display name if given_name and family_name are empty](subshq/server/api@7c248c0addb710ecf0fac3283cf7cc06bb876528) ([merge request](subshq/server/api!486))

## 1.0.5 (2023-02-27)

### added (2 changes)

- [Add theme of icons to the images hash](subshq/server/api@9fc1804f63c6cddccf486225e1eaecbe12c7d21e) ([merge request](subshq/server/api!487))
- [Add trigger property to Sign In event](subshq/server/api@f5693b134fb658352ca882cc3d96766683cfd345) ([merge request](subshq/server/api!490))

## 1.0.4 (2023-02-24)

### changed (2 changes)

- [Update mixpanel event to better recognize them in mixpanel](subshq/server/api@9dbf0634fb9bbef2be9f7f12583f07230a60c923) ([merge request](subshq/server/api!494))
- [Capture JWT error execption in Sentry](subshq/server/api@e4904c2fb186cbc0af3f5fcd26c648fb77c2fda5) ([merge request](subshq/server/api!488))

### fixed (2 changes)

- [Return null for the creator if the creator is removed](subshq/server/api@aed35611cac27441e532490be58e551ffe240b0f) ([merge request](subshq/server/api!492))
- [Sanitize user input before putting it in the mail](subshq/server/api@fe037c960c325fb20572f309c6837166a434fd47) ([merge request](subshq/server/api!483))

### added (2 changes)

- [Create organizations](subshq/server/api@4bc787d44255c0ece5713afebe69d4f80aa6d815) ([merge request](subshq/server/api!489))
- [Allow webrequest api status as anonymous event](subshq/server/api@96c491b982b2a2d4c08017165886de79f7273969) ([merge request](subshq/server/api!485))

### performance (1 change)

- [Added more logging logic to Rack-Attack](subshq/server/api@6978bbb4f2206ce379c74e16f9a153e2c162c672) ([merge request](subshq/server/api!466))

## 1.0.3 (2023-02-17)

### changed (1 change)

- [Resolve "Return the clients used to the user, as part of the user profile"](subshq/server/api@e41eac5158b5338d2fa1dbf6852977fbb2eec040) ([merge request](subshq/server/api!470))

### other (2 changes)

- [Cleanup Swagger and tests](subshq/server/api@dd0140c7417189de48d0dcb212672acd804ae1e1) ([merge request](subshq/server/api!472))
- [Add style-src CSP](subshq/server/api@079d4947ba6ef452d5c8ff6b3066f96507fd39d7) ([merge request](subshq/server/api!473))

### added (5 changes)

- [Remove users from mailchimp list after destroying their profile](subshq/server/api@3dab4ae861f151caca275492d682b81cf3fdcc93) ([merge request](subshq/server/api!443))
- [Update rate limit thresholds](subshq/server/api@449d97947a87c1dc9f66cb91c52b1d7471a5e921)
- [Add folders](subshq/server/api@69bd7f4a76ab283b58d193ec0804ee7e8e9acacc) ([merge request](subshq/server/api!462))
- [Add public_key endpoint logic](subshq/server/api@51cd3bd6fd0118873cc240b28166cbe4d637c049) ([merge request](subshq/server/api!458))
- [Add rack-attack](subshq/server/api@ff588da558767dcf782cc908edea4291c4d22940) ([merge request](subshq/server/api!436))

### fixed (4 changes)

- [Accept the share link encrypted_master_key for an item](subshq/server/api@4e164093f999feab11a24d9d59de44d1198dd582) ([merge request](subshq/server/api!475))
- [Resolve "Users without keypair are not correctly filtered in te publik_key...](subshq/server/api@c24dc88270e5888556bd6d00fe1c678473d603b4) ([merge request](subshq/server/api!469))
- [Remove temporary field acceptance_token](subshq/server/api@c8130ea11e5ecb0a8f1bfca7fec3e7b25708448e) ([merge request](subshq/server/api!465))
- [Fix link to an item on the success page](subshq/server/api@6b5c3fdb04675efccba9df756003fe1aac647a2c) ([merge request](subshq/server/api!464))

## 1.0.2 (2023-02-01)

### fixed (1 change)

- [Lower the transaction samples for sentry](subshq/server/api@72d6a95003ba991828c61cc7d89c4fa656612f3a) ([merge request](subshq/server/api!446))

### performance (1 change)

- [Update gems](subshq/server/api@4dae12ea18624131bf6997f935045039b1cdd549) ([merge request](subshq/server/api!447))

### other (1 change)

- [Increase the timeout for brandfetch](subshq/server/api@857904797978dade4535dbaeed90ebcb3804d80e) ([merge request](subshq/server/api!444))

## 1.0.1 (2023-01-31)

### added ( change)

- [Add setting for access token expiration time](https://gitlab.com/subshq/server/api/-/commit/3f6eba7b90497f24f0e25c2da7b762277b7d348c) ([merge request](https://gitlab.com/subshq/server/api/-/merge_requests/442))

### performance ( change)

- [Use oj to optimize the json generation](https://gitlab.com/subshq/server/api/-/commit/54ea125939132ef85580657319a77d235229a76f) ([merge request](https://gitlab.com/subshq/server/api/-/merge_requests/437))

### fixed (2 changes)

- [Fix insert fail for brands](subshq/server/api@0b53b3663a404e5a671e36620ee2e52917a22d19) ([merge request](subshq/server/api!422))
- [Remove catch all in the routes](https://gitlab.com/subshq/server/api/-/commit/1506460121c816c94e78bb82821c537749e2051e) ([merge request](https://gitlab.com/subshq/server/api/-/merge_requests/427))

### other (1 change)

- [Split up the swagger schemas](https://gitlab.com/subshq/server/api/-/commit/04e72571ffecd05be74c6b14a02db84a938d31b4) ([merge request](https://gitlab.com/subshq/server/api/-/merge_requests/430))

## 1.0.0 (2023-01-26)

### added (1 change)

- [Update the versions with a script](https://gitlab.com/subshq/server/api/-/commit/ad9aecf0eab91bb5f64e06a24b6c0938072db94e) ([merge request](https://gitlab.com/subshq/server/api/-/merge_requests/419))
