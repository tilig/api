json.extract!(
  @keypair,
  :encrypted_private_key,
  :public_key,
  :key_type,
  :meta_data,
  :kid
)
