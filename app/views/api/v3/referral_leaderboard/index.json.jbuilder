json.referral_leaderboard do
  json.me do
    json.partial! "api/v3/referral_leaderboard/referral_leaderboard", leader: @current_user_position
  end

  json.top_list do
    json.array! @leaders, partial: "api/v3/referral_leaderboard/referral_leaderboard", as: :leader
  end
end
