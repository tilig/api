json.extract!(organization, :id, :name, :created_at, :updated_at)

json.users do
  json.array! organization.users, partial: "user", as: :user
end

json.invitations do
  json.array! organization.invitations.ordered, partial: "api/v3/organizations/invitations/invitation", as: :invitation
end
