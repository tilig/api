json.extract!(invitation, :id, :expire_at, :created_at, :updated_at, :accepted_at)

json.creator do
  if invitation.creator
    json.extract!(invitation.creator, :id, :email, :display_name, :picture)
  else
    json.null!
  end
end

json.organization do
  json.extract!(invitation.organization, :id, :name)
end
