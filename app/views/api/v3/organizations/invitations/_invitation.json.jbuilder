json.extract!(invitation, :id, :email, :accepted_at, :expire_at, :created_at, :updated_at)

json.pending invitation.pending?
json.expired invitation.expired?

json.creator do
  if invitation.creator
    json.extract! invitation.creator, :id, :email, :display_name, :picture
  else
    json.null!
  end
end

json.invitee do
  if invitation.accepted? && invitation.invitee.present?
    json.extract! invitation.invitee, :id, :email, :display_name, :picture
  else
    json.null!
  end
end
