json.invitations do
  json.array! @invitations, partial: "api/v3/organizations/invitations/invitation", as: :invitation
end
