json.invitation do
  json.partial! "api/v3/organizations/invitations/show_invitation", invitation: @invitation
end
