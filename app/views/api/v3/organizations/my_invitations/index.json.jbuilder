json.pending_invitations do
  json.array! @pending_invitations, partial: "api/v3/organizations/invitations/show_invitation", as: :invitation
end
