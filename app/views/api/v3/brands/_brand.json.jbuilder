if brand
  json.extract! brand.parent || brand, :totp, :main_color_hex, :main_color_brightness, :images, :logo_source, :logo_icon_source

  json.extract! brand, :id, :domain, :public_suffix_domain, :breached_at, :breach_published_at

  json.name brand.name || brand.domain

  json.is_fetched brand.parent&.fetch_date? || brand.fetch_date?

else
  json.null!
end
