json.extract!(
  secret,
  :id,
  :template,
  :website,
  :encrypted_details,
  :encrypted_overview,
  :encryption_version,
  :created_at,
  :updated_at
)

# Legacy names of keys
json.encrypted_key secret.encrypted_dek
json.encrypted_dek secret.rsa_encrypted_dek

# Migration helper fields
json.legacy_encryption_disabled secret.legacy_encryption_disabled?

# Legacy Fields
unless secret.legacy_encryption_disabled?
  json.extract!(
    secret,
    :android_app_id,
    :name,
    :notes,
    :otp,
    :password,
    :username
  )
end

# Domain and brands
json.domain secret.public_suffix_domain
json.brand do
  json.partial!("api/v3/brands/brand", brand: secret.brand, as: :brand)
end
json.share do
  if secret.share_link.present?
    json.partial!("api/v3/shares/share", share: secret.share_link)
  else
    json.null!
  end
end
