json.extract! contact, :id, :email, :display_name, :picture
json.public_key contact.keypair.public_key

json.source (contact.organization.present? && contact.organization == Current.user.organization) ? "Team" : "Contact"
json.organization do
  if contact.organization.present? && contact.organization == Current.user.organization
    json.extract!(contact.organization, :id, :name)
  else
    json.null!
  end
end
