json.extract! membership, :id, :role, :updated_at, :created_at, :user_id

json.group do
  json.partial!("api/v3/groups/group", group: membership.group)
end
