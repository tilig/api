json.updates do
  json.array! @updated_secrets, partial: "api/v3/secrets/secret", as: :secret
end

json.deletions @deleted_secret_ids

json.fetched_at @fetched_at
