json.updates do
  json.array! @updated_items, partial: "api/v3/items/item", as: :item
end

json.deletions @deleted_item_ids

json.fetched_at @fetched_at
