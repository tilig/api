json.extract!(
  user,
  :id,
  :email
)

json.extract!(
  user.keypair,
  :kid,
  :public_key,
  :key_type
)
