json.user do
  json.partial! "api/v3/users/public_keys/public_key", user: @user
end
