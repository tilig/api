json.extract!(
  item,
  :id,
  :template,
  :website,
  :encrypted_dek,
  :encrypted_folder_dek,
  :rsa_encrypted_dek,
  :encrypted_details,
  :encrypted_overview,
  :encryption_version,
  :created_at,
  :updated_at
)

# Migration helper fields
json.legacy_encryption_disabled item.legacy_encryption_disabled?

# Legacy Fields
unless item.legacy_encryption_disabled?
  json.extract!(
    item,
    :android_app_id,
    :name,
    :notes,
    :otp,
    :password,
    :username
  )
end

# Domain and brands
json.domain item.public_suffix_domain
json.brand do
  if item.brand
    json.partial! "api/v3/brands/brand", brand: item.brand
  else
    json.null!
  end
end

json.folder do
  if item.folder
    json.partial!("api/v3/folders/folder", folder: item.folder)
  else
    json.null!
  end
end

json.share_link do
  if item.share_link
    json.partial!("api/v3/items/share_links/share_link", share_link: item.share_link)
  else
    json.null!
  end
end
