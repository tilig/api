json.array! @password_versions, as: :password_versions do |password_version|
  json.extract! password_version, :id, :password, :created_at
end
