json.extract!(
  Current.user,
  :id,
  :email,
  :public_key,
  :display_name,
  :given_name,
  :family_name,
  :locale,
  :picture,
  :provider_id,
  :created_at,
  :updated_at
)

json.keypair do
  if Current.user.keypair.present?
    json.extract!(
      Current.user.keypair,
      :encrypted_private_key,
      :public_key,
      :key_type,
      :kid
    )
  else
    json.null!
  end
end

json.extract!(Current.user.profile, :application_settings, :user_settings)
