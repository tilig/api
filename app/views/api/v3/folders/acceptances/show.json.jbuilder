json.folder_membership do
  json.partial! @folder_membership, partial: "api/v3/folders/folder_memberships/folder_membership", as: :folder_membership
end
