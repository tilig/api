json.folders do
  json.array! @folders, partial: "api/v3/folders/folder", as: :folder
end

json.pending_memberships do
  json.array! @pending_memberships, partial: "api/v3/folders/folder_memberships/folder_membership", as: :folder_membership
end
