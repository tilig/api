json.extract! folder_membership, :id, :role, :accepted_at, :created_at, :updated_at

json.accepted folder_membership.accepted?
json.pending folder_membership.pending?
json.is_me folder_membership.user_id == Current.user&.id
json.missing_private_key folder_membership.encrypted_private_key.blank? && folder_membership.user.present?

json.creator do
  if folder_membership.creator
    json.extract! folder_membership.creator, :id, :display_name, :email, :picture
  else
    json.id nil
    json.email nil
    json.display_name nil
    json.picture nil
  end
end

json.user do
  if folder_membership.user
    json.extract! folder_membership.user, :id, :email
    json.public_key folder_membership.user.keypair&.public_key

    if folder_membership.accepted?
      json.extract! folder_membership.user, :display_name, :picture
    else
      json.display_name nil
      json.picture nil
    end
  else
    json.email folder_membership.email
    json.id nil
    json.display_name nil
    json.picture nil
    json.public_key nil
  end
end
