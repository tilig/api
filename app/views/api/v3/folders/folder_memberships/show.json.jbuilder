json.folder_membership do
  json.partial! @folder_membership
end

json.folder do
  json.extract! @folder, :id, :name, :created_at, :updated_at
end
