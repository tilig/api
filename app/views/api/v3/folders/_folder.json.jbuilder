json.extract!(folder, :id, :name, :public_key, :single_item, :description, :created_at, :updated_at)

json.my_encrypted_private_key folder.user_encrypted_private_key(Current.user) || json.null!

json.folder_memberships do
  json.array! folder.accepted_folder_memberships,
    partial: "api/v3/folders/folder_memberships/folder_membership",
    as: :folder_membership
end

json.folder_invitations do
  json.array! folder.pending_folder_invitations,
    partial: "api/v3/folders/folder_memberships/folder_membership",
    as: :folder_membership
end
