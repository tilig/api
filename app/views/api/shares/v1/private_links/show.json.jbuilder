json.extract! @share, :uuid, :encrypted_dek
# Expose as new name
json.encrypted_key @share.encrypted_dek

json.extract!(
  @share.item,
  :encrypted_details,
  :encrypted_overview,
  :encryption_version,
  :template,
  :website
)

json.domain @share.item.public_suffix_domain
json.user_display_name @share.item.user.present? ? @share.item.user.display_name : "Shared item"

json.brand do
  json.partial!("api/v3/brands/brand", brand: @share.item.brand, as: :brand)
end
