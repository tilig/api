class FetchBrandJob < ApplicationJob
  queue_as :default

  def perform(brand)
    brand.fetch_brand_attributes(brand.domain)
  end
end
