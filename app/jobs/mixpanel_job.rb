class MixpanelJob < ApplicationJob
  queue_as :default

  def perform(type, content)
    consumer.send!(type, content)
  end

  private

  def consumer
    @consumer ||= if Rails.application.config.mixpanel_token
      Mixpanel::FaradayConsumer.new
    else
      Mixpanel::VoidConsumer.new(logger)
    end
  end
end
