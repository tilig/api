class Mailchimp::AddMemberJob < ApplicationJob
  queue_as :default

  def perform(user)
    return unless Rails.application.config.mailchimp_api_key
    @user = user

    client.lists.add_list_member(list_id, member_properties)
  end

  private

  def client
    @client ||= MailchimpMarketing::Client.new({
      api_key: Rails.application.config.mailchimp_api_key,
      server: Rails.application.config.mailchimp_server
    })
  end

  def member_properties
    {
      email_address: @user.email,
      status: "subscribed",
      language: @user.locale,
      merge_fields:
    }.compact_blank
  end

  def merge_fields
    {
      FNAME: @user.given_name || (@user.display_name unless @user.family_name),
      LNAME: @user.family_name
    }.compact_blank
  end

  def list_id
    Rails.application.config.mailchimp_list_id
  end
end
