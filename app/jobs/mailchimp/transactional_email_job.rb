class Mailchimp::TransactionalEmailJob < ApplicationJob
  queue_as :default

  def perform(template)
    client.messages.send_template(template)
  end

  private

  def client
    @client ||= MailchimpTransactional::Client.new(
      Rails.application.config.mandrill_api_key
    )
  end
end
