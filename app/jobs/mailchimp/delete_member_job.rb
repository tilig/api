class Mailchimp::DeleteMemberJob < ApplicationJob
  queue_as :default

  def perform(user_hash)
    return unless Rails.application.config.mailchimp_api_key
    @user_hash = user_hash

    client.lists.delete_list_member_permanent(list_id, user_email_hash)
  end

  private

  def client
    @client ||= MailchimpMarketing::Client.new({
      api_key: Rails.application.config.mailchimp_api_key,
      server: Rails.application.config.mailchimp_server
    })
  end

  def list_id
    Rails.application.config.mailchimp_list_id
  end

  def user_email_hash
    Digest::MD5.hexdigest(@user_hash["email"])
  end
end
