class MailchimpService
  def initialize(event, user)
    @event = event
    @user = user
  end

  def send_event
    case @event
    when Events::SIGN_UP
      Mailchimp::AddMemberJob.perform_later(@user)
    when Events::USER_DELETED
      Mailchimp::DeleteMemberJob.perform_later(@user.attributes)
    end
  end
end
