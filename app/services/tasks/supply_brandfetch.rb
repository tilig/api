class Tasks::SupplyBrandfetch
  BRANDS_INSERT_LIMIT = 1000
  STEPS = 7

  def execute
    STEPS.times do
      attributes = Parallel
        .map(updateable_brands) { |brand| Brandfetch::AttributesFetcher.new(brand.domain, brand).fetch }
        .compact
        .reject { |b| b[:name].nil? }

      upsert_all attributes
    end
  end

  private

  def updateable_brands
    Brand
      .where(brandfetch_json: nil)
      .reorder("rank::integer asc")
      .limit(BRANDS_INSERT_LIMIT)
  end

  def upsert_all(attributes)
    return if attributes.blank?

    Brand
      .where(created_at: Time.current, updated_at: Time.current)
      .upsert_all(attributes, unique_by: :domain, returning: :id)
  end
end
