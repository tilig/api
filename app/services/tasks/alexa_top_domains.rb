require "csv"

class Tasks::AlexaTopDomains
  ALEXA_CSV = "./files/alexa_1_million.csv"
  TOTAL_DOMAINS = 1000000
  DOMAINS_PER_INSERT_ALL = 1000

  def initialize
    @csv = CSV.open(ALEXA_CSV, "r", headers: true)
  end

  def import_brands
    # Slicing in steps of 1000 for postgres insert into
    (0..TOTAL_DOMAINS - 1).each_slice(DOMAINS_PER_INSERT_ALL) do |rows|
      csv_rows = rows.map { @csv.readline }
      break unless csv_rows.any?

      domains = csv_rows.pluck("Domain").compact
      brands = Brand.where(domain: domains)

      attributes = generate_attributes_for(csv_rows, brands)

      upsert_all attributes
    end
  end

  private

  def upsert_all(attributes)
    return if attributes.empty?

    Brand
      .where(created_at: Time.now.utc, updated_at: Time.now.utc)
      .upsert_all(attributes, unique_by: :domain)
  end

  def generate_attributes_for(csv_rows, brands)
    csv_rows.map do |csv_row|
      normalized_domain = DomainNormalizer.new(csv_row["Domain"])
      domain = normalized_domain.domain
      public_suffix_domain = normalized_domain.public_suffix_domain

      brand = brands.find { |brand| brand.domain == domain }

      {
        domain: domain,
        public_suffix_domain: public_suffix_domain,
        rank: csv_row["GlobalRank"],
        totp: brand&.totp || false
      }
    end
      .reject { |attributes| attributes[:public_suffix_domain].nil? }
      .uniq { |attributes| attributes[:domain] }
  end
end
