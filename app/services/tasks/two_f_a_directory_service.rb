class Tasks::TwoFADirectoryService
  API_URL = "https://2fa.directory/api/v3/all.json"

  def import_brands
    fetch_json
      .then { |json| map_tfa_directory_brands(json) }
      .then { |map| map.uniq { |brand| brand[:domain] } }
      .then { |map| map.reject { |brand| brand[:domain].nil? || brand[:public_suffix_domain].nil? } }
      .then { |brands| upsert_all(brands) }
  end

  private

  def fetch_json
    response = Faraday.new(API_URL).get

    JSON.parse(response.body)
  end

  def map_tfa_directory_brands(json)
    json.flat_map do |brand_name, brand_info|
      all_domains(brand_info).map do |domain|
        normalized_domain = DomainNormalizer.new(domain)

        brand_info_to_hash(brand_name, brand_info, normalized_domain)
      end
    end
  end

  def upsert_all(brands)
    # The where is needed to set the timestamps on insert and cannot be nil.
    Brand
      .where(created_at: Time.current, updated_at: Time.current)
      .upsert_all(brands, unique_by: :domain, returning: :id)
  end

  def all_domains(brand_info)
    (brand_info["additional-domains"] || []).push(brand_info["domain"])
  end

  def brand_info_to_hash(brand_name, brand_info, normalized_domain)
    {
      name: brand_name,
      domain: normalized_domain.domain,
      public_suffix_domain: normalized_domain.public_suffix_domain,
      totp: totp?(brand_info),
      json: brand_info
    }
  end

  def totp?(brand_info)
    brand_info["tfa"]&.include?("totp") || false
  end
end
