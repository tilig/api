class Tasks::Contacts::Fill
  def execute
    folder_memberships = FolderMembership.accepted.joins(:folder).includes(:user, folder: :creator).where("folder_memberships.user_id != folders.creator_id")

    folder_memberships.find_each do |folder_membership|
      Contact.make_contacts(folder_membership.user, folder_membership.folder.creator)
    end
  end
end
