class Tasks::ReparseBrandsBrandfetchjson
  UPSERT_LIMIT = 500

  def execute
    fetched_brands.in_batches(of: UPSERT_LIMIT) do |brands|
      attributes = brands.map do |brand|
        Brandfetch::AttributesParser.new(brand.brandfetch_json, brand.domain, brand).parse
      end
      upsert_all attributes
    end
  end

  private

  def fetched_brands
    Brand.where.not(fetch_date: nil)
  end

  def upsert_all(attributes)
    return if attributes.blank?

    Brand
      .where(created_at: Time.now.utc, updated_at: Time.now.utc)
      .upsert_all(attributes, unique_by: :domain)
  end
end
