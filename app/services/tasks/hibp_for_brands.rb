class Tasks::HibpForBrands
  HIBP_BREACHES_URL = "https://haveibeenpwned.com/api/v3/breaches"
  HIBP_API_TOKEN = Rails.application.config.hibp_api_token
  TIMEOUT_SECONDS = 5

  def import_breaches
    breached_hashes = fetch_breaches

    breached_domains = breached_hashes.pluck("Domain")
    breached_brands = Brand.where(domain: breached_domains)

    insert_data = breached_hashes.map { |bh|
      brand = breached_brands.to_a.find { |b| b.domain == bh["Domain"] }
      brand_attributes(bh, brand)
    }

    updated_brand_ids = upsert_all(insert_data).to_a.pluck("id")
    Item.where(brand_id: updated_brand_ids).touch_all
  end

  def self.connection
    @connection ||= Faraday.new(url: HIBP_BREACHES_URL).tap do |conn|
      conn.request :authorization, "Bearer", HIBP_API_TOKEN
      conn.request :json
      conn.response :json
      conn.options.timeout = TIMEOUT_SECONDS
      conn.options[:open_timeout] = TIMEOUT_SECONDS
    end
  end

  private

  def fetch_breaches
    latest_breach = Brand.maximum(:breach_published_at)
    response = connection.get

    response.body
      .filter { |breach| valid_breach?(breach, latest_breach) }
      .sort_by { |bh| Time.zone.parse(bh["AddedDate"]) }
      .uniq { |bh| bh["Domain"] }
  end

  def brand_attributes(breached_hash, brand)
    DomainNormalizer.new(breached_hash["Domain"]).domains => {domain:, public_suffix_domain:}
    {
      domain: brand&.domain || domain,
      public_suffix_domain: brand&.public_suffix_domain || public_suffix_domain,
      hibp_json: breached_hash,
      breached_at: breached_hash["BreachDate"],
      breach_published_at: breached_hash["AddedDate"]
    }
  end

  # We need the following to save the data as a brand
  # * A domain
  # * Breach needs to be verified
  # * Current brand breach_published_at should either
  #   * Empty
  #   * Before the latest breach added date
  def valid_breach?(breach, latest_breach)
    latest_published_at = Time.zone.parse(breach["AddedDate"])
    breach["Domain"].present? && breach["IsVerified"] &&
      (latest_breach.blank? || latest_published_at > latest_breach)
  end

  def upsert_all(brands)
    # The where is needed to set the timestamps on insert and cannot be nil.
    Brand
      .where(created_at: Time.current, updated_at: Time.current)
      .upsert_all(brands, unique_by: :domain, returning: :id)
  end

  delegate :connection, to: :class
end
