class Tasks::DisableLegacyEncryption
  def update_users
    users_to_set.find_each { |user| user.profile.disable_legacy_encryption! }
  end

  private

  def users_to_set
    User
      .includes(:profile)
      .references(:profile)
      .where(<<~SQL.squish)
        NOT EXISTS (
          SELECT 1 FROM items 
          WHERE items.user_id = users.id 
          AND items.encryption_version = 1 
          AND items.deleted_at IS NULL
        )
      SQL
      .where(<<~SQL.squish)
        profiles.application_settings->>'legacy_encryption_disabled' != 'true'
        OR profiles.application_settings->>'legacy_encryption_disabled' IS NULL
      SQL
  end
end
