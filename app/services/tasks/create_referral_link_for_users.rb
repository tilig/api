class Tasks::CreateReferralLinkForUsers
  def execute
    profiles = Profile.where("application_settings ->> 'referral_link' IS NULL").order(created_at: :desc)

    profiles.find_each do |profile|
      profile.application_settings["referral_link"] = "squirrel-#{SecureRandom.base58(10)}"
      profile.save
    end
  end
end
