class Tasks::MoveMetaClientsToApplicationSettings
  BATCH_SIZE = 1000

  def execute
    profiles = Profile.where.not(meta: {}).order(created_at: :desc)

    profiles.find_each(batch_size: BATCH_SIZE) do |profile|
      # Get the values
      source_value = profile.meta || {}
      target_value = profile.application_settings || {}
      # Merge the hashes
      merged_value = source_value.deep_merge(target_value)

      # Update the application_settings with the merged value
      profile.update_columns(application_settings: merged_value, meta: {})
    end
  end
end
