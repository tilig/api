class DomainNormalizer
  attr :url, :uri
  def initialize(url)
    raise URI::InvalidURIError, "Invalid URL" if url.blank?

    @url = url.strip
    set_scheme unless has_scheme?

    @uri = URI.parse(@url).normalize
  rescue URI::InvalidURIError
    @url = ""
    @uri = URI("")
  end

  def domain
    return if ip_address?
    hostname = uri.hostname
    hostname&.sub!("www.", "") if hostname&.starts_with?("www.")
    hostname
  end

  # `ignore_private: false` is only used for the secrets controller search (done
  # in `app/models/secret.rb`).
  def public_suffix_domain(ignore_private: true)
    return if ip_address?
    PublicSuffix.domain(uri.hostname, ignore_private: ignore_private)
  end

  # Returns both the domain and public_suffix_domain
  def domains(ignore_private: true)
    {domain: domain, public_suffix_domain: public_suffix_domain(ignore_private: ignore_private)}
  end

  private

  def set_scheme
    @url = "https://" + @url
  end

  def has_scheme?
    @url.include?("://")
  end

  def ip_address?
    IPAddr.new(uri.hostname).ipv4? || IPAddr.new(uri.hostname).ipv6?
  rescue
    false
  end
end
