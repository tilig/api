module Mixpanel
  class VoidConsumer < Mixpanel::Consumer
    def initialize(logger)
      super
      @logger = logger
    end

    def send!(type, _message)
      @logger.info("[Mixpanel] Voiding event #{type}")
    end
  end
end
