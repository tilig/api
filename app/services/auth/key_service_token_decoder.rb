module Auth
  class KeyServiceTokenDecoder
    class KeyServiceTokenDecodeError < StandardError
    end

    class FirebasePublicKeyMissingError < KeyServiceTokenDecodeError
    end

    class JwtDecodeError < KeyServiceTokenDecodeError
    end

    class KindMismatchError < JwtDecodeError
    end

    def decode(jwt, kind: nil)
      payload, _header =
        JWT.decode(
          jwt,
          # Load the ES256 Public Key
          firebase_public_key,
          # Set validation to true
          true,
          # Hardcode validation information
          {
            algorithm: "ES256",
            verify_expiration: true,
            iss: "tilig-ks",
            verify_iss: true,
            aud: "tilig-saas",
            verify_aud: true,
            required_claims: %w[kind]
          }
        )

      if kind && kind != payload["kind"]
        raise KindMismatchError,
          "Kind #{payload["kind"]} does not match required kind #{kind}"
      end

      payload
    rescue JWT::DecodeError => e
      raise JwtDecodeError, "Could not decode token: #{e.inspect}"
    end

    def firebase_public_key
      @firebase_public_key ||=
        OpenSSL::PKey::EC.new(
          Rails.application.config.firebase_public_key ||
            raise(
              FirebasePublicKeyMissingError,
              "No Firebase Public Key specified"
            )
        )
    end
  end
end
