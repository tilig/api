module Auth
  class TiligTokenDecoder
    class TiligTokenDecoderError < StandardError
    end

    class JwtDecodeError < TiligTokenDecoderError
    end

    class EmailMissingError < JwtDecodeError
    end

    class JtiExistsError < JwtDecodeError
    end

    def decode_email(jwt)
      payload = decode_jwt jwt

      validate_email_presence payload
      validate_jti payload

      payload["email"]
    end

    private

    def decode_jwt(jwt)
      payload, _header =
        JWT.decode(
          jwt,
          # Load the ES256 Public Key
          firebase_public_key,
          # Set validation to true
          true,
          # Hardcode the algorithm
          {algorithm: "ES256"}
        )

      payload
    rescue JWT::DecodeError => e
      raise JwtDecodeError, "Could not decode token: #{e.inspect}"
    end

    def validate_jti(payload)
      jti = payload["email"] + payload["iat"].to_s

      if JwtJti.exists?(jti: jti)
        raise JtiExistsError, "Jwt with jti:#{jti} already exists"
      end

      JwtJti.create(jti: jti)
    end

    def validate_email_presence(payload)
      if payload["email"].blank?
        raise EmailMissingError, "Email not present in payload"
      end
    end

    def firebase_public_key
      @firebase_public_key ||=
        OpenSSL::PKey::EC.new(
          Rails.application.config.firebase_public_key ||
            raise(
              FirebasePublicKeyMissingError,
              "No Firebase Public Key specified"
            )
        )
    end
  end
end
