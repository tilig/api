module Auth
  class AccessTokenDecoder
    class AccessTokenDecoderError < StandardError
    end

    class AccessTokenSecretMissing < AccessTokenDecoderError
    end

    class JwtDecodeError < AccessTokenDecoderError
    end

    def decode(token)
      payload, _header =
        JWT.decode(
          token,
          # The HS256 secret
          hmac_secret,
          # Set validation to true
          true,
          # Hardcode the algorithm
          {algorithm: "HS256", iss: issuer, verify_iss: true}
        )

      payload
    rescue JWT::DecodeError => e
      raise JwtDecodeError, "Could not decode token: #{e.inspect}"
    end

    private

    def hmac_secret
      Rails.application.config.access_token_secret ||
        raise(AccessTokenSecretMissing, "No access token secret specified")
    end

    def issuer
      "https://app.tilig.com"
    end
  end
end
