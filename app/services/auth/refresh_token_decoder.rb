module Auth
  class RefreshTokenDecoder
    class RefreshTokenDecoder < StandardError
    end

    class RefreshTokenSecretMissing < RefreshTokenDecoder
    end

    class JwtDecodeError < RefreshTokenDecoder
    end

    def decode(token)
      payload, _header =
        JWT.decode(
          token,
          # The HS256 secret
          hmac_secret,
          # Set validation to true
          true,
          # Hardcode the algorithm
          {algorithm: "HS256", iss: issuer, verify_iss: true}
        )

      payload
    rescue JWT::DecodeError => e
      raise JwtDecodeError, "Could not decode token: #{e.inspect}"
    end

    private

    def hmac_secret
      @hmac_secret ||=
        Rails.application.config.refresh_token_secret ||
        raise(RefreshTokenSecretMissing, "No refresh token secret specified")
    end

    def issuer
      "https://app.tilig.com"
    end
  end
end
