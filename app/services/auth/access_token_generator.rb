class Auth::AccessTokenGenerator
  class << self
    def generate(user)
      JWT.encode payload(user), hmac_secret, "HS256"
    end

    private

    def payload(user)
      {
        jti: generate_jti,
        uid: user.id,
        exp: expire_at,
        iss: issuer,
        iat: Time.now.to_i
      }
    end

    def expire_at
      access_token_expiration_minutes.minutes.from_now.to_i
    end

    def access_token_expiration_minutes
      Rails.application.config.access_token_expiration_minutes || 24 * 60
    end

    def hmac_secret
      Rails.application.config.access_token_secret
    end

    def issuer
      "https://app.tilig.com"
    end

    def generate_jti
      SecureRandom.uuid
    end
  end
end
