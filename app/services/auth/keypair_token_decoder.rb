module Auth
  class KeypairTokenDecoder
    def decode(jwt)
      KeyServiceTokenDecoder.new.decode(jwt, kind: "encrypted-keypair")
    end
  end
end
