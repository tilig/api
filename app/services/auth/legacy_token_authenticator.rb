module Auth
  class LegacyTokenAuthenticator
    def authenticate(token)
      payload = AccessTokenDecoder.new.decode(token)
      User.find payload["uid"]
    rescue AccessTokenDecoder::JwtDecodeError, ActiveRecord::RecordNotFound
      nil
    end
  end
end
