module Auth
  class SessionTokenAuthenticator
    def authenticate(token)
      payload = KeyServiceTokenDecoder.new.decode(token, kind: "session-token")
      find_or_create_by_email email: payload["email"], uid: payload["uid"]
    rescue KeyServiceTokenDecoder::JwtDecodeError
      nil
    end

    private

    def find_or_create_by_email(email:, uid:)
      user = User.find_or_create_by! email: email do |u|
        u.firebase_uid = uid
      end

      user.update_column(:firebase_uid, uid) unless user.firebase_uid?
      user
    end
  end
end
