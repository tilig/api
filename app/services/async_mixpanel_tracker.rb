class AsyncMixpanelTracker < Mixpanel::Tracker
  def initialize
    mixpanel_token = Rails.application.config.mixpanel_token

    super(mixpanel_token) do |*message|
      MixpanelJob.perform_later(*message)
    end
  end
end
