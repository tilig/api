class Mailchimp::SendReferralThankYou
  attr_reader :recipient, :referred_user

  def initialize(recipient:, referred_user:)
    @recipient = recipient
    @referred_user = referred_user
  end

  def call
    template = Mailchimp::ReferralThankYouTemplate.new(recipient: recipient, referred_user: referred_user).build_template
    Mailchimp::TransactionalEmailJob.perform_later(template)
  end
end
