class Mailchimp::OrganizationInviteAcceptedConfirmationService
  def initialize(invitation)
    @invitation = invitation
  end

  def call
    template = Mailchimp::OrganizationInviteAcceptedConfirmationTemplate.new(invitation: invitation).build_template
    Mailchimp::TransactionalEmailJob.perform_later(template)
  end
  attr_reader :invitation
end
