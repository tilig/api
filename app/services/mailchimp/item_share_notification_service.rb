class Mailchimp::ItemShareNotificationService
  def initialize(membership:, inviter:)
    @membership = membership
    @inviter = inviter
  end

  def call
    template = Mailchimp::ItemShareNotificationTemplate.new(membership: @membership, inviter: @inviter).build_template
    Mailchimp::TransactionalEmailJob.perform_now(template)
  end
end
