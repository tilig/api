class Mailchimp::OrganizationInviteService
  def initialize(invitations)
    @invitations = invitations
  end

  def call
    invitations.each do |invitation|
      template = Mailchimp::OrganizationInviteTemplate.new(invitation: invitation).build_template
      Mailchimp::TransactionalEmailJob.perform_later(template)
    end
  end
  attr_reader :invitations
end
