class Mailchimp::SendFolderInvite
  attr_reader :membership, :inviter

  def initialize(membership:, inviter:)
    @membership = membership
    @inviter = inviter
  end

  def call
    Mailchimp::TransactionalEmailJob.perform_later(template) if template
  end

  def template
    template_object&.build_template
  end

  def template_object
    return Mailchimp::ItemShareInviteTemplate.new(membership:, inviter:) if membership.user
    Mailchimp::ItemShareInviteExternalTemplate.new(membership:, inviter:) if membership.email
  end
end
