class Mailchimp::SendInvitedMemberSignedUpNotice
  def initialize(folder_memberships:)
    @folder_memberships = folder_memberships
  end

  def call
    grouped_memberships.each do |_creator_id, folder_memberships|
      template = Mailchimp::SignUpNoticeForFolderMembershipCreatorTemplate.new(folder_memberships:).build_template

      Mailchimp::TransactionalEmailJob.perform_later(template)
    end
  end

  attr_reader :folder_memberships

  def grouped_memberships
    @folder_memberships.select(&:creator_id).group_by(&:creator_id)
  end
end
