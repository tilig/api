module Mailchimp
  class SendCancellationFollowUp
    def initialize(recipient:)
      @recipient = recipient.attributes.symbolize_keys
    end

    def call
      template = Mailchimp::CancellationFollowUpTemplate.new(recipient: @recipient).build_template
      Mailchimp::TransactionalEmailJob.perform_later(template)
    end
  end
end
