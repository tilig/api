module Items
  class UpdateItem
    attr_reader :user, :item, :item_params

    def initialize(user:, item:, item_params:)
      @user = user
      @item = item
      @item_params = item_params
    end

    def call
      item.assign_attributes(item_params)

      if item.folder_id && !folder_belongs_to_user?
        item.folder_id = nil
        item.errors.add(:folder_id, :invalid)
        return false
      end

      item.user = if item.folder
        nil
      else
        user
      end

      item.save
    end

    private

    def folder_belongs_to_user?
      user.folders.exists?(item.folder_id)
    end
  end
end
