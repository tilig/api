class MixpanelErrorHandler
  def initialize(logger)
    @logger = logger
  end

  def handle(error)
    @logger.error(error)
  end
end
