module Folders
  class CreateFolder
    attr_reader :creator, :folder, :folder_membership_params, :item_params

    def initialize(creator:, folder_params:)
      @creator = creator
      @folder_membership_params = folder_params.fetch(:folder_memberships, [])
      @item_params = folder_params.fetch(:items, [])
      @folder = Folder.new folder_params.except(:folder_memberships, :items).merge(creator:)
    end

    def create
      Folder.transaction do
        create_folder
        add_memberships
        accept_memberships_for_contacts
        check_owner_is_admin
        move_items
        send_invites
        folder
      end
    rescue ActiveRecord::RecordInvalid
      folder
    end

    private

    def create_folder
      Rails.logger.debug { "Creating folder" }
      folder.save!
    end

    def add_memberships
      Rails.logger.debug { "Adding memberships" }
      folder_memberships = folder_membership_params.map { |fm| fm.merge(creator: creator) }

      if folder_memberships.blank?
        folder.errors.add(:base, "No members were added to this folder")
        raise ActiveRecord::RecordInvalid
      end

      begin
        folder.folder_memberships.create!(folder_memberships)
        folder.folder_memberships.where(user: creator).touch_all(:accepted_at)
      rescue ActiveRecord::RecordInvalid => e
        folder.errors.add(:base, "Error creating memberships: #{e.record.errors.full_messages.to_sentence}")
        raise ActiveRecord::RecordInvalid
      end
    end

    def accept_memberships_for_contacts
      Rails.logger.debug { "Checking if memberships are a contact" }
      folder.folder_memberships.where(user: creator.all_contact_users).touch_all(:accepted_at)
    end

    def check_owner_is_admin
      Rails.logger.debug { "Checking if creator is admin" }

      unless folder.admin?(creator)
        folder.errors.add(:base, "The creator of the folder should be a member")
        raise ActiveRecord::RecordInvalid
      end
    end

    def move_items
      return if items.blank?
      Rails.logger.debug { "Moving items to folder" }

      begin
        items.zip(item_params).each do |item, params|
          item.update! user: nil, folder: folder, encrypted_folder_dek: params[:encrypted_folder_dek]
          if item.share_link.present? && params.dig(:share_link, :encrypted_master_key).present?
            item.share_link.update!(params[:share_link])
          else
            item.share_link&.destroy!
          end
        end
      rescue ActiveRecord::RecordInvalid => e
        folder.errors.add(:base, "Could not move items to new folder: #{e.record.errors.full_messages.to_sentence}")
        raise ActiveRecord::RecordInvalid
      end
    end

    def items
      @items = creator.items.active.find(item_params.pluck(:id))
    rescue ActiveRecord::RecordNotFound
      folder.errors.add(:base, "Could not find item(s)")
      raise ActiveRecord::RecordInvalid
    end

    def send_invites
      pending_invitations.each do |membership|
        Mailchimp::SendFolderInvite.new(membership:, inviter: creator).call
      end
      accepted_memberships.each do |membership|
        Mailchimp::ItemShareNotificationService.new(membership:, inviter: creator).call
      end
    end

    def pending_invitations
      folder
        .folder_memberships
        .pending
    end

    def accepted_memberships
      folder
        .folder_memberships
        .accepted
        .where.not(user: creator)
    end
  end
end
