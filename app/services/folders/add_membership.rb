module Folders
  class AddMembership
    def initialize(creator:, folder:, folder_membership_params:)
      @creator = creator
      @folder_membership_params = folder_membership_params
      @folder = folder
      @folder_membership = folder.folder_memberships.build folder_membership_params.merge(creator:)
    end

    def create
      FolderMembership.transaction do
        create_folder_membership
        check_membership_is_contact
        check_creator_is_admin
        send_invite
        folder_membership
      rescue ActiveRecord::RecordInvalid
        folder_membership
      end
    end

    private

    def create_folder_membership
      Rails.logger.debug { "Adding folder_membership" }
      folder_membership.save!
    end

    def check_creator_is_admin
      Rails.logger.debug { "Checking if creator is admin" }

      unless folder.admin?(creator)
        folder.errors.add(:base, "The creator of the folder should be a member")
        raise ActiveRecord::RecordInvalid
      end
    end

    def check_membership_is_contact
      Rails.logger.debug { "Checking if membership is a contact" }
      folder_membership.touch(:accepted_at) if creator.all_contact_users.pluck(:id).include? folder_membership.user_id
    end

    def send_invite
      if folder_membership.accepted?
        Mailchimp::ItemShareNotificationService.new(membership: folder_membership, inviter: creator).call
      else
        Mailchimp::SendFolderInvite.new(membership: folder_membership, inviter: creator).call
      end
    end

    attr_reader :creator, :folder, :folder_membership, :folder_membership_params
  end
end
