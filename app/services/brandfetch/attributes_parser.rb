module Brandfetch
  class AttributesParser
    def initialize(brandfetch_json, url, brand = nil)
      @brandfetch_json = brandfetch_json
      @url = url
      @brand = brand
    end

    attr_reader :brand, :brandfetch_json, :url

    def parse
      return {} if brandfetch_json.nil?

      {
        name: name,
        domain: domain,
        public_suffix_domain: public_suffix_domain,
        totp: brand&.totp || false,
        rank: brand&.rank,
        main_color_hex: main_color_hex,
        main_color_brightness: main_color_brightness,
        brandfetch_json: brandfetch_json.as_json,
        fetch_date: Time.current,
        images: images
      }
    end

    private

    def name
      brand&.name.presence || brandfetch_json["name"] || name_from_description || domain
    end

    def normalized_domain
      @normalized_domain ||= DomainNormalizer.new(url)
    end

    def domain
      normalized_domain.domain
    end

    def public_suffix_domain
      normalized_domain.public_suffix_domain
    end

    def images
      {
        icon: {
          svg: {
            original: imgproxy_url(svg_source, "svg")
          },
          png: {
            original: imgproxy_url(png_source, "png")
          },
          theme: theme
        }
      }
    end

    def icon
      return @icon if defined?(@icon)

      @icon = (
        brandfetch_json["logos"]&.find { |l| l["type"] == "symbol" } ||
        brandfetch_json["logos"]&.find { |l| l["type"] == "icon" }
      )
    end

    def icon_formats
      return @icon_formats if defined?(@icon_formats)

      icon&.dig("formats")
    end

    def format_src(format)
      icon_formats&.detect { |l| l["format"] == format }&.dig("src")
    end

    def theme
      icon&.dig("theme")
    end

    def svg_source
      format_src("svg")
    end

    def png_source
      format_src("png") ||
        format_src("svg") ||
        format_src("jpeg")
    end

    def imgproxy_url(src, extention)
      src.present? ? Imgproxy.url_for(src, format: extention) : nil
    end

    def main_color
      return @main_color if defined?(@main_color)

      @main_color = brandfetch_json["colors"]&.detect { |l| l["type"] == "accent" } ||
        brandfetch_json["colors"]&.detect { |l| l["type"] == "brand" }
    end

    def main_color_hex
      main_color["hex"] if main_color
    end

    def main_color_brightness
      main_color["brightness"] if main_color
    end

    ##
    # This tries to match the domain name in a description and return the name in
    # the description.
    # Both the sld + tld combination of the domain and the plain sld are tried in
    # the description, in that order.
    #
    # Examples
    #
    # ```rb
    # domain = "youtu.be"
    # description = "the world on YouTube."
    #
    # name_from_description
    # #=> YouTube
    # ```
    #
    # ```rb
    # domain = "youtube.com"
    # description = "the world on YouTube."
    #
    # name_from_description
    # #=> YouTube
    # ```
    #
    # ```rb
    # domain = "youtube.com"
    # description = "something entirely else."
    #
    # name_from_description
    # #=> nil
    # ```
    def name_from_description
      description = brandfetch_json["description"]
      return if domain.blank? || description.blank?

      sld = domain.split(".")[0]
      domain_match = description.match(%r{#{domain.sub('.', '')}}i) { |i| i[0] }
      sld_match = description.match(%r{#{sld}}i) { |i| i[0] }

      domain_match.presence || sld_match.presence
    end
  end
end

# Example of a Brandfetch json response
# https://api.brandfetch.io/v2/brands/google.com
#   {
# 	"name": "Google",
# 	"domain": "google.com",
# 	"claimed": false,
# 	"description": "Our mission is to organize the world’s information and make it universally accessible and useful.",
# 	"links": [
# 		{
# 			"name": "crunchbase",
# 			"url": "https://crunchbase.com/organization/google"
# 		},
# 		{
# 			"name": "twitter",
# 			"url": "https://twitter.com/google"
# 		},
# 		{
# 			"name": "instagram",
# 			"url": "https://instagram.com/google"
# 		},
# 		{
# 			"name": "linkedin",
# 			"url": "https://linkedin.com/company/google"
# 		},
# 		{
# 			"name": "facebook",
# 			"url": "https://facebook.com/Google"
# 		}
# 	],
# 	"logos": [
# 		{
# 			"type": "logo",
# 			"theme": "dark",
# 			"formats": [
# 				{
# 					"src": "https://asset.brandfetch.io/id6O2oGzv-/idP6g0Ep4y.svg",
# 					"background": "transparent",
# 					"format": "svg",
# 					"size": 1933
# 				},
# 				{
# 					"src": "https://asset.brandfetch.io/id6O2oGzv-/idSuJ5ik7i.png",
# 					"background": "transparent",
# 					"format": "png",
# 					"height": 251,
# 					"width": 800,
# 					"size": 21577
# 				}
# 			]
# 		},
# 		{
# 			"type": "symbol",
# 			"theme": "dark",
# 			"formats": [
# 				{
# 					"src": "https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg",
# 					"background": "transparent",
# 					"format": "svg",
# 					"size": 851
# 				},
# 				{
# 					"src": "https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png",
# 					"background": "transparent",
# 					"format": "png",
# 					"height": 800,
# 					"width": 800,
# 					"size": 29934
# 				}
# 			]
# 		},
# 		{
# 			"type": "icon",
# 			"theme": "dark",
# 			"formats": [
# 				{
# 					"src": "https://asset.brandfetch.io/id6O2oGzv-/idNEgS9h8q.jpeg",
# 					"background": null,
# 					"format": "jpeg",
# 					"height": 400,
# 					"width": 400,
# 					"size": 11895
# 				}
# 			]
# 		}
# 	],
# 	"colors": [
# 		{
# 			"hex": "#4285F4",
# 			"type": "accent",
# 			"brightness": 127
# 		},
# 		{
# 			"hex": "#FFFFFF",
# 			"type": "light",
# 			"brightness": 255
# 		},
# 		{
# 			"hex": "#424242",
# 			"type": "dark",
# 			"brightness": 66
# 		},
# 		{
# 			"hex": "#34A853",
# 			"type": "brand",
# 			"brightness": 137
# 		}
# 	],
# 	"fonts": [
# 		{
# 			"name": "Google Sans",
# 			"type": "title",
# 			"origin": "custom",
# 			"originId": null,
# 			"weights": []
# 		},
# 		{
# 			"name": "Google Sans Text",
# 			"type": "body",
# 			"origin": "custom",
# 			"originId": null,
# 			"weights": []
# 		}
# 	],
# 	"images": [
# 		{
# 			"type": "banner",
# 			"formats": [
# 				{
# 					"src": "https://asset.brandfetch.io/id6O2oGzv-/ide1CTsR8v.jpeg",
# 					"background": null,
# 					"format": "jpeg",
# 					"height": 500,
# 					"width": 1500,
# 					"size": 46016
# 				}
# 			]
# 		}
# 	]
# }
