module Brandfetch
  class AttributesFetcher
    BRANDFETCH_API_ROOT = "https://api.brandfetch.io/v2/brands/"
    DISCARDING_ERROR_CODES = [403, *(500..599)]
    TIMEOUT_SECONDS = 10

    delegate :connection, to: :class
    attr_reader :domain, :public_suffix_domain, :brand

    def initialize(domain, brand = nil)
      @domain = DomainNormalizer.new(domain).domain
      @public_suffix_domain = DomainNormalizer.new(domain).public_suffix_domain
      @brand = brand || Brand.new(domain: domain)
    end

    def fetch
      Rails.logger.debug { "Fetching brand details for #{domain}" }
      return {} unless fetchable?
      fetch_attributes
    rescue URI::InvalidURIError
      {}
    end

    def self.connection
      @connection ||= Faraday.new(url: BRANDFETCH_API_ROOT).tap do |conn|
        conn.request :authorization, "Bearer", Rails.application.config.brandfetch_api_token
        conn.request :json
        conn.response :json
        conn.options.timeout = TIMEOUT_SECONDS
        conn.options[:open_timeout] = TIMEOUT_SECONDS
      end
    end

    private

    def fetchable?
      return false unless domain.present? && public_suffix_domain.present?
      return false unless domain == brand.domain || brand.domain.nil?
      return false if brand.fetch_date?
      true
    end

    def fetch_attributes
      Brandfetch::AttributesParser.new(fetch_json, domain, brand).parse
    end

    def fetch_json
      response = connection.get(domain)
      response.body unless DISCARDING_ERROR_CODES.include?(response.status)
    rescue Faraday::Error => e
      Sentry.capture_exception(e)
      {}
    end
  end
end
