require "active_support/concern"

module SoftDestroyable
  extend ActiveSupport::Concern
  extend ActiveModel::Callbacks

  included do
    define_model_callbacks :soft_destroy
    scope :active, -> { where(deleted_at: nil) }
    scope :inactive, -> { where.not(deleted_at: nil) }

    def soft_destroy
      run_callbacks :soft_destroy do
        touch :deleted_at
      end
    end
  end
end
