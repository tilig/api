class Contact < ApplicationRecord
  belongs_to :user
  belongs_to :contact, class_name: "User"

  validates :contact, uniqueness: {scope: :user}

  def self.make_contacts(user, other_user)
    user.contacts.create_or_find_by(contact: other_user)
    other_user.contacts.create_or_find_by(contact: user)
  end

  after_destroy do
    Contact.where(user: contact, contact: user).destroy_all
  end
end

# == Schema Information
#
# Table name: contacts
#
#  id         :uuid             not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  contact_id :uuid             not null
#  user_id    :uuid             not null
#
# Indexes
#
#  index_contacts_on_contact_id              (contact_id)
#  index_contacts_on_user_id                 (user_id)
#  index_contacts_on_user_id_and_contact_id  (user_id,contact_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (contact_id => users.id)
#  fk_rails_...  (user_id => users.id)
#
