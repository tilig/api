class Answer < ApplicationRecord
  belongs_to :question
  belongs_to :user

  accepts_nested_attributes_for :question
  before_validation :find_or_create_question

  # The find_or_create_question is used in the before_validation callback to
  # support creating questions through answers.
  #
  # ## Example
  #
  # ```rb
  # params =
  #   answer: {
  #    question_attributes: {
  #      token: "b291972e-caa6-4f8c-ad80-5ef91b5775b2",
  #      ...
  #    },
  #    ...
  #   }
  # ```
  # Answer.create(params)
  #
  # Normally this example would fail on the second attempt since there already
  # is a question by this token and the token is unique. Therfore we have to
  # find question and assign the answer to __that__ question.
  #
  # This function does the following:
  #
  # 1. Checks if the question (which is set with the question_attributes) is actually set on the answer
  # 2. Tries to find an existing question by the token
  # 3. Updates the question with the existing question, only if an existing question was found.
  #
  # There is not a real difference between self.question and question, only semantically the
  # self notifies that the question of the answer is updated outside the scope.
  #
  # See the original comment and thread here: https://gitlab.com/subshq/server/api/-/merge_requests/191#note_911244929

  def find_or_create_question
    return unless question
    existing_question = Question.find_by(token: question.token)

    self.question = existing_question if existing_question
  end
end

# == Schema Information
#
# Table name: answers
#
#  id                :uuid             not null, primary key
#  chosen_options    :string           default([]), not null, is an Array
#  skipped           :boolean          default(FALSE), not null
#  x_browser_name    :string
#  x_browser_version :string
#  x_os_name         :string
#  x_tilig_platform  :string
#  x_tilig_version   :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  question_id       :uuid             not null
#  user_id           :uuid             not null
#
# Indexes
#
#  index_answers_on_question_id  (question_id)
#  index_answers_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (question_id => questions.id)
#
