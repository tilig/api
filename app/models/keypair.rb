class Keypair < ApplicationRecord
  belongs_to :user

  validates :encrypted_private_key, :public_key, :key_type, :kid, :meta_data, presence: true

  def payload=(jwt_params)
    self.encrypted_private_key = jwt_params["key"]["encrypted_private_key"]
    self.public_key = jwt_params["key"]["public_key"]
    self.key_type = jwt_params["key"]["key_type"]
    self.meta_data = jwt_params["meta"]
    self.kid = jwt_params["kid"]
  end
end

# == Schema Information
#
# Table name: keypairs
#
#  id                    :uuid             not null, primary key
#  encrypted_private_key :text
#  key_type              :string
#  kid                   :string
#  meta_data             :jsonb            not null
#  public_key            :text
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  user_id               :uuid
#
# Indexes
#
#  index_keypairs_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
