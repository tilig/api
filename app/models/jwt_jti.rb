class JwtJti < ApplicationRecord
end

# == Schema Information
#
# Table name: jwt_jtis
#
#  id         :uuid             not null, primary key
#  jti        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_jwt_jtis_on_jti  (jti)
#
