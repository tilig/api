class Invitation < ApplicationRecord
  belongs_to :creator, class_name: "User"
  belongs_to :organization
  belongs_to :invitee, class_name: "User", optional: true

  has_secure_token :view_token

  before_create :set_expire_at

  validates :email,
    presence: true,
    uniqueness: {scope: :organization_id},
    format: {with: /\A[a-z0-9+\-_.]+@[a-z\d\-.]+\.[a-z]+\z/i},
    length: {minimum: 4, maximum: 254}

  validates :invitee, presence: true, uniqueness: {scope: :organization_id}, if: :accepted_at?

  validate :invitee_has_no_organization, on: :create

  scope :accepted, -> { where.not(accepted_at: nil) }
  scope :pending, -> { where(accepted_at: nil) }
  scope :not_expired, -> { where("expire_at > ?", Time.zone.now) }

  scope :ordered, -> {
                    joins("LEFT JOIN users as invitees ON invitees.id = invitee_id ")
                      .order(["invitees.display_name ASC", "email ASC"])
                  }

  def email=(value)
    super(value&.downcase&.strip)
  end

  def accepted?
    accepted_at?
  end

  def pending?
    !accepted_at?
  end

  def expired?
    expire_at < Time.current
  end

  def authorize_view_token(token)
    return false unless token
    ActiveSupport::SecurityUtils.secure_compare(view_token, token.to_s)
  end

  def set_expire_at
    self.expire_at = 30.days.from_now
  end

  private

  def invitee_has_no_organization
    errors.add(:email, "user already belongs to an organization") if User.where(email: email).where.not(organization: nil).exists?
  end
end

# == Schema Information
#
# Table name: invitations
#
#  id              :uuid             not null, primary key
#  accepted_at     :datetime
#  email           :string           not null
#  expire_at       :datetime
#  view_token      :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  creator_id      :uuid
#  invitee_id      :uuid
#  organization_id :uuid             not null
#
# Indexes
#
#  index_invitations_on_creator_id                      (creator_id)
#  index_invitations_on_email_and_organization_id       (email,organization_id) UNIQUE
#  index_invitations_on_invitee_id                      (invitee_id)
#  index_invitations_on_invitee_id_and_organization_id  (invitee_id,organization_id) UNIQUE
#  index_invitations_on_organization_id                 (organization_id)
#
# Foreign Keys
#
#  fk_rails_...  (creator_id => users.id)
#  fk_rails_...  (invitee_id => users.id)
#  fk_rails_...  (organization_id => organizations.id)
#
