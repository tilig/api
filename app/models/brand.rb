class Brand < ApplicationRecord
  DEFAULT_PAGE_SIZE = 15

  belongs_to :parent, optional: true, class_name: "Brand"
  has_many :items, dependent: :nullify

  validates_presence_of :domain, :public_suffix_domain
  validates_uniqueness_of :domain

  before_validation :set_public_suffix_domain_from_domain, if: proc { public_suffix_domain.blank? && domain.present? }

  # We don't do an after_destroy, because we don't have all the secrets anoymore
  # since they are nullified on destroy. But doesn't change the updated_at.
  after_update :touch_items, if: :saved_changes?

  scope :with_prefix, ->(prefix, page_size) do
    ## '.where.not(rank: nil)' is important because it will ensure that
    ## only brands that are ranked (for now only the alexa top 1m list)
    ## are shown in the search result response. This will increase privacy.
    ## We should consider to perform some update on new brands that do not
    ## have a rank yet but are used a lot.

    ## The where to select parents or children is explained at:
    ## https://gitlab.com/subshq/server/api/-/merge_requests/352#note_1181289765
    page_size = DEFAULT_PAGE_SIZE unless (1..DEFAULT_PAGE_SIZE).cover?(page_size)
    joins("LEFT JOIN brands parent_brands ON parent_brands.id = brands.parent_id")
      .where("brands.domain = brands.public_suffix_domain")
      .where.not(rank: nil)
      .where(
        "(brands.parent_id IS NULL AND (brands.name ILIKE :prefix OR brands.domain ILIKE :prefix))
         OR (parent_brands.name NOT ILIKE :prefix AND parent_brands.domain NOT ILIKE :prefix)
         AND (brands.name ILIKE :prefix OR brands.domain ILIKE :prefix)",
        prefix: "#{sanitize_sql_like(prefix)}%"
      )
      .order(rank: :asc)
      .order(name: :asc)
      .limit(page_size)
  end

  def logo_icon_source
    parent&.logo_icon_source || super || images.dig("icon", "png", "original") || images.dig("icon", "svg", "original")
  end

  def self.find_or_create_by_url(url)
    DomainNormalizer.new(url).domains => {domain:, public_suffix_domain:}
    return unless domain.present? && public_suffix_domain.present?

    brand = create_or_find_by!(domain: domain)

    brand.tap(&:schedule_brandfetch)
  rescue ActiveRecord::RecordInvalid
    find_by(domain: domain).tap(&:schedule_brandfetch)
  end

  def self.find_normalized_domain(domain)
    normalized_domain = DomainNormalizer.new(domain)
    public_suffix_domain = normalized_domain.public_suffix_domain

    find_by(domain: normalized_domain.domain) || find_by(public_suffix_domain: public_suffix_domain)
  end

  def fetch_brand_attributes(domain)
    self.attributes = Brandfetch::AttributesFetcher.new(domain, self).fetch if fetch_date.nil?
    save
  end

  def schedule_brandfetch
    FetchBrandJob.perform_later(self) unless fetch_date?
  end

  private

  def touch_items
    items.touch_all
  end

  def set_public_suffix_domain_from_domain
    self.public_suffix_domain = DomainNormalizer.new(domain).public_suffix_domain
  end
end

# == Schema Information
#
# Table name: brands
#
#  id                    :uuid             not null, primary key
#  brandfetch_json       :json
#  breach_published_at   :datetime
#  breached_at           :datetime
#  domain                :string           not null
#  fetch_date            :datetime
#  hibp_json             :jsonb
#  images                :jsonb            not null
#  json                  :json
#  logo_icon_source      :string
#  logo_source           :string
#  main_color_brightness :string
#  main_color_hex        :string
#  name                  :string
#  parent_source         :string
#  public_suffix_domain  :string           not null
#  rank                  :integer
#  totp                  :boolean          default(FALSE), not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  parent_id             :uuid
#
# Indexes
#
#  index_brands_on_domain      (domain) UNIQUE
#  index_brands_on_domain_gin  (domain) USING gin
#  index_brands_on_name        (name) USING gin
#  index_brands_on_parent_id   (parent_id)
#  index_brands_on_rank        (rank)
#
