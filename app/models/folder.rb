class Folder < ApplicationRecord
  include SoftDestroyable

  belongs_to :creator, class_name: "User", optional: true

  has_many :items, dependent: :destroy
  has_many :share_links, through: :items

  has_many :folder_memberships, dependent: :destroy
  has_many :accepted_folder_memberships, -> { accepted }, class_name: "FolderMembership", dependent: nil
  has_many :pending_folder_invitations, -> { pending }, class_name: "FolderMembership", dependent: nil
  has_many :admin_folder_memberships, -> { accepted.admin }, class_name: "FolderMembership", dependent: nil
  has_many :users, through: :folder_memberships

  validates :public_key, presence: true
  validates :name, presence: true, unless: :single_item

  scope :not_single_item, -> { where(single_item: false) }

  before_validation do
    self.single_item = false if items.size > 1
  end

  after_soft_destroy do
    items.each(&:soft_destroy)
  end

  def admin?(user)
    member?(user, role: "admin")
  end

  def member?(user, **attributes)
    folder_memberships.exists?(user:, **attributes)
  end

  def user_encrypted_private_key(user)
    return unless user
    folder_memberships.find_by(user:)&.encrypted_private_key
  end
end

# == Schema Information
#
# Table name: folders
#
#  id          :uuid             not null, primary key
#  deleted_at  :datetime
#  description :text
#  name        :string
#  public_key  :text             not null
#  single_item :boolean          default(TRUE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  creator_id  :uuid
#
# Indexes
#
#  index_folders_on_creator_id  (creator_id)
#
# Foreign Keys
#
#  fk_rails_...  (creator_id => users.id)
#
