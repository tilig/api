class Profile < ApplicationRecord
  belongs_to :user
  validates :user, uniqueness: true

  before_create do
    application_settings["referral_link"] = "squirrel-#{SecureRandom.base58(10)}"
  end

  def self.find_user_by_referral_link(referral_link)
    Profile.find_by("application_settings ->> 'referral_link' = ?", referral_link)&.user
  end

  def referral_link
    application_settings["referral_link"]
  end

  def save_sign_up_client(os_name)
    return if application_settings["client_signed_up"]
    application_settings["client_signed_up"] = os_name.to_s[0, 200]
    save
  end

  def save_signed_in_client(client)
    application_settings["clients_signed_in"] = application_settings.fetch("clients_signed_in", []) | [client.to_s[0, 200]]
    save
  end

  # The provider_id in User model can eventually phased out.
  def save_sign_up_provider_id(provider_id)
    return if application_settings["provider_id_signed_up"]
    application_settings["provider_id_signed_up"] = provider_id.to_s[0, 200]
    save
  end

  def save_signed_in_provider_id(provider_id)
    application_settings["provider_ids_signed_in"] = application_settings.fetch("provider_ids_signed_in", []) | [provider_id.to_s[0, 200]]
    save
  end

  def save_referral(referral_link)
    return if application_settings["used_referral_link"]
    Profile.where("application_settings ->> 'referral_link' = ?", referral_link).update_counters(referral_count: 1)
    application_settings["used_referral_link"] = referral_link
    save
  end

  def legacy_encryption_disabled?
    !!application_settings["legacy_encryption_disabled"]
  end

  def disable_legacy_encryption!
    application_settings["legacy_encryption_disabled"] = true
    save
  end
end

# == Schema Information
#
# Table name: profiles
#
#  id                   :uuid             not null, primary key
#  application_settings :jsonb            not null
#  meta                 :jsonb            not null
#  referral_count       :integer          default(0), not null
#  user_settings        :jsonb            not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  user_id              :uuid             not null
#
# Indexes
#
#  index_profiles_on_user_id  (user_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
