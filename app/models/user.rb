class User < ApplicationRecord
  has_one :profile, dependent: :destroy
  has_one :keypair, dependent: :destroy

  belongs_to :organization, optional: true
  has_one :created_organization, class_name: "Organization",
    foreign_key: :creator_id, inverse_of: :creator, dependent: :nullify
  # TODO: Add function to revoke a Invitation
  has_many :invitations, class_name: "Invitation",
    foreign_key: :invitee_id, dependent: :destroy
  # TODO: Add function to revoke a Invitation
  has_many :created_invitations, class_name: "Invitation",
    foreign_key: :creator_id, dependent: nil

  has_many :created_folder_memberships, class_name: "FolderMembership",
    foreign_key: :creator_id, dependent: :nullify

  # Pending invitation created by the user automatically get removed
  before_destroy do
    created_invitations.pending.delete_all
    created_invitations.accepted.update_all creator_id: nil, updated_at: Time.zone.now
  end

  has_many :identities, dependent: :destroy

  has_many :items, dependent: :destroy
  has_many :share_links, through: :items

  # From a user perspective, only accepted folder memberships are relevant
  has_many :folder_memberships, -> { accepted }, dependent: nil
  has_many :folders, through: :folder_memberships

  has_many :all_folder_memberships, class_name: "FolderMembership", dependent: :destroy
  has_many :admin_folder_memberships, -> { accepted.admin }, class_name: "FolderMembership", dependent: nil
  has_many :managing_folders, through: :admin_folder_memberships, source: :folder

  has_many :created_folders, class_name: "Folder", foreign_key: :creator_id, dependent: :nullify

  has_many :contacts, dependent: :destroy
  has_many :contact_of_users, class_name: "Contact", foreign_key: :contact_id, dependent: :destroy
  has_many :contact_users, through: :contacts, source: :contact

  before_create :build_profile, unless: :profile
  after_destroy :cleanup_organization

  validates :locale, presence: true

  scope :ordered, -> { order(display_name: :asc, email: :asc) }

  delegate :disable_legacy_encryption!, :legacy_encryption_disabled?, :referral_link, to: :profile

  # Both Contacts as Organization/Team members
  def all_contact_users
    # Relations do not work, therfor must be like this
    contacts = User.joins("LEFT JOIN contacts ON users.id = contacts.contact_id")
      .includes(:keypair, :organization)
      .where(contacts: {user_id: id})
    if organization.present?
      contacts = contacts.or(
        User.includes(:keypair, :organization)
          .where.not(organization_id: nil)
          .where(organization_id: organization_id)
      )
        .where.not(id: id)
    end
    contacts
  end

  # Quickfix for now. We want to save all names we get back from Firebase: given_name, family_name and display_name
  # However, for OUR display_name we want to use the given_name with fallback to display_name
  def display_name
    given_name || super || ""
  end

  private

  def cleanup_organization
    organization&.destroy if organization&.users&.empty?
  end
end

# == Schema Information
#
# Table name: users
#
#  id              :uuid             not null, primary key
#  display_name    :string
#  email           :string
#  family_name     :string
#  firebase_uid    :string
#  given_name      :string
#  locale          :string           default("en")
#  picture         :string
#  public_key      :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  organization_id :uuid
#  provider_id     :string
#
# Indexes
#
#  index_users_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_...  (organization_id => organizations.id)
#
