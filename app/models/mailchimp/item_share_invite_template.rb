class Mailchimp::ItemShareInviteTemplate < Mailchimp::Template
  include Rails.application.routes.url_helpers
  include ERB::Util
  attr_reader :recipient, :membership, :inviter, :folder

  def initialize(membership:, inviter:)
    @folder = membership.folder
    @recipient = membership.user
    @membership = membership
    @inviter = inviter
  end

  private

  def message
    {
      subject: "#{html_escape inviter.display_name} has shared #{item_or_folder} with you on Tilig",
      from_email: "no-reply@tilig.com",
      from_name: "Tilig",
      to: [{
        email: recipient.email,
        name: recipient.display_name
      }]
    }
  end

  def template_slug
    Rails.application.config.accept_share_item_template_slug
  end

  def content
    [{
      name: "body",
      content: body_content
    }, {
      name: "button",
      content: button_content
    }]
  end

  def body_content
    "<b>#{html_escape inviter.display_name}</b> (#{inviter.email}) has shared #{item_or_folder} #{folder_name} with you on Tilig. Click this link to get access to it:" \
    "<br><br>" \
  end

  def button_content
    "<a class=\"mcnButton\" title=\"Get access\" href=\"#{acceptance_link}\" target=\"_blank\" style=\"font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;\">Get access</a>"
  end

  def acceptance_link
    api_v3_folder_folder_membership_get_acceptance_url(folder.id, membership.id, acceptance_token: membership.acceptance_token)
  end

  def folder_name
    "<b>'#{html_escape folder.name}'</b>" if folder.name.present?
  end

  def item_or_folder
    folder.single_item? ? "an item" : "a folder"
  end

  def accept_invitation_path
    Rails.application.config.accept_share_item_inviation_path
  end
end
