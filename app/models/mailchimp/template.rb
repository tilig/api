class Mailchimp::Template
  include ERB::Util

  def build_template
    {
      "template_name" => template_slug,
      "template_content" => content,
      "message" => message,
      "sent_at" => sent_at
    }.compact_blank
  end

  def webapp_domain
    Rails.application.config.webapp_domain
  end

  # Override this method in subclasses
  def sent_at
    nil
  end
end
