class Mailchimp::OrganizationInviteTemplate < Mailchimp::Template
  attr_reader :invitation, :organization, :inviter

  def initialize(invitation:)
    @invitation = invitation
    @organization = invitation.organization
    @inviter = invitation.creator
  end

  private

  def message
    {
      subject: "#{html_escape inviter.display_name} invited you to an organization in Tilig",
      from_email: "no-reply@tilig.com",
      from_name: "Tilig",
      to: [{
        email: invitation.email
      }]
    }
  end

  def template_slug
    Rails.application.config.accept_organization_invitation_template_slug
  end

  def content
    [{
      name: "body",
      content: body_content
    }, {
      name: "button",
      content: button_content
    }]
  end

  def body_content
    "<b>#{html_escape inviter.display_name}</b> (#{html_escape inviter.email}) has invited you to the organization #{organization_name} with you on Tilig. Click this link to get accept it:" \
    "<br><br>" \
  end

  def button_content
    "<a class=\"mcnButton\" title=\"Join #{raw_organization_name}\" href=\"#{acceptance_link}\" target=\"_blank\" style=\"font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;\">Join #{raw_organization_name}</a>"
  end

  def acceptance_link
    acceptance_link = URI.join("https://" + webapp_domain, accept_invitation_path, invitation.id)
    acceptance_link.to_s + "?view_token=#{invitation.view_token}"
  end

  def organization_name
    "<b>'#{raw_organization_name}'</b>" if organization.name.present?
  end

  def raw_organization_name
    html_escape organization.name if organization.name.present?
  end

  def accept_invitation_path
    Rails.application.config.accept_organization_invitation_path&.strip
  end
end
