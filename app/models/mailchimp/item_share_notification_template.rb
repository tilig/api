class Mailchimp::ItemShareNotificationTemplate < Mailchimp::Template
  include Rails.application.routes.url_helpers
  include ERB::Util
  attr_reader :recipient, :membership, :inviter

  def initialize(membership:, inviter:)
    @recipient = membership.user
    @membership = membership
    @inviter = inviter
  end

  private

  def message
    {
      subject: "#{html_escape iniviter_name} shared '#{folder_name}' with you on Tilig",
      from_email: "no-reply@tilig.com",
      from_name: "Tilig",
      to: [{
        email: recipient.email,
        name: recipient.display_name
      }]
    }
  end

  def template_slug
    Rails.application.config.notification_share_item_template_slug
  end

  def content
    [{
      name: "body",
      content: body_content
    }, {
      name: "button",
      content: button_content
    }]
  end

  def body_content
    "#{html_inviter_name} has shared an item #{folder_name} with you on Tilig. To access the item, click this button:" \
    "<br><br>" \
  end

  def button_content
    "<a class=\"mcnButton\" title=\"View item\" href=\"https://#{Rails.application.config.webapp_domain}/#{item_id}\" target=\"_blank\" style=\"font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;\">Get access</a>"
  end

  def folder_name
    "'#{html_escape membership.folder.name}'" if membership.folder.name.present?
  end

  def html_inviter_name
    if inviter.display_name.present?
      "<b>#{html_escape inviter.display_name}</b> (#{inviter.email})"
    else
      inviter.email
    end
  end

  def iniviter_name
    inviter.display_name.presence || inviter.email
  end

  def item_id
    membership.folder.items.first.id if membership.folder.items.present?
  end
end
