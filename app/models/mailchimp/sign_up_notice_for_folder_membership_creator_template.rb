class Mailchimp::SignUpNoticeForFolderMembershipCreatorTemplate < Mailchimp::Template
  def initialize(folder_memberships:)
    @memberships = folder_memberships
    @recipient = folder_memberships.first.creator
    @member = folder_memberships.first.user
  end

  private

  def message
    {
      subject: "You can now share your items with #{member_name}",
      from_email: "no-reply@tilig.com",
      from_name: "Tilig",
      to: [{
        email: recipient.email,
        name: recipient.display_name
      }]
    }
  end

  def template_slug
    Rails.application.config.sign_up_notice_for_folder_membership_creator_slug
  end

  def content
    [{
      name: "body",
      content: body_content
    }, {
      name: "items",
      content: items_content
    }, {
      name: "button",
      content: button_content
    }]
  end

  def body_content
    "<b>#{html_escape member.display_name}</b> (#{member.email}) has created an account on Tilig and you can now share your items with them." \
    "<br><br>" \
  end

  def items_content
    memberships.flat_map do |folder_membership|
      folder_membership.folder.items.map do |item|
        if item.folder.name.present?
          "<li><a href=\"#{item_url(item)}\">" \
            "#{html_escape item.folder.name}" \
            "</a></li>"
        end
      end
    end.join("\n")
  end

  def button_content
    "<a class=\"mcnButton\" title=\"Get access\" href=\"https://#{webapp_domain}\" target=\"_blank\" style=\"font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;\">Open Tilig</a>"
  end

  def member_name
    html_escape(member.display_name.presence || member.given_name.presence || member.email)
  end

  def item_url(item)
    URI.join("https://#{webapp_domain}", item.id).to_s
  end

  attr_reader :memberships, :recipient, :member
end
