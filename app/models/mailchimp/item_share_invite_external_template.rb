class Mailchimp::ItemShareInviteExternalTemplate < Mailchimp::Template
  include Rails.application.routes.url_helpers
  include ERB::Util
  attr_reader :recipient, :membership, :inviter

  def initialize(membership:, inviter:)
    @recipient = membership.email
    @membership = membership
    @inviter = inviter
  end

  private

  def message
    {
      subject: "#{html_escape inviter.display_name} has shared an item with you on Tilig",
      from_email: "no-reply@tilig.com",
      from_name: "Tilig",
      to: [{
        email: recipient
      }]
    }
  end

  def template_slug
    Rails.application.config.accept_share_item_template_slug
  end

  def content
    [{
      name: "body",
      content: body_content
    }, {
      name: "button",
      content: button_content
    }]
  end

  def body_content
    "<b>#{html_escape inviter.display_name}</b> (#{inviter.email}) has shared an item #{folder_name} with you on Tilig. To access the item, click this button:" \
    "<br><br>" \
  end

  def button_content
    "<a class=\"mcnButton\" title=\"Get access\" href=\"https://#{webapp_domain}/signup\" target=\"_blank\" style=\"font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;\">Get access</a>"
  end

  def folder_name
    "<b>'#{html_escape membership.folder.name}'</b>" if membership.folder.name.present?
  end

  def accept_invitation_path
    Rails.application.config.accept_share_item_inviation_path
  end
end
