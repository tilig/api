class Mailchimp::CancellationFollowUpTemplate < Mailchimp::Template
  attr_reader :recipient

  def initialize(recipient:)
    @recipient = recipient
  end

  private

  def message
    {
      subject: "Your Tilig account has been canceled",
      from_email: "sjoerd@tilig.com",
      from_name: "Sjoerd from Tilig",
      to: [{
        email: recipient[:email],
        name: recipient[:display_name]
      }]
    }
  end

  def send_at
    time = 1.day.from_now
    time.strftime("%Y-%m-%d %H:%M:%S")
  end

  def template_slug
    Rails.application.config.cancellation_follow_up_template_slug
  end

  def content
    [{
      name: "greetings",
      content: greetings_content
    }]
  end

  def greetings_content
    "Dear #{recipient_name},"
  end

  def recipient_name
    recipient[:display_name].presence || recipient[:email]
  end
end
