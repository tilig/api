class Mailchimp::OrganizationInviteAcceptedConfirmationTemplate < Mailchimp::Template
  attr_reader :invitation, :organization, :inviter, :invitee

  def initialize(invitation:)
    @invitation = invitation
    @organization = invitation.organization
    @inviter = invitation.creator
    @invitee = invitation.invitee
  end

  private

  def message
    {
      subject: "#{html_escape invitee.display_name} accepted the invite to your organization in Tilig",
      from_email: "no-reply@tilig.com",
      from_name: "Tilig",
      to: [{
        email: inviter.email,
        name: inviter.display_name
      }]
    }
  end

  def content
    [{
      name: "body",
      content: body_content
    }]
  end

  def template_slug
    Rails.application.config.accept_organization_invitation_confirmation_template_slug
  end

  def body_content
    "<b>#{html_escape invitee.display_name}</b> (#{html_escape invitee.email}) has accepted the invite to your organization #{organization_name} on Tilig."
  end

  def organization_name
    "<b>'#{raw_organization_name}'</b>" if organization.name.present?
  end

  def raw_organization_name
    html_escape organization.name if organization.name.present?
  end
end
