class Mailchimp::ReferralThankYouTemplate < Mailchimp::Template
  def initialize(recipient:, referred_user:)
    @recipient = recipient
    @referred_user = referred_user
  end

  private

  def message
    {
      subject: "#{referred_user_name}, which you referred, has just joined Tilig!",
      from_email: "no-reply@tilig.com",
      from_name: "Tilig",
      to: [{
        email: recipient.email,
        name: recipient.display_name
      }]
    }
  end

  def content
    [{
      name: "body",
      content: body_content
    }]
  end

  def template_slug
    Rails.application.config.referral_thank_you_template_slug
  end

  def body_content
    "Hello there, #{recipient_name}.<br><br>" \
    "Congratulations! Your colleague or friend #{referred_user_name} has just signed up for Tilig using your referral link.<br>" \
    "That earns you 10 bonus points in our weekly squirrel plushie giveaway.<br><br>" \
    "Please keep in mind that viewing your points is only possible in the web app right now. (Not on your mobile Tilig app.)<br><br>" \
    "Thank you for helping us in spreading the word about the importance of password security!<br><br>" \
    "Best wishes,<br>The Tilig Team"
  end

  def recipient_name
    html_escape(recipient.display_name.presence || recipient.email)
  end

  def referred_user_name
    html_escape(referred_user.display_name.presence || referred_user.email)
  end

  attr_reader :recipient, :referred_user
end
