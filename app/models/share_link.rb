class ShareLink < ApplicationRecord
  belongs_to :item, touch: true

  validates :encrypted_dek, :encrypted_master_key, presence: true
  validates :uuid, format: {with: /\A[a-f0-9]{32}\z/}, uniqueness: true
  validates :access_token, presence: true, unless: :access_token_digest?
  validates :access_token, format: {with: /\A[a-f0-9]{32}\z/}, allow_blank: true

  attr_reader :access_token

  def access_token=(access_token)
    @access_token = access_token
    self.access_token_digest = access_token.nil? ? nil : RbNaCl::PasswordHash.argon2_str(access_token)
  end

  def authenticate_access_token(access_token)
    RbNaCl::PasswordHash.argon2_valid?(access_token, access_token_digest)
  end
end

# == Schema Information
#
# Table name: share_links
#
#  id                   :uuid             not null, primary key
#  access_token_digest  :string
#  disabled_at          :datetime
#  encrypted_dek        :text
#  encrypted_master_key :text
#  expired_at           :datetime
#  times_viewed         :integer          default(0)
#  uuid                 :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  item_id              :uuid             not null
#
# Indexes
#
#  index_share_links_on_item_id  (item_id)
#  index_share_links_on_uuid     (uuid) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (item_id => items.id)
#
