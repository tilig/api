class Item < ApplicationRecord
  include SoftDestroyable

  belongs_to :user, optional: true
  belongs_to :folder, optional: true
  belongs_to :brand, optional: true

  has_many :password_versions, dependent: :destroy
  has_one :share_link, dependent: :destroy

  validates :name, presence: true, unless: :new_encryption?
  validates :template, :encrypted_overview, :encrypted_details,
    presence: true, if: :new_encryption?

  validates :encrypted_folder_dek, presence: true, if: :folder

  # We need to make sure the RSA encrypted DEK is still present for legacy clients
  validates :rsa_encrypted_dek, presence: true,
    if: :new_encryption?, unless: proc { encrypted_dek? || encrypted_folder_dek? }

  validates :encryption_version, numericality: {greater_than_or_equal_to: 2}, if: :folder

  # Secrets have to belong to EITHER a user OR a folder
  validates :user, presence: true, unless: :folder
  validates :folder, absence: true, if: :user

  # If the folder changes, the encrypted_folder_dek should also have changed
  validate :encrypted_folder_dek_changed_if_folder_changed, if: %i[encrypted_folder_dek folder]

  before_save :set_brand, :update_domain, if: :website_change_to_be_saved

  # Trying again after save because validation might have failed
  after_save :reload_brand,
    if: proc { saved_change_to_website && brand_id.present? }
  after_save :track_password_version,
    if:
      proc {
        saved_change_to_password? && password_before_last_save.present?
      }

  scope :for_user, ->(user) { where(user:).or(where(folder: user.folder_ids)) }
  scope :default_order, -> { order(:created_at) }
  scope :updated_after, ->(time, limit: nil) {
                          begin
                            time = Time.zone.parse(time) if time.is_a? String
                          rescue
                            time = nil
                          end
                          time = [time, limit].compact.max
                          where("updated_at >= ?", time) if time
                        }

  after_soft_destroy { share_link&.destroy }
  after_destroy { folder.destroy if folder&.single_item? }

  def legacy_encryption_disabled?
    return true if folder
    return true unless [nil, "login/1"].include?(template)
    return legacy_encryption_disabled unless legacy_encryption_disabled.nil?

    user.legacy_encryption_disabled?
  end

  # Normalize Base64 encoded attributes
  %i[encrypted_dek
    encrypted_details
    encrypted_folder_dek
    encrypted_overview
    notes
    otp
    password
    username].each do |b64_attr|
    define_method b64_attr, proc { super()&.gsub(/[\s\n\r]/, "") }
  end

  # Some older clients store rsa_encrypted_dek with base64url encoding
  #
  # Translate characters and add padding
  def rsa_encrypted_dek
    orig = super()&.gsub(/[\s\n\r]/, "")&.tr("-_", "+/")
    orig&.ljust((orig.size / 4.0).ceil * 4, "=")
  end

  private

  def update_domain
    self.public_suffix_domain = DomainNormalizer.new(website).public_suffix_domain(ignore_private: false)
  end

  def set_brand
    return self.brand = nil if website.blank?
    self.brand = Brand.find_or_create_by_url(website)
  end

  def reload_brand
    brand.reload
  end

  def track_password_version
    password_versions.create(password: password_before_last_save)
  end

  def new_encryption?
    encryption_version >= 2
  end

  def encrypted_folder_dek_changed_if_folder_changed
    errors.add(:encrypted_folder_dek, :invalid) if folder_changed? && !encrypted_folder_dek_changed?
  end
end

# == Schema Information
#
# Table name: items
#
#  id                         :uuid             not null, primary key
#  create_platform            :string
#  deleted_at                 :datetime
#  encrypted_dek              :string
#  encrypted_details          :text
#  encrypted_folder_dek       :text
#  encrypted_overview         :text
#  encryption_version         :integer          default(1)
#  legacy_encryption_disabled :boolean
#  name                       :string
#  notes                      :string
#  otp                        :string
#  password                   :string
#  public_suffix_domain       :string
#  rsa_encrypted_dek          :text
#  template                   :string
#  update_platform            :string
#  username                   :string
#  website                    :string
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  android_app_id             :string
#  brand_id                   :uuid
#  folder_id                  :uuid
#  user_id                    :uuid
#
# Indexes
#
#  index_items_on_folder_id             (folder_id)
#  index_items_on_public_suffix_domain  (public_suffix_domain)
#  index_items_on_user_id               (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (brand_id => brands.id)
#  fk_rails_...  (folder_id => folders.id)
#  fk_rails_...  (user_id => users.id)
#
