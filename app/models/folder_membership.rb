class FolderMembership < ApplicationRecord
  enum role: {admin: "admin"}

  has_secure_token :acceptance_token

  belongs_to :user, optional: true
  belongs_to :folder
  belongs_to :creator, class_name: "User", optional: true

  # Completely accepted, with a user and encrypted private key
  scope :accepted, -> { where.not(accepted_at: nil).where.not(encrypted_private_key: nil).where.not(user: nil) }
  # Anything that isn't completely accepted
  scope :pending, -> { where(accepted_at: nil).or(where(encrypted_private_key: nil)).or(where(user: nil)) }
  # With a user but missing a private key, should be automatically accepted when a private key is added
  scope :missing_private_key, -> { where(encrypted_private_key: nil).where.not(user: nil) }

  validates :encrypted_private_key, :user, presence: true, if: proc { accepted? || email.blank? }
  validates :user, uniqueness: {scope: :folder}, allow_blank: true

  validates :email, presence: true, unless: :user
  validates :email, uniqueness: {scope: :folder},
    format: {with: URI::MailTo::EMAIL_REGEXP},
    length: {minimum: 4, maximum: 254},
    allow_blank: true

  def self.add_user_for_email(user)
    missing_user = where(email: user.email, user: nil)
    ids = missing_user.ids
    missing_user.update_all(user_id: user.id, accepted_at: Time.zone.now)
    folder_memberships = where(id: ids).includes(:creator).to_a

    # Add creators as contacts
    folder_memberships.map(&:creator).uniq.each do |c|
      Contact.make_contacts(user, c)
    end

    Mailchimp::SendInvitedMemberSignedUpNotice.new(folder_memberships:).call
  end

  def email=(value)
    super(value&.downcase&.strip)
  end

  def accepted?
    accepted_at?
  end

  def pending?
    !accepted_at?
  end

  def accept!
    touch :accepted_at
    add_contacts
  end

  def authorize_acceptance_token(token)
    return false unless token
    ActiveSupport::SecurityUtils.secure_compare(acceptance_token, token.to_s)
  end

  private

  def add_contacts
    return if folder.creator.blank?
    Contact.make_contacts(user, folder.creator)
  end
end

# == Schema Information
#
# Table name: folder_memberships
#
#  id                    :uuid             not null, primary key
#  acceptance_token      :string
#  accepted_at           :datetime
#  email                 :string
#  encrypted_private_key :text
#  role                  :string           default("admin"), not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  creator_id            :uuid
#  folder_id             :uuid             not null
#  user_id               :uuid
#
# Indexes
#
#  index_folder_memberships_on_creator_id             (creator_id)
#  index_folder_memberships_on_email_and_folder_id    (email,folder_id) UNIQUE
#  index_folder_memberships_on_folder_id              (folder_id)
#  index_folder_memberships_on_user_id                (user_id)
#  index_folder_memberships_on_user_id_and_folder_id  (user_id,folder_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (creator_id => users.id)
#  fk_rails_...  (folder_id => folders.id)
#  fk_rails_...  (user_id => users.id)
#
