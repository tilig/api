class PasswordVersion < ApplicationRecord
  belongs_to :item
end

# == Schema Information
#
# Table name: password_versions
#
#  id         :uuid             not null, primary key
#  password   :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  item_id    :uuid             not null
#
# Indexes
#
#  index_password_versions_on_item_id  (item_id)
#
