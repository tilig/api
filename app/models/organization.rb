class Organization < ApplicationRecord
  belongs_to :creator, class_name: "User", optional: true

  has_many :users, -> { User.ordered }, dependent: :nullify
  has_many :invitations, -> { Invitation.ordered }, dependent: :destroy

  validates :name, presence: true
  validates :creator, presence: true, on: :create
end

# == Schema Information
#
# Table name: organizations
#
#  id         :uuid             not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  creator_id :uuid
#
# Indexes
#
#  index_organizations_on_creator_id  (creator_id)
#
# Foreign Keys
#
#  fk_rails_...  (creator_id => users.id)
#
