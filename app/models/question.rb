class Question < ApplicationRecord
  has_many :answers, dependent: :destroy

  validates_uniqueness_of :token
end

# == Schema Information
#
# Table name: questions
#
#  id             :uuid             not null, primary key
#  answer_options :string           default([]), not null, is an Array
#  content        :text             not null
#  survey_token   :string           not null
#  token          :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_questions_on_token  (token) UNIQUE
#
