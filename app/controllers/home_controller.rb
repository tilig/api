class HomeController < ApplicationController
  include ActionController::MimeResponds

  skip_before_action :authenticate

  def index
    respond_to do |format|
      format.any { render json: {ok: true} }
    end
  end
end
