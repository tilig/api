module Paginatable
  extend ActiveSupport::Concern
  DEFAULT_PAGE_SIZE = 5000

  protected

  def page_size
    page_params[:size].to_i.positive? ? page_params[:size].to_i : DEFAULT_PAGE_SIZE
  end

  def offset
    max_offset = [page_params[:offset].to_i, 1].max
    (max_offset - 1) * page_size
  end

  def page_params
    return @page_params if defined?(@page_params)

    @page_params = begin
      params.require(:page).permit(:size, :offset)
    rescue ActionController::ParameterMissing
      {}
    end
  end
end
