module Authenticatable
  extend ActiveSupport::Concern
  include ActionController::HttpAuthentication::Token::ControllerMethods

  included { before_action :authenticate }

  private

  def authenticate
    authenticate_or_request_with_http_token do |token|
      Current.user = Auth::LegacyTokenAuthenticator.new.authenticate(token)
    end
  end

  def current_user
    Current.user
  end

  def signed_in?
    current_user.present?
  end
end
