module Trackable
  extend ActiveSupport::Concern

  class HeaderMissingError < StandardError
    def initialize(properties)
      @properties = properties
    end
  end

  class InvalidEventError < StandardError
  end

  def track(event, properties = {}, user_properties = {})
    Rails.logger.debug { "Tracking #{event} for #{current_user&.id || "anonymous"}" }
    properties = properties.reverse_merge(default_properties)

    if version.blank? || platform.blank?
      Sentry.capture_exception(HeaderMissingError.new(properties))
    end

    async_mixpanel_tracker.track(distinct_id, event, properties)
    async_mailchimp_action(event) if [Events::SIGN_UP, Events::USER_DELETED].include?(event)

    update_people_profile(user_properties)
  end

  def track_person(user_properties = {})
    return if user_properties.blank?

    update_people_profile(user_properties)
  end

  private

  def update_people_profile(user_properties = {})
    return unless distinct_id

    attributes = {
      "$email": current_user.email,
      "$first_name": current_user.given_name,
      "$last_name": current_user.family_name,
      display_name: current_user.display_name,
      sign_up_platform: current_user.profile.application_settings["client_signed_up"],
      sign_in_platforms: current_user.profile.application_settings["clients_signed_in"],
      sign_up_provider_id: current_user.profile.application_settings["provider_id_signed_up"],
      sign_in_provider_ids: current_user.profile.application_settings["provider_ids_signed_in"]
    }.merge(user_properties || {})

    async_mixpanel_tracker.people.set(distinct_id, attributes, user_ip)
  end

  private

  def distinct_id
    current_user&.id
  end

  def default_properties
    {
      "x-tilig-platform": platform,
      "x-tilig-version": version,
      browserName: browser_name,
      browserVersion: browser_version,
      "$os": os_name,
      ip: user_ip
    }
  end

  def async_mixpanel_tracker
    @async_mixpanel_tracker ||= AsyncMixpanelTracker.new
  end

  def async_mailchimp_action(event)
    MailchimpService.new(event, Current.user).send_event
  end

  def version
    request.headers["x-tilig-version"] || "UNKNOWN"
  end

  def platform
    request.headers["x-tilig-platform"] || "UNKNOWN"
  end

  def browser_name
    request.headers["x-browser-name"]
  end

  def browser_version
    request.headers["x-browser-version"]
  end

  def os_name
    request.headers["x-os-name"] || fallback_os
  end

  def fallback_os
    return platform if %w[iOS Android].include?(platform)

    "UNKNOWN"
  end

  def user_ip
    Sentry.capture_message("x-nf-client-connect-ip header has a value") if request.headers["x-nf-client-connect-ip"].present?
    request.headers["x-nf-client-connection-ip"] || request.remote_ip
  end
end
