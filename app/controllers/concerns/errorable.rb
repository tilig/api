module Errorable
  extend ActiveSupport::Concern

  class InvitationExpired < StandardError; end

  class InvitationAlreadyAccepted < StandardError; end

  class InvitationEmailMismatch < StandardError; end

  class UserAlreadyBelongsToOrganization < StandardError; end
  included do
    rescue_from InvitationExpired, with: :invitation_expired

    rescue_from InvitationAlreadyAccepted, with: :invitation_already_accepted

    rescue_from InvitationEmailMismatch, with: :invitation_email_mismatch

    rescue_from UserAlreadyBelongsToOrganization, with: :user_already_belongs_to_organization
  end

  def invitation_expired
    render json: {errors: {base: ["Invitation is expired"]}}, status: :unprocessable_entity
  end

  def invitation_already_accepted
    render json: {errors: {base: ["Invitation is already accepted"]}}, status: :unprocessable_entity
  end

  def invitation_email_mismatch
    render json: {errors: {base: ["Invitation email differs from logged in user's email"]}}, status: :unprocessable_entity
  end

  def user_already_belongs_to_organization
    render json: {errors: {base: ["The logged in user already belongs to an organization"]}}, status: :unprocessable_entity
  end
end
