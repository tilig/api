class ApplicationController < ActionController::API
  include Authenticatable
  include Trackable

  before_action :set_sentry_context

  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  private

  def set_sentry_context
    Sentry.set_user(id: current_user.id, email: current_user.email) if current_user.present?
  end

  def bad_request
    render status: :bad_request, json: {msg: "Bad request"}
  end

  def not_found
    render status: :not_found, json: {msg: "Not found"}
  end

  def unprocessable_entity
    render status: :unprocessable_entity, json: {msg: "Unprocessable entity"}
  end

  def render_unauthorized
    render status: :unauthorized, json: {error: "Unauthorized"}
  end

  def forbidden
    render status: :forbidden, json: {msg: "Forbidden"}
  end
end
