class Api::V3::Organizations::MyInvitationsController < ApplicationController
  include Errorable
  before_action :set_invitation, only: [:destroy]

  def index
    @pending_invitations = Invitation.where(email: current_user.email).pending

    render :index, status: :ok
  end

  def update
    @invitation = Invitation.find(params[:id])

    raise InvitationExpired if @invitation.expired?
    raise InvitationEmailMismatch if @invitation.email != current_user.email
    raise InvitationAlreadyAccepted if @invitation.accepted?
    raise UserAlreadyBelongsToOrganization if current_user.organization.present?

    if @invitation.update(invitee: current_user, accepted_at: Time.zone.now)
      current_user.update(organization: @invitation.organization)

      track Events::INVITATION_ACCEPTED
      Mailchimp::OrganizationInviteAcceptedConfirmationService.new(@invitation).call

      render :show, status: :ok
    else
      render json: {errors: @invitation.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @invitation.destroy

    track Events::INVITATION_DECLINED

    render :show, status: :ok
  end

  def set_invitation
    @invitation = current_user.invitations.find(params[:id])
  end
end
