class Api::V3::Organizations::InvitationsController < ApplicationController
  include Errorable
  before_action :set_organization, only: [:create, :destroy]
  skip_before_action :authenticate, only: [:show]

  def show
    @invitation = Invitation.find(params[:id])

    if @invitation.authorize_view_token(invitation_show_params[:view_token])
      raise InvitationExpired if @invitation.expired?
      raise InvitationAlreadyAccepted if @invitation.accepted?

      render :show
    else
      raise ActiveRecord::RecordNotFound
    end
  end

  def create
    invitation_list = invitation_create_params[:emails].map { |email| {email:} }

    Invitation.transaction do
      @invitations = @organization.invitations.ordered.where(creator: current_user).create(invitation_list)
      @all_persisted = @invitations.all?(&:persisted?)
      raise ActiveRecord::Rollback unless @all_persisted
    end

    if @all_persisted
      track Events::INVITATIONS_CREATED
      Mailchimp::OrganizationInviteService.new(@invitations).call

      track Events::ORGANIZATION_MEMBERSHIP_CREATED, {count: @invitations.size}
      render :index, status: :created
    else
      render json: {
        errors: @invitations.filter_map do |invitation|
          {value: invitation.email, errors: invitation.errors} if invitation.errors.present?
        end
      }, status: :unprocessable_entity
    end
  end

  def update
    @invitation = Invitation.find(params[:id])

    raise InvitationExpired if @invitation.expired?
    raise InvitationEmailMismatch if @invitation.email != current_user.email
    raise InvitationAlreadyAccepted if @invitation.accepted?
    raise UserAlreadyBelongsToOrganization if current_user.organization.present?

    if @invitation.update(invitee: current_user, accepted_at: Time.zone.now)
      current_user.update(organization: @invitation.organization)

      track Events::INVITATION_ACCEPTED
      Mailchimp::OrganizationInviteAcceptedConfirmationService.new(@invitation).call

      render :update, status: :ok
    else
      render json: {errors: @invitation.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @invitation = @organization.invitations.pending.find(params[:id])
    if @invitation.destroy
      track Events::INVITATION_DELETED
      render :destroy, status: :ok
    else
      render json: {errors: @invitation.errors}, status: :unprocessable_entity
    end
  end

  private

  def invitation_show_params
    params.require(:invitation).permit(:view_token)
  end

  def invitation_create_params
    params.require(:invitations).permit(emails: [])
  end

  def set_organization
    @organization = current_user.organization
  end
end
