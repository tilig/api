class Api::V3::ItemsController < ApplicationController
  include Paginatable
  before_action :set_item, only: %i[show update destroy]

  ITEM_PARAMS = %i[
    encrypted_dek
    encrypted_details
    encrypted_folder_dek
    encrypted_overview
    encryption_version
    folder_id
    rsa_encrypted_dek
    template
    website
  ].freeze

  LEGACY_ITEM_PARAMS = %i[
    android_app_id
    name
    notes
    otp
    password
    username
  ].freeze

  def index
    @items =
      Item.for_user(current_user)
        .default_order
        .active
        .limit(page_size)
        .offset(offset)
        .updated_after(params[:after])
        .includes(
          :user, :share_link,
          brand: :parent,
          folder: {
            accepted_folder_memberships: %i[user creator],
            pending_folder_invitations: %i[user creator]
          }
        )
  end

  def show
  end

  def create
    @item = current_user.items.build
    success = Items::UpdateItem.new(
      user: current_user,
      item: @item,
      item_params: resolved_item_params.merge(create_platform: platform, update_platform: platform)
    ).call

    if success
      track Events::ACCOUNT_CREATED
      render :show, status: :created
    else
      render json: {errors: @item.errors}, status: :unprocessable_entity
    end
  end

  def update
    success = Items::UpdateItem.new(
      user: current_user,
      item: @item,
      item_params: resolved_item_params.merge(update_platform: platform)
    ).call

    if success
      track Events::ACCOUNT_UPDATED
      render :show, status: :ok
    else
      render json: {errors: @item.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    track Events::ACCOUNT_DELETED
    @item.soft_destroy
    render :show, status: :ok
  end

  private

  def set_item
    @item = Item.for_user(current_user).active.find(params[:id])
  end

  def resolved_item_params
    current_user.legacy_encryption_disabled? ? item_params : legacy_item_params
  end

  def item_params
    params.require(:item).permit(ITEM_PARAMS)
  end

  def legacy_item_params
    params.require(:item).permit(*ITEM_PARAMS, *LEGACY_ITEM_PARAMS)
  end
end
