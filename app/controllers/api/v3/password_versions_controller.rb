class Api::V3::PasswordVersionsController < ApplicationController
  before_action :set_secret, only: %i[index]

  def index
    @password_versions = @secret.password_versions
  end

  private

  def set_secret
    @secret = current_user.items.active.find(params[:secret_id])
  end
end
