class Api::V3::Profile::ReferralsController < ApplicationController
  def update
    if current_user.profile.save_referral(referral_params[:referral_link])
      track Events::REFERRAL_REDEEMED
      recipient = Profile.find_user_by_referral_link(referral_params[:referral_link])
      Mailchimp::SendReferralThankYou.new(recipient: recipient, referred_user: current_user).call if recipient.present?
    end

    render :show
  end

  private

  def referral_params
    params.require(:referral).permit(:referral_link)
  end
end
