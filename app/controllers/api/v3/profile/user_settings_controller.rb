class Api::V3::Profile::UserSettingsController < ApplicationController
  def update
    current_user.profile.update!(user_settings: current_user.profile.user_settings.deep_merge(user_settings_params))

    render json: current_user.profile.user_settings
  end

  private

  def user_settings_params
    params.require(:user_settings).permit!
  end
end
