class Api::V3::AnswersController < ApplicationController
  class UnprocessableEntity < StandardError; end

  before_action :validate_question

  def create
    @answer = Answer.new(answer_params.merge(user: current_user).merge(request_headers))

    if @answer.save
      track Events::ANSWER_CREATED, answer_properties
      render :show, status: :created
    else
      render json: @answer.errors, status: :unprocessable_entity
    end
  end

  private

  def validate_question
    question = answer_params[:question_attributes]
    return if question && question[:content].present?

    Sentry.capture_exception(UnprocessableEntity.new)
    unprocessable_entity
  end

  def answer_params
    params.require(:answer).permit(
      :skipped,
      chosen_options: [],
      question_attributes: [:content, :survey_token, :token, answer_options: []]
    )
  end

  def request_headers
    {x_tilig_version: request.headers["x-tilig-version"],
     x_tilig_platform: request.headers["x-tilig-platform"],
     x_browser_name: request.headers["x-browser-name"],
     x_browser_version: request.headers["x-browser-version"],
     x_os_name: request.headers["x-os-name"]}
  end

  def answer_properties
    properties = answer_params.to_h
    question_attributes = properties.delete("question_attributes")

    question_attributes&.each_with_object(properties) { |(key, value), properties|
      properties.store("question_#{key}", value)
    }
  end
end
