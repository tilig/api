class Api::V3::AnonymousTrackController < ApplicationController
  ACCEPTED_EVENTS = [
    Events::BROWSER_EXTENSION_INSTALLED,
    Events::BROWSER_EXTENSION_UNINSTALLED,
    Events::BROWSER_EXTENSION_UPDATED,
    Events::REFERRAL_LINK_CLICKED,
    Events::SHARED_ITEM_2FA_CODE_COPIED,
    Events::SHARED_ITEM_GETTING_STARTED_SELECTED,
    Events::SHARED_ITEM_NOTES_COPIED,
    Events::SHARED_ITEM_PASSWORD_COPIED,
    Events::SHARED_ITEM_USERNAME_COPIED,
    Events::SHARED_ITEM_VIEWED,
    Events::SHARED_ITEM_WEBSITE_COPIED,
    Events::SHARED_ITEM_WEBSITE_OPENED,
    Events::UNINSTALL_SURVEY_ANSWERED,
    Events::UNINSTALL_SURVEY_ANSWERED_FULL,
    Events::WEBREQUEST_API_STATUS
  ]

  skip_before_action :authenticate
  before_action :validate_event

  def create
    track valid_params[:event], valid_params[:properties]

    render json: {msg: "ok"}
  end

  def valid_params
    params.permit(:event, properties: {})
  end

  def validate_event
    return if ACCEPTED_EVENTS.include?(valid_params[:event])

    Sentry.capture_exception(Trackable::InvalidEventError.new)
    render json: {error: "Unprocessable Entity"}, status: :unprocessable_entity
  end
end
