class Api::V3::SecretsController < ApplicationController
  include Paginatable
  before_action :set_secret, only: %i[show update destroy]

  def index
    # Only needed until clients all clients can support multiple formats
    # in the new Security Architecture
    included_templates =
      Array(params[:include_template]).flat_map do |template|
        template.to_s.split(",")
      end

    @secrets =
      current_user
        .items
        .default_order
        .active
        .limit(page_size)
        .offset(offset)
        .updated_after(params[:after])
        .includes(:share_link, :folder, brand: :parent)
        .where(template: [nil, "login/1"] | included_templates)
  end

  # TODO: Remove this endpoint
  def search
    @secrets = if params[:domain].present?
      # We expect the given domain name to be properly parsed, taking into account
      # the Public Suffix List. Therfore we can do exact comparisons.
      current_user.items
        .includes(:share_link, :folder, brand: :parent)
        .default_order
        .active
        .limit(page_size)
        .offset(offset)
        .where(public_suffix_domain: params[:domain])
    else
      Item.none
    end
    render :index
  rescue URI::InvalidURIError
    @secrets = []
    render :index
  end

  def show
  end

  def create
    @secret = current_user.items.build secret_params.merge(create_platform: platform, update_platform: platform)

    if @secret.save
      track Events::ACCOUNT_CREATED
      render :show, status: :created
    else
      render json: @secret.errors, status: :unprocessable_entity
    end
  end

  def update
    if @secret.update(secret_params.merge(update_platform: platform))
      track Events::ACCOUNT_UPDATED
      render :show, status: :ok
    else
      render json: @secret.errors, status: :unprocessable_entity
    end
  end

  def destroy
    track Events::ACCOUNT_DELETED

    @secret.soft_destroy
    render json: {status: "deleted"}, status: :ok
  end

  private

  def set_secret
    @secret = current_user.items.active.find(params[:id])
  end

  def secret_params
    current_user.legacy_encryption_disabled? ? v2_secret_params : legacy_secret_params
  end

  def legacy_secret_params
    params.permit(
      :template,
      :website,
      :encrypted_key,
      :encrypted_dek,
      :encrypted_details,
      :encrypted_overview,
      :encryption_version,
      :android_app_id,
      :name,
      :notes,
      :otp,
      :password,
      :username
    ).transform_keys do |k|
      {
        "encrypted_key" => "encrypted_dek",
        "encrypted_dek" => "rsa_encrypted_dek"
      }[k] || k
    end
  end

  def v2_secret_params
    params.permit(
      :template,
      :website,
      :encrypted_key,
      :encrypted_dek,
      :encrypted_details,
      :encrypted_overview,
      :encryption_version
    ).transform_keys do |k|
      {
        "encrypted_key" => "encrypted_dek",
        "encrypted_dek" => "rsa_encrypted_dek"
      }[k] || k
    end
  end
end
