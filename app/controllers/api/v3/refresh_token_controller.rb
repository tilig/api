class Api::V3::RefreshTokenController < ApplicationController
  skip_before_action :authenticate, on: :refresh

  def refresh
    payload = Auth::RefreshTokenDecoder.new.decode(refresh_token)
    user = User.find(payload["uid"])

    # An identity that contains this exact refresh token should exist
    identity = user.identities.find_by!(
      "refresh_token = ? OR previous_refresh_token = ?",
      refresh_token, refresh_token
    )
    identity.previous_refresh_token = identity.refresh_token
    identity.previous_access_token = identity.access_token
    identity.refresh_token = Auth::RefreshTokenGenerator.generate(user)
    identity.access_token = Auth::AccessTokenGenerator.generate(user)
    identity.save!

    render json: {
      accessToken: identity.access_token,
      refreshToken: identity.refresh_token
    }, status: :ok
  rescue Auth::RefreshTokenDecoder::JwtDecodeError, ActiveRecord::RecordNotFound => e
    Sentry.capture_exception(e)
    render_unauthorized
  end

  private

  # Read the refresh token from the Authorization header
  def refresh_token
    request.headers["Authorization"]
      .match(/^(Token)\s+(.*)$/)[2]
  rescue NoMethodError
    nil
  end

  # I'm not sure if we want the token to be in the header or in POST body.
  # POST body is what OAuth2 does, but we don't follow the exact OAuth2 implementation.
  # The other tokens we use (key service token and access token) are sent in a
  # header as well. Therefore I decided to go with a header as well for the refresh token.
  # An advantage of this approach is that there's a smaller chance of tokens ending up
  # in log files.

  # That said, when we DO want to POST, use this:
  # def refresh_token
  #   params.require(:refresh_token)
  # end
end
