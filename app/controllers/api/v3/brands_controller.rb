class Api::V3::BrandsController < ApplicationController
  before_action :verify_url
  before_action :set_brand
  before_action :update_with_brandfetch

  def index
    render :index, status: :ok
  end

  private

  def verify_url
    unprocessable_entity if DomainNormalizer.new(params[:url]).url.blank?
  end

  def set_brand
    @brand = Brand.find_normalized_domain(params[:url]) || Brand.new
  end

  def update_with_brandfetch
    @brand.attributes = Brandfetch::AttributesFetcher.new(params[:url], @brand).fetch

    not_found unless @brand.save
  end
end
