class Api::V3::ProfileController < ApplicationController
  def show
  end

  def update
    if current_user.update(user_params)
      render :show, status: :ok

      track Events::USER_UPDATED
    else
      # TODO: test
      render json: current_user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    current_user.destroy

    track Events::USER_DELETED
    Mailchimp::SendCancellationFollowUp.new(recipient: current_user).call
  end

  private

  def user_params
    params.require(:user).permit(
      :public_key,
      :display_name,
      :given_name,
      :family_name,
      :locale,
      :picture,
      :provider_id
    )
  end
end
