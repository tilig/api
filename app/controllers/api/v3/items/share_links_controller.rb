class Api::V3::Items::ShareLinksController < ApplicationController
  before_action :set_item

  class ShareLinkExists < StandardError; end
  rescue_from ShareLinkExists, with: :share_link_exists

  def create
    raise ShareLinkExists if @item.share_link.present?
    @share_link = @item.create_share_link(share_link_params)

    if @share_link.persisted?
      track Events::SHARE_CREATED
      render :show, status: :created
    else
      render json: {errors: @share_link.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @share_link = @item.share_link
    raise ActiveRecord::RecordNotFound if @share_link.blank?
    if @share_link.destroy
      track Events::SHARE_DELETED
      render :show, status: :ok
    else
      render json: {errors: @share_link.errors}, status: :unprocessable_entity
    end
  end

  private

  def share_link_params
    params.require(:share_link).permit(
      :access_token,
      :encrypted_dek,
      :encrypted_master_key,
      :uuid
    )
  end

  def share_link_exists
    render json: {errors: {base: ["Share link already exists"]}}, status: :unprocessable_entity
  end

  def set_item
    @item = Item.for_user(current_user).active.find(params[:item_id])
  end
end
