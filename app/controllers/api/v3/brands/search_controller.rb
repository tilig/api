class Api::V3::Brands::SearchController < ApplicationController
  before_action :validate_name

  def index
    @brands = Brand.includes([:parent]).with_prefix(prefix, page_size)
    @brands.each { |brand| FetchBrandJob.perform_later(brand) if brand.fetch_date.nil? }

    render :index, status: :ok
  end

  private

  def valid_params
    params.permit(:prefix, :page_size)
  end

  def validate_name
    @brands = []
    render :index, status: :ok if prefix.blank?
  end

  def prefix
    prefix_value = valid_params[:prefix].to_s
    prefix_value.downcase.strip.delete_prefix("www.")
  end

  def page_size
    valid_params[:page_size]&.to_i
  end
end
