# TODO: rethink naming of AuthenticationController and Authenticatable
class Api::V3::AuthenticationController < ApplicationController
  # Skip the access token validation for this controller
  skip_before_action :authenticate

  def authenticate
    email = Auth::TiligTokenDecoder.new.decode_email(key_service_jwt)
    Current.user = User.find_by(email:)

    if current_user.nil?
      render json: {errors: {base: ["Cannot create new account"]}}, status: :forbidden
      return
    end

    if current_user.previously_new_record?
      current_user.profile.save_sign_up_client(platform)
      current_user.profile.save_sign_up_provider_id(params[:provider_id])
      FolderMembership.add_user_for_email(current_user)
      save_referral(params[:referral]) if params[:referral]
      current_user.profile.disable_legacy_encryption!
      track Events::SIGN_UP
    end

    current_user.profile.save_signed_in_client(platform)
    current_user.profile.save_signed_in_provider_id(params[:provider_id])
    track Events::SIGN_IN, trigger: params[:trigger]

    identity = create_identity current_user

    render json: {
             accessToken: identity.access_token,
             refreshToken: identity.refresh_token
           },
      status: :ok
  rescue Auth::TiligTokenDecoder::JwtDecodeError => error
    Rails.logger.error("Rescued: #{error.inspect}") if Rails.env.development?
    Sentry.capture_exception(error)
    render_unauthorized
  end

  private

  def create_identity(user)
    user.identities.create!(
      access_token: Auth::AccessTokenGenerator.generate(user),
      refresh_token: Auth::RefreshTokenGenerator.generate(user)
    )
  end

  def key_service_jwt
    request.headers["Authorization"].match(/^(Token)\s+(.*)$/)[2]
  rescue NoMethodError
    nil
  end

  def save_referral(referral)
    return if referral.blank?
    if current_user.profile.save_referral(referral)
      track Events::REFERRAL_REDEEMED
      recipient = Profile.find_user_by_referral_link(referral)
      Mailchimp::SendReferralThankYou.new(recipient: recipient, referred_user: current_user).call if recipient.present?
    end
  end
end
