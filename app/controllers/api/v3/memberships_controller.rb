class Api::V3::MembershipsController < ApplicationController
  def index
    @memberships = current_user.memberships.includes([:group])

    render :index
  end
end
