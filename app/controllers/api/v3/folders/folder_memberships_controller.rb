class Api::V3::Folders::FolderMembershipsController < ApplicationController
  before_action :set_folder, only: %i[create update destroy]
  before_action :set_folder_membership, only: %i[update destroy]

  def create
    @folder.update(folder_params) if folder_params.present?
    @folder_membership = Folders::AddMembership.new(creator: current_user, folder: @folder, folder_membership_params:).create

    if @folder_membership.persisted?
      track Events::FOLDER_MEMBERSHIP_CREATED, {count: @folder.folder_memberships.size}
      render :show, status: :created
    else
      render json: {errors: @folder_membership.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @folder_membership.update(folder_membership_params)
      track Events::FOLDER_MEMBERSHIP_UPDATED, {count: @folder.folder_memberships.size}
      Mailchimp::SendFolderInvite.new(membership: @folder_membership, inviter: current_user).call

      render :show, status: :ok
    else
      render json: {errors: @folder_membership.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    if @folder.admin_folder_memberships.excluding(@folder_membership).any?
      @folder_membership.destroy
      track Events::FOLDER_MEMBERSHIP_DELETED
      render :show
    else
      render json: {errors: {base: ["Cannot leave a folder without an admin"]}}, status: :unprocessable_entity
    end
  end

  private

  def folder_membership_params
    params.require(:folder_membership).permit(:user_id, :encrypted_private_key, :email)
  end

  def folder_params
    params.require(:folder).permit(:name)
  rescue ActionController::ParameterMissing
    nil
  end

  def set_folder
    @folder = current_user.managing_folders.active.find(params[:folder_id])
  end

  def set_folder_membership
    @folder_membership = @folder.folder_memberships.find(params[:id])
  end
end
