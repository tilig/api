class Api::V3::Folders::AcceptancesController < ApplicationController
  skip_before_action :authenticate

  before_action :set_folder, only: %i[create]
  before_action :set_folder_membership, only: %i[create]

  def create
    if @folder_membership.authorize_acceptance_token(acceptance_params[:acceptance_token])
      @folder_membership.accept!
      track Events::FOLDER_MEMBERSHIP_ACCEPTED
      render :show, status: :created
    else
      render json: {errors: {base: ["Something is wrong with the acceptance token"]}}, status: :unprocessable_entity
    end
  end

  # TODO Remove when ready -> This is a controller that can be removed once the clients are able to handle the accept via the frontend
  def accept
    @folder = Folder.active.find(params[:folder_id])
    @folder_membership = @folder.folder_memberships.find(params[:folder_membership_id])
    if @folder_membership.authorize_acceptance_token(params[:acceptance_token])
      @folder_membership.accept!
      track Events::FOLDER_MEMBERSHIP_ACCEPTED
      render "/api/v3/folder_memberships/acceptance_success", layout: "application/invite", formats: :html, status: :created
    else
      render "/api/v3/folder_memberships/acceptance_fail", layout: "application/invite", formats: :html
    end
  rescue ActiveRecord::RecordNotFound
    render "/api/v3/folder_memberships/acceptance_fail", layout: "application/invite", formats: :html
  end

  private

  def acceptance_params
    params.require(:acceptance).permit(:acceptance_token)
  end

  def set_folder
    @folder = Folder.active.find(params[:folder_id])
  end

  def set_folder_membership
    @folder_membership = @folder.folder_memberships.find(params[:folder_membership_id])
  end
end
