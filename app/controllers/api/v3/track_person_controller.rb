class Api::V3::TrackPersonController < ApplicationController
  def create
    track_person valid_params[:properties]

    render json: {msg: "ok"}, status: :created
  end

  private

  def valid_params
    params.permit(properties: {})
  end
end
