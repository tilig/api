class Api::V3::ContactsController < ApplicationController
  def index
    @contacts = current_user.all_contact_users
  end
end
