class Api::V3::FoldersController < ApplicationController
  before_action :set_folder, only: %i[update destroy]

  def index
    @folders = current_user.folders.active.not_single_item.includes(
      accepted_folder_memberships: %i[user creator],
      pending_folder_invitations: %i[user creator]
    )
    @pending_memberships = current_user.created_folder_memberships.includes(:user, :creator).missing_private_key
    render :index, status: :ok
  end

  def create
    @folder = Folders::CreateFolder.new(creator: current_user, folder_params:).create

    if @folder.persisted?
      track Events::FOLDER_CREATED
      track Events::FOLDER_MEMBERSHIP_CREATED, {count: @folder.folder_memberships.size}

      render :show, status: :created
    else
      render json: {errors: @folder.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @folder.update(folder_update_params)
      track Events::FOLDER_UPDATED
      render :show, status: :ok
    else
      render json: {errors: @folder.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @folder.soft_destroy
    track Events::FOLDER_DELETED

    render :show, status: :ok
  end

  private

  def folder_update_params
    params.require(:folder).permit(:name, :description)
  end

  def folder_params
    params.require(:folder).permit(
      :name, :public_key, :single_item, :description, items: [:id, :encrypted_folder_dek, share_link: [:encrypted_master_key]], folder_memberships: %i[user_id encrypted_private_key email]
    )
  end

  def set_folder
    @folder = current_user.managing_folders.active.find(params[:id])
  end
end
