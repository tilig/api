class Api::V3::ReferralLeaderboardController < ApplicationController
  def index
    leading_users = User
      .includes(:profile)
      .joins(:profile)
      .where("profiles.referral_count > 0")
      .order("profiles.referral_count DESC")
      .limit(10)

    @leaders = leading_users.map.with_index(1) do |leader, rank|
      is_me = leader == current_user

      {
        rank:,
        points: leader.profile.referral_count * 10,
        is_me:,
        email_short: is_me ? current_user.email : leader.email&.slice(0, 2)
      }
    end

    current_user_rank = if current_user.profile.referral_count.positive?
      Profile.find_by_sql(
        [<<~SQL.squish,
          SELECT referral_rank FROM
          (SELECT *, RANK() OVER (ORDER BY referral_count DESC) referral_rank FROM profiles WHERE referral_count > 0)
          as profiles_with_rank WHERE id = :id
        SQL
          id: current_user.profile.id]
      ).first.referral_rank
    else
      9999
    end

    @current_user_position = {
      rank: current_user_rank,
      points: current_user.profile.referral_count * 10,
      is_me: true,
      email_short: current_user.email
    }
  end
end
