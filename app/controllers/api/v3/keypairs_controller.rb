class Api::V3::KeypairsController < ApplicationController
  class KeypairAlreadyCreated < StandardError; end
  rescue_from KeypairAlreadyCreated, with: :keypair_already_created

  class KeypairPayloadInvalid < StandardError; end
  rescue_from KeypairPayloadInvalid, with: :keypair_payload_invalid

  def create
    raise KeypairAlreadyCreated if current_user.keypair.present?

    decoded_payload = Auth::KeypairTokenDecoder.new.decode(params[:encrypted_keypair])

    @keypair = current_user.create_keypair(
      payload: decoded_payload
    )
    if @keypair.persisted?
      track Events::KEYPAIR_SAVED
      render :create, status: :created
    else
      render json: @keypair.errors, status: :unprocessable_entity
    end
  rescue Auth::KeyServiceTokenDecoder::JwtDecodeError => error
    Rails.logger.error("Error during token decoding: #{error}")
    raise KeypairPayloadInvalid
  end

  private

  def keypair_already_created
    render json: {base: "Keypair already created"}, status: :unprocessable_entity
  end

  def keypair_payload_invalid
    render json: {base: "JWT payload invalid"}, status: :unprocessable_entity
  end
end
