class Api::V3::Users::PublicKeysController < ApplicationController
  before_action :set_user

  def show
  end

  private

  def set_user
    @user = User.joins(:keypair).find_by!(email: params[:email]&.downcase&.strip)
  end
end
