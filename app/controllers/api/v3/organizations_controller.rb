class Api::V3::OrganizationsController < ApplicationController
  before_action :verify_organization, only: [:create]
  before_action :set_organization, only: [:show, :update]

  def show
    render :show, status: :ok
  end

  def create
    @organization = Organization.new(organization_params.merge(creator: current_user))

    if @organization.save
      current_user.update(organization: @organization)
      track Events::ORGANIZATION_CREATED
      track Events::ORGANIZATION_MEMBERSHIP_CREATED, {count: @organization.users.size}

      render :show, status: :created
    else
      render json: {errors: @organization.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @organization.update(organization_params)
      track Events::ORGANIZATION_UPDATED

      render :show, status: :ok
    else
      render json: {errors: @organization.errors}, status: :unprocessable_entity
    end
  end

  private

  def organization_params
    params.require(:organization).permit(
      :name
    )
  end

  def verify_organization
    render json: {errors: {organization: ["already exists"]}}, status: :unprocessable_entity if current_user.organization.present?
  end

  def set_organization
    @organization = Organization
      .includes([:users, invitations: [:creator, :invitee]])
      .find(current_user.organization_id)

    not_found if @organization.blank?
  end
end
