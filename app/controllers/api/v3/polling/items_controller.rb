class Api::V3::Polling::ItemsController < ApplicationController
  def index
    @fetched_at = Time.current

    filtered_items =
      Item.for_user(current_user)
        .default_order
        .updated_after(params[:after], limit: 7.days.ago)

    @updated_items =
      filtered_items
        .active
        .includes([:user, :share_link, :folder, brand: :parent])

    @deleted_item_ids =
      filtered_items
        .inactive
        .ids
  end
end
