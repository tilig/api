class Api::V3::Polling::SecretsController < ApplicationController
  def index
    @fetched_at = Time.current
    filtered_secrets =
      current_user
        .items
        .default_order
        .updated_after(params[:after], limit: 7.days.ago)

    @updated_secrets =
      filtered_secrets
        .active
        .includes(:share_link, :folder, brand: :parent)

    @deleted_secret_ids =
      filtered_secrets
        .inactive
        .ids
  end
end
