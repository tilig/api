class Api::V3::TrackController < ApplicationController
  before_action :validate_event

  def create
    track valid_params[:event], valid_params[:properties], valid_params[:userProperties]

    render json: {msg: "ok"}
  end

  private

  def valid_params
    params.permit(:event, properties: {}, userProperties: {})
  end

  def validate_event
    return if valid_params[:event].present?

    Sentry.capture_exception(Trackable::InvalidEventError.new)
    unprocessable_entity
  end
end
