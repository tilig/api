class Api::V3::SharesController < ApplicationController
  before_action :set_secret, only: %i[create destroy]

  class ShareAlreadyCreated < StandardError; end
  rescue_from ShareAlreadyCreated, with: :share_already_created

  def index
    @shares = current_user.share_links.joins(:item).where(item: {deleted_at: nil}).order(created_at: :desc)
  end

  def create
    raise ShareAlreadyCreated if @secret.share_link.present?
    @share = @secret.create_share_link(share_params)

    if @share.persisted?
      track Events::SHARE_CREATED
      render :create, status: :created
    else
      render json: @share.errors, status: :unprocessable_entity
    end
  end

  def destroy
    raise ActiveRecord::RecordNotFound if @secret.share_link.blank?
    if @secret.share_link.destroy
      track Events::SHARE_DELETED
      render json: {status: "deleted"}, status: :ok
    else
      render json: @secret.share_link.errors, status: :unprocessable_entity
    end
  end

  private

  def share_params
    params.permit(
      :access_token,
      :encrypted_key,
      :encrypted_master_key,
      :uuid
    ).transform_keys do |k|
      {
        "encrypted_key" => "encrypted_dek"
      }[k] || k
    end
  end

  def share_already_created
    render json: {base: "Share already created"}, status: :unprocessable_entity
  end

  def set_secret
    @secret = current_user.items.active.find(params[:secret_id])
  end
end
