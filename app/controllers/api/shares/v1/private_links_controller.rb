class Api::Shares::V1::PrivateLinksController < ApplicationController
  skip_before_action :authenticate
  before_action :set_share

  class IncorrectShareCredentials < StandardError; end
  rescue_from IncorrectShareCredentials, with: :not_found

  def show
    if @share.authenticate_access_token(access_token_param)
      @share.increment!(:times_viewed)
      render :show
    else
      raise IncorrectShareCredentials
    end
  end

  private

  def set_share
    @share = ShareLink.joins(:item).find_by!(uuid: uuid_param, item: {deleted_at: nil})
  rescue ActiveRecord::RecordNotFound
    raise IncorrectShareCredentials
  end

  def uuid_param
    params[:uuid]
      .match(/\A([a-f0-9]{32})\z/)[1]
  rescue NoMethodError
    raise IncorrectShareCredentials
  end

  def access_token_param
    request.headers["Access-token"]
      .match(/\A([a-f0-9]{32})\z/)[1]
  rescue NoMethodError
    raise IncorrectShareCredentials
  end
end
