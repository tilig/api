module Events
  ACCOUNT_CREATED = "Account Created"
  ACCOUNT_DELETED = "Account Deleted"
  ACCOUNT_UPDATED = "Account Updated"

  FOLDER_MEMBERSHIP_ACCEPTED = "Folder Membership Accepted"
  FOLDER_MEMBERSHIP_CREATED = "Folder Membership Created"
  FOLDER_MEMBERSHIP_DELETED = "Folder Membership Deleted"
  FOLDER_MEMBERSHIP_UPDATED = "Folder Membership Updated"

  ANSWER_CREATED = "Answer Created"

  BROWSER_EXTENSION_INSTALLED = "Browser Extension Installed"
  BROWSER_EXTENSION_UNINSTALLED = "Browser Extension Uninstalled"
  BROWSER_EXTENSION_UPDATED = "Browser Extension Updated"

  FOLDER_CREATED = "Folder Created"
  FOLDER_UPDATED = "Folder Updated"
  FOLDER_DELETED = "Folder Deleted"

  KEYPAIR_SAVED = "Keypair Saved"

  ORGANIZATION_CREATED = "Team Created"
  ORGANIZATION_UPDATED = "Team Updated"
  ORGANIZATION_MEMBERSHIP_CREATED = "Team Membership Created"

  INVITATIONS_CREATED = "Team Invite Sent"
  INVITATION_ACCEPTED = "Team Invite Accepted"
  INVITATION_DELETED = "Team Invite Deleted"
  INVITATION_DECLINED = "Team Invite Declined"

  SHARED_ITEM_2FA_CODE_COPIED = "[Shared Item] 2FA Code Copied"
  SHARED_ITEM_GETTING_STARTED_SELECTED = "[Shared Item] Getting Started Selected"
  SHARED_ITEM_NOTES_COPIED = "[Shared Item] Notes Copied"
  SHARED_ITEM_PASSWORD_COPIED = "[Shared Item] Password Copied"
  SHARED_ITEM_USERNAME_COPIED = "[Shared Item] Username Copied"
  SHARED_ITEM_VIEWED = "[Shared Item] Viewed"
  SHARED_ITEM_WEBSITE_COPIED = "[Shared Item] Website Copied"
  SHARED_ITEM_WEBSITE_OPENED = "[Shared Item] Website Opened"

  SHARE_CREATED = "Share Created"
  SHARE_DELETED = "Share Deleted"

  REFERRAL_REDEEMED = "Referral Redeemed"
  REFERRAL_LINK_CLICKED = "Referral Link Clicked"

  SIGN_IN = "Sign In"
  SIGN_UP = "Sign Up"

  UNINSTALL_SURVEY_ANSWERED = "Uninstall Survey Answered"
  UNINSTALL_SURVEY_ANSWERED_FULL = "Uninstall Survey Answered (Full)"

  USER_DELETED = "User Deleted"
  USER_UPDATED = "User Updated"

  WEBREQUEST_API_STATUS = "WebRequest API Status"
end
