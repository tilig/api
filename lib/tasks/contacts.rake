namespace :contacts do
  desc "Fills the contacts based on the shared folders (only for first run)"
  task fill: :environment do
    Tasks::Contacts::Fill.new.execute
  end
end
