namespace :users do
  desc "Create a referral link for each user"
  task create_referral_link: :environment do
    Tasks::CreateReferralLinkForUsers.new.execute
  end
end
