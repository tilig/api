namespace :move do
  desc "Moves client_signed_up and clients_signed_in from meta column within Profiel to the application_settings column"
  task meta_clients_to_application_settings: :environment do
    Tasks::MoveMetaClientsToApplicationSettings.new.execute
  end
end
