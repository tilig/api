namespace :disable do
  desc "Sets the legacy_encryption_disabled flag for users that only have not deleted encryption_version 2 secrets, wich disables the legacy encryption"
  task legacy_encryption: :environment do
    Tasks::DisableLegacyEncryption.new.update_users
  end
end
