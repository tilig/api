namespace :brands do
  desc "Download and save all 2fa.directory services"
  task import_two_fa_directory: :environment do
    Tasks::TwoFADirectoryService.new.import_brands
  end

  desc "Import all alexa domains from a csv"
  task import_alexa: :environment do
    Tasks::AlexaTopDomains.new.import_brands
  end

  desc "Update a batch of 7000 brands with brandfetch"
  task supply_empty_brands: :environment do
    Tasks::SupplyBrandfetch.new.execute
  end

  desc "Reparse the brandfetch json of all Brands"
  task reparse_brands_brandfetchjson: :environment do
    Tasks::ReparseBrandsBrandfetchjson.new.execute
  end
end
