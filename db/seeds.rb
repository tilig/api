require "csv"

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if ENV["SEED_DAST_USER"]
  puts "SEED_DAST_USER true, seed DB"

  puts "User.."
  user = User.find_or_initialize_by("id" => "c09aff96-4e51-44d7-870e-82be7b7a852e")
  user.assign_attributes(
    public_key: "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtbu4XfRjgyQb4MmVfYmRtftF1n5MA7THQ7bk264LkkEWrcNJ+qqevI0XFHoMYUrn/e7u98oQ0ZI/Op89xtKwmGSKBAeTiQXPwDyjTqyU3N+Pyx0rn4awc8njHrirTcPUdCli5mpnaWmmDUVUR/IAK5s4CMMJ+LAEPzaMsTWGaVWLSwoZ7UwMj04b5eHGAQqHH3nbEQZGjID1NqImmRv7blaw4eD0tpv5AZ29t+Ric1yE4idLLghi0Rr+Br5UYRGFVAqIWdjwkB+iVp8rG1rRX5Xg2qAVYuVquxpA2ZyS7vj3X0yLFMoDIm0KJtfrohdf+uf9+FBErOuZr17+5jSj1wIDAQAB\n-----END PUBLIC KEY-----",
    email: "tili@tilig.com",
    display_name: "Tili",
    given_name: "G",
    family_name: "G",
    locale: "en",
    picture: "https://lh3.googleusercontent.com/a/AItbvmnko3gPRAGK05pAx1oYOA3KjiC4kkceTnHnKMYY=s96-c",
    provider_id: "google.com"
  )
  user.save

  puts "Identity.."
  Identity.find_or_create_by("access_token" => "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI3YjJkOWU1Mi1mYzI2LTQzMmMtOTZkMS1jM2Y3NzhkMjVlY2QiLCJ1aWQiOiJjMDlhZmY5Ni00ZTUxLTQ0ZDctODcwZS04MmJlN2I3YTg1MmIiLCJleHAiOjE2NjE0Mzg1MDgsImlzcyI6Imh0dHBzOi8vYXBwLnRpbGlnLmNvbSIsImlhdCI6MTY2MTM1MjEwOH0.NX1D4n6PLQuHbCbAOd5SakIGRiPM0-QeK9u4-4oqDKg", "refresh_token" => "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJkODdhMzcwYi00MWQ5LTQ0ZmUtYWQ2NS1iNDkzYTA3NzMxNTEiLCJ1aWQiOiJjMDlhZmY5Ni00ZTUxLTQ0ZDctODcwZS04MmJlN2I3YTg1MmIiLCJleHAiOjE2OTI4ODgxMDgsImlzcyI6Imh0dHBzOi8vYXBwLnRpbGlnLmNvbSIsImlhdCI6MTY2MTM1MjEwOH0.fkka-ms5LwqZ4iRCyblFFVji1sO53EYX76lgTfoueSw", "user_id" => user.id, "previous_access_token" => nil, "previous_refresh_token" => nil)

  puts "Keypair.."
  keypair = Keypair.find_or_initialize_by(
    "user_id" => user.id
  )
  keypair.assign_attributes(
    encrypted_private_key: Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false),
    public_key: Base64.urlsafe_encode64(SecureRandom.hex(10), padding: false),
    key_type: "x25519",
    kid: Base64.urlsafe_encode64(SecureRandom.hex(10), padding: false),
    meta_data: {"app_platform" => "Web"}
  )
  keypair.save

  puts "Brand.."
  brands = [
    {"domain" => "facebook.com", "name" => "Facebook", "totp" => false, "json" => nil, "public_suffix_domain" => "facebook.com", "brandfetch_json" => {}, "logo_source" => "https://asset.brandfetch.io/idpKX136kp/idNZ6hqFNO.svg", "main_color_hex" => "#1877f2", "main_color_brightness" => "108", "logo_icon_source" => "https://asset.brandfetch.io/idpKX136kp/idQBiBTxsm.jpeg", "rank" => 10, "fetch_date" => 3.days.ago},
    {"domain" => "instagram.com", "name" => "Instagram", "totp" => false, "json" => nil, "public_suffix_domain" => "instagram.com", "brandfetch_json" => {}, "logo_source" => "https://asset.brandfetch.io/ido5G85nya/id3iKOtd4p.svg", "main_color_hex" => nil, "main_color_brightness" => nil, "logo_icon_source" => "https://asset.brandfetch.io/ido5G85nya/idnsPRMIrl.jpeg", "rank" => 11, "fetch_date" => 3.days.ago},
    {"domain" => "pinterest.com", "name" => "Pinterest", "totp" => false, "json" => nil, "public_suffix_domain" => "pinterest.com", "brandfetch_json" => {}, "logo_source" => "https://asset.brandfetch.io/idGP0S1Jjj/idrcPTMuDp.svg", "main_color_hex" => "#e60023", "main_color_brightness" => "51", "logo_icon_source" => "https://asset.brandfetch.io/idGP0S1Jjj/idrnjFoKEw.jpeg", "rank" => 12, "fetch_date" => 3.days.ago},
    {"domain" => "twitter.com", "name" => "Twitter ", "totp" => false, "json" => nil, "public_suffix_domain" => "twitter.com", "brandfetch_json" => {}, "logo_source" => "https://asset.brandfetch.io/idCvQYuc_q/ideT8OzYlX.svg", "main_color_hex" => "#1da1f2", "main_color_brightness" => "139", "logo_icon_source" => "https://asset.brandfetch.io/idCvQYuc_q/idBrNPHmqO.jpeg", "rank" => 115, "fetch_date" => 3.days.ago},
    {"domain" => "paypal.com", "name" => "PayPal", "totp" => false, "json" => nil, "public_suffix_domain" => "paypal.com", "brandfetch_json" => {}, "logo_source" => "https://asset.brandfetch.io/id-Wd4a4TS/id4wNJYpBf.svg", "main_color_hex" => "#FFD140", "main_color_brightness" => "208", "logo_icon_source" => "https://asset.brandfetch.io/id-Wd4a4TS/id8rCKc-lT.png", "rank" => 200, "fetch_date" => 3.days.ago}
  ]
  brands.each do |brand|
    new_brand = Brand.find_or_initialize_by(domain: brand["domain"])
    new_brand.attributes = brand
    new_brand.save!
  end

  puts "Item 1.."
  secret = Item.find_or_initialize_by(
    "id" => "f076ac78-c750-45f0-b9a0-7faf52d2f715",
    "user_id" => user.id
  )
  secret.assign_attributes(
    name: "Special account",
    website: "dropbox.com",
    encryption_version: 2,
    template: "login/1",
    rsa_encrypted_dek: Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false),
    encrypted_overview: Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false),
    encrypted_details: Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false)
  )
  secret.save

  puts "Item 2.."
  secret2 = Item.find_or_initialize_by(
    "id" => "0a048468-fa74-4f20-a72f-7a097d55383",
    "user_id" => user.id
  )
  secret2.assign_attributes(
    name: "Special account 2",
    website: "facebook.com",
    encryption_version: 2,
    template: "login/1",
    rsa_encrypted_dek: Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false),
    encrypted_overview: Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false),
    encrypted_details: Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false)
  )
  secret2.save

  puts "Share.."
  if ShareLink.find_by(uuid: "cfdf368e870e2e7f78c77925714d25a2").nil?
    secret.create_share_link(
      uuid: "cfdf368e870e2e7f78c77925714d25a2",
      access_token: "4e6b3ea5017ffb27180c8b44c496fc55",
      encrypted_dek: Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false),
      encrypted_master_key: Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false)
    )
  end

  puts "Seed DB finished"
end

if User.count == 0
  puts "Nothing to seed, no users found"
  return
end

if Rails.env.development?
  puts "Update/create Brands with logo details"
  brands_dataset = YAML.load_file(Rails.root.join("db", "brands_dataset.yaml"))
  brands_dataset.each do |brand_data|
    print "."
    brand = Brand.find_or_initialize_by(domain: brand_data["domain"])
    brand.attributes = {
      name: brand_data["name"],
      public_suffix_domain: brand_data["public_suffix_domain"],
      totp: brand_data["totp"],
      main_color_hex: brand_data["main_color_hex"],
      main_color_brightness: brand_data["main_color_brightness"],
      logo_source: brand_data["logo_source"],
      logo_icon_source: brand_data["logo_icon_source"],
      brandfetch_json: {},
      rank: 10
    }
    brand.save!
  end
  puts "Done!"
end
