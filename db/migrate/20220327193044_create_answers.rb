class CreateAnswers < ActiveRecord::Migration[6.1]
  def change
    create_table :answers, id: :uuid do |t|
      t.boolean :skipped, null: false, default: false
      t.string :chosen_options, null: false, array: true, default: []
      t.string :x_tilig_platform
      t.string :x_tilig_version
      t.string :x_browser_name
      t.string :x_browser_version
      t.string :x_os_name

      t.references :user, null: false, foreign_key: true, type: :uuid
      t.references :question, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
