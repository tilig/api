class AddMigratedToLegacySecrets < ActiveRecord::Migration[6.1]
  def change
    add_column :legacy_secrets, :migrated, :boolean
  end
end
