class CreateProfileForExistingUsers < ActiveRecord::Migration[7.0]
  def up
    User.where.missing(:profile).find_each(&:create_profile)
  end
end
