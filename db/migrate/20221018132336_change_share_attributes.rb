class ChangeShareAttributes < ActiveRecord::Migration[7.0]
  def change
    remove_column :shares, :encrypted_secret, :string
    remove_column :shares, :email, :string
    remove_column :shares, :public_key, :string
    remove_reference :shares, :user, foreign_key: true, index: true, type: :uuid

    add_column :shares, :encrypted_dek, :text
    add_column :shares, :encrypted_master_key, :text
    add_column :shares, :uuid, :string
    add_index :shares, :uuid, unique: true
    add_column :shares, :access_token_digest, :string
    add_column :shares, :expired_at, :datetime
    add_column :shares, :disabled_at, :datetime
    add_column :shares, :times_viewed, :integer, default: 0
  end
end
