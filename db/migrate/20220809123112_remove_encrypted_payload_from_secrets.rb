class RemoveEncryptedPayloadFromSecrets < ActiveRecord::Migration[7.0]
  def change
    remove_column :secrets, :encrypted_payload
  end
end
