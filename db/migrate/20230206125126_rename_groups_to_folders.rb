class RenameGroupsToFolders < ActiveRecord::Migration[7.0]
  def change
    rename_table :groups, :folders
    rename_table :memberships, :folder_memberships
    rename_column :secrets, :group_id, :folder_id
    rename_column :folder_memberships, :group_id, :folder_id
    change_column_null :folders, :name, true
  end
end
