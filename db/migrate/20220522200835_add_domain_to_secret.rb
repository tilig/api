class AddDomainToSecret < ActiveRecord::Migration[7.0]
  def change
    add_column :secrets, :public_suffix_domain, :string
    add_index :secrets, :public_suffix_domain
  end
end
