class AddParentToBrands < ActiveRecord::Migration[7.0]
  def change
    add_reference :brands, :parent, type: :uuid
    add_column :brands, :parent_source, :string
  end
end
