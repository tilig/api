class DropPasswordApiSecrets < ActiveRecord::Migration[6.0]
  def change
    drop_table :password_api_secrets
  end
end
