class RemoveReferenceOfUserOnAnswers < ActiveRecord::Migration[6.1]
  def change
    remove_foreign_key :answers, :users
  end
end
