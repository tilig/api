class CreateV3Shares < ActiveRecord::Migration[6.0]
  def change
    create_table :v3_shares, id: :uuid do |t|
      t.references :v3_secret, null: false, foreign_key: true, type: :uuid
      t.references :v3_user, null: true, foreign_key: true, type: :uuid
      t.string :encrypted_secret
      t.string :email
      t.string :public_key

      t.timestamps
    end
  end
end
