class IncreaseTokenColumnLengths < ActiveRecord::Migration[6.1]
  def up
    change_column :identities, :access_token, :string, limit: 512
    change_column :identities, :refresh_token, :string, limit: 512
  end

  def down
    change_column :identities, :access_token, :string, limit: 255
    change_column :identities, :refresh_token, :string, limit: 255
  end
end
