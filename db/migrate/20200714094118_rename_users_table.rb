class RenameUsersTable < ActiveRecord::Migration[6.0]
  def change
    rename_table "Users", :users
  end
end
