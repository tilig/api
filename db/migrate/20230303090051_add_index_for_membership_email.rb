class AddIndexForMembershipEmail < ActiveRecord::Migration[7.0]
  def change
    add_index :folder_memberships, [:email, :folder_id], unique: true
  end
end
