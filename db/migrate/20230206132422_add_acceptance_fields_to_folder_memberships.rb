class AddAcceptanceFieldsToFolderMemberships < ActiveRecord::Migration[7.0]
  def change
    add_column :folder_memberships, :accepted_at, :datetime
    add_column :folder_memberships, :accept_secret, :string
  end
end
