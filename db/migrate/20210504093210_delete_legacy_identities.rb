class DeleteLegacyIdentities < ActiveRecord::Migration[6.1]
  def up
    Identity.delete_all
  end

  def down
  end
end
