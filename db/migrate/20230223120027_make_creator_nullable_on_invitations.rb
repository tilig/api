class MakeCreatorNullableOnInvitations < ActiveRecord::Migration[7.0]
  def change
    change_column_null :invitations, :creator_id, true
  end
end
