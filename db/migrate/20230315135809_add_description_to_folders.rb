class AddDescriptionToFolders < ActiveRecord::Migration[7.0]
  def change
    add_column :folders, :description, :text
  end
end
