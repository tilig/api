class RenameIdentityFields < ActiveRecord::Migration[6.1]
  def change
    rename_column :identities, :accessToken, :access_token
    rename_column :identities, :refreshToken, :refresh_token
    remove_column :identities, :UserId, :bigint
    add_reference :identities, :user, type: :uuid, foreign_key: true
  end
end
