class AddFirebaseFieldsToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :display_name, :string
    add_column :users, :locale, :string
    add_column :users, :picture, :string
    add_column :users, :providerID, :string
  end
end
