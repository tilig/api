class RenameEncryptedDekToKeyOnShare < ActiveRecord::Migration[7.0]
  def change
    rename_column :shares, :encrypted_dek, :encrypted_key
  end
end
