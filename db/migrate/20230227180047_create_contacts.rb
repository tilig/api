class CreateContacts < ActiveRecord::Migration[7.0]
  def change
    create_table :contacts, id: :uuid do |t|
      t.references :user, foreign_key: true, type: :uuid, null: false
      t.references :contact, type: :uuid, null: false, foreign_key: {to_table: :users}

      t.timestamps

      t.index([:user_id, :contact_id], unique: true)
    end
  end
end
