class RenameNamespacedModels < ActiveRecord::Migration[6.1]
  def change
    rename_table :v3_users, :users
    rename_table :v3_secrets, :secrets
    rename_table :v3_shares, :shares
    rename_table :v3_jwt_jtis, :jwt_jtis
  end
end
