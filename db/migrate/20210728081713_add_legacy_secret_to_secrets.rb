class AddLegacySecretToSecrets < ActiveRecord::Migration[6.1]
  def change
    add_reference :secrets, :legacy_secret, type: :uuid
  end
end
