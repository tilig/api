class MakeOldTimestampsNullable < ActiveRecord::Migration[6.0]
  def change
    tables = %i[favicons identities secrets shares users]

    tables.each do |table|
      change_column_null table, "createdAt", true
      change_column_null table, "updatedAt", true
    end
  end
end
