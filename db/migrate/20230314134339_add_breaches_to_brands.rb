class AddBreachesToBrands < ActiveRecord::Migration[7.0]
  def change
    add_column :brands, :hibp_json, :jsonb, default: []
    add_column :brands, :breached_at, :datetime
    add_column :brands, :breach_published_at, :datetime
  end
end
