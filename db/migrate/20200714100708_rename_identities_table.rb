class RenameIdentitiesTable < ActiveRecord::Migration[6.0]
  def change
    rename_table "Identities", :identities
  end
end
