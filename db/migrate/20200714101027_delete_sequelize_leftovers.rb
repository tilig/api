class DeleteSequelizeLeftovers < ActiveRecord::Migration[6.0]
  def change
    drop_table "SequelizeMeta"
    drop_table "CreditCards"
  end
end
