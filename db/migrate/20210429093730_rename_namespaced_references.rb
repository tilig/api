class RenameNamespacedReferences < ActiveRecord::Migration[6.1]
  def change
    rename_column :shares, :v3_secret_id, :secret_id
    rename_column :shares, :v3_user_id, :user_id
    rename_column :secrets, :v3_user_id, :user_id
  end
end
