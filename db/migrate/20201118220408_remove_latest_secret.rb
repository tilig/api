class RemoveLatestSecret < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :latest_secret_id
  end
end
