class CreateJwtJtis < ActiveRecord::Migration[6.1]
  def change
    create_table :v3_jwt_jtis, id: :uuid do |t|
      t.string :jti

      t.timestamps
    end

    add_index :v3_jwt_jtis, :jti
  end
end
