class RenameSharesTable < ActiveRecord::Migration[6.0]
  def change
    rename_table "Shares", :shares
  end
end
