class CreateV3Secrets < ActiveRecord::Migration[6.0]
  def change
    create_table :v3_secrets, id: :uuid do |t|
      t.string :name
      t.string :website
      t.string :username
      t.string :email
      t.string :notes
      t.string :secret
      t.string :otp

      t.timestamps
    end
  end
end
