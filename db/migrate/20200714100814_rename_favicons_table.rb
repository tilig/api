class RenameFaviconsTable < ActiveRecord::Migration[6.0]
  def change
    rename_table "Favicons", :favicons
  end
end
