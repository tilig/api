class AddSingleItemBooleanToFolders < ActiveRecord::Migration[7.0]
  def change
    add_column :folders, :single_item, :boolean, default: true
  end
end
