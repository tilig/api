class MigrateFromPasswordApiSecretToJsonb < ActiveRecord::Migration[6.0]
  def up
    Secret.includes(:password_api_secret).find_in_batches.with_index do |secret_group, batch|
      say_with_time "Processing batch #{batch}" do
        secret_group.each do |secret|
          password = secret.password_api_secret

          if password
            secret.update!(fields: password.attributes.except("id", "uuid", "created_at", "updated_at"))
          else
            say "Secret without a password: #{secret.id}"
          end
        end
      end
    end
  end
end
