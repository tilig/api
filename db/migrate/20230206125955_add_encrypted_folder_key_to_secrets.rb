class AddEncryptedFolderKeyToSecrets < ActiveRecord::Migration[7.0]
  def change
    add_column :secrets, :encrypted_folder_dek, :text
  end
end
