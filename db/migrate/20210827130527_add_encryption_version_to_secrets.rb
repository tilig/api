class AddEncryptionVersionToSecrets < ActiveRecord::Migration[6.1]
  def change
    add_column :secrets, :encryption_version, :integer, default: 1
  end
end
