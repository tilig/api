class AddDefaultMigratedValueToLegacySecrets < ActiveRecord::Migration[6.1]
  def change
    change_column :legacy_secrets, :migrated, :boolean, default: false
  end
end
