class AddAndroidAppIdToSecret < ActiveRecord::Migration[6.0]
  def change
    add_column :secrets, :android_app_id, :string, null: true
  end
end
