class AddLegacyEncryptionDisabledToSecrets < ActiveRecord::Migration[7.0]
  def change
    add_column :secrets, :legacy_encryption_disabled, :boolean
  end
end
