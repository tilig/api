class AddLogoIconSourceToBrands < ActiveRecord::Migration[6.1]
  def change
    change_table(:brands) do |t|
      t.string :brands, :logo_icon_source
    end
  end
end
