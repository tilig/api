class AddRailsTypeTimestampFields < ActiveRecord::Migration[6.0]
  def change
    tables = %i[CreditCards Favicons Identities Secrets Shares Users]

    tables.each do |table|
      migrate_timestamps(table)
    end
  end

  private

  def migrate_timestamps(table_name)
    say "Migrating #{table_name}"

    add_timestamps table_name, null: true

    migrate_timestamp_column table_name, :created_at, :createdAt
    migrate_timestamp_column table_name, :updated_at, :updatedAt

    change_column_null table_name, :created_at, false
    change_column_null table_name, :updated_at, false
  end

  def migrate_timestamp_column(table_name, old_column_name, new_column_name)
    reversible do |dir|
      dir.up { execute "UPDATE #{connection.quote_table_name(table_name)} SET #{connection.quote_column_name(old_column_name)} = #{connection.quote_column_name(new_column_name)}" }
    end
  end
end
