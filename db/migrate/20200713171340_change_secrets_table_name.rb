class ChangeSecretsTableName < ActiveRecord::Migration[6.0]
  def change
    rename_table "Secrets", :secrets
  end
end
