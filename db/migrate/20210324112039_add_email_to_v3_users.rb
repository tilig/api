class AddEmailToV3Users < ActiveRecord::Migration[6.0]
  def change
    add_column :v3_users, :email, :string
  end
end
