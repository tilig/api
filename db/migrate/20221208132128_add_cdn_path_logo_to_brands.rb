class AddCdnPathLogoToBrands < ActiveRecord::Migration[7.0]
  def change
    add_column :brands, :images, :jsonb, null: false, default: {}
  end
end
