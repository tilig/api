class CreatePasswordApiSecrets < ActiveRecord::Migration[6.0]
  def change
    create_table :password_api_secrets, id: :serial, force: :cascade do |t|
      t.string "secret", limit: 1000
      t.string "login", limit: 1000
      t.string "username", limit: 1000
      t.string "email", limit: 1000
      t.string "website", limit: 5000
      t.string "notes", limit: 1000
      t.string "uuid", limit: 255
      t.integer "userId"
      t.string "otp", limit: 255
      t.timestamps
    end

    PasswordApi::Secret.all.each do |s|
      PasswordApiSecret.create!(
        id: s.id,
        userId: s.userId,
        secret: s.secret,
        login: s.login,
        username: s.username,
        email: s.email,
        website: s.website,
        notes: s.notes,
        uuid: s.uuid,

        otp: s.otp,
        created_at: s.createdAt,
        updated_at: s.updatedAt
      )
    end
  end
end
