class ChangeNullForCreator < ActiveRecord::Migration[7.0]
  def change
    change_column_null :organizations, :creator_id, true
  end
end
