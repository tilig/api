class AddAndroidAppIdToSecrets < ActiveRecord::Migration[6.1]
  def change
    add_column :secrets, :android_app_id, :string
  end
end
