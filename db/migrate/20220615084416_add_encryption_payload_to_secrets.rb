class AddEncryptionPayloadToSecrets < ActiveRecord::Migration[7.0]
  def change
    add_column :secrets, :encrypted_dek, :text
    add_column :secrets, :encrypted_payload, :text
    add_column :secrets, :encrypted_overview, :text
    add_column :secrets, :encrypted_details, :text
  end
end
