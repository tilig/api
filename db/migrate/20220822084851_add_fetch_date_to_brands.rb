class AddFetchDateToBrands < ActiveRecord::Migration[7.0]
  def up
    add_column :brands, :fetch_date, :datetime
    Brand.where.not(brandfetch_json: nil).update_all(fetch_date: DateTime.now)
  end

  def down
    remove_column :brands, :fetch_date, :datetime
  end
end
