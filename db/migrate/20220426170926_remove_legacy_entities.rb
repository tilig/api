class RemoveLegacyEntities < ActiveRecord::Migration[6.1]
  def change
    drop_table :legacy_shares
    drop_table :legacy_secrets
    drop_table :legacy_users
    remove_column :secrets, :legacy_secret_id
  end
end
