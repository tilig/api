class AddReferralCountToProfiles < ActiveRecord::Migration[7.0]
  def change
    add_column :profiles, :referral_count, :integer, default: 0, null: false
  end
end
