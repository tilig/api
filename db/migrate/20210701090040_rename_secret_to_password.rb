class RenameSecretToPassword < ActiveRecord::Migration[6.1]
  def change
    rename_column :secrets, :secret, :password
  end
end
