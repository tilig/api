class AddEmailToMembership < ActiveRecord::Migration[7.0]
  def change
    add_column :folder_memberships, :email, :string, null: true
    change_column_null :folder_memberships, :encrypted_private_key, true
    change_column_null :folder_memberships, :user_id, true
  end
end
