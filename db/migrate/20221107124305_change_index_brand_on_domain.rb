class ChangeIndexBrandOnDomain < ActiveRecord::Migration[7.0]
  def change
    reversible do |dir|
      dir.up do
        change_column :brands, :public_suffix_domain, :string
        change_column :brands, :domain, :string
      end
      dir.down do
        change_column :brands, :public_suffix_domain, :citext
        change_column :brands, :domain, :citext
      end
    end

    add_index :brands, :domain, name: "index_brands_on_domain_gin", using: :gin, opclass: :gin_trgm_ops
  end
end
