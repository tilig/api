class CreateMemberships < ActiveRecord::Migration[7.0]
  def change
    create_table :memberships, id: :uuid do |t|
      t.belongs_to :user, foreign_key: true, type: :uuid, null: false
      t.belongs_to :group, foreign_key: true, type: :uuid, null: false
      t.string :role, null: false, default: "admin"

      t.timestamps

      t.index([:user_id, :group_id], unique: true)
    end
  end
end
