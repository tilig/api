class AddBrandToSecrets < ActiveRecord::Migration[6.1]
  def change
    add_column :secrets, :brand_id, :uuid, null: true
  end
end
