class ChangeIdToUuidInSecrets < ActiveRecord::Migration[6.0]
  def up
    rename_column :secrets, :id, :old_id
    rename_column :shares, :SecretId, :OldSecretId

    change_column :secrets, :uuid, "uuid USING CAST(uuid AS uuid)"
    rename_column :secrets, :uuid, :id

    add_reference :shares, :secret, type: :uuid

    remove_foreign_key :shares, :secrets, column: "OldSecretId"
    execute "ALTER TABLE secrets DROP CONSTRAINT secrets_pkey;"
    execute %{ ALTER TABLE secrets ADD PRIMARY KEY (id); }
    change_column :secrets, :id, :uuid, default: "gen_random_uuid()"
    change_column :secrets, :old_id, :integer, default: nil, null: true
    change_column_null :shares, :OldSecretId, true
    remove_column :users, :latest_secret_id
    add_reference :users, :latest_secret, type: :uuid
    Share.all.each do |share|
      share.secret = Secret.find_by(old_id: share.OldSecretId)
      share.save(validate: false)
    end
  end

  def down
    remove_column :users, :latest_secret_id
    add_reference :users, :latest_secret
    change_column :secrets, :id, :string, limit: 255
    rename_column :secrets, :id, :uuid
    remove_reference :shares, :secret
    rename_column :secrets, :old_id, :id
    rename_column :shares, :OldSecretId, :SecretId
    execute "ALTER TABLE secrets DROP CONSTRAINT secrets_pkey;"
    execute "ALTER TABLE secrets ADD PRIMARY KEY (id);"
    add_foreign_key :shares, :secrets, column: "SecretId", name: "Shares_SecretId_fkey", on_update: :cascade, on_delete: :cascade
  end
end
