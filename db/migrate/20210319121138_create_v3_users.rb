class CreateV3Users < ActiveRecord::Migration[6.0]
  def change
    create_table :v3_users, id: :uuid do |t|
      t.string :given_name
      t.string :family_name
      t.string :public_key

      t.timestamps
    end
  end
end
