class AddJsonbToSecrets < ActiveRecord::Migration[6.0]
  def change
    add_column :secrets, :fields, :jsonb, null: false, default: {}
  end
end
