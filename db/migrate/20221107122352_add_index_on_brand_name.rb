class AddIndexOnBrandName < ActiveRecord::Migration[7.0]
  def change
    enable_extension :pg_trgm
    add_index :brands, :name, using: :gin, opclass: :gin_trgm_ops
  end
end
