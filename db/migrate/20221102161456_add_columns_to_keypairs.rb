class AddColumnsToKeypairs < ActiveRecord::Migration[7.0]
  def change
    add_column :keypairs, :key_type, :string
    add_column :keypairs, :meta_data, :jsonb, null: false, default: {}
    add_column :keypairs, :kid, :string
  end
end
