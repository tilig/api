class RemoveOldTimestamps < ActiveRecord::Migration[6.0]
  def change
    tables = %i[favicons identities secrets shares users]

    tables.each do |table|
      remove_columns table, "createdAt", "updatedAt"
    end
  end
end
