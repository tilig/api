class AddGroupIdToSecrets < ActiveRecord::Migration[7.0]
  def change
    add_reference :secrets, :group, type: :uuid, foreign_key: true
    change_column_null :secrets, :user_id, true

    add_check_constraint :secrets, 'num_nulls("secrets"."group_id", "secrets"."user_id") = 1'
  end
end
