class AddBrandfetchAttributesToBrand < ActiveRecord::Migration[6.1]
  def change
    add_column :brands, :logo_source, :string
    add_column :brands, :main_color_hex, :string
    add_column :brands, :main_color_brightness, :string
  end
end
