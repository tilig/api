class RenameAcceptSecretOnFolderMemberships < ActiveRecord::Migration[7.0]
  def change
    rename_column :folder_memberships, :accept_secret, :acceptance_token
  end
end
