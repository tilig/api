class DecryptedLoginField < ActiveRecord::Migration[6.0]
  def change
    add_column :secrets, :login, :string

    reversible do |dir|
      dir.up do
        User.find_each do |user|
          say_with_time "Processing User#{user.id}" do
            decrypted_secrets = Hash[*encryptor.decrypt_secrets(user.secrets, 1).flatten]

            user.secrets.find_each do |secret|
              secret.update!(login: decrypted_secrets[secret.id]["login"].delete("\u0000"))
            end
          end
        end
      end
    end
  end

  private

  def encryptor
    @encryptor ||= Encryptor.new
  end
end
