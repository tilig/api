class AddEncryptedKeyToSecrets < ActiveRecord::Migration[7.0]
  def change
    add_column :secrets, :encrypted_key, :string
  end
end
