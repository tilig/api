class AddDefaultTotpValueToBrands < ActiveRecord::Migration[7.0]
  def change
    change_column_default :brands, :totp, from: nil, to: false
  end
end
