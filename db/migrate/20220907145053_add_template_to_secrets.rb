class AddTemplateToSecrets < ActiveRecord::Migration[7.0]
  def change
    add_column :secrets, :template, :string
  end
end
