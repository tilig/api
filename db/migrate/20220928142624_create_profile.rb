class CreateProfile < ActiveRecord::Migration[7.0]
  def change
    create_table :profiles, id: :uuid do |t|
      t.references :user, null: false, foreign_key: true, type: :uuid, index: {unique: true}
      t.jsonb :meta, null: false, default: {}
      t.jsonb :application_settings, null: false, default: {}
      t.jsonb :user_settings, null: false, default: {}

      t.timestamps
    end
  end
end
