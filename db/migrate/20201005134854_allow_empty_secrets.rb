class AllowEmptySecrets < ActiveRecord::Migration[6.0]
  def up
    change_column :secrets, :FaviconId, :integer, null: true
    change_column :secrets, :website, :string, null: true
    remove_foreign_key :secrets, :favicons
    add_foreign_key :secrets, :favicons, column: :FaviconId, validate: false
  end

  def down
    change_column :secrets, :FaviconId, :integer, null: false
    change_column :secrets, :website, :string, null: false
    remove_foreign_key :secrets, :favicons
    add_foreign_key :secrets, :favicons, column: :FaviconId, validate: true
  end
end
