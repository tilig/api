class AddCreatorToFolders < ActiveRecord::Migration[7.0]
  def change
    add_reference :folders, :creator, null: true, foreign_key: {to_table: :users}, type: :uuid
  end
end
