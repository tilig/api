class RenameProfileIdField < ActiveRecord::Migration[6.1]
  def change
    rename_column :users, :providerID, :provider_id
  end
end
