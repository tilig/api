class AddCreateAndUpdatePlatformToItems < ActiveRecord::Migration[7.0]
  def change
    add_column :items, :create_platform, :string
    add_column :items, :update_platform, :string
  end
end
