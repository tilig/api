class NormalizeOauthIds < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :uid, :string

    reversible do |dir|
      dir.up do
        change_column :users, :provider, :string, default: nil

        User.all.each do |u|
          say_with_time "Updating User##{u.id}" do
            if u.appleID
              u.update(uid: u.appleID, provider: :apple)
            elsif u.googleID
              u.update(uid: u.googleID, provider: :google)
            else
              raise "No appleID nor googleID for User with ID #{u.id}"
            end
          end
        end
      end

      dir.down do
        User.all.each do |u|
          say_with_time "Updating User##{u.id}" do
            case u.provider
            when "apple"
              u.update(appleID: u.uid)
            when "google"
              u.update(googleID: u.uid)
            else
              raise "No appleID nor googleID for User with ID #{u.id}"
            end
          end
        end

        change_column :users, :provider, "integer using 0"
      end
    end
  end
end
