class AddCreatorToFolderMembership < ActiveRecord::Migration[7.0]
  def change
    add_reference :folder_memberships, :creator, index: true, foreign_key: {to_table: :users}, type: :uuid
  end
end
