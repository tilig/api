class RenameModelsToNewDataModel < ActiveRecord::Migration[7.0]
  def change
    rename_table :secrets, :items
    rename_column :items, :encrypted_dek, :rsa_encrypted_dek
    rename_column :items, :encrypted_key, :encrypted_dek
    rename_table :shares, :share_links
    rename_column :share_links, :secret_id, :item_id
    rename_column :share_links, :encrypted_key, :encrypted_dek
    rename_column :password_versions, :secret_id, :item_id
  end
end
