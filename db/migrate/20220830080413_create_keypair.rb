class CreateKeypair < ActiveRecord::Migration[7.0]
  def change
    create_table :keypairs, id: :uuid do |t|
      t.references(:user, type: :uuid, foreign_key: true)
      t.text :encrypted_private_key
      t.text :public_key

      t.timestamps
    end
  end
end
