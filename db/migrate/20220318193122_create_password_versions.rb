class CreatePasswordVersions < ActiveRecord::Migration[6.1]
  def change
    create_table :password_versions, id: :uuid do |t|
      t.string :password, null: false
      t.references(:secret, type: :uuid, index: true, null: false)

      t.timestamps
    end
  end
end
