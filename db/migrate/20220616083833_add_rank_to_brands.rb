class AddRankToBrands < ActiveRecord::Migration[7.0]
  def change
    add_column :brands, :rank, :integer

    add_index :brands, :rank
  end
end
