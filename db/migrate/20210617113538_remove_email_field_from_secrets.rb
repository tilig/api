class RemoveEmailFieldFromSecrets < ActiveRecord::Migration[6.1]
  def change
    remove_column :secrets, :email, :string
  end
end
