class AddKeysToGroups < ActiveRecord::Migration[7.0]
  def change
    add_column :memberships, :encrypted_private_key, :text, null: false
    add_column :groups, :public_key, :text, null: false
  end
end
