class AddPublicSuffixDomainToBrands < ActiveRecord::Migration[6.1]
  def change
    add_column :brands, :public_suffix_domain, :citext, null: true
  end
end
