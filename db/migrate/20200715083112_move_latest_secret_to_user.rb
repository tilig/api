class MoveLatestSecretToUser < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :latest_secret
    remove_column :secrets, :CurrentSecretId, :integer
  end
end
