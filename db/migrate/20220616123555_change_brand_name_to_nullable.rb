class ChangeBrandNameToNullable < ActiveRecord::Migration[7.0]
  def change
    change_column_null(:brands, :name, true)
  end
end
