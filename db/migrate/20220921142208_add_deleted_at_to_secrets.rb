class AddDeletedAtToSecrets < ActiveRecord::Migration[7.0]
  def change
    add_column :secrets, :deleted_at, :datetime
  end
end
