class AddV3UserToV3Secrets < ActiveRecord::Migration[6.0]
  def change
    add_reference :v3_secrets, :v3_user, null: false, foreign_key: true, type: :uuid
  end
end
