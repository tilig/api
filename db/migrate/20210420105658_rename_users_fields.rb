class RenameUsersFields < ActiveRecord::Migration[6.1]
  def change
    rename_table :users, :legacy_users
    rename_table :secrets, :legacy_secrets
    rename_table :shares, :legacy_shares
  end
end
