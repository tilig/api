class CreateInvitation < ActiveRecord::Migration[7.0]
  def change
    create_table :invitations, id: :uuid do |t|
      t.string :email, null: false
      t.references :creator, type: :uuid, null: false, foreign_key: {to_table: :users}
      t.references :organization, type: :uuid, null: false, foreign_key: true
      t.datetime :accepted_at
      t.datetime :expire_at
      t.references :invitee, type: :uuid, null: true, foreign_key: {to_table: :users}
      t.string :view_token, null: false

      t.timestamps

      t.index([:invitee_id, :organization_id], unique: true)
      t.index([:email, :organization_id], unique: true)
    end
  end
end
