class ChangePublicSuffixDomainOfBrandToNotNull < ActiveRecord::Migration[7.0]
  def change
    execute("DELETE FROM brands WHERE public_suffix_domain IS NULL")
    change_column_null(:brands, :public_suffix_domain, false)
  end
end
