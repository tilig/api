class CreateBrands < ActiveRecord::Migration[6.1]
  def change
    enable_extension "citext"
    create_table :brands, id: :uuid do |t|
      t.string :name, null: false
      t.citext :domain, null: false
      t.boolean :totp, null: false
      t.json :json
      t.timestamps
    end

    add_index :brands, :domain, unique: true
  end
end
