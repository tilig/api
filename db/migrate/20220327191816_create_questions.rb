class CreateQuestions < ActiveRecord::Migration[6.1]
  def change
    create_table :questions, id: :uuid do |t|
      t.string :survey_token, null: false
      t.text :content, null: false
      t.string :token, null: false
      t.string :answer_options, null: false, array: true, default: []
      t.timestamps
    end

    add_index(:questions, :token, unique: true)
  end
end
