class AddLongNoteToLegacySecrets < ActiveRecord::Migration[6.1]
  def change
    add_column :legacy_secrets, :long_note, :boolean, default: false
  end
end
