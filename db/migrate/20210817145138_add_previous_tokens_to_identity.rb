class AddPreviousTokensToIdentity < ActiveRecord::Migration[6.1]
  def change
    add_column :identities, :previous_access_token, :string
    add_column :identities, :previous_refresh_token, :string
  end
end
