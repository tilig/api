class AddBrandfetchJsonToBrands < ActiveRecord::Migration[6.1]
  def change
    add_column :brands, :brandfetch_json, :json, default: nil
  end
end
