# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_03_15_135809) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "citext"
  enable_extension "pg_trgm"
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "answers", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.boolean "skipped", default: false, null: false
    t.string "chosen_options", default: [], null: false, array: true
    t.string "x_tilig_platform"
    t.string "x_tilig_version"
    t.string "x_browser_name"
    t.string "x_browser_version"
    t.string "x_os_name"
    t.uuid "user_id", null: false
    t.uuid "question_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_answers_on_question_id"
    t.index ["user_id"], name: "index_answers_on_user_id"
  end

  create_table "brands", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "domain", null: false
    t.boolean "totp", default: false, null: false
    t.json "json"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "public_suffix_domain", null: false
    t.json "brandfetch_json"
    t.string "logo_source"
    t.string "main_color_hex"
    t.string "main_color_brightness"
    t.string "logo_icon_source"
    t.integer "rank"
    t.datetime "fetch_date"
    t.uuid "parent_id"
    t.string "parent_source"
    t.jsonb "images", default: {}, null: false
    t.jsonb "hibp_json", default: []
    t.datetime "breached_at"
    t.datetime "breach_published_at"
    t.index ["domain"], name: "index_brands_on_domain", unique: true
    t.index ["domain"], name: "index_brands_on_domain_gin", opclass: :gin_trgm_ops, using: :gin
    t.index ["name"], name: "index_brands_on_name", opclass: :gin_trgm_ops, using: :gin
    t.index ["parent_id"], name: "index_brands_on_parent_id"
    t.index ["rank"], name: "index_brands_on_rank"
  end

  create_table "contacts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.uuid "contact_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_contacts_on_contact_id"
    t.index ["user_id", "contact_id"], name: "index_contacts_on_user_id_and_contact_id", unique: true
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "folder_memberships", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "folder_id", null: false
    t.string "role", default: "admin", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "encrypted_private_key"
    t.datetime "accepted_at"
    t.string "acceptance_token"
    t.string "email"
    t.uuid "creator_id"
    t.index ["creator_id"], name: "index_folder_memberships_on_creator_id"
    t.index ["email", "folder_id"], name: "index_folder_memberships_on_email_and_folder_id", unique: true
    t.index ["folder_id"], name: "index_folder_memberships_on_folder_id"
    t.index ["user_id", "folder_id"], name: "index_folder_memberships_on_user_id_and_folder_id", unique: true
    t.index ["user_id"], name: "index_folder_memberships_on_user_id"
  end

  create_table "folders", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.text "public_key", null: false
    t.uuid "creator_id"
    t.boolean "single_item", default: true
    t.text "description"
    t.index ["creator_id"], name: "index_folders_on_creator_id"
  end

  create_table "identities", id: :serial, force: :cascade do |t|
    t.string "access_token", limit: 512
    t.string "refresh_token", limit: 512
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "user_id"
    t.string "previous_access_token"
    t.string "previous_refresh_token"
    t.index ["user_id"], name: "index_identities_on_user_id"
  end

  create_table "invitations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", null: false
    t.uuid "creator_id"
    t.uuid "organization_id", null: false
    t.datetime "accepted_at"
    t.datetime "expire_at"
    t.uuid "invitee_id"
    t.string "view_token", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["creator_id"], name: "index_invitations_on_creator_id"
    t.index ["email", "organization_id"], name: "index_invitations_on_email_and_organization_id", unique: true
    t.index ["invitee_id", "organization_id"], name: "index_invitations_on_invitee_id_and_organization_id", unique: true
    t.index ["invitee_id"], name: "index_invitations_on_invitee_id"
    t.index ["organization_id"], name: "index_invitations_on_organization_id"
  end

  create_table "items", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "website"
    t.string "username"
    t.string "notes"
    t.string "password"
    t.string "otp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "user_id"
    t.string "android_app_id"
    t.integer "encryption_version", default: 1
    t.uuid "brand_id"
    t.string "public_suffix_domain"
    t.text "rsa_encrypted_dek"
    t.text "encrypted_overview"
    t.text "encrypted_details"
    t.string "template"
    t.datetime "deleted_at"
    t.string "encrypted_dek"
    t.uuid "folder_id"
    t.boolean "legacy_encryption_disabled"
    t.text "encrypted_folder_dek"
    t.string "create_platform"
    t.string "update_platform"
    t.index ["folder_id"], name: "index_items_on_folder_id"
    t.index ["public_suffix_domain"], name: "index_items_on_public_suffix_domain"
    t.index ["user_id"], name: "index_items_on_user_id"
    t.check_constraint "num_nulls(folder_id, user_id) = 1"
  end

  create_table "jwt_jtis", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "jti"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["jti"], name: "index_jwt_jtis_on_jti"
  end

  create_table "keypairs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.text "encrypted_private_key"
    t.text "public_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "key_type"
    t.jsonb "meta_data", default: {}, null: false
    t.string "kid"
    t.index ["user_id"], name: "index_keypairs_on_user_id"
  end

  create_table "organizations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.uuid "creator_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["creator_id"], name: "index_organizations_on_creator_id"
  end

  create_table "password_versions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "password", null: false
    t.uuid "item_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_password_versions_on_item_id"
  end

  create_table "profiles", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.jsonb "meta", default: {}, null: false
    t.jsonb "application_settings", default: {}, null: false
    t.jsonb "user_settings", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "referral_count", default: 0, null: false
    t.index ["user_id"], name: "index_profiles_on_user_id", unique: true
  end

  create_table "questions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "survey_token", null: false
    t.text "content", null: false
    t.string "token", null: false
    t.string "answer_options", default: [], null: false, array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["token"], name: "index_questions_on_token", unique: true
  end

  create_table "share_links", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "item_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "encrypted_dek"
    t.text "encrypted_master_key"
    t.string "uuid"
    t.string "access_token_digest"
    t.datetime "expired_at"
    t.datetime "disabled_at"
    t.integer "times_viewed", default: 0
    t.index ["item_id"], name: "index_share_links_on_item_id"
    t.index ["uuid"], name: "index_share_links_on_uuid", unique: true
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "public_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.string "display_name"
    t.string "given_name"
    t.string "family_name"
    t.string "locale", default: "en"
    t.string "picture"
    t.string "provider_id"
    t.string "firebase_uid"
    t.uuid "organization_id"
    t.index ["organization_id"], name: "index_users_on_organization_id"
  end

  add_foreign_key "answers", "questions"
  add_foreign_key "contacts", "users"
  add_foreign_key "contacts", "users", column: "contact_id"
  add_foreign_key "folder_memberships", "folders"
  add_foreign_key "folder_memberships", "users"
  add_foreign_key "folder_memberships", "users", column: "creator_id"
  add_foreign_key "folders", "users", column: "creator_id"
  add_foreign_key "identities", "users"
  add_foreign_key "invitations", "organizations"
  add_foreign_key "invitations", "users", column: "creator_id"
  add_foreign_key "invitations", "users", column: "invitee_id"
  add_foreign_key "items", "brands"
  add_foreign_key "items", "folders"
  add_foreign_key "items", "users"
  add_foreign_key "keypairs", "users"
  add_foreign_key "organizations", "users", column: "creator_id"
  add_foreign_key "profiles", "users"
  add_foreign_key "share_links", "items"
  add_foreign_key "users", "organizations"
end
