require "rails_helper"

RSpec.describe Invitation, type: :model do
  context "when a creator is destroyed" do
    let(:creator) { create :user }
    let!(:accepted_invitation) { create :invitation, :accepted, creator: }
    let!(:pending_invitation) { create :invitation, creator: }

    it "removes pending invitations" do
      creator.destroy!
      expect(Invitation.find_by(id: pending_invitation)).to be_nil
      expect(Invitation.find_by(id: accepted_invitation)).not_to be_nil
    end
  end
end

# == Schema Information
#
# Table name: invitations
#
#  id              :uuid             not null, primary key
#  accepted_at     :datetime
#  email           :string           not null
#  expire_at       :datetime
#  view_token      :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  creator_id      :uuid
#  invitee_id      :uuid
#  organization_id :uuid             not null
#
# Indexes
#
#  index_invitations_on_creator_id                      (creator_id)
#  index_invitations_on_email_and_organization_id       (email,organization_id) UNIQUE
#  index_invitations_on_invitee_id                      (invitee_id)
#  index_invitations_on_invitee_id_and_organization_id  (invitee_id,organization_id) UNIQUE
#  index_invitations_on_organization_id                 (organization_id)
#
# Foreign Keys
#
#  fk_rails_...  (creator_id => users.id)
#  fk_rails_...  (invitee_id => users.id)
#  fk_rails_...  (organization_id => organizations.id)
#
