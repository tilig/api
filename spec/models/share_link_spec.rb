require "rails_helper"

describe ShareLink, type: :model, v3: true do
  describe "belongs_to Secret" do
    it "updates a secret each time a share is added" do
      item = create(:item)
      expect { create(:share_link, item: item) }.to change(item, :updated_at)
    end

    it "updates a secret when the related share is destroyed" do
      share_link = create(:share_link)
      item = share_link.item
      expect { share_link.destroy }.to change(item, :updated_at)
    end
  end
end
# rubocop:enable Lint/EmptyBlock

# == Schema Information
#
# Table name: share_links
#
#  id                   :uuid             not null, primary key
#  access_token_digest  :string
#  disabled_at          :datetime
#  encrypted_dek        :text
#  encrypted_master_key :text
#  expired_at           :datetime
#  times_viewed         :integer          default(0)
#  uuid                 :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  item_id              :uuid             not null
#
# Indexes
#
#  index_share_links_on_item_id  (item_id)
#  index_share_links_on_uuid     (uuid) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (item_id => items.id)
#
