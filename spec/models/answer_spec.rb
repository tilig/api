require "rails_helper"
describe "Answer" do
  let(:user) { create(:user) }
  let(:attributes) {
    {
      question_attributes: {
        content: "On what devices do you want to have your password and account information available?",
        survey_token: "e2120eb0-428b-471d-ba4a-9b109141acb6",
        token: "b291972e-caa6-4f8c-ad80-5ef91b5775b2",
        answer_options: ["Option A", "Option b", "Option C"]
      },
      chosen_options: ["Option A", "Option C"]
    }.merge(user: user)
  }

  describe "create " do
    subject { Answer.create(attributes) }

    describe "with question attributes" do
      it "adds the question if it does not yet exist" do
        expect { subject }.to change(Question, :count).by(1)
        expect(Question).to exist
      end

      it "uses the existing question if it already exists" do
        create(:question, token: "b291972e-caa6-4f8c-ad80-5ef91b5775b2")

        expect { subject }.to_not change(Question, :count)
        expect(Answer).to exist
      end

      it "does not need question_attributes if the question already exists" do
        question = create(:question)

        attributes.delete "question_attributes"
        attributes["question_id"] = question.id

        expect { subject }.to change(Answer, :count).by(1)
      end
    end
  end

  describe "deleting a user" do
    subject { Answer.create(attributes) }

    it "keeps the user_id on the answer" do
      subject

      expect { user.destroy }.to_not change(subject, :user_id)
    end
  end
end

# == Schema Information
#
# Table name: answers
#
#  id                :uuid             not null, primary key
#  chosen_options    :string           default([]), not null, is an Array
#  skipped           :boolean          default(FALSE), not null
#  x_browser_name    :string
#  x_browser_version :string
#  x_os_name         :string
#  x_tilig_platform  :string
#  x_tilig_version   :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  question_id       :uuid             not null
#  user_id           :uuid             not null
#
# Indexes
#
#  index_answers_on_question_id  (question_id)
#  index_answers_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (question_id => questions.id)
#
