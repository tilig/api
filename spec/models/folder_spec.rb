require "rails_helper"

describe Folder, type: :model do
  let(:folder) { create(:folder) }
  let(:folder_membership) { create(:folder_membership, folder: folder) }

  it "deletes secrets when the folder is deleted" do
    create(:item, folder: folder, user: nil, encrypted_folder_dek: "1234")
    create(:item)
    expect { folder.destroy }.to change { Item.count }.by(-1)
  end

  it "deletes a folder_membership if the folder is destroyed" do
    folder_membership
    create(:folder_membership)
    expect { folder.destroy }.to change { FolderMembership.count }.by(-1)
  end

  it "can be soft deleted" do
    folder = create(:folder)
    folder.soft_destroy
    expect(folder.reload.deleted_at).not_to be_nil
    expect(Folder.active).to eq []
  end

  describe "admin?" do
    let!(:user) { create(:user) }
    let!(:folder_membership) { create(:folder_membership, folder: folder, user: user) }
    let(:requested_user) { user }

    subject { folder.admin?(requested_user) }

    context "when the user is admin of the folder" do
      it "returns true" do
        expect(subject).to eq true
      end
    end

    context "when the user is member but no admin of the folder" do
      before { folder_membership.update_column(:role, "member") }

      it "returns false" do
        expect(subject).to eq false
      end
    end

    context "when the user is not a member of the folder" do
      let(:requested_user) { create(:user) }

      it "returns false" do
        expect(subject).to eq false
      end
    end

    context "when the user 'nil'" do
      let(:requested_user) { nil }

      it "returns false" do
        expect(subject).to eq false
      end
    end
  end
end

# == Schema Information
#
# Table name: folders
#
#  id          :uuid             not null, primary key
#  deleted_at  :datetime
#  description :text
#  name        :string
#  public_key  :text             not null
#  single_item :boolean          default(TRUE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  creator_id  :uuid
#
# Indexes
#
#  index_folders_on_creator_id  (creator_id)
#
# Foreign Keys
#
#  fk_rails_...  (creator_id => users.id)
#
