require "rails_helper"

describe FolderMembership, type: :model do
  it "sets the membership_type to admin by default" do
    expect(FolderMembership.new.role).to eq("admin")
  end

  it "does not allow undifined roles" do
    expect { FolderMembership.new(role: "Other") }.to raise_error(ArgumentError, "'Other' is not a valid role")
  end

  describe "admin?" do
    let(:folder_membership) { create(:folder_membership) }

    subject { folder_membership.admin? }

    context "when the user is admin of the group" do
      it "returns true" do
        expect(subject).to eq true
      end
    end

    context "when the user is member but no admin of the group" do
      before { folder_membership.update_column(:role, "member") }

      it "returns false" do
        expect(subject).to eq false
      end
    end
  end

  describe "add_user_for_email" do
    let(:folder) { create(:folder) }
    let(:creator) { create(:user) }
    let(:email) { "john@example.com" }
    let(:user) { create(:user, email:) }
    let(:folder_membership_1) { create(:folder_membership, email:, user: nil, folder:, creator:) }
    let(:folder_membership_2) { create(:folder_membership, email:, user: nil) }
    let(:folder_membership_3) { create(:folder_membership, email: "other@tilig.com", user: nil, folder:, creator:) }

    before {
      folder_membership_1
      folder_membership_2
      folder_membership_3
    }

    subject { FolderMembership.add_user_for_email(user) }

    it "adds the user to the memberships with the same email" do
      expect { subject }.to change { FolderMembership.where(user:).count }.by(2)
    end

    it "automatically marks the memberships as accepted" do
      subject
      expect(FolderMembership.where(user:, accepted_at: nil)).to_not exist
    end

    it "make the user and creator contacts" do
      subject
      expect(Contact.where(user:, contact: creator)).to exist
      expect(Contact.where(user: creator, contact: user)).to exist
    end

    it "does not change other pending memberships on the same folder" do
      folder_membership_3.reload
      expect(folder_membership_3.user).to be_nil
      expect(folder_membership_3.accepted_at).to be_nil
    end

    context "when there is a single creator for multiple memberships" do
      let(:creator) { create(:user) }
      let(:folder_membership_1) { create(:folder_membership, email:, user: nil, creator:, folder:) }
      let(:folder_membership_2) { create(:folder_membership, email:, user: nil, creator:) }

      it "only sends the email notification once" do
        template = instance_double(Mailchimp::SignUpNoticeForFolderMembershipCreatorTemplate, build_template: nil)
        expect(Mailchimp::SignUpNoticeForFolderMembershipCreatorTemplate).to receive(:new).once.and_return(template)
        subject
      end
    end

    context "when the creator is deleted" do
      let(:folder_membership_1) { create(:folder_membership, creator: user) }

      it "nullifies the creator_id" do
        user.destroy
        expect(folder_membership_1.reload.creator_id).to be_nil
      end
    end
  end
end

# == Schema Information
#
# Table name: folder_memberships
#
#  id                    :uuid             not null, primary key
#  acceptance_token      :string
#  accepted_at           :datetime
#  email                 :string
#  encrypted_private_key :text
#  role                  :string           default("admin"), not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  creator_id            :uuid
#  folder_id             :uuid             not null
#  user_id               :uuid
#
# Indexes
#
#  index_folder_memberships_on_creator_id             (creator_id)
#  index_folder_memberships_on_email_and_folder_id    (email,folder_id) UNIQUE
#  index_folder_memberships_on_folder_id              (folder_id)
#  index_folder_memberships_on_user_id                (user_id)
#  index_folder_memberships_on_user_id_and_folder_id  (user_id,folder_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (creator_id => users.id)
#  fk_rails_...  (folder_id => folders.id)
#  fk_rails_...  (user_id => users.id)
#
