require "rails_helper"
require "fixtures/brandfetch/fixtures"

describe "Brand" do
  let(:brand) { create(:brand) }
  let(:url) { "https://google.com" }

  it "has a default page size of 50 " do
    expect(Brand::DEFAULT_PAGE_SIZE).to be 15
  end

  describe "#logo_icon_source" do
    subject { brand.logo_icon_source }

    context "when logo_icon_source is present" do
      let(:brand) {
        create :brand, images: {
          icon: {
            png: {
              original: "https://proxy.img.infra.tilig.com/thisisaurl/image.png"
            },
            svg: {
              original: "https://proxy.img.infra.tilig.com/thisisaurl/image.svg"
            }
          }
        }
      }

      it "uses the original logo_icon_source value" do
        expect(subject).to eq "https://asset.brandfetch.io/id6O2oGzv-/idNEgS9h8q.jpeg"
      end
    end

    context "when logo_icon_source is nil and images PNG is present" do
      let(:brand) {
        create :brand, logo_icon_source: nil, images: {
          icon: {
            png: {
              original: "https://proxy.img.infra.tilig.com/thisisaurl/image.png"
            },
            svg: {
              original: "https://proxy.img.infra.tilig.com/thisisaurl/image.svg"
            }
          }
        }
      }

      it "uses the images PNG url as fallback" do
        expect(subject).to eq "https://proxy.img.infra.tilig.com/thisisaurl/image.png"
      end
    end

    context "when logo_icon_source is nil and images PNG is not present but the SVG is present" do
      let(:brand) {
        create :brand, logo_icon_source: nil, images: {
          icon: {
            png: {
              original: nil
            },
            svg: {
              original: "https://proxy.img.infra.tilig.com/thisisaurl/image.svg"
            }
          }
        }
      }

      it "uses the images SVG url as fallback" do
        expect(subject).to eq "https://proxy.img.infra.tilig.com/thisisaurl/image.svg"
      end
    end

    context "when logo_icon_source is nil and images PNG and SVG are both not present" do
      let(:brand) { create :brand, logo_icon_source: nil, images: {} }

      it "wil be nil" do
        expect(subject).to eq nil
      end
    end

    context "with parents" do
      context "when logo_icon_source is present" do
        let(:brand) { create :brand, parent: google_com, logo_icon_source: "https://asset.brandfetch.io/child.jpeg", images: {icon: {png: {original: "https://url-of-the-child.png"}, svg: {original: "https://url-of-the-child.svg"}}} }
        let(:google_com) {
          create :brand, domain: "google.com", public_suffix_domain: "google.com", logo_icon_source: "https://asset.brandfetch.io/parent.jpeg", images: {
            icon: {
              png: {
                original: "https://proxy.img.infra.tilig.com/thisisaurl/image.png"
              },
              svg: {
                original: "https://proxy.img.infra.tilig.com/thisisaurl/image.svg"
              }
            }
          }
        }

        it "uses the original logo_icon_source value (of the parent)" do
          expect(subject).to eq "https://asset.brandfetch.io/parent.jpeg"
        end
      end

      context "when parent logo_icon_source is nil and images PNG is present" do
        let(:brand) { create :brand, parent: google_com, logo_icon_source: "https://asset.brandfetch.io/child.jpeg", images: {icon: {png: {original: "https://url-of-the-child.png"}, svg: {original: "https://url-of-the-child.svg"}}} }
        let(:google_com) {
          create :brand, domain: "google.com", public_suffix_domain: "google.com", logo_icon_source: nil, images: {
            icon: {
              png: {
                original: "https://proxy.img.infra.tilig.com/thisisaurl/image.png"
              },
              svg: {
                original: "https://proxy.img.infra.tilig.com/thisisaurl/image.svg"
              }
            }
          }
        }

        it "uses the images PNG url (of the parent) as fallback" do
          expect(subject).to eq "https://proxy.img.infra.tilig.com/thisisaurl/image.png"
        end
      end

      context "when parent logo_icon_source is nil and parent images PNG is not present but the parent SVG is present" do
        let(:brand) { create :brand, parent: google_com, logo_icon_source: "https://asset.brandfetch.io/child.jpeg", images: {icon: {png: {original: "https://url-of-the-child.png"}, svg: {original: "https://url-of-the-child.svg"}}} }
        let(:google_com) {
          create :brand, domain: "google.com", public_suffix_domain: "google.com", logo_icon_source: nil, images: {
            icon: {
              png: {
                original: nil
              },
              svg: {
                original: "https://proxy.img.infra.tilig.com/thisisaurl/image.svg"
              }
            }
          }
        }

        it "uses the images SVG (of the parent) url as fallback" do
          expect(subject).to eq "https://proxy.img.infra.tilig.com/thisisaurl/image.svg"
        end
      end

      context "when parent logo_icon_source is nil and parent images are not present " do
        let(:brand) { create :brand, parent: google_com, logo_icon_source: "https://asset.brandfetch.io/child.jpeg", images: {icon: {png: {original: "https://url-of-the-child.png"}, svg: {original: "https://url-of-the-child.svg"}}} }
        let(:google_com) { create :brand, domain: "google.com", public_suffix_domain: "google.com", logo_icon_source: nil, images: {} }

        it "it wil default to the child again" do
          expect(subject).to eq "https://asset.brandfetch.io/child.jpeg"
        end
      end

      context "all fallbacks are nil or empty" do
        let(:brand) { create :brand, parent: google_com, logo_icon_source: nil, images: {} }
        let!(:google_com) { create(:brand, domain: "google.com", public_suffix_domain: "google.com", logo_icon_source: nil, images: {}) }

        it "wil be nil" do
          expect(subject).to eq nil
        end
      end
    end
  end

  describe "#find_or_create_by_url" do
    subject { Brand.find_or_create_by_url("https://play.google.com") }

    it "finds an existing brand" do
      brand

      expect(subject.domain).to be_present
      expect(subject).to be_persisted
      expect(FetchBrandJob).to have_been_enqueued.with(subject)
    end

    it "does not relay the call to brandfetch when fetch_date is set" do
      brand.update(fetch_date: 1.day.ago)

      expect(FetchBrandJob).to_not have_been_enqueued.with(subject.id)
    end

    context "when supplying an invalid domain" do
      subject { Brand.find_or_create_by_url("withouttld") }

      it "does not create a brand" do
        expect(subject).to be_nil
      end
    end

    context "in a race condition" do
      it "uses the existing brand" do
        brand

        allow(Brand).to receive(:find_or_create_by!).and_raise(ActiveRecord::RecordNotUnique, "not unique")
        expect(subject).to eq(brand)
      end
    end
  end

  context "#find_normalized_domain" do
    subject { Brand.find_normalized_domain(url) }

    let(:url) { "play.google.com" }
    let(:brand) { create(:brand, domain: "play.google.com", public_suffix_domain: "not-matching.org") }
    let(:other_brand) { create(:brand, domain: "not-matching.org", public_suffix_domain: "google.com") }

    before do
      brand
    end

    it "matches on the domain before the public_suffix_domain" do
      other_brand

      expect(subject.domain).to eq "play.google.com"
    end

    describe "when the url doesn't match the domain" do
      let(:brand) { create(:brand, domain: "not-matching.org", public_suffix_domain: "google.com") }

      it "uses the public_suffix_domain as a fallback" do
        expect(subject.id).to eq brand.id
      end
    end

    describe "when the incomming url contains spaces and upper case" do
      let(:url) { " https://Play.GooGle.com   " }

      it "still matches" do
        expect(subject.id).to eq brand.id
      end
    end
  end

  describe "#with_prefix" do
    let(:prefix) { "face" }

    subject { Brand.with_prefix(prefix, 10) }

    context "searching matching for domain" do
      let(:brand) { create(:brand, domain: "facebook.com", name: "other name", public_suffix_domain: "facebook.com") }

      it "finds the brands by the prefix of the name" do
        expect(subject).to match [brand]
      end
    end

    context "searching matching for name" do
      let(:brand) { create(:brand, domain: "some-name.com", name: "facebook", public_suffix_domain: "some-name.com") }

      it "finds brands by the prefix of the domain" do
        expect(subject).to match [brand]
      end
    end

    context "with two brands of the same public suffix" do
      let(:brand_instagram) { create(:brand, name: "Instagram", domain: "instagram.com", public_suffix_domain: "instagram.com", brandfetch_json: nil) }
      let(:prefix) { "insta" }

      before { brand_instagram }
      before { create(:brand, name: "Instagram", domain: "policies.instagram.com", public_suffix_domain: "instagram.com") }

      it "distincts on the public_suffix_domain" do
        expect(subject).to match [brand_instagram]
      end
    end

    context "searching a brand where rank equels 'nil'" do
      let(:brand) { create(:brand, domain: "some-other-name.com", name: "some other name", public_suffix_domain: "some-other-name.com", rank: nil) }
      let(:prefix) { "some other name" }

      before { brand }

      it "should return an empty array" do
        expect(subject).to match []
      end
    end
  end

  describe "with a secret" do
    let(:secret) { create :item, website: "play.google.com" }

    it "touches related secrets on an update" do
      old_updated_at = secret.updated_at
      brand = secret.brand
      brand.update(name: "old name")
      brand.update(name: "New New name")
      expect(secret.reload.updated_at).to_not eq old_updated_at
    end

    it "nullifies the brand_id on the secret if the brand is destroyed" do
      secret = create(:item, website: "play.google.com")
      brand = secret.brand

      expect { brand.destroy }.to change { secret.reload.brand_id }.to(nil)
    end
  end
end

# == Schema Information
#
# Table name: brands
#
#  id                    :uuid             not null, primary key
#  brandfetch_json       :json
#  breach_published_at   :datetime
#  breached_at           :datetime
#  domain                :string           not null
#  fetch_date            :datetime
#  hibp_json             :jsonb
#  images                :jsonb            not null
#  json                  :json
#  logo_icon_source      :string
#  logo_source           :string
#  main_color_brightness :string
#  main_color_hex        :string
#  name                  :string
#  parent_source         :string
#  public_suffix_domain  :string           not null
#  rank                  :integer
#  totp                  :boolean          default(FALSE), not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  parent_id             :uuid
#
# Indexes
#
#  index_brands_on_domain      (domain) UNIQUE
#  index_brands_on_domain_gin  (domain) USING gin
#  index_brands_on_name        (name) USING gin
#  index_brands_on_parent_id   (parent_id)
#  index_brands_on_rank        (rank)
#
