require "rails_helper"

RSpec.describe User, type: :model do
  describe "destroy" do
    # Create User with Keypair and Organization
    let!(:user) { create(:user, :with_keypair, :with_organization) }
    let!(:user_2) { create(:user, :with_keypair, :with_organization) }

    before {
      # Create identity
      create(:identity, user: user)
      # Create invitation for other user
      create(:invitation, creator: user, organization: user.organization)
      # Create secrets/items
      create(:item, user: user, share_link: create(:share_link))
      create(:item, user: user)
      # Soft destroy some secrets/items
      create(:item, user: user, share_link: create(:share_link)).soft_destroy
      create(:item, user: user).soft_destroy
      create(:item, user: user)
      # Create a folder and folder_membership
      folder = create(:folder)
      create(:folder_membership, :accepted, user: user, folder: folder)
      create(:item, folder: folder, user: nil, encrypted_folder_dek: "leflef")
    }

    it "deletes all dependents nicely" do
      user.destroy!

      expect(User.find_by(id: user.id)).to be nil
      expect(User.find_by(id: user_2.id)).to_not be nil
    end
  end

  describe "display_name" do
    describe "when a first name is present" do
      it "returns the first name" do
        user = create(:user, given_name: "Mario", display_name: "Mario di Italia")
        expect(user.display_name).to eq("Mario")
      end
    end

    describe "when a first name is not present" do
      it "returns the display_name" do
        user = create(:user, given_name: nil, display_name: "Mario di Italia")
        expect(user.display_name).to eq("Mario di Italia")
      end
    end
  end

  describe "after_destroy cleanup_organization" do
    it "destroys the organization if the user was the last member" do
      organization = create(:organization)
      user = create(:user, organization: organization)
      user.destroy
      expect(Organization.find_by(id: organization.id)).to be_nil
    end
  end
end

# == Schema Information
#
# Table name: users
#
#  id              :uuid             not null, primary key
#  display_name    :string
#  email           :string
#  family_name     :string
#  firebase_uid    :string
#  given_name      :string
#  locale          :string           default("en")
#  picture         :string
#  public_key      :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  organization_id :uuid
#  provider_id     :string
#
# Indexes
#
#  index_users_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_...  (organization_id => organizations.id)
#
