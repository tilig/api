require "rails_helper"

RSpec.describe Identity, type: :model do
  subject { create(:identity) }

  describe "destroy dependencies" do
    describe "when destroying an identity" do
      it "does not destroy the user" do
        subject.destroy
        expect(subject.user.reload).to be_present
      end
    end

    describe "when destroying a user" do
      it "destroys the identity" do
        subject.user.destroy
        expect { subject.reload }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe "uniqueness validation" do
    describe "when trying to set an access token that already exists" do
      it "does not validate" do
        existing_identity = create(:identity)
        new_identity = build(:identity, access_token: existing_identity.access_token)
        expect(new_identity).to be_invalid
      end
    end

    describe "when trying to set a refresh token that already exists" do
      it "does not validate" do
        existing_identity = create(:identity)
        new_identity = build(:identity, refresh_token: existing_identity.refresh_token)
        expect(new_identity).to be_invalid
      end
    end
  end
end

# == Schema Information
#
# Table name: identities
#
#  id                     :integer          not null, primary key
#  access_token           :string(512)
#  previous_access_token  :string
#  previous_refresh_token :string
#  refresh_token          :string(512)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  user_id                :uuid
#
# Indexes
#
#  index_identities_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
