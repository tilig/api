require "rails_helper"

describe Item, type: :model do
  describe "validation" do
    it "validates the presence of new encryption fields" do
      item = build(:item)

      item.template = nil
      item.encrypted_dek = nil
      item.rsa_encrypted_dek = nil
      item.encrypted_overview = nil
      item.encrypted_details = nil

      expect(item).not_to be_valid
      expect(item.errors[:template]).not_to be_empty
      expect(item.errors[:rsa_encrypted_dek]).not_to be_empty
      expect(item.errors[:encrypted_overview]).not_to be_empty
      expect(item.errors[:encrypted_details]).not_to be_empty
    end

    describe "legacy items" do
      it "validates the presence of name" do
        item = build(:item, :legacy)

        item.name = nil
        expect(item).not_to be_valid
        expect(item.errors[:name]).not_to be_empty
      end

      it "has an override flag to disable legacy encryption" do
        item = create(:item)
        item.user.profile = build(:profile, application_settings: {legacy_encryption_disabled: false})

        # Delegation to the User
        expect(item.legacy_encryption_disabled?).to be false

        # Positive override with the User as owner
        item.legacy_encryption_disabled = true
        expect(item.legacy_encryption_disabled?).to be true

        # Negative override with the User as owner
        item.user.profile.application_settings["legacy_encryption_disabled"] = true
        item.legacy_encryption_disabled = false
        expect(item.legacy_encryption_disabled?).to be false

        # folder_item = create(:item, user: nil, folder: create(:membership, user: create(:user)).folder)

        # # Deligate to the Folder
        # expect(folder_item.legacy_encryption_disabled?).to be true

        # # Negative override with the User as owner
        # folder_item.legacy_encryption_disabled = false
        # expect(item.legacy_encryption_disabled?).to be false
      end

      it "always returns true if the template is not a login" do
        item = create(:item, template: "wifi/1", legacy_encryption_disabled: false)

        expect(item.legacy_encryption_disabled?).to be true
      end

      context "when the template is a login" do
        let(:params) { {template: "login/1"} }

        it "it will not override it to true" do
          item = create(:item, legacy_encryption_disabled: false)

          expect(item.legacy_encryption_disabled?).to be false
        end
      end
    end

    describe "folders" do
      let(:folder) { create(:folder) }
      let(:item) { build(:item) }

      it "cannot belong to both a folder and a user" do
        expect(item.update(folder: folder, user: create(:user))).to be false
        expect(item.errors).not_to be_empty
      end

      it "has to belong to either a folder or a user" do
        expect(item.update(folder: nil, user: nil)).to be false
        expect(item.errors).not_to be_empty
      end

      it "can belong to a folder and no user" do
        expect(item.update(folder: folder, user: nil, encrypted_folder_dek: "1234")).to be true
      end
    end
  end

  describe "soft_destroy" do
    it "sets the deleted_at for a item" do
      item = create(:item)
      expect { item.soft_destroy }.to change(item, :deleted_at).from(nil)
      expect(Item.active).to_not exist
    end

    it "remove share for a item" do
      item = create(:item)
      create(:share_link, item: item)
      expect { item.soft_destroy }.to change { item.reload.share_link }.to(nil)
    end
  end

  describe "public_suffix_domain" do
    let(:item) { create(:item, website: website) }
    let(:website) { nil }

    describe "when the website field is blank" do
      let(:website) { "" }

      it "public_suffix_domain is nil" do
        expect(item.public_suffix_domain).to be_nil
      end
    end

    describe "when the website field is nil" do
      let(:website) { nil }

      it "public_suffix_domain is nil" do
        expect(item.public_suffix_domain).to be_nil
      end
    end

    describe "when the website field is contains an ip number" do
      let(:website) { "10.0.0.20" }

      it "public_suffix_domain is nil" do
        expect(item.public_suffix_domain).to be_nil
      end
    end

    describe "when the website field contains an invalid URL" do
      let(:website) { "!@)#@#)" }

      it "public_suffix_domain is nil" do
        expect(item.public_suffix_domain).to be_nil
      end
    end

    describe "when website is a valid URL" do
      let(:website) { "https://play.google.com" }

      it "returns the public_suffix_domain" do
        expect(item.public_suffix_domain).to eq("google.com")
      end
    end

    describe "when the website does not contain a protocol" do
      let(:website) { "websitewithoutprotocol.com" }

      it "returns the public_suffix_domain" do
        expect(item.public_suffix_domain).to eq("websitewithoutprotocol.com")
      end
    end

    describe "in case of a subdomain of a non-private domain" do
      let(:website) { "http://subdomain.non-private.com" }

      it "does NOT include the private part in the domain" do
        expect(item.public_suffix_domain).to eq("non-private.com")
      end
    end

    describe "in case of a private domain" do
      let(:website) { "http://example.blogspot.com" }

      it "includes the private part in the domain" do
        expect(item.public_suffix_domain).to eq("example.blogspot.com")
      end
    end
  end

  describe "create password versions" do
    it "creates a password version after updating a password" do
      item = create(:item, password: "first password")
      item.update(password: "second password")
      item.update(password: "thid password")

      expect(PasswordVersion.count).to be 2
      expect(PasswordVersion.first.password).to eq "first password"
      expect(PasswordVersion.second.password).to eq "second password"
    end

    it "does not add password versions if the password stayed the same" do
      item = create(:item, password: "first password")
      item.update(password: "first password")

      expect(PasswordVersion).to_not exist
    end

    it "does not create password_versions if other fields are changed " do
      item = create(:item, name: "first name")
      item.update(name: "second name")

      expect(PasswordVersion).to_not exist
    end

    it "does not create password_versions if the password was emptied " do
      item = create(:item, password: "first password")
      item.update(password: "")
      item.update(password: "second passowrd")
      item.update(password: nil)
      item.update(password: "third passowrd")

      expect(PasswordVersion.pluck(:password)).to match [
        "first password",
        "second passowrd"
      ]
    end
  end

  describe "before_save #set_brand/1 " do
    let(:brand) { create(:brand) }
    let(:user) { create(:user) }

    it "sets a brand before saving" do
      item = create(:item, website: "", name: "Google Play")
      brand

      expect { item.update(website: "play.google.com") }.to change(
        item,
        :brand
      ).from(nil).to(brand)
    end

    it "when the parsing the website to a domain returns nil don't try to fetch the brand" do
      item = create(:item, website: "play.google.com<img src=\"src.com\">")
      expect(item.brand).to be_nil
      expect(FetchBrandJob).to_not have_been_enqueued.with(brand)
    end

    it "does not update the brand when website stays the same" do
      item = create :item

      brand

      expect { item.update(name: "Change Name") }.to_not change(
        item,
        :brand
      )
    end

    it "sets the brand on creation as well" do
      brand
      item =
        Item.create(
          name: "Google Play",
          website: "play.google.com",
          user: user
        )

      expect(item.brand).to eq brand
    end

    it "relays the call to brandfetch and updates brandfetch_json and fetch_date on the brand" do
      item = create(:item, website: "", name: "Google Play")
      brand

      item.update(website: "play.google.com")

      expect(FetchBrandJob).to have_been_enqueued.with(brand)
    end

    it "will update item brand based on the domain, not public suffix domain" do
      create(
        :brand,
        name: "Azure",
        domain: "azure.microsoft.comm",
        public_suffix_domain: "microsoft.com"
      )
      item = create(:item, website: "todo.microsoft.com")

      expect(item.brand.domain).to eq "todo.microsoft.com"
      expect(item.brand.domain).to_not be "Azure"
    end
  end

  describe "inactive" do
    let!(:inactive_item) { create :item, updated_at: 3.days.ago, deleted_at: 3.days.ago }
    let!(:active_item) { create(:item) }

    subject { Item.inactive }

    it "only returns items that are inactive (do have a deleted_at set)" do
      expect(subject).to match [inactive_item]
    end
  end

  describe "active" do
    let!(:inactive_item) { create :item, updated_at: 3.days.ago, deleted_at: 3.days.ago }
    let!(:active_item) { create(:item) }

    subject { Item.active }

    it "only returns items that are active (do not have a deleted_at set)" do
      expect(subject).to match [active_item]
    end
  end

  describe "updated_after" do
    let(:oldest_item) { create(:item, updated_at: 2.days.ago) }
    let(:newer_item) { create(:item) }
    let(:time) { 1.day.ago.iso8601 }

    subject { Item.updated_after(time) }

    before do
      oldest_item
      newer_item
    end

    it "only returns items that are updated after the given time" do
      expect(subject).to match [newer_item]
    end

    context "with a random string" do
      let(:time) { "this_is_random" }

      it "returns all items" do
        expect(subject.count).to be 2
      end
    end

    context "when given time is empty" do
      let(:time) { nil }

      it "does not break when it's empty" do
        expect(subject.count).to be 2
      end
    end

    context "when given limit that is within the time range" do
      subject { Item.updated_after(1.day.ago, limit: 7.days.ago) }

      it "only returns items that are updated after the given time" do
        expect(subject).to match [newer_item]
      end
    end

    context "when given limit that is within the time range" do
      before { create(:item, updated_at: 4.days.ago) }

      subject { Item.updated_after(9.days.ago, limit: 1.day.ago) }

      it "only returns items that are updated after the given limit" do
        expect(subject).to match [newer_item]
      end
    end
  end

  describe "base64 normalization" do
    it "normalizes all base64 encoded attributes" do
      item = Item.new(encrypted_dek: "\s\na\ra\n\s\ra\ra\n\s",
        encrypted_details: "\s\nb\rb\n\s\rb\rb\n\s",
        encrypted_folder_dek: "\s\nc\rc\n\s\rc\rc\n\s",
        encrypted_overview: "\s\nd\rd\n\s\rd\rd\n\s",
        rsa_encrypted_dek: "\s\ne\re\n\s\re\re\n\s",
        notes: "\s\nf\rf\n\s\rf\rf\n\s",
        otp: "\s\ng\rg\n\s\rg\rg\n\s",
        password: "\s\nh\rh\n\s\rh\rh\n\s",
        username: "\s\ni\ri\n\s\ri\ri\n\s")

      expect(item.encrypted_dek).to eql "aaaa"
      expect(item.encrypted_details).to eql "bbbb"
      expect(item.encrypted_folder_dek).to eql "cccc"
      expect(item.encrypted_overview).to eql "dddd"
      expect(item.rsa_encrypted_dek).to eql "eeee"
      expect(item.notes).to eql "ffff"
      expect(item.otp).to eql "gggg"
      expect(item.password).to eql "hhhh"
      expect(item.username).to eql "iiii"
    end

    it "normalizes the rsa_encrypted_dek into b64 with padding" do
      item = Item.new rsa_encrypted_dek: "A-_AAA"
      expect(item.rsa_encrypted_dek).to eq "A+/AAA=="
      expect(Base64.strict_decode64(item.rsa_encrypted_dek)).to eq "\x03\xEF\xC0\x00".b
    end

    it "handles nil values" do
      item = Item.new

      expect(item.encrypted_dek).to be_nil
      expect(item.encrypted_details).to be_nil
      expect(item.encrypted_folder_dek).to be_nil
      expect(item.encrypted_overview).to be_nil
      expect(item.rsa_encrypted_dek).to be_nil
      expect(item.notes).to be_nil
      expect(item.otp).to be_nil
      expect(item.password).to be_nil
      expect(item.username).to be_nil
    end
  end
end

# == Schema Information
#
# Table name: items
#
#  id                         :uuid             not null, primary key
#  create_platform            :string
#  deleted_at                 :datetime
#  encrypted_dek              :string
#  encrypted_details          :text
#  encrypted_folder_dek       :text
#  encrypted_overview         :text
#  encryption_version         :integer          default(1)
#  legacy_encryption_disabled :boolean
#  name                       :string
#  notes                      :string
#  otp                        :string
#  password                   :string
#  public_suffix_domain       :string
#  rsa_encrypted_dek          :text
#  template                   :string
#  update_platform            :string
#  username                   :string
#  website                    :string
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  android_app_id             :string
#  brand_id                   :uuid
#  folder_id                  :uuid
#  user_id                    :uuid
#
# Indexes
#
#  index_items_on_folder_id             (folder_id)
#  index_items_on_public_suffix_domain  (public_suffix_domain)
#  index_items_on_user_id               (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (brand_id => brands.id)
#  fk_rails_...  (folder_id => folders.id)
#  fk_rails_...  (user_id => users.id)
#
