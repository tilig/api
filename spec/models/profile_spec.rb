require "rails_helper"

describe Profile, type: :model do
  let(:profile) { create(:profile) }

  describe "#save_sign_up_client" do
    it "sets a client when the application_settings is empty" do
      expect { profile.save_sign_up_client("Web") }.to(
        change(profile, :application_settings)
        .from({"referral_link" => profile.application_settings["referral_link"]})
        .to({"client_signed_up" => "Web", "referral_link" => profile.application_settings["referral_link"]})
      )
    end

    describe "when the application_settings is already set" do
      let(:profile) { create(:profile, application_settings: {"client_signed_up" => "Web", "clients_signed_in" => ["Web"]}) }

      it "it does not change the application_settings" do
        expect { profile.save_sign_up_client("iOS") }.to_not change(profile, :application_settings)
      end
    end
  end

  describe "#save_signed_in_client" do
    it "adds a new client if none yet exist" do
      profile.save_signed_in_client("Web")
      expect(profile.application_settings).to eq({"clients_signed_in" => ["Web"], "referral_link" => profile.application_settings["referral_link"]})
    end

    describe "when clients_signed_in is already filled" do
      let(:profile) { create(:profile, application_settings: {"client_signed_up" => "Web", "clients_signed_in" => ["Web"]}) }

      it "adds a new client and leaves the old clients in" do
        profile.save_signed_in_client("iOS")
        expect(profile.application_settings["clients_signed_in"]).to eq(["Web", "iOS"])
      end

      it "does not double the client when the same client is added" do
        profile.save_signed_in_client("Web")
        expect(profile.application_settings["clients_signed_in"]).to eq(["Web"])
      end
    end
  end

  describe "#save_signed_in_provider_id" do
    it "adds the provider id to an array" do
      expect { profile.save_signed_in_provider_id("google.com") }.to(
        change(profile, :application_settings)
        .from({"referral_link" => profile.application_settings["referral_link"]})
        .to({"provider_ids_signed_in" => ["google.com"], "referral_link" => profile.application_settings["referral_link"]})
      )
    end

    context "with the same provider already added" do
      let(:profile) { create(:profile, application_settings: {"provider_ids_signed_in" => ["google.com"]}) }

      it "saves the provider only once if its the same" do
        expect { profile.save_signed_in_provider_id("google.com") }.to_not change(profile, :application_settings)
      end

      it "adds a new one if it's not the same" do
        expect { profile.save_signed_in_provider_id("apple.com") }.to(
          change(profile, :application_settings)
          .from({"referral_link" => profile.application_settings["referral_link"], "provider_ids_signed_in" => ["google.com"]})
          .to({"provider_ids_signed_in" => ["google.com", "apple.com"], "referral_link" => profile.application_settings["referral_link"]})
        )
      end
    end
  end

  describe "#save_sign_up_provider_id" do
    it "saves the provider id" do
      expect { profile.save_sign_up_provider_id("google.com") }.to(
        change(profile, :application_settings)
        .from({"referral_link" => profile.application_settings["referral_link"]})
        .to({"provider_id_signed_up" => "google.com", "referral_link" => profile.application_settings["referral_link"]})
      )
    end

    context "when a provider is already added " do
      let(:profile) { create(:profile, application_settings: {"provider_id_signed_up" => "google.com"}) }

      it "it doesn't change the value" do
        expect { profile.save_sign_up_provider_id("apple.com") }.to_not change(profile, :application_settings)
      end
    end
  end
end

# == Schema Information
#
# Table name: profiles
#
#  id                   :uuid             not null, primary key
#  application_settings :jsonb            not null
#  meta                 :jsonb            not null
#  referral_count       :integer          default(0), not null
#  user_settings        :jsonb            not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  user_id              :uuid             not null
#
# Indexes
#
#  index_profiles_on_user_id  (user_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
