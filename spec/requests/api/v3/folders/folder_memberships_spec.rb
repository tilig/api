require "rails_helper"

describe "Api::V3::Folders::FolderMemberships", type: :request, v3: true do
  let(:user) { create(:user, :with_keypair) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:default_headers) { {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"} }
  let(:headers) { {Authorization: "Bearer #{access_token}"}.merge(default_headers) }
  let(:folder) { create(:folder) }
  let!(:folder_membership) { create(:folder_membership, :accepted, user: user, folder: folder) }

  let(:folder_id) { folder.id }

  before { stub_request(:post, "https://mandrillapp.com/api/1.0/messages/send-template.json").to_return(status: 200, body: {}.to_json, headers: {}) }

  describe "POST /create" do
    let(:new_member) { create(:user, :with_keypair) }
    let(:params) {
      {
        folder_membership: {
          user_id: new_member.id,
          encrypted_private_key: "123456"
        }
      }
    }

    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :post }
      let(:path) { api_v3_folder_folder_memberships_path(folder_id) }
    end

    context "with valid parameters" do
      it "creates a new folder_membership" do
        expect do
          post api_v3_folder_folder_memberships_path(folder_id), params: params, headers: headers, as: :json
        end.to change { FolderMembership.count }.by(1)
      end

      it "tracks the 'Folder Membership Created'" do
        expect_any_instance_of(Trackable).to receive(:track).with("Folder Membership Created", {count: 2})
        post api_v3_folder_folder_memberships_path(folder_id), params: params, headers: headers, as: :json
      end

      it "returns a created response" do
        post api_v3_folder_folder_memberships_path(folder_id), params: params, headers: headers, as: :json
        expect(response).to have_http_status(:created)
        new_folder_memberships = FolderMembership.last
        expect(json_response).to match({
          folder_membership: {
            id: new_folder_memberships.id,
            role: new_folder_memberships.role,
            accepted_at: new_folder_memberships.accepted_at.as_json,
            created_at: new_folder_memberships.created_at.as_json,
            updated_at: new_folder_memberships.updated_at.as_json,
            missing_private_key: false,
            accepted: false,
            pending: true,
            is_me: false,
            creator: {

              id: user.id,
              display_name: user.display_name,
              email: user.email,
              picture: user.picture
            },
            user: {
              id: new_member.id,
              email: new_member.email,
              display_name: nil,
              picture: nil,
              public_key: new_member.keypair.public_key
            }
          },
          folder: {
            id: folder.id,
            name: folder.name,
            created_at: folder.created_at.as_json,
            updated_at: folder.updated_at.as_json
          }
        })
      end

      context "with just an email address" do
        let(:params) {
          {
            folder_membership: {
              email: "john@example.com"
            }
          }
        }

        it "creates a new folder_membership" do
          expect do
            post api_v3_folder_folder_memberships_path(folder_id), params: params, headers: headers, as: :json
          end.to change { FolderMembership.count }.by(1)
          new_folder_memberships = FolderMembership.last

          expect(json_response).to match({
            folder_membership: {
              id: new_folder_memberships.id,
              role: new_folder_memberships.role,
              accepted_at: new_folder_memberships.accepted_at.as_json,
              created_at: new_folder_memberships.created_at.as_json,
              updated_at: new_folder_memberships.updated_at.as_json,
              missing_private_key: false,
              accepted: false,
              pending: true,
              is_me: false,
              creator: {
                id: user.id,
                display_name: user.display_name,
                email: user.email,
                picture: user.picture
              },
              user: {
                id: nil,
                email: "john@example.com",
                display_name: nil,
                picture: nil,
                public_key: nil
              }
            },
            folder: {
              id: folder.id,
              name: folder.name,
              created_at: folder.created_at.as_json,
              updated_at: folder.updated_at.as_json
            }

          })
        end
      end
    end

    context "with invalid parameters" do
      let(:params) {
        {
          folder_membership: {
            user_id: new_member.id,
            encrypted_private_key: "123456"
          }
        }
      }

      it "returns a unprocessable_entity and error 'can't be blank'" do
        params[:folder_membership][:encrypted_private_key] = nil
        post api_v3_folder_folder_memberships_path(folder_id), params: params, headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors]).to include({encrypted_private_key: ["can't be blank"]})
      end
      it "returns a unprocessable_entity and error 'can't be blank'" do
        params[:folder_membership][:user_id] = nil
        post api_v3_folder_folder_memberships_path(folder_id), params: params, headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors]).to include({user: ["can't be blank"], email: ["can't be blank"]})
      end
    end

    context "when user is already added" do
      before { create(:folder_membership, :accepted, user: new_member, folder: folder) }

      let(:params) {
        {
          folder_membership: {
            user_id: new_member.id,
            encrypted_private_key: "123456"
          }
        }
      }

      it "returns a unprocessable_entity and error 'has already been taken'" do
        post api_v3_folder_folder_memberships_path(folder_id), params: params, headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors]).to include({user: ["has already been taken"]})
      end
    end
  end

  describe "DELETE /destroy" do
    let(:new_member) { create(:user, :with_keypair) }
    let(:new_folder_membership) { create(:folder_membership, :accepted, user: new_member, folder: folder) }
    let(:id) { new_folder_membership.id }

    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :delete }
      let(:path) { api_v3_folder_folder_membership_path(folder_id, id) }
      let(:params) { {} }
      let(:expected_status_code) { :ok }
    end

    context "with valid parameters" do
      before { new_folder_membership }

      it "creates a new folder_membership" do
        expect do
          delete api_v3_folder_folder_membership_path(folder_id, id), headers: headers, as: :json
        end.to change { FolderMembership.count }.by(-1)
      end

      it "tracks the 'Folder Membership Deleted'" do
        expect_any_instance_of(Trackable).to receive(:track).with("Folder Membership Deleted")
        delete api_v3_folder_folder_membership_path(folder_id, id), headers: headers, as: :json
      end

      it "returns a created response" do
        delete api_v3_folder_folder_membership_path(folder_id, id), headers: headers, as: :json
        expect(response).to have_http_status(:ok)
        expect(json_response).to match({
          folder_membership: {
            id: new_folder_membership.id,
            role: new_folder_membership.role,
            accepted_at: new_folder_membership.accepted_at.as_json,
            created_at: new_folder_membership.created_at.as_json,
            updated_at: new_folder_membership.updated_at.as_json,
            accepted: true,
            pending: false,
            missing_private_key: false,
            is_me: false,
            creator: {
              id: new_folder_membership.creator.id,
              email: new_folder_membership.creator.email,
              display_name: new_folder_membership.creator.display_name,
              picture: new_folder_membership.creator.picture
            },
            user: {
              id: new_member.id,
              email: new_member.email,
              display_name: new_member.display_name,
              picture: new_member.picture,
              public_key: new_member.keypair.public_key
            }
          },
          folder: {
            id: folder.id,
            name: folder.name,
            created_at: folder.created_at.as_json,
            updated_at: folder.updated_at.as_json
          }

        })
      end
    end

    context "when the deleted user is the last member with a admin role" do
      let(:id) { folder_membership.id }

      it "creates a new folder_membership" do
        expect do
          delete api_v3_folder_folder_membership_path(folder_id, id), headers: headers, as: :json
        end.to change { FolderMembership.count }.by(0)
      end
    end

    context "with invalid parameters" do
      let!(:other_folder) { create(:folder) }
      let!(:other_folder_membership) { create(:folder_membership, :accepted, user: create(:user, :with_keypair), folder: other_folder) }
      let!(:other_folder_membership_2) { create(:folder_membership, :accepted, user: create(:user, :with_keypair), folder: other_folder) }

      it "returns a not_found when an membershop of an other folder is removed" do
        expect do
          delete api_v3_folder_folder_membership_path(folder_id, other_folder_membership_2), headers: headers, as: :json
        end.to change { FolderMembership.count }.by(0)
        expect(response).to have_http_status(:not_found)
      end

      it "returns a not_found when an membership of an other folder is removed" do
        expect do
          delete api_v3_folder_folder_membership_path(other_folder, other_folder_membership_2), headers: headers, as: :json
        end.to change { FolderMembership.count }.by(0)
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
