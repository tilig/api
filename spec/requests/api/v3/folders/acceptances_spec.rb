require "swagger_helper"

describe "Api::V3::Folders::Acceptances", type: :request, v3: true do
  let(:user) { create(:user, :with_keypair) }
  let(:folder) { create(:folder) }
  let(:folder_membership) { create(:folder_membership, :accepted, user: user, folder: folder) }
  let(:headers) { {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"} }
  let!(:folder_item) { create(:item, folder: folder, user: nil, encrypted_folder_dek: "leflef") }

  describe "#create" do
    let(:new_folder_membership) { create(:folder_membership, user: create(:user, :with_keypair), folder: folder) }
    let(:folder_membership_id) { new_folder_membership.id }
    let(:other_folder_membership) { create(:folder_membership, user: create(:user, :with_keypair), folder: create(:folder)) }

    let(:params) { {acceptance: {acceptance_token: new_folder_membership.acceptance_token}} }

    context "with valid parameters" do
      it "accepts a folder membership" do
        expect do
          post api_v3_folder_folder_membership_acceptance_path(folder_id: folder.id, folder_membership_id: new_folder_membership.id), params:, headers:, as: :json
        end.to change { new_folder_membership.reload.accepted_at }.from(nil)

        expect(response).to have_http_status(:created)
      end

      it "tracks the 'Folder Membership Deleted'" do
        expect_any_instance_of(Trackable).to receive(:track).with("Folder Membership Accepted")
        post api_v3_folder_folder_membership_acceptance_path(folder_id: folder.id, folder_membership_id: new_folder_membership.id), params:, headers:, as: :json
      end
    end

    context "with invalid parameters" do
      let(:params) { {acceptance: {acceptance_token: new_folder_membership.acceptance_token}} }

      it "returns a unprocessable_entity and error 'Something is wrong with the acceptance token'" do
        params[:acceptance][:acceptance_token] = nil
        post api_v3_folder_folder_membership_acceptance_path(folder_id: folder.id, folder_membership_id: new_folder_membership.id), params:, headers:, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors]).to include({base: ["Something is wrong with the acceptance token"]})
      end

      it "returns an error" do
        expect { post api_v3_folder_folder_membership_acceptance_path(folder_id: folder.id, folder_membership_id: new_folder_membership.id), headers:, as: :json }.to raise_error ActionController::ParameterMissing
      end

      context "Using an other folder_membership ID" do
        let(:params) { {acceptance: {acceptance_token: other_folder_membership.acceptance_token}} }

        it "returns a not_found" do
          post api_v3_folder_folder_membership_acceptance_path(folder_id: folder.id, folder_membership_id: other_folder_membership.id), params:, headers:, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end

      context "Using an other folder ID" do
        it "returns a not_found" do
          post api_v3_folder_folder_membership_acceptance_path(folder_id: other_folder_membership.folder.id, folder_membership_id: new_folder_membership.id), params:, headers:, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end
    end
  end

  describe "#accept" do
    let(:new_folder_membership) { create(:folder_membership, user: create(:user, :with_keypair), folder: folder) }
    let(:folder_membership_id) { new_folder_membership.id }
    let(:other_folder_membership) { create(:folder_membership, user: create(:user, :with_keypair), folder: create(:folder)) }

    let(:params) { {acceptance_token: new_folder_membership.acceptance_token} }

    context "with valid parameters" do
      it "accepts a folder membership" do
        expect do
          get api_v3_folder_folder_membership_get_acceptance_path(folder_id: folder.id, folder_membership_id: new_folder_membership.id), params:
        end.to change { new_folder_membership.reload.accepted_at }.from(nil)
        expect(response.body).to include("Your shared item is available!")

        expect(response).to have_http_status(:created)
      end

      it "tracks the 'Folder Membership Deleted'" do
        expect_any_instance_of(Trackable).to receive(:track).with("Folder Membership Accepted")
        get api_v3_folder_folder_membership_get_acceptance_path(folder_id: folder.id, folder_membership_id: new_folder_membership.id), params:
      end

      it "adds contacts" do
        get api_v3_folder_folder_membership_get_acceptance_path(folder_id: folder.id, folder_membership_id: new_folder_membership.id), params: params

        expect(Contact.find_by(user: new_folder_membership.user)).to_not be nil
        expect(Contact.find_by(user: folder.creator)).to_not be nil
      end
    end

    context "with invalid parameters" do
      let(:params) { {acceptance_token: new_folder_membership.acceptance_token} }

      it "returns a error 'Sorry, this link has expired.'" do
        params[:acceptance_token] = nil
        get api_v3_folder_folder_membership_get_acceptance_path(folder_id: folder.id, folder_membership_id: new_folder_membership.id), params:, headers: headers

        expect(response).to have_http_status(:ok)
        expect(response.body).to include("Sorry, this link has expired.")
      end

      context "Using an other folder_membership ID" do
        it "returns a not_found" do
          get api_v3_folder_folder_membership_get_acceptance_path(folder_id: folder.id, folder_membership_id: other_folder_membership.id), params:, headers: headers

          expect(response.body).to include("Sorry, this link has expired.")
        end
      end

      context "Using an other folder_membership ID" do
        let(:params) { {acceptance_token: other_folder_membership.acceptance_token} }

        it "returns a not_found" do
          get api_v3_folder_folder_membership_get_acceptance_path(folder_id: folder.id, folder_membership_id: other_folder_membership.id), params:, headers: headers

          expect(response.body).to include("Sorry, this link has expired.")
        end
      end

      context "Using an other folder ID" do
        it "returns a not_found" do
          get api_v3_folder_folder_membership_get_acceptance_path(folder_id: other_folder_membership.folder.id, folder_membership_id: new_folder_membership.id), params:, headers: headers

          expect(response.body).to include("Sorry, this link has expired.")
        end
      end
    end

    context "Creator and invitee are already contacts" do
      let!(:contact) { create(:contact, user: folder.creator, contact: new_folder_membership.user) }
      let!(:contact) { create(:contact, user: new_folder_membership.user, contact: folder.creator) }

      it "silently ignores existing contacts" do
        get api_v3_folder_folder_membership_get_acceptance_path(folder_id: folder.id, folder_membership_id: new_folder_membership.id), params: params
        expect(response).to have_http_status(:created)
      end
    end
  end
end
