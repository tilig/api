require "rails_helper"

describe "Api::V3::Secrets", type: :request, v3: true do
  let(:user) { create(:user) }
  let(:user2) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let!(:brand) do
    create(:brand,
      name: "Dropbox",
      domain: "dropbox.com",
      public_suffix_domain: "dropbox.com",
      logo_source: "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg",
      logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
      main_color_brightness: "88",
      main_color_hex: "#0061ff",
      totp: true,
      brandfetch_json: {filled: true},
      fetch_date: Time.current)
  end

  let(:own_secret) { create(:item, user: user, brand: brand, share_link: create(:share_link)) }
  let(:other_secret) { create(:item) }
  let(:default_headers) { {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"} }
  let(:headers) { {Authorization: "Bearer #{access_token}"}.merge(default_headers) }
  let(:params) { nil }

  # let(:membership) { create(:membership, user: user) }
  # let(:group) { membership.group }
  # let(:group_id) { group.id }
  # let(:group_secret) { create(:item, group: group, user: nil, brand: brand, share_link: create(:share_link)) }
  # let(:other_group_secret) { create(:item, user: nil, group: create(:membership, user: user2).group, brand: brand) }

  describe "For User Secrets" do
    describe "GET /index" do
      it_should_behave_like "an authenticated endpoint" do
        let(:method) { :get }
        let(:path) { api_v3_secrets_path }
      end

      it "only renders the secrets of the user" do
        own_secret
        other_secret

        get api_v3_secrets_url, headers: headers

        expect(json_response.size).to eq 1
        expect(json_response).to eq(
          [
            {
              id: own_secret.id,
              template: "login/1",
              domain: "dropbox.com",
              website: "dropbox.com",
              encryption_version: 2,
              encrypted_key: own_secret.encrypted_dek,
              encrypted_dek: own_secret.rsa_encrypted_dek,
              encrypted_details: own_secret.encrypted_details,
              encrypted_overview: own_secret.encrypted_overview,
              android_app_id: nil,
              username: nil,
              name: nil,
              notes: nil,
              otp: nil,
              password: nil,
              created_at: own_secret.created_at.utc.as_json,
              updated_at: own_secret.updated_at.utc.as_json,
              brand: {
                breached_at: nil,
                breach_published_at: nil,
                main_color_hex: brand.main_color_hex,
                main_color_brightness: brand.main_color_brightness,
                logo_source: brand.logo_source,
                logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
                domain: brand.domain,
                public_suffix_domain: brand.domain,
                name: brand.name,
                totp: brand.totp,
                id: brand.id,
                is_fetched: true,
                images: {
                  icon: {
                    png: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
                    },
                    svg: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
                    },

                    theme: "dark"
                  }
                }
              },
              share: {
                id: own_secret.share_link.id,
                times_viewed: own_secret.share_link.times_viewed,
                encrypted_master_key: own_secret.share_link.encrypted_master_key
              },

              # Migration fields
              legacy_encryption_disabled: false
            }
          ]
        )
      end

      context "with secrets outside the default page scope" do
        before { stub_const("Paginatable::DEFAULT_PAGE_SIZE", 1) }

        it "paginates the secrets" do
          create :item, name: "new", user: user, template: "login/1"
          create :item, name: "old", user: user, template: "login/1", created_at: 1.day.ago

          get api_v3_secrets_url, headers: headers

          expect(json_response.size).to eq 1
          expect(json_response.first[:name]).to eq "old"
        end

        it "paginates to the next page" do
          create :item, name: "new", user: user, template: "login/1"
          create :item, name: "old", user: user, template: "login/1", created_at: 1.day.ago

          get api_v3_secrets_url, params: {page: {offset: 2}}, headers: headers

          expect(json_response.first).to include(name: "new")
        end
      end

      context "add a timestamp to get updated secrets" do
        it "Only returns the newest secrets after the given timestamp" do
          create :item, updated_at: 3.days.ago, user: user
          new_secret = create :item, user: user, website: "myblog.blogspot.com"

          get api_v3_secrets_url, params: {after: 1.day.ago.iso8601}, headers: headers

          expect(json_response.count).to eq 1
          expect(json_response.first[:id]).to eq(new_secret.id)
        end
      end

      context "invalid datetime params " do
        let(:params) { {after: "invalid_date"} }

        it "will return a 200" do
          create :item, name: "new", user: user
          get api_v3_secrets_url, params: params, headers: headers, as: :json
          expect(response.status).to eq 200
          expect(json_response.count).to eq 1
        end
      end

      context "include_template filters" do
        let(:params) { {include_template: "securenote/1"} }
        let!(:secure_note) do
          create :item, template: "securenote/1", user: user
        end

        it "will not include securenote/1 secrets" do
          get api_v3_secrets_url, headers: headers

          expect(json_response.size).to eq 0
        end

        it "will return securenote/1 if instructed" do
          create :item, template: "securenote/1"

          get api_v3_secrets_url, params: params, headers: headers

          expect(json_response.size).to eq 1
          expect(json_response.first[:id]).to eq(secure_note.id)
        end
      end

      context "with multiple include templates params given" do
        let(:params) { {include_template: %w[securenote/1 second/1,third/1]} }

        it "will return securenote/1" do
          create :item, template: "securenote/1", user: user
          create :item, template: "second/1", user: user
          create :item, template: "third/1", user: user

          get api_v3_secrets_url, params: params, headers: headers
          expect(json_response.size).to eq 3
        end
      end
    end

    describe "GET /show" do
      it_should_behave_like "an authenticated endpoint" do
        let(:method) { :get }
        let(:path) { api_v3_secrets_path(own_secret) }
      end

      context "accessing the users own secret" do
        it "renders the secret" do
          get api_v3_secret_url(own_secret),
            headers: {
              Authorization: "Bearer #{access_token}"
            },
            as: :json

          expect(json_response).to eq(
            {
              android_app_id: nil,
              domain: "dropbox.com",
              encrypted_key: own_secret.encrypted_dek,
              encrypted_dek: own_secret.rsa_encrypted_dek,
              encrypted_details: own_secret.encrypted_details,
              encrypted_overview: own_secret.encrypted_overview,
              encryption_version: 2,
              template: "login/1",
              id: own_secret.id,
              username: own_secret.username,
              name: own_secret.name,
              notes: own_secret.notes,
              otp: own_secret.otp,
              password: own_secret.password,
              website: own_secret.website,
              created_at: own_secret.created_at.utc.as_json,
              updated_at: own_secret.updated_at.utc.as_json,
              brand: {
                breached_at: nil,
                breach_published_at: nil,
                main_color_hex: brand.main_color_hex,
                is_fetched: true,
                main_color_brightness: brand.main_color_brightness,
                logo_source: brand.logo_source,
                logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
                domain: brand.domain,
                public_suffix_domain: brand.domain,
                name: brand.name,
                totp: brand.totp,
                id: brand.id,
                images: {
                  icon: {
                    png: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
                    },
                    svg: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
                    },

                    theme: "dark"
                  }
                }
              },
              share: {
                id: own_secret.share_link.id,
                times_viewed: own_secret.share_link.times_viewed,
                encrypted_master_key: own_secret.share_link.encrypted_master_key
              },
              # Migration fields
              legacy_encryption_disabled: false
            }
          )
        end

        it "works without a share present" do
          own_secret.share_link.destroy!

          get api_v3_secret_url(own_secret), headers: headers

          expect(json_response.fetch(:share)).to be_nil
        end
      end

      context "with legacy encryption disabled" do
        before { user.disable_legacy_encryption! }

        it "renders the item without legacy fields" do
          get api_v3_secret_url(own_secret),
            headers: {
              Authorization: "Bearer #{access_token}"
            },
            as: :json

          expect(json_response).to eq(
            {
              domain: "dropbox.com",
              website: "dropbox.com",
              encrypted_key: own_secret.encrypted_dek,
              encrypted_dek: own_secret.rsa_encrypted_dek,
              encrypted_details: own_secret.encrypted_details,
              encrypted_overview: own_secret.encrypted_overview,
              encryption_version: 2,
              template: "login/1",
              id: own_secret.id,

              created_at: own_secret.created_at.utc.as_json,
              updated_at: own_secret.updated_at.utc.as_json,
              brand: {
                breached_at: nil,
                breach_published_at: nil,
                main_color_hex: brand.main_color_hex,
                main_color_brightness: brand.main_color_brightness,
                logo_source: brand.logo_source,
                logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
                domain: brand.domain,
                public_suffix_domain: brand.domain,
                name: brand.name,
                totp: brand.totp,
                is_fetched: true,
                id: brand.id,
                images: {
                  icon: {
                    png: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
                    },
                    svg: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
                    },

                    theme: "dark"
                  }
                }
              },
              share: {
                id: own_secret.share_link.id,
                encrypted_master_key: own_secret.share_link.encrypted_master_key,
                times_viewed: own_secret.share_link.times_viewed
              },
              # Migration fields
              legacy_encryption_disabled: true
            }
          )
        end
      end

      context "accessing someone else secret" do
        it "renders a not found (404) response" do
          get api_v3_secret_url(other_secret), headers: headers, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end

      context "accessing a non-existent secret" do
        it "renders a not found response" do
          get api_v3_secret_url(SecureRandom.uuid), headers: headers, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end

      describe "domain field" do
        describe "for a normal subdomain" do
          let(:own_secret) do
            create(:item, user: user, website: "myblog.example.com")
          end

          it "returns the correct domain" do
            get api_v3_secret_url(own_secret),
              headers: {
                Authorization: "Bearer #{access_token}"
              },
              as: :json
            expect(json_response[:domain]).to eq("example.com")
          end
        end

        describe "for a secret that has private domain" do
          let(:own_secret) do
            create(:item, user: user, website: "myblog.blogspot.com")
          end

          it "returns the correct domain" do
            get api_v3_secret_url(own_secret),
              headers: {
                Authorization: "Bearer #{access_token}"
              },
              as: :json
            expect(json_response[:domain]).to eq("myblog.blogspot.com")
          end
        end
      end
    end

    describe "POST /create" do
      it_should_behave_like "an authenticated endpoint" do
        let(:method) { :post }
        let(:path) { api_v3_secrets_path(own_secret) }
        let(:params) { {name: "Name"} }
      end

      context "with valid parameters" do
        let(:params) { {name: "Name"} }

        it "creates a new Secret" do
          expect do
            post api_v3_secrets_url, params: params, headers: headers, as: :json
          end.to change { Item.count }.by(1)
        end

        it "returns a created response" do
          post api_v3_secrets_url, params: params, headers: headers, as: :json
          expect(response).to have_http_status(:created)
        end

        it "has stored the attributes in the new Secret" do
          post api_v3_secrets_url, params: params, headers: headers, as: :json
          new_secret = Item.last
          expect(new_secret.name).to eq("Name")
          expect(new_secret.website).to be_nil
          expect(new_secret.username).to be_nil
          expect(new_secret.notes).to be_nil
          expect(new_secret.password).to be_nil
          expect(new_secret.otp).to be_nil
          expect(new_secret.user).to eq(user)
        end

        it "tracks the 'Account Created' event" do
          expect_any_instance_of(Trackable).to receive(:track).with(
            "Account Created"
          )
          post api_v3_secrets_url, params: params, headers: headers, as: :json
        end
      end

      context "with all parameters" do
        let(:params) do
          {
            name: "Name",
            website: "dropbox.com",
            notes: "notes",
            password: "very_secret_string",
            otp: "otp_secret"
          }
        end

        before { brand }

        it "creates a new Secret" do
          expect do
            post api_v3_secrets_url, params: params, headers: headers, as: :json
          end.to change { Item.count }.by(1)
        end

        it "renders a JSON response with the new secret" do
          post api_v3_secrets_url, params: params, headers: headers, as: :json
          expect(response).to have_http_status(:created)
        end

        it "has stored the attributes in the new Secret" do
          post api_v3_secrets_url, params: params, headers: headers, as: :json
          new_secret = Item.last
          expect(new_secret.name).to eq("Name")
          expect(new_secret.website).to eq("dropbox.com")
          expect(new_secret.notes).to eq("notes")
          expect(new_secret.password).to eq("very_secret_string")
          expect(new_secret.otp).to eq("otp_secret")
          expect(new_secret.user).to eq(user)
          expect(new_secret.brand).to eq(brand)
        end
      end

      context "with invalid parameters" do
        let(:params) { {wrong_attr: "wrong_value"} }

        it "does not create a new Secret" do
          expect do
            post api_v3_secrets_url, params: params, headers: headers, as: :json
          end.to change { Item.count }.by(0)
        end

        it "returns an unprocessable_entity response" do
          post api_v3_secrets_url, params: params, headers: headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context "with legacy decryption disabled" do
        let(:params) do
          {
            name: "NO NAME",
            notes: "NO NOTES",
            password: "NO PASSWORD",
            otp: "NO OTP",
            username: "NO USERNAME",

            template: "template",
            encryption_version: 2,
            website: "dropbox.com",
            encrypted_dek: "dek=",
            encrypted_overview: "overview",
            encrypted_details: "details"
          }
        end

        before { user.disable_legacy_encryption! }

        it "doest not store the legacy fields" do
          post api_v3_secrets_url, params: params, headers: headers, as: :json
          new_secret = Item.last
          expect(new_secret.name).to be_nil
          expect(new_secret.notes).to be_nil
          expect(new_secret.password).to be_nil
          expect(new_secret.otp).to be_nil
          expect(new_secret.username).to be_nil

          expect(new_secret.website).to eq("dropbox.com")
          expect(new_secret.user).to eq(user)
          expect(new_secret.brand).to eq(brand)
          expect(new_secret.rsa_encrypted_dek).to eq("dek=")
        end
      end
    end

    describe "PATCH /update" do
      subject { patch api_v3_secret_url(own_secret), params: params, headers: headers }

      it_should_behave_like "an authenticated endpoint" do
        let(:method) { :patch }
        let(:path) { api_v3_secret_url(own_secret) }
        let(:params) { {name: "Name"} }
      end

      context "with valid parameters" do
        let(:params) { {name: "Name"} }

        it "updates the requested secret" do
          subject

          expect(own_secret.reload.name).to eq("Name")
        end

        it "returns an ok response" do
          subject
          expect(response).to have_http_status(:ok)
        end
      end

      context "with all parameters" do
        let(:params) do
          {
            name: "Name",
            website: "dropbox.com",
            username: "username",
            notes: "notes",
            password: "very_secret_string",
            otp: "otp_secret",
            android_app_id: "some.app.id.989",
            encryption_version: 1
          }
        end

        it "returns an ok response" do
          subject
          expect(response).to have_http_status(:ok)
        end

        it "has stored the attributes in the new Secret" do
          subject
          own_secret.reload

          expect(own_secret.name).to eq("Name")
          expect(own_secret.website).to eq("dropbox.com")
          expect(own_secret.username).to eq("username")
          expect(own_secret.notes).to eq("notes")
          expect(own_secret.password).to eq("very_secret_string")
          expect(own_secret.otp).to eq("otp_secret")
          expect(own_secret.user).to eq(user)
          expect(own_secret.android_app_id).to eq("some.app.id.989")
          expect(own_secret.encryption_version).to eq(1)
        end
      end

      context "with missing new encryption parameters" do
        let(:params) do
          {
            name: "Name",
            website: "dropbox.com",
            username: "username",
            notes: "notes",
            password: "very_secret_string",
            otp: "otp_secret",
            android_app_id: "some.app.id.989",
            encryption_version: 2,
            template: nil,
            encrypted_dek: nil,
            encrypted_overview: nil,
            encrypted_details: nil
          }
        end

        it "returns an unprocessable entity response" do
          subject

          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context "with new encryption parameters" do
        let(:params) do
          {
            name: "Name",
            website: "dropbox.com",
            username: "username",
            notes: "notes",
            password: "very_secret_string",
            otp: "otp_secret",
            android_app_id: "some.app.id.989",
            encryption_version: 2,
            template: "template",
            encrypted_dek: "I'm encrypted",
            encrypted_overview: "I'm also encrypted",
            encrypted_details: "Me too"
          }
        end

        it "returns an ok response" do
          subject

          expect(response).to have_http_status(:ok)
        end
      end

      context "with invalid parameters" do
        it "returns a unprocessable_entity (422) response" do
          params = {template: nil, encryption_version: 2}
          patch api_v3_secret_url(own_secret), params: params, headers: headers

          expect(response).to have_http_status(:unprocessable_entity)
        end

        it "returns a unprocessable_entity (422) response for old encryption" do
          params = {name: nil, encryption_version: 1}
          patch api_v3_secret_url(own_secret), params: params, headers: headers
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context "for someone else's secret" do
        it "renders a not found response" do
          patch api_v3_secret_url(other_secret), headers: headers, as: :json

          expect(response).to have_http_status(:not_found)
        end
      end

      context "for a non-existent secret" do
        it "renders a not found response" do
          patch api_v3_secret_url(SecureRandom.uuid), headers: headers, as: :json

          expect(response).to have_http_status(:not_found)
        end
      end
    end

    describe "DELETE /destroy" do
      it_should_behave_like "an authenticated endpoint" do
        let(:method) { :delete }
        let(:path) { api_v3_secret_path(own_secret) }
        let(:expected_status_code) { :ok }
      end

      context "without an access token" do
        it "returns an access denied code" do
          delete api_v3_secret_url(own_secret), as: :json
          expect(response).to have_http_status(:unauthorized)
        end
      end

      context "with a valid access token" do
        # Make sure the secret exists to test deletion
        before { own_secret }

        it "soft deletes the requested secret where it's not in the active scope anymore" do
          expect do
            delete api_v3_secret_url(own_secret), headers: headers, as: :json
          end.to change { Item.active.count }.by(-1)
        end

        it "returns an ok response" do
          delete api_v3_secret_url(own_secret), headers: headers, as: :json
          expect(response).to have_http_status(:ok)
        end

        context "for someone else secret" do
          it "renders a not found response" do
            delete api_v3_secret_url(other_secret), headers: headers, as: :json
            expect(response).to have_http_status(:not_found)
          end
        end

        context "for a non-existent secret" do
          it "renders a not found response" do
            delete api_v3_secret_url(SecureRandom.uuid),
              headers: headers,
              as: :json
            expect(response).to have_http_status(:not_found)
          end
        end
      end
    end

    describe "POST /search" do
      let(:domain) { nil }
      let(:params) { {domain: domain} }
      let!(:secret) do
        create(:item, user: user, website: "www.dropbox.com", brand: brand)
      end

      it_should_behave_like "an authenticated endpoint" do
        let(:method) { :post }
        let(:path) { search_api_v3_secrets_path }
        let(:expected_status_code) { :ok }
      end

      # create some other secrets as well
      before { create_list(:item, 3, user: user) }

      before do
        post search_api_v3_secrets_path,
          params: params,
          headers: headers,
          as: :json
      end

      context "with the exact domain" do
        let(:domain) { "dropbox.com" }

        it "returns the matching secret" do
          expect(response).to have_http_status(:ok)
          expect(json_response).to include(
            {
              android_app_id: nil,
              domain: "dropbox.com",
              template: secret.template,
              encrypted_key: secret.encrypted_dek,
              encrypted_dek: secret.rsa_encrypted_dek,
              encrypted_details: secret.encrypted_details,
              encrypted_overview: secret.encrypted_overview,
              encryption_version: 2,
              id: secret.id,
              username: secret.username,
              name: secret.name,
              notes: secret.notes,
              otp: secret.otp,
              password: secret.password,
              website: secret.website,
              created_at: secret.created_at.utc.as_json,
              updated_at: secret.updated_at.utc.as_json,
              brand: {
                breached_at: nil,
                breach_published_at: nil,
                main_color_hex: brand.main_color_hex,
                main_color_brightness: brand.main_color_brightness,
                logo_source: brand.logo_source,
                is_fetched: true,
                logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
                domain: brand.domain,
                public_suffix_domain: brand.domain,
                name: brand.name,
                totp: brand.totp,
                id: brand.id,
                images: {
                  icon: {
                    png: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
                    },
                    svg: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
                    },
                    theme: "dark"
                  }
                }
              },
              share: nil,
              # Migration fields
              legacy_encryption_disabled: false
            }
          )
          expect(json_response.size).to be 4
        end
      end

      context "when searching for a different subdomain" do
        let(:domain) { "messenger.facebook.com" }

        it "does not return the secret" do
          expect(response).to have_http_status(:ok)
          expect(json_response).to eq([])
        end
      end
    end
  end
end
