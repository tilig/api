require "rails_helper"

describe "Api::V3::Keypairs", type: :request, v3: true do
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:headers) { {Authorization: "Bearer #{access_token}"} }
  let(:params) { {} }
  let(:keypair) { create(:keypair, user: user) }

  let(:private_key) do
    '-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIKZ8vLJtU5EzqbMfhRJE1bPhdbIPYC7OBlSOoIjJquS7oAoGCCqGSM49
AwEHoUQDQgAEj3qy5L8FlEMjj0PIvVLbOTSCKR+rizHzVqMsvRKA/4O63XMEEwGd
uv5bl+/B1f1sTI4UyymYZ+jTpnrVgjs+qQ==
-----END EC PRIVATE KEY-----'
  end
  let(:invalid_private_key) do
    '-----BEGIN EC PRIVATE KEY-----
MHcCAQEEINmawoJxtxjYS77CJDRIdDAEsx8q+7sb7HbGAqgYW5TkoAoGCCqGSM49
AwEHoUQDQgAEC8syfrpKh5/b9nzUG3j1+82FmnZ1ughc8LY8Iebc0MTBBLYWAYqm
bz1anI/0CrCwHnyedpFCCh6HakN/Kllwnw==
-----END EC PRIVATE KEY-----'
  end

  let(:token) { JWT.encode(payload, OpenSSL::PKey::EC.new(private_key), "ES256") }
  let(:invalid_token) { JWT.encode(payload, OpenSSL::PKey::EC.new(invalid_private_key), "ES256") }

  let(:payload) {
    {
      kid: "t5DMJYRHJZ4Mp6dF63PfFQi3xO6jfIjKASLSq97jJEM",
      key: {
        encrypted_private_key: "Er4DEoADbjH9BUjxIyxSyPQs5XcVuKOv_1w4fc6Z--nUCiRF2sF4IoArBe4Te_7G7ae52JZjlM0ZdftsxvTI8ckkZEQSPc8CHYmz_xLW1TJq_Mo2pa4AbhY2_bbBzemEeRelpWxee5wYW-2s4-cUB1tq0Z_mLfoMr9LFAfoXlYgg8b9ouUrnrKiJQG-hweACvTsjtyseAsQ4KJrB10byhpOKcdDv8xZS2dGQImZqamAgUNvH3o_0eGXTWlygHYppMjnYn78WjwkPMf_rRRsEw_Mi73_KXqvduTQQ6JFRh2_p6qIagRpajIUkRiLBWl3EvO4-xc77i_U9r3xNhP9YpAo_pfcqubjs-r54f5jAotMUIcQwsCy3ubtgZW8BlPo6xeEVEolWFrJSZcIpvHJoC_51jH44PYp3JkwRDivwQofWqtEQ-QVq8thQpNQacnfAeuu6zVcxalJKWHiIYBtPJcpK_PHT7y3A2STM6_dkrnjMU4-K8JrAvyl3sOugcRdQRC4pl46IGhxpeWlPaWF0ZGowZDVwcEFEa055UVl0azl0T2gxIhV3b3V0ZXJ0aWxpZ0BnbWFpbC5jb20oraeUmwYaRjBEAiAEOb0r20UgeoaFLd2juclyUBTfcESqdJPjHKHodu1xGgIgNjjZE2yyWLRKD5twltphumAs_gGYNTCuLYUAOUCNOEY",
        public_key: "XXAqsNbMz760m_OX2S4e-hhtcoHR5M8uFZCC8wf451w",
        key_type: "x25519"
      },
      meta: {
        app_platform: "platform",
        app_version: "version"
      },
      kind: "encrypted-keypair",
      aud: "tilig-saas",
      iss: "tilig-ks",
      iat: iat,
      sub: "iyiOiatdj0d5ppADkNyQYtk9tOh1#key-t5DMJYRHJZ4Mp6dF63PfFQi3xO6jfIjKASLSq97jJEM"
    }
  }
  let(:iat) { Time.current.to_i }

  describe "POST /create" do
    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :post }
      let(:path) { api_v3_keypairs_path }
      let(:params) { {encrypted_keypair: token} }
    end

    context "invalid Authorization header" do
      it "returns a Unauthorization code" do
        post api_v3_keypairs_url, params: {encrypted_keypair: token}, headers: {Authorization: "Bearer #{SecureRandom.hex(16)}"}, as: :json
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "valid Authorization header and params" do
      it "renders the correct information" do
        post api_v3_keypairs_url, params: {encrypted_keypair: token}, headers: headers, as: :json
        expect(response).to be_successful
        expect(json_response).to match(
          {
            encrypted_private_key: user.keypair.encrypted_private_key,
            public_key: user.keypair.public_key,
            key_type: user.keypair.key_type,
            meta_data: {
              app_platform: user.keypair.meta_data.dig("app_platform"),
              app_version: user.keypair.meta_data.dig("app_version")
            },
            kid: user.keypair.kid
          }
        )
      end
    end

    context "valid Authorization header and params but Keypair already created" do
      before { keypair }

      it "renders error 'Keypair already created'" do
        post api_v3_keypairs_url, params: {encrypted_keypair: token}, headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response).to match({base: "Keypair already created"})
      end
    end

    context "valid Authorization header but Keypair is invalid" do
      it "renders error 'JWT payload invalid'" do
        post api_v3_keypairs_url, params: {encrypted_keypair: invalid_token}, headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response).to match({base: "JWT payload invalid"})
      end
    end
  end
end
