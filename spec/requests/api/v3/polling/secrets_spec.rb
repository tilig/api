require "rails_helper"
require "fixtures/brandfetch/fixtures"

describe "Api::V3::Polling::Secrets", type: :request, v3: true do
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let!(:brand) do
    create(
      :brand,
      name: "Dropbox",
      domain: "dropbox.com",
      public_suffix_domain: "dropbox.com",
      logo_source: "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg",
      logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
      main_color_brightness: "88",
      main_color_hex: "#0061ff",
      totp: true,
      brandfetch_json: {filled: true},
      fetch_date: Time.current
    )
  end

  let!(:own_secret) { create(:item, user: user, brand: brand, share_link: create(:share_link)) }
  let!(:other_secret) { create(:item, user: user) }
  let(:default_headers) { {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"} }
  let(:headers) { {Authorization: "Bearer #{access_token}"}.merge(default_headers) }
  let(:params) { nil }

  # let(:membership) { create(:membership, user: user) }

  before do
    other_secret.soft_destroy
  end

  describe "For User Secrets" do
    describe "GET /index" do
      it_should_behave_like "an authenticated endpoint" do
        let(:method) { :get }
        let(:path) { api_v3_polling_secrets_path }
      end

      it "only renders the secrets of the user" do
        get api_v3_polling_secrets_path, headers: headers

        expect(json_response[:updates].size).to eq 1
        expect(json_response[:updates]).to eq(
          [
            {
              id: own_secret.id,
              template: "login/1",
              domain: "dropbox.com",
              website: "dropbox.com",
              encryption_version: 2,
              encrypted_key: own_secret.encrypted_dek,
              encrypted_dek: own_secret.rsa_encrypted_dek,
              encrypted_details: own_secret.encrypted_details,
              encrypted_overview: own_secret.encrypted_overview,
              android_app_id: nil,
              username: nil,
              name: nil,
              notes: nil,
              otp: nil,
              password: nil,
              created_at: own_secret.created_at.utc.as_json,
              updated_at: own_secret.updated_at.utc.as_json,
              brand: {
                breached_at: nil,
                breach_published_at: nil,
                main_color_hex: brand.main_color_hex,
                main_color_brightness: brand.main_color_brightness,
                logo_source: brand.logo_source,
                logo_icon_source: brand.logo_icon_source,
                domain: brand.domain,
                public_suffix_domain: brand.domain,
                name: brand.name,
                totp: brand.totp,
                id: brand.id,
                is_fetched: true,
                images: {
                  icon: {
                    png: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
                    },
                    svg: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
                    },
                    theme: "dark"
                  }
                }
              },
              share: {
                id: own_secret.share_link.id,
                times_viewed: own_secret.share_link.times_viewed,
                encrypted_master_key: own_secret.share_link.encrypted_master_key
              },
              # Migration fields
              legacy_encryption_disabled: false
            }
          ]
        )
        expect(json_response[:deletions].first).to eq other_secret.id
        expect(json_response[:deletions].size).to eq 1
        expect(json_response[:fetched_at]).to_not be_nil
      end

      context "add a timestamp to get updated secrets" do
        let(:params) { {after: 1.day.ago.iso8601} }

        it "Only returns the newest secrets after the given timestamp" do
          create :item, updated_at: 3.days.ago, user: user
          create :item, user: user, website: "myblog.blogspot.com", updated_at: 3.days.ago, deleted_at: 3.days.ago

          get api_v3_polling_secrets_path, params: params, headers: headers

          expect(json_response[:updates].size).to eq 1
          expect(json_response[:updates].first[:id]).to eq(own_secret.id)

          expect(json_response[:deletions].size).to eq 1
          expect(json_response[:deletions].first).to eq(other_secret.id)
        end
      end

      context "invalid datetime params " do
        let(:params) { {after: "this is no date"} }

        it "will return a 200" do
          create :item, name: "new", user: user
          get api_v3_polling_secrets_path, params: params, headers: headers
          expect(response.status).to eq 200
        end
      end

      context "after params that is larger as 7 days in the present" do
        let(:params) { {after: 10.days.ago.iso8601} }

        it "will return only the secrets with a updated_at date thtat is maz 7 days ago" do
          create :item, updated_at: 8.days.ago, user: user
          create :item, user: user, website: "myblog.blogspot.com", updated_at: 8.days.ago, deleted_at: 8.days.ago

          get api_v3_polling_secrets_path, params: params, headers: headers
          expect(json_response[:updates].size).to eq 1
          expect(json_response[:updates].first[:id]).to eq(own_secret.id)

          expect(json_response[:deletions].size).to eq 1
          expect(json_response[:deletions].first).to eq(other_secret.id)
        end
      end
    end
  end
end
