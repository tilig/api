require "rails_helper"

describe "Api::V3::Polling::Items", type: :request, v3: true do
  let(:user) { create(:user, :with_keypair) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:default_headers) { {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"} }
  let(:headers) { {Authorization: "Bearer #{access_token}"}.merge(default_headers) }

  let!(:brand) do
    create(
      :brand,
      name: "Dropbox",
      domain: "dropbox.com",
      public_suffix_domain: "dropbox.com",
      logo_source: "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg",
      logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
      main_color_brightness: "88",
      main_color_hex: "#0061ff",
      totp: true,
      brandfetch_json: {filled: true},
      fetch_date: Time.current
    )
  end
  let!(:own_item) { create(:item, user: user, brand: brand, share_link: create(:share_link)) }
  let!(:own_item_2) { create(:item, user: user) }
  let!(:own_item_3) { create(:item, user: user) }

  let(:folder) { create(:folder) }
  let!(:folder_membership) { create(:folder_membership, user: user, folder: folder, role: "admin", accepted_at: Time.zone.now) }
  let!(:folder_item) { create(:item, folder: folder, user: nil, encrypted_folder_dek: "leflef") }
  let!(:folder_item_2) { create(:item, folder: folder, user: nil, encrypted_folder_dek: "leflef") }

  let(:params) { nil }

  before do
    own_item_3.soft_destroy
    folder_item_2.soft_destroy
  end

  describe "GET /index" do
    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :get }
      let(:path) { api_v3_polling_items_path }
    end

    it "only renders the items of the user" do
      get api_v3_polling_items_path, headers: headers

      expect(json_response[:updates].size).to eq 3
      expect(json_response[:updates].first).to eq(
        {
          id: own_item.id,
          template: "login/1",
          domain: "dropbox.com",
          website: "dropbox.com",
          encryption_version: 2,
          encrypted_dek: own_item.encrypted_dek,
          encrypted_folder_dek: own_item.encrypted_folder_dek,
          rsa_encrypted_dek: own_item.rsa_encrypted_dek,
          encrypted_details: own_item.encrypted_details,
          encrypted_overview: own_item.encrypted_overview,
          folder: nil,
          android_app_id: nil,
          username: nil,
          name: nil,
          notes: nil,
          otp: nil,
          password: nil,
          created_at: own_item.created_at.utc.as_json,
          updated_at: own_item.updated_at.utc.as_json,
          brand: {
            breached_at: nil,
            breach_published_at: nil,
            main_color_hex: brand.main_color_hex,
            main_color_brightness: brand.main_color_brightness,
            logo_source: brand.logo_source,
            logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
            domain: brand.domain,
            public_suffix_domain: brand.domain,
            name: brand.name,
            totp: brand.totp,
            id: brand.id,
            is_fetched: true,
            images: {
              icon: {
                png: {
                  original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
                },
                svg: {
                  original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
                },
                theme: "dark"

              }
            }
          },
          share_link: {
            id: own_item.share_link.id,
            times_viewed: own_item.share_link.times_viewed,
            encrypted_master_key: own_item.share_link.encrypted_master_key,
            created_at: own_item.share_link.created_at.utc.as_json,
            updated_at: own_item.share_link.updated_at.utc.as_json
          },
          # Migration fields
          legacy_encryption_disabled: false
        }
      )
      expect(json_response[:deletions].first).to eq own_item_3.id
      expect(json_response[:deletions].second).to eq folder_item_2.id
      expect(json_response[:deletions].size).to eq 2
      expect(json_response[:fetched_at]).to_not be_nil
    end

    context "add a timestamp to get updated secrets" do
      let(:params) { {after: 1.day.ago.iso8601} }

      it "Only returns the newest secrets after the given timestamp" do
        create :item, updated_at: 3.days.ago, user: user
        create :item, user: user, website: "myblog.blogspot.com", updated_at: 3.days.ago, deleted_at: 3.days.ago

        get api_v3_polling_items_path, params: params, headers: headers

        expect(json_response[:updates].size).to eq 3
        expect(json_response[:updates].first[:id]).to eq(own_item.id)

        expect(json_response[:deletions].size).to eq 2
        expect(json_response[:deletions].first).to eq(own_item_3.id)
      end
    end

    context "invalid datetime params " do
      let(:params) { {after: "this is no date"} }

      it "will return a 200" do
        create :item, name: "new", user: user
        get api_v3_polling_items_path, params: params, headers: headers
        expect(response.status).to eq 200
      end
    end

    context "after params that is larger as 7 days in the present" do
      let(:params) { {after: 10.days.ago.iso8601} }

      it "will return only the secrets with a updated_at date thtat is maz 7 days ago" do
        create :item, updated_at: 8.days.ago, user: user
        create :item, user: user, website: "myblog.blogspot.com", updated_at: 8.days.ago, deleted_at: 8.days.ago

        get api_v3_polling_items_path, params: params, headers: headers
        expect(json_response[:updates].size).to eq 3
        expect(json_response[:updates].first[:id]).to eq(own_item.id)

        expect(json_response[:deletions].size).to eq 2
        expect(json_response[:deletions].first).to eq(own_item_3.id)
      end
    end
  end
end
