require "rails_helper"

describe "Api::V3::TrackPerson", type: :request, v3: true do
  let(:current_user) { create(:user) }
  let(:access_token) { create(:identity, user: current_user).access_token }
  let(:default_headers) { {"Authorization" => "Bearer #{access_token}"} }
  let(:params) { {properties: {variant_survey: "A"}} }

  describe :create do
    let(:headers) { default_headers }

    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :post }
      let(:path) { api_v3_track_person_index_path }
      let(:expected_status_code) { :ok }
    end

    it "schedules a MixpanelJob with the provided arguments and current_user id" do
      expect do
        post api_v3_track_person_index_path,
          params: params,
          headers: headers

        expect(response).to have_http_status(:created)
      end.to(
        have_enqueued_job(MixpanelJob).once.with do |event, json|
          data = JSON.parse(json)["data"]
          expect(data["$distinct_id"]).to eq current_user.id
          expect(data["$set"]["$email"]).to eq current_user.email
          expect(data["$set"]["$first_name"]).to eq current_user.given_name
          expect(data["$set"]["$last_name"]).to eq current_user.family_name
          expect(data["$set"]["variant_survey"]).to eq "A"
        end
      )
    end
  end
end
