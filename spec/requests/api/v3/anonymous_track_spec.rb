require "rails_helper"

describe "Api::V3::AnonymousTrack", type: :request, v3: true do
  describe :create do
    let(:headers) do
      {
        "x-tilig-platform" => "Test Client",
        "x-tilig-version" => "0.1",
        "x-browser-name" => "Safari",
        "x-browser-version" => "15.3",
        "x-os-name" => "Intel Mac OS X 10_15_7"
      }
    end

    it "schedules a MixpanelJob with the provided arguments" do
      expect do
        post api_v3_anonymous_track_index_path,
          params: {event: "Uninstall Survey Answered", properties: {test_property: "yes"}},
          headers: headers

        expect(response).to have_http_status(:ok)
      end.to(
        have_enqueued_job(MixpanelJob).once.with do |event, json|
          data = JSON.parse(json)["data"]
          case event
          when :event
            expect(data["event"]).to eq "Uninstall Survey Answered"
            expect(data["properties"]["distinct_id"]).to be nil
            expect(data["properties"]["x-tilig-platform"]).to eq("Test Client")
            expect(data["properties"]["x-tilig-version"]).to eq("0.1")
            expect(data["properties"]["browserName"]).to eq("Safari")
            expect(data["properties"]["browserVersion"]).to eq("15.3")
            expect(data["properties"]["$os"]).to eq("Intel Mac OS X 10_15_7")
            expect(data["properties"]["test_property"]).to eq "yes"
          when :profile_update
            raise Error
          else
            raise Error
          end
        end
      )
    end
  end
end
