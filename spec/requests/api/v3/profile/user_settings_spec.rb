require "rails_helper"

describe "Api::V3::Profile::UserSettings", type: :request, v3: true do
  let(:profile) { create(:profile) }
  let(:user) { profile.user }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let(:headers) { {Authorization: authorization} }
  let(:params) { {user_settings: {receive_notifications: true}} }

  describe "PUT /api/v3/profile/user_settings" do
    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :put }
      let(:path) { api_v3_profile_user_settings_path }
    end

    describe "when other values are already stored in user settings" do
      let(:profile) { create(:profile, user_settings: {onboarding: {finished: true}}) }

      it "updates the user settings of the current_user" do
        patch "/api/v3/profile/user_settings", params:, headers:, as: :json

        expect(response).to have_http_status(:ok)
        expect(json_response).to match(
          {
            onboarding: {
              finished: true
            },
            receive_notifications: true
          }
        )
      end
    end

    describe "when the same keys are already in the settings" do
      let(:params) { {user_settings: {onboarding: {step_1: {uploaded_photo: true}}}} }
      let(:profile) { create(:profile, user_settings: {onboarding: {step_1: {finished: true}}}) }

      it "deep merges the user settings" do
        patch "/api/v3/profile/user_settings", params:, headers:, as: :json

        expect(response).to have_http_status(:ok)
        expect(json_response).to match(
          {
            onboarding: {
              step_1: {
                finished: true,
                uploaded_photo: true
              }
            }
          }
        )
      end
    end
  end
end
