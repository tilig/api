require "rails_helper"
require "fixtures/mailchimp/stubs"

describe "Api::V3::Profile::Referrals", type: :request, v3: true do
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:default_headers) { {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"} }
  let(:headers) { {Authorization: "Bearer #{access_token}"}.merge(default_headers) }
  let!(:referral_user) { create(:user) }

  let(:params) { {referral: {referral_link: referral_user.profile.application_settings["referral_link"]}} }

  before { stub_mailchimp_request }

  describe "PUT /update" do
    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :put }
      let(:path) { api_v3_profile_referrals_path }
    end

    context "with valid parameters" do
      it "changes the refereal count" do
        expect do
          put api_v3_profile_referrals_path, headers: headers, params: params
        end.to change { referral_user.profile.reload.referral_count }.by(1)
      end

      it "returns the referal link in the profile applications settings" do
        put api_v3_profile_referrals_path, headers: headers, params: params

        expect(json_response.dig(:profile, :application_settings)).to match(
          {
            referral_link: user.profile.application_settings["referral_link"],
            used_referral_link: referral_user.profile.application_settings["referral_link"]
          }
        )
      end

      it "adds the referral link" do
        put api_v3_profile_referrals_path, headers: headers, params: params

        expect(user.profile.reload.application_settings["used_referral_link"]).to eq(referral_user.profile.application_settings["referral_link"])
      end

      it "tracks the 'Referral Redeemed'" do
        expect_any_instance_of(Trackable).to receive(:track).with("Referral Redeemed")
        put api_v3_profile_referrals_path, headers: headers, params: params
      end

      it "Calls the transaction email job once" do
        expect(Mailchimp::ReferralThankYouTemplate).to receive(:new).once.and_call_original
        put api_v3_profile_referrals_path, headers: headers, params: params
      end

      context "when a referral does not exists with a user" do
        let(:params) { {referral: {referral_link: "squirrel-apTGWpStv4"}} }

        it "Calls the transaction email job once" do
          expect(Mailchimp::ReferralThankYouTemplate).not_to receive(:new)

          put api_v3_profile_referrals_path, headers: headers, params: params
        end
      end
    end
  end
end
