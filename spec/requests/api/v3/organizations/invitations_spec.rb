require "rails_helper"

describe "Api::V3::Organizations::Invitations", type: :request, v3: true do
  let(:user) { create(:user, :with_organization) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:default_headers) { {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"} }
  let(:headers) { {Authorization: "Bearer #{access_token}"}.merge(default_headers) }

  let(:organization) { user.organization }
  let(:invited_user) { create(:user) }

  # th mailchip task needs to be added

  before {
    stub_request(:post, "https://mandrillapp.com/api/1.0/messages/send-template.json").and_return(status: 200, body: {}.to_json, headers: {})
  }

  describe "GET /show" do
    let(:invitation) { create(:invitation, creator: user, email: invited_user.email) }
    let(:params) {
      {
        invitation: {view_token: invitation.view_token}
      }
    }

    context "with valid parameters" do
      it "returns the Invitation" do
        get api_v3_organization_invitation_path(invitation), params: params, headers: headers, as: :json

        expect(json_response[:invitation]).to match({
          id: invitation.id,
          accepted_at: nil,
          expire_at: invitation.expire_at.as_json,
          created_at: invitation.created_at.as_json,
          updated_at: invitation.updated_at.as_json,
          creator: {
            id: invitation.creator.id,
            email: invitation.creator.email,
            display_name: invitation.creator.display_name,
            picture: invitation.creator.picture
          },
          organization: {
            id: invitation.organization.id,
            name: invitation.organization.name
          }
        })
      end

      context "when the creator of the invitation is deleted" do
        before { invitation.update_column(:creator_id, nil) }

        it "returns the Invitation without creator" do
          get api_v3_organization_invitation_path(invitation), params: params, headers: headers, as: :json

          expect(json_response[:invitation]).to match({
            id: invitation.id,
            accepted_at: nil,
            expire_at: invitation.expire_at.as_json,
            created_at: invitation.created_at.as_json,
            updated_at: invitation.updated_at.as_json,
            creator: nil,
            organization: {
              id: invitation.organization.id,
              name: invitation.organization.name
            }
          })
        end
      end
    end

    context "with invalid parameters" do
      context "invalid id" do
        let(:other_invitation) { create(:invitation, creator: create(:user), email: create(:user).email) }

        # ID invalid
        it "returns a not_found" do
          get api_v3_organization_invitation_path("empty"), params: params, headers: headers, as: :json
          expect(response).to have_http_status(:not_found)
        end

        # ID of other Invitation (therfore a token mismatch)
        it "returns a not_found" do
          get api_v3_organization_invitation_path(other_invitation), params: params, headers: headers, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end

      context "invalid view_token" do
        let(:params) {
          {
            invitation: {view_token: "empty"}
          }
        }
        let(:other_invitation) { create(:invitation, creator: create(:user), email: create(:user).email) }

        # Invalid view_token
        it "returns a not_found" do
          get api_v3_organization_invitation_path(other_invitation), params: params, headers: headers, as: :json
          expect(response).to have_http_status(:not_found)
          expect(json_response).to match({msg: "Not found"})
        end
        # No parameters
        it "returns a not_found" do
          expect { get api_v3_organization_invitation_path(other_invitation), headers: headers, as: :json }.to raise_error ActionController::ParameterMissing
        end
      end

      context "Invitation expired" do
        before { invitation.update!(expire_at: 2.days.ago) }

        it "returns a unprocessable_entity with error: 'Invitation is expired'" do
          get api_v3_organization_invitation_path(invitation), params: params, headers: headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to match({errors: {base: ["Invitation is expired"]}})
        end
      end

      context "Invitation already accepted" do
        before { invitation.update!(accepted_at: 2.days.ago, invitee: invited_user) }

        it "returns a unprocessable_entity with error: 'Invitation is already accepted'" do
          get api_v3_organization_invitation_path(invitation), params: params, headers: headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to match({errors: {base: ["Invitation is already accepted"]}})
        end
      end
    end
  end

  describe "POST /create" do
    let(:params) {
      {
        invitations: {
          emails: ["john.doe@tilig.com", "jane.doe@tilig.com"]
        }
      }
    }

    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :post }
      let(:path) { api_v3_organization_invitations_path }
    end

    context "with valid parameters" do
      it "creates a new Invitation" do
        expect do
          post api_v3_organization_invitations_path, params: params, headers: headers, as: :json
        end.to change { Invitation.count }.by(2)
      end

      it "returns a created response" do
        post api_v3_organization_invitations_path, params: params, headers: headers, as: :json

        new_invitation = Invitation.last
        expect(json_response[:invitations].last).to match({
          id: new_invitation.id,
          email: new_invitation.email,
          accepted_at: nil,
          pending: true,
          expired: false,
          expire_at: new_invitation.expire_at.as_json,
          created_at: new_invitation.created_at.as_json,
          updated_at: new_invitation.updated_at.as_json,
          creator: {
            id: new_invitation.creator.id,
            email: new_invitation.creator.email,
            display_name: new_invitation.creator.display_name,
            picture: new_invitation.creator.picture
          },
          invitee: nil
        })
      end

      it "tracks the 'Organization Invitations Created' and 'Organization Membership Created' event" do
        expect_any_instance_of(Trackable).to receive(:track).with("Team Invite Sent")
        expect_any_instance_of(Trackable).to receive(:track).with("Team Membership Created", {count: 2})
        post api_v3_organization_invitations_path, params: params, headers: headers, as: :json
      end
    end

    context "with invalid parameters" do
      it "returns an error when the email is invalid" do
        post api_v3_organization_invitations_path, params: {
          invitations: {
            emails: ["john.doe@tilig"] # No TLD
          }
        }, headers: headers, as: :json

        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors][0][:errors]).to include({email: ["is invalid"]})
      end

      it "returns an error when the email is too short" do
        post api_v3_organization_invitations_path, params: {
          invitations: {
            # Incorrect email (to short)
            emails: ["joh"]
          }
        }, headers: headers, as: :json

        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors][0][:errors]).to include({email: ["is invalid", "is too short (minimum is 4 characters)"]})
      end

      context "when the email is to short" do
        let!(:existand_user) { create(:user, :with_organization, email: "john.doe@tilig.com") }
        let(:params) { {invitations: {emails: ["john.doe@tilig.com"]}} }

        it "returns an error" do
          post api_v3_organization_invitations_path, params: params, headers: headers, as: :json

          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response[:errors][0][:errors]).to include({email: ["user already belongs to an organization"]})
        end
      end

      it "returns an error when the email is too long" do
        post api_v3_organization_invitations_path, params: {
          invitations: {
            # Incorrect email (to long)
            emails: ["this-is-a-verry-long-email-and-therfore-it-should-trigger-a-error-this-is-a-verry-long-email-and-therfore-it-should-trigger-a-error-this-is-a-verry-long-email-and-therfore-it-should-trigger-a-error-this-is-a-verry-long-email-and-therfore-it-should-trigger-a-error@tilig.com"]
          }
        }, headers: headers, as: :json

        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors][0][:errors]).to include({email: ["is too long (maximum is 254 characters)"]})
      end

      it "returns an error when there is no post body" do
        expect { post api_v3_organization_invitations_path, headers: headers, as: :json }.to raise_error ActionController::ParameterMissing
      end

      it "returns an error when there's a duplicate email" do
        post api_v3_organization_invitations_path, params: {
          invitations: {
            emails: ["john.doe@tilig.com", "john.doe@tilig.com"]
          }
        }, headers: headers, as: :json

        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors]).to match([
          {
            value: "john.doe@tilig.com",
            errors: {email: ["has already been taken"]}
          }
        ])
      end

      it "does not create any invites if one is invalid" do
        expect do
          post api_v3_organization_invitations_path, params: {
            invitations: {
              emails: ["john.doe@tilig.com", "invalid"]
            }
          }, headers: headers, as: :json
        end.not_to change { Invitation.count }
      end
    end
  end

  describe "POST /update" do
    let(:invitation) { create(:invitation, creator: user, email: invited_user.email) }
    let(:access_token) { create(:identity, user: invited_user).access_token }
    let(:headers) { {Authorization: "Bearer #{access_token}"}.merge(default_headers) }

    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :patch }
      let(:path) { api_v3_organization_invitation_path(invitation) }
      let(:params) { {} }
    end

    context "with valid parameters" do
      it "creates a new Invitation" do
        expect do
          patch api_v3_organization_invitation_path(invitation), headers: headers, as: :json
        end.to change { invitation.organization.users.count }.by(1)
      end

      it "accepts the invite" do
        patch api_v3_organization_invitation_path(invitation), headers: headers, as: :json

        invitation.reload

        expect(invitation.accepted?).to be true
        expect(json_response[:invitation]).to match({
          id: invitation.id,
          email: invitation.email,
          accepted_at: invitation.accepted_at.as_json,
          expire_at: invitation.expire_at.as_json,
          pending: false,
          expired: false,
          created_at: invitation.created_at.as_json,
          updated_at: invitation.updated_at.as_json,
          creator: {
            id: invitation.creator.id,
            email: invitation.creator.email,
            display_name: invitation.creator.display_name,
            picture: invitation.creator.picture
          },
          invitee: {
            id: invitation.invitee.id,
            email: invitation.invitee.email,
            display_name: invitation.invitee.display_name,
            picture: invitation.invitee.picture
          }
        })
      end

      it "tracks the 'Organization Invitation Accepted' event" do
        expect_any_instance_of(Trackable).to receive(:track).with("Team Invite Accepted")
        patch api_v3_organization_invitation_path(invitation), headers: headers, as: :json
      end
    end

    context "when invitation id is wrong" do
      let(:other_invitation) { create(:invitation, creator: create(:user), email: create(:user).email) }

      # Using id of other invitation (email differs)
      it "returns a unprocessable_entity" do
        patch api_v3_organization_invitation_path(other_invitation), headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context "Invitation expired" do
      before { invitation.update!(expire_at: 2.days.ago) }

      it "returns a unprocessable_entity with error: 'Invitation is expired'" do
        patch api_v3_organization_invitation_path(invitation), headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response).to match({errors: {base: ["Invitation is expired"]}})
      end
    end

    context "Invitation already accepted" do
      before { invitation.update!(accepted_at: 2.days.ago, invitee: invited_user) }

      it "returns a unprocessable_entity with error: 'Invitation is already accepted'" do
        patch api_v3_organization_invitation_path(invitation), headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response).to match({errors: {base: ["Invitation is already accepted"]}})
      end
    end

    context "Invitation email is diffrent from the logged in user" do
      before { invitation.update!(email: user.email) }

      it "returns a unprocessable_entity with error: 'Invitation email differs from logged in user's email'" do
        patch api_v3_organization_invitation_path(invitation), headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response).to match({errors: {base: ["Invitation email differs from logged in user's email"]}})
      end
    end

    context "Invitation email is diffrent from the logged in user" do
      before {
        invitation
        invited_user.update!(organization: user.organization)
      }

      it "returns a unprocessable_entity with error: 'The logged in user already belongs to an organization'" do
        patch api_v3_organization_invitation_path(invitation), headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response).to match({errors: {base: ["The logged in user already belongs to an organization"]}})
      end
    end
  end

  describe "POST /destroy" do
    let!(:invitation) { create(:invitation, creator: user, email: invited_user.email, organization: user.organization) }

    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :delete }
      let(:path) { api_v3_organization_invitation_path(invitation) }
      let(:params) { {} }
      let(:expected_status_code) { :ok }
    end

    context "with valid parameters" do
      it "respond with ok" do
        delete api_v3_organization_invitation_path(invitation), headers: headers, as: :json
        expect(response).to have_http_status(:ok)
        expect(json_response[:invitation]).to match({
          id: invitation.id,
          email: invitation.email,
          accepted_at: invitation.accepted_at.as_json,
          expire_at: invitation.expire_at.as_json,
          pending: true,
          expired: invitation.expired?,
          created_at: invitation.created_at.as_json,
          updated_at: invitation.updated_at.as_json,
          creator: {
            id: invitation.creator.id,
            email: invitation.creator.email,
            display_name: invitation.creator.display_name,
            picture: invitation.creator.picture
          },
          invitee: nil
        })
      end
      it "deletes the invitation" do
        expect do
          delete api_v3_organization_invitation_path(invitation), headers: headers, as: :json
        end.to change { Invitation.count }.by(-1)
      end

      it "tracks the 'Organization Invitation Deleted' event" do
        expect_any_instance_of(Trackable).to receive(:track).with("Team Invite Deleted")
        delete api_v3_organization_invitation_path(invitation), headers: headers, as: :json
      end
    end

    context "when invitation id is wrong" do
      let!(:other_invitation) { create(:invitation, creator: create(:user), email: create(:user).email) }

      # Using id of other invitation (email differs)
      it "returns a not_found" do
        delete api_v3_organization_invitation_path(other_invitation), headers: headers, as: :json
        expect(response).to have_http_status(:not_found)
      end
      it "does not delete the invitation" do
        expect do
          delete api_v3_organization_invitation_path(other_invitation), headers: headers, as: :json
        end.to change { Invitation.count }.by(0)
      end
    end
  end
end
