require "rails_helper"

describe "Api::V3::Organization", type: :request, v3: true do
  let(:organization) { create(:organization) }
  let(:user) { create(:user, :with_keypair, email: "ba@tilg.com", organization: organization) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:default_headers) { {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"} }
  let(:headers) { {Authorization: "Bearer #{access_token}"}.merge(default_headers) }

  describe "GET /show", bullet: :skip do
    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :get }
      let(:path) { api_v3_organization_path }
      let(:params) { {} }
    end

    context "when the user has an Organization" do
      let(:first_invitation) { create(:invitation, email: "ba@tilig.com", creator: user, organization: organization) }
      let(:last_invitation) { create(:invitation, email: "ab@tilig.com", organization: organization) }
      let(:other_user) { create(:user, :with_keypair, email: "ab@tilg.com", organization: organization) }

      before {
        first_invitation
        last_invitation
        other_user
      }

      it "returns the Organization", bullet: :skip do
        get api_v3_organization_path, headers: headers, as: :json

        expect(json_response).to match({
          organization: {
            id: organization.id,
            name: organization.name,
            created_at: organization.created_at.as_json,
            updated_at: organization.updated_at.as_json,
            users: [
              {
                id: other_user.id,
                email: other_user.email,
                display_name: other_user.display_name,
                picture: other_user.picture
              },
              {
                id: user.id,
                email: user.email,
                display_name: user.display_name,
                picture: user.picture
              }
            ],
            invitations: [{
              accepted_at: nil,
              pending: true,
              expired: false,
              created_at: last_invitation.created_at.as_json,
              creator: {
                display_name: last_invitation.creator.display_name,
                email: last_invitation.creator.email,
                id: last_invitation.creator.id,
                picture: nil
              },
              email: last_invitation.email,
              expire_at: last_invitation.expire_at.as_json,
              id: last_invitation.id,
              invitee: nil,
              updated_at: last_invitation.updated_at.as_json
            }, {
              accepted_at: nil,
              pending: true,
              expired: false,
              created_at: first_invitation.created_at.as_json,
              creator: {
                display_name: user.display_name,
                email: user.email,
                id: user.id,
                picture: nil
              },
              email: first_invitation.email,
              expire_at: first_invitation.expire_at.as_json,
              id: first_invitation.id,
              invitee: nil,
              updated_at: first_invitation.updated_at.as_json
            }]
          }
        })
      end

      context "when the creator of the invitation is deleted" do
        before { organization.invitations.first.update_column(:creator_id, nil) }

        it "returns the Organization without a creator" do
          get api_v3_organization_path, headers: headers, as: :json

          expect(json_response[:organization][:creator]).to be_nil
        end
      end
    end

    context "when the user has no Organization" do
      let(:organization) { nil }

      it "returns not_found" do
        get api_v3_organization_path, headers: headers, as: :json

        expect(response).to have_http_status(:not_found)
        expect(json_response).to match({msg: "Not found"})
      end
    end
  end

  describe "POST /create" do
    let(:user) { create(:user, :with_keypair) }

    let(:params) {
      {organization: {name: "Tilig"}}
    }

    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :post }
      let(:path) { api_v3_organization_path }
    end

    context "with valid parameters" do
      it "creates the Organization" do
        expect do
          post api_v3_organization_path, params: params, headers: headers, as: :json
        end.to change { Organization.count }.by(1)
      end

      it "returns a created response" do
        post api_v3_organization_path, params: params, headers: headers, as: :json

        new_organization = Organization.last
        expect(json_response).to match({
          organization: {
            id: new_organization.id,
            name: new_organization.name,
            created_at: new_organization.created_at.as_json,
            updated_at: new_organization.updated_at.as_json,
            users: [{
              id: user.id,
              email: user.email,
              display_name: user.display_name,
              picture: user.picture
            }],
            invitations: []
          }
        })
      end

      it "tracks the 'Organization Created' and 'Organization Membership Created' event" do
        expect_any_instance_of(Trackable).to receive(:track).with("Team Created")
        expect_any_instance_of(Trackable).to receive(:track).with("Team Membership Created", {count: 1})
        post api_v3_organization_path, params: params, headers: headers, as: :json
      end
    end

    context "with invalid parameters" do
      context "with empty name" do
        let(:params) {
          {organization: {name: ""}}
        }

        it "returns a unprocessable_entity" do
          post api_v3_organization_path, params: params, headers: headers, as: :json

          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to include({errors: {name: ["can't be blank"]}})
        end
      end

      context "with no parameters" do
        it "returns an error" do
          expect { post api_v3_organization_path, headers: headers, as: :json }.to raise_error ActionController::ParameterMissing
        end
      end
    end

    context "when Organization is already set" do
      let(:user) { create(:user, :with_keypair, organization: organization) }

      it "returns an error" do
        post api_v3_organization_path, params: params, headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response).to include({errors: {organization: ["already exists"]}})
      end
    end
  end

  describe "PATCH /update", bullet: :skip do
    let(:params) {
      {organization: {name: "Tilig Inc."}}
    }

    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :patch }
      let(:path) { api_v3_organization_path }
    end

    context "with valid parameters" do
      it "returns a created response" do
        patch api_v3_organization_path, params: params, headers: headers, as: :json

        organization.reload
        expect(json_response).to match({
          organization: {
            id: organization.id,
            name: "Tilig Inc.",
            created_at: organization.created_at.as_json,
            updated_at: organization.updated_at.as_json,
            users: [{
              id: user.id,
              email: user.email,
              display_name: user.display_name,
              picture: user.picture
            }],
            invitations: []
          }
        })
      end

      it "tracks the 'Organization Updated'" do
        expect_any_instance_of(Trackable).to receive(:track).with("Team Updated")
        patch api_v3_organization_path, params: params, headers: headers, as: :json
      end
    end

    context "with invalid parameters" do
      context "with empty name" do
        let(:params) {
          {organization: {name: ""}}
        }

        it "returns a unprocessable_entity" do
          patch api_v3_organization_path, params: params, headers: headers, as: :json

          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to include({errors: {name: ["can't be blank"]}})
        end
      end

      context "with no parameters" do
        it "returns an error" do
          expect { patch api_v3_organization_path, headers: headers, as: :json }.to raise_error ActionController::ParameterMissing
        end
      end
    end
  end
end
