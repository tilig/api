require "rails_helper"

describe "Api::V3::Answers", type: :request, v3: true do
  let(:access_token) { create(:identity).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let(:headers) { {Authorization: authorization} }

  let(:params) {
    {
      answer: {
        question_attributes: {
          content: "On what devices do you want to have your password and account information available?",
          survey_token: "e2120eb0-428b-471d-ba4a-9b109141acb6",
          token: "b291972e-caa6-4f8c-ad80-5ef91b5775b2",
          answer_options: ["Option A", "Option b", "Option C"]
        },
        chosen_options: ["Option A", "Option C"],
        skipped: false
      }
    }
  }

  describe "POST /api/v3/answers" do
    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :post }
      let(:path) { api_v3_answers_path }
    end

    it "tracks the Answer Created event" do
      expect {
        post api_v3_answers_path, params: params, headers: headers
      }.to(have_enqueued_job(MixpanelJob).twice.with do |event, json|
        data = JSON.parse(json)["data"]
        properties = data["properties"]
        if event == :event
          expect(data["event"]).to eq "Answer Created"
          expect(properties["question_content"]).to eq "On what devices do you want to have your password and account information available?"
          expect(properties["question_survey_token"]).to eq("e2120eb0-428b-471d-ba4a-9b109141acb6")
          expect(properties["question_token"]).to eq("b291972e-caa6-4f8c-ad80-5ef91b5775b2")
          expect(properties["question_answer_options"]).to eq ["Option A", "Option b", "Option C"]
          expect(properties["chosen_options"]).to eq ["Option A", "Option C"]
          expect(properties["skipped"]).to eq "false"

        end
      end)
    end

    it "allows the same question to be answerd by two users" do
      post api_v3_answers_path, params: params, headers: headers

      headers = {Authorization: "Bearer #{create(:identity).access_token}"}

      expect {
        post api_v3_answers_path, params: params, headers: headers
      }.to change(Answer, :count).by(1)
    end

    it "can skip an answer" do
      params[:answer].delete :chosen_options
      params[:answer][:skipped] = true

      expect {
        post api_v3_answers_path, params: params, headers: headers
      }.to change(Answer, :count).by(1)
      expect(Answer).to exist(skipped: true)
    end
  end

  describe "invalid request" do
    it "will send a sentry error" do
      params[:answer][:question_attributes] = nil

      expect(Sentry).to receive(:capture_exception).with(Api::V3::AnswersController::UnprocessableEntity)
      post api_v3_answers_path, params: params, headers: headers
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end
