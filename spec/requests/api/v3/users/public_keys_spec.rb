require "rails_helper"

describe "Api::V3::Users::PublicKeys", type: :request, v3: true do
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let(:headers) { {Authorization: authorization} }

  let(:email) { "john.doe@tilig.com" }
  let(:user_2) { create(:user, email: email) }
  let!(:user_2_keypair) { create(:keypair, user: user_2) }

  let(:params) { {email: "john.doe@tilig.com"} }

  context "#show" do
    it_should_behave_like "an authenticated endpoint" do
      let(:path) { api_v3_users_public_key_path }
      let(:method) { :get }
    end

    it "returns the public_key of the user that is the owner of the email" do
      get api_v3_users_public_key_path, params: params, headers: headers, as: :json

      expect(json_response[:user]).to eq({
        public_key: user_2_keypair.public_key,
        key_type: user_2_keypair.key_type,
        kid: user_2_keypair.kid,
        id: user_2.id,
        email: email
      })
    end

    context "with uppercase email" do
      let(:params) { {email: "JOHN.DOE@TILIG.COM"} }

      it "downcases the email before searching for the user" do
        get api_v3_users_public_key_path, params: params, headers: headers, as: :json

        expect(response).to have_http_status(:ok)
        expect(json_response[:user]).to include({public_key: user_2_keypair.public_key})
      end
    end

    context "when the email is not connected to a user" do
      let(:params) { {email: "does-not-exist@tilig.com"} }

      it "returns an not found" do
        get api_v3_users_public_key_path, params: params, headers: headers, as: :json

        expect(response).to have_http_status(:not_found)
      end
    end

    context "when the email missing" do
      let(:params) { nil }

      it "returns an not found" do
        get api_v3_users_public_key_path, params: params, headers: headers, as: :json

        expect(response).to have_http_status(:not_found)
      end
    end

    context "when the user has no keypair" do
      before { create(:user, email: "jane.doe@tilig.com") }

      let(:params) { {email: "jane.doe@tilig.com"} }

      it "returns an not found" do
        get api_v3_users_public_key_path, params: params, headers: headers, as: :json

        expect(response).to have_http_status(:not_found)
      end
    end

    context "when a lot of requests are made" do
      include ActiveSupport::Testing::TimeHelpers
      # Enable Rack::Attack for the test
      before do
        Rack::Attack.enabled = true
        Rack::Attack.reset!
      end

      # Disable Rack::Attack again
      after { Rack::Attack.enabled = false }

      let(:headers) { {:Authorization => authorization, "REMOTE_ADDR" => "1.2.3.4"} }

      it "successful for 10 requests, then blocks the IP" do
        30.times do
          get api_v3_users_public_key_path, params: params, headers: headers, as: :json
          expect(response).to have_http_status(:ok)
        end
        get api_v3_users_public_key_path, params: params, headers: headers, as: :json
        expect(response).to have_http_status(:too_many_requests)
        travel_to(2.minutes.from_now) do
          get api_v3_users_public_key_path, params: params, headers: headers, as: :json
          expect(response).to have_http_status(:ok)
        end
      end
    end
  end
end
