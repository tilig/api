require "rails_helper"

describe "Api::V3::Items", type: :request, v3: true do
  let(:user) { create(:user) }
  let(:user2) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let!(:brand) do
    create(
      :brand,
      name: "Dropbox",
      domain: "dropbox.com",
      public_suffix_domain: "dropbox.com",
      logo_source: "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg",
      logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
      main_color_brightness: "88",
      main_color_hex: "#0061ff",
      totp: true,
      brandfetch_json: {filled: true},
      fetch_date: Time.current
    )
  end

  let(:own_item) { create(:item, user: user, brand: brand, share_link: create(:share_link)) }
  let(:own_item_2) { create(:item, user: user, brand: brand, share_link: create(:share_link)) }
  let(:other_item) { create(:item) }
  let(:default_headers) { {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"} }
  let(:headers) { {Authorization: "Bearer #{access_token}"}.merge(default_headers) }
  let(:params) { nil }

  # let(:membership) { create(:membership, user: user) }
  # let(:group) { membership.group }
  # let(:group_id) { group.id }
  # let(:group_item) { create(:item, group: group, user: nil, brand: brand, share_link: create(:share_link)) }
  # let(:other_group_item) { create(:item, user: nil, group: create(:membership, user: user2).group, brand: brand) }

  describe "For User items" do
    describe "GET /index" do
      it_should_behave_like "an authenticated endpoint" do
        let(:method) { :get }
        let(:path) { api_v3_items_path }
      end

      # TODO: Fix bullet false positive
      before { Bullet.enable = false }
      after { Bullet.enable = true }

      it "only renders the items of the user" do
        own_item
        own_item_2
        other_item

        get api_v3_items_url, headers: headers

        expect(json_response[:items].size).to eq 2
        expect(json_response[:items].first).to eq(
          {
            id: own_item.id,
            template: "login/1",
            domain: "dropbox.com",
            website: "dropbox.com",
            encryption_version: 2,
            encrypted_dek: own_item.encrypted_dek,
            encrypted_folder_dek: own_item.encrypted_folder_dek,
            rsa_encrypted_dek: own_item.rsa_encrypted_dek,
            encrypted_details: own_item.encrypted_details,
            encrypted_overview: own_item.encrypted_overview,
            folder: nil,
            android_app_id: nil,
            username: nil,
            name: nil,
            notes: nil,
            otp: nil,
            password: nil,
            created_at: own_item.created_at.utc.as_json,
            updated_at: own_item.updated_at.utc.as_json,
            brand: {
              breached_at: nil,
              breach_published_at: nil,
              main_color_hex: brand.main_color_hex,
              main_color_brightness: brand.main_color_brightness,
              logo_source: brand.logo_source,
              logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
              domain: brand.domain,
              public_suffix_domain: brand.domain,
              name: brand.name,
              totp: brand.totp,
              id: brand.id,
              is_fetched: true,
              images: {
                icon: {
                  png: {
                    original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
                  },
                  svg: {
                    original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
                  },
                  theme: "dark"

                }
              }
            },
            share_link: {
              id: own_item.share_link.id,
              times_viewed: own_item.share_link.times_viewed,
              encrypted_master_key: own_item.share_link.encrypted_master_key,
              created_at: own_item.share_link.created_at.utc.as_json,
              updated_at: own_item.share_link.updated_at.utc.as_json
            },
            # Migration fields
            legacy_encryption_disabled: false
          }
        )
      end

      context "add a timestamp to get updated items" do
        it "Only returns the newest items after the given timestamp" do
          create :item, updated_at: 3.days.ago, user: user
          new_item = create :item, user: user, website: "myblog.blogspot.com"

          get api_v3_items_url, params: {after: 1.day.ago.iso8601}, headers: headers

          expect(json_response[:items].count).to eq 1
          expect(json_response[:items].first[:id]).to eq(new_item.id)
        end
      end

      context "invalid datetime params " do
        let(:params) { {after: "invalid_date"} }

        it "will return a 200" do
          create :item, name: "new", user: user
          get api_v3_items_url, params: params, headers: headers, as: :json
          expect(response.status).to eq 200
          expect(json_response[:items].count).to eq 1
        end
      end

      context "with items outside the default page scope" do
        before { stub_const("Paginatable::DEFAULT_PAGE_SIZE", 1) }

        it "paginates the items" do
          create :item, name: "new", user: user, template: "login/1"
          create :item, name: "old", user: user, template: "login/1", created_at: 1.day.ago

          get api_v3_items_url, headers: headers

          expect(json_response[:items].count).to eq 1
          expect(json_response[:items].first[:name]).to eq "old"
        end

        it "paginates to the next page" do
          create :item, name: "new", user: user, template: "login/1"
          create :item, name: "old", user: user, template: "login/1", created_at: 1.day.ago

          get api_v3_items_url, params: {page: {offset: 2}}, headers: headers

          expect(json_response[:items].count).to eq 1
          expect(json_response[:items].first[:name]).to eq "new"
        end
      end
    end

    describe "GET /show" do
      it_should_behave_like "an authenticated endpoint" do
        let(:method) { :get }
        let(:path) { api_v3_items_path(own_item) }
      end

      context "accessing the users own item" do
        it "renders the item" do
          get api_v3_item_url(own_item),
            headers: {
              Authorization: "Bearer #{access_token}"
            },
            as: :json

          expect(json_response[:item]).to eq(
            {
              android_app_id: nil,
              domain: "dropbox.com",
              encrypted_dek: own_item.encrypted_dek,
              rsa_encrypted_dek: own_item.rsa_encrypted_dek,
              encrypted_folder_dek: nil,
              folder: nil,
              encrypted_details: own_item.encrypted_details,
              encrypted_overview: own_item.encrypted_overview,
              encryption_version: 2,
              template: "login/1",
              id: own_item.id,
              username: own_item.username,
              name: own_item.name,
              notes: own_item.notes,
              otp: own_item.otp,
              password: own_item.password,
              website: own_item.website,
              created_at: own_item.created_at.utc.as_json,
              updated_at: own_item.updated_at.utc.as_json,
              brand: {
                breached_at: nil,
                breach_published_at: nil,
                main_color_hex: brand.main_color_hex,
                is_fetched: true,
                main_color_brightness: brand.main_color_brightness,
                logo_source: brand.logo_source,
                logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
                domain: brand.domain,
                public_suffix_domain: brand.domain,
                name: brand.name,
                totp: brand.totp,
                id: brand.id,
                images: {
                  icon: {
                    png: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
                    },
                    svg: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
                    },
                    theme: "dark"
                  }
                }
              },
              share_link: {
                id: own_item.share_link.id,
                times_viewed: own_item.share_link.times_viewed,
                encrypted_master_key: own_item.share_link.encrypted_master_key,
                created_at: own_item.share_link.created_at.utc.as_json,
                updated_at: own_item.share_link.updated_at.utc.as_json
              },
              # Migration fields
              legacy_encryption_disabled: false
            }
          )
        end

        it "works without a share present" do
          own_item.share_link.destroy!

          get api_v3_item_url(own_item), headers: headers

          expect(json_response[:item][:share_link]).to be_nil
        end
      end

      context "with legacy encryption disabled" do
        before { user.disable_legacy_encryption! }

        it "renders the item without legacy fields" do
          get api_v3_item_url(own_item),
            headers: {
              Authorization: "Bearer #{access_token}"
            },
            as: :json

          expect(json_response[:item]).to eq(
            {
              domain: "dropbox.com",
              website: "dropbox.com",
              encrypted_dek: own_item.encrypted_dek,
              encrypted_folder_dek: own_item.encrypted_folder_dek,
              rsa_encrypted_dek: own_item.rsa_encrypted_dek,
              folder: nil,
              encrypted_details: own_item.encrypted_details,
              encrypted_overview: own_item.encrypted_overview,
              encryption_version: 2,
              template: "login/1",
              id: own_item.id,

              created_at: own_item.created_at.utc.as_json,
              updated_at: own_item.updated_at.utc.as_json,
              brand: {
                breached_at: nil,
                breach_published_at: nil,
                main_color_hex: brand.main_color_hex,
                main_color_brightness: brand.main_color_brightness,
                logo_source: brand.logo_source,
                logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
                domain: brand.domain,
                public_suffix_domain: brand.domain,
                name: brand.name,
                totp: brand.totp,
                is_fetched: true,
                id: brand.id,
                images: {
                  icon: {
                    png: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
                    },
                    svg: {
                      original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
                    },
                    theme: "dark"
                  }
                }
              },
              share_link: {
                id: own_item.share_link.id,
                encrypted_master_key: own_item.share_link.encrypted_master_key,
                times_viewed: own_item.share_link.times_viewed,
                created_at: own_item.share_link.created_at.utc.as_json,
                updated_at: own_item.share_link.updated_at.utc.as_json
              },
              # Migration fields
              legacy_encryption_disabled: true
            }
          )
        end
      end

      context "accessing someone else item" do
        it "renders a not found (404) response" do
          get api_v3_item_url(other_item), headers: headers, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end

      context "accessing a non-existent item" do
        it "renders a not found response" do
          get api_v3_item_url(SecureRandom.uuid), headers: headers, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end

      describe "domain field" do
        describe "for a normal subdomain" do
          let(:own_item) do
            create(:item, user: user, website: "myblog.example.com")
          end

          it "returns the correct domain" do
            get api_v3_item_url(own_item),
              headers: {
                Authorization: "Bearer #{access_token}"
              },
              as: :json
            expect(json_response[:item][:domain]).to eq("example.com")
          end
        end

        describe "for a item that has private domain" do
          let(:own_item) do
            create(:item, user: user, website: "myblog.blogspot.com")
          end

          it "returns the correct domain" do
            get api_v3_item_url(own_item),
              headers: {
                Authorization: "Bearer #{access_token}"
              },
              as: :json
            expect(json_response[:item][:domain]).to eq("myblog.blogspot.com")
          end
        end
      end
    end

    describe "POST /create" do
      it_should_behave_like "an authenticated endpoint" do
        let(:method) { :post }
        let(:path) { api_v3_items_path(own_item) }
        let(:params) { {item: {name: "Name"}} }
      end

      context "with valid parameters" do
        let(:params) { {item: {name: "Name"}} }

        it "creates a new item" do
          expect do
            post api_v3_items_url, params: params, headers: headers, as: :json
          end.to change { Item.count }.by(1)
        end

        it "returns a created response" do
          post api_v3_items_url, params: params, headers: headers, as: :json
          expect(response).to have_http_status(:created)
        end

        it "has stored the attributes in the new item" do
          post api_v3_items_url, params: params, headers: headers, as: :json
          new_item = Item.last
          expect(new_item.name).to eq("Name")
          expect(new_item.website).to be_nil
          expect(new_item.username).to be_nil
          expect(new_item.notes).to be_nil
          expect(new_item.password).to be_nil
          expect(new_item.otp).to be_nil
          expect(new_item.user).to eq(user)
        end

        it "stores which platform did the request" do
          post api_v3_items_url, params: params, headers: headers, as: :json

          expect(Item.last.create_platform).to eq "Web"
        end

        it "tracks the 'Account Created' event" do
          expect_any_instance_of(Trackable).to receive(:track).with(
            "Account Created"
          )
          post api_v3_items_url, params: params, headers: headers, as: :json
        end
      end

      context "with all parameters" do
        let(:params) do
          {item: {
            name: "Name",
            website: "dropbox.com",
            notes: "notes",
            password: "very_item_string",
            otp: "otp_item"
          }}
        end

        before { brand }

        it "creates a new item" do
          expect do
            post api_v3_items_url, params: params, headers: headers, as: :json
          end.to change { Item.count }.by(1)
        end

        it "renders a JSON response with the new item" do
          post api_v3_items_url, params: params, headers: headers, as: :json
          expect(response).to have_http_status(:created)
        end

        it "has stored the attributes in the new item" do
          post api_v3_items_url, params: params, headers: headers, as: :json
          new_item = Item.last
          expect(new_item.name).to eq("Name")
          expect(new_item.website).to eq("dropbox.com")
          expect(new_item.notes).to eq("notes")
          expect(new_item.password).to eq("very_item_string")
          expect(new_item.otp).to eq("otp_item")
          expect(new_item.user).to eq(user)
          expect(new_item.brand).to eq(brand)
        end
      end

      context "with invalid parameters" do
        let(:params) { {item: {wrong_attr: "wrong_value"}} }

        it "does not create a new item" do
          expect do
            post api_v3_items_url, params: params, headers: headers, as: :json
          end.to change { Item.count }.by(0)
        end

        it "returns an unprocessable_entity response" do
          post api_v3_items_url, params: params, headers: headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context "with legacy decryption disabled" do
        let(:params) do
          {item: {
            name: "NO NAME",
            notes: "NO NOTES",
            password: "NO PASSWORD",
            otp: "NO OTP",
            username: "NO USERNAME",

            template: "template",
            encryption_version: 2,
            website: "dropbox.com",
            rsa_encrypted_dek: "dek=",
            encrypted_overview: "overview",
            encrypted_details: "details"
          }}
        end

        before { user.disable_legacy_encryption! }

        it "doest not store the legacy fields" do
          post api_v3_items_url, params: params, headers: headers, as: :json
          new_item = Item.last
          expect(new_item.name).to be_nil
          expect(new_item.notes).to be_nil
          expect(new_item.password).to be_nil
          expect(new_item.otp).to be_nil
          expect(new_item.username).to be_nil

          expect(new_item.website).to eq("dropbox.com")
          expect(new_item.user).to eq(user)
          expect(new_item.brand).to eq(brand)
          expect(new_item.rsa_encrypted_dek).to eq("dek=")
        end
      end
    end

    describe "PATCH /update" do
      subject { patch api_v3_item_url(own_item), params: params, headers: headers }

      it_should_behave_like "an authenticated endpoint" do
        let(:method) { :patch }
        let(:path) { api_v3_item_url(own_item) }
        let(:params) { {item: {name: "Name"}} }
      end

      context "with valid parameters" do
        let(:params) { {item: {name: "Name"}} }

        it "updates the requested item" do
          subject

          expect(own_item.reload.name).to eq("Name")
        end

        it "returns an ok response" do
          subject
          expect(response).to have_http_status(:ok)
        end
      end

      context "with all parameters" do
        let(:params) do
          {item: {
            name: "Name",
            website: "dropbox.com",
            username: "username",
            notes: "notes",
            password: "very_item_string",
            otp: "otp_item",
            android_app_id: "some.app.id.989",
            encryption_version: 1
          }}
        end

        it "returns an ok response" do
          subject
          expect(response).to have_http_status(:ok)
        end

        it "stores which platform did the request" do
          subject

          expect(Item.last.update_platform).to eq "Web"
        end

        it "has stored the attributes in the new item" do
          subject
          own_item.reload

          expect(own_item.name).to eq("Name")
          expect(own_item.website).to eq("dropbox.com")
          expect(own_item.username).to eq("username")
          expect(own_item.notes).to eq("notes")
          expect(own_item.password).to eq("very_item_string")
          expect(own_item.otp).to eq("otp_item")
          expect(own_item.user).to eq(user)
          expect(own_item.android_app_id).to eq("some.app.id.989")
          expect(own_item.encryption_version).to eq(1)
        end
      end

      context "with missing new encryption parameters" do
        let(:params) do
          {item: {
            name: "Name",
            website: "dropbox.com",
            username: "username",
            notes: "notes",
            password: "very_item_string",
            otp: "otp_item",
            android_app_id: "some.app.id.989",
            encryption_version: 2,
            template: nil,
            rsa_encrypted_dek: nil,
            encrypted_overview: nil,
            encrypted_details: nil
          }}
        end

        it "returns an unprocessable entity response" do
          subject

          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context "with new encryption parameters" do
        let(:params) do
          {item: {
            name: "Name",
            website: "dropbox.com",
            username: "username",
            notes: "notes",
            password: "very_item_string",
            otp: "otp_item",
            android_app_id: "some.app.id.989",
            encryption_version: 2,
            template: "template",
            rsa_encrypted_dek: "I'm encrypted",
            encrypted_overview: "I'm also encrypted",
            encrypted_details: "Me too"
          }}
        end

        it "returns an ok response" do
          subject

          expect(response).to have_http_status(:ok)
        end
      end

      context "with invalid parameters" do
        it "returns a unprocessable_entity (422) response" do
          params = {item: {template: nil, encryption_version: 2}}
          patch api_v3_item_url(own_item), params: params, headers: headers

          expect(response).to have_http_status(:unprocessable_entity)
        end

        it "returns a unprocessable_entity (422) response for old encryption" do
          params = {item: {name: nil, encryption_version: 1}}
          patch api_v3_item_url(own_item), params: params, headers: headers
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context "for someone else's item" do
        it "renders a not found response" do
          patch api_v3_item_url(other_item), headers: headers, as: :json

          expect(response).to have_http_status(:not_found)
        end
      end

      context "for a non-existent item" do
        it "renders a not found response" do
          patch api_v3_item_url(SecureRandom.uuid), headers: headers, as: :json

          expect(response).to have_http_status(:not_found)
        end
      end
    end

    describe "DELETE /destroy" do
      it_should_behave_like "an authenticated endpoint" do
        let(:method) { :delete }
        let(:path) { api_v3_item_path(own_item) }
        let(:expected_status_code) { :ok }
      end

      context "without an access token" do
        it "returns an access denied code" do
          delete api_v3_item_url(own_item), as: :json
          expect(response).to have_http_status(:unauthorized)
        end
      end

      context "with a valid access token" do
        # Make sure the item exists to test deletion
        before { own_item }

        it "soft deletes the requested item where it's not in the active scope anymore" do
          expect do
            delete api_v3_item_url(own_item), headers: headers, as: :json
          end.to change { Item.active.count }.by(-1)
        end

        it "returns an ok response" do
          delete api_v3_item_url(own_item), headers: headers, as: :json
          expect(response).to have_http_status(:ok)
        end

        context "for someone else item" do
          it "renders a not found response" do
            delete api_v3_item_url(other_item), headers: headers, as: :json
            expect(response).to have_http_status(:not_found)
          end
        end

        context "for a non-existent item" do
          it "renders a not found response" do
            delete api_v3_item_url(SecureRandom.uuid),
              headers: headers,
              as: :json
            expect(response).to have_http_status(:not_found)
          end
        end
      end
    end
  end

  describe "for folder items" do
    let(:folder) { create(:folder) }
    let(:folder_id) { folder.id }
    let(:folder_membership) { create(:folder_membership, user: user, folder: folder, role: "admin", accepted_at: Time.zone.now) }
    let(:folder_membership_id) { folder_membership.id }
    let(:group_item) { create(:item, folder: folder, user: nil, encrypted_folder_dek: "leflef") }
    let!(:keypair) { create(:keypair, user: user) }

    describe "GET /index", bullet: :skip do
      before do
        folder_membership
        group_item
      end

      it "returns a success response" do
        get api_v3_items_url, headers: headers

        expect(response).to be_successful
        expect(json_response[:items].count).to eq(1)
      end

      context "when the encrypted_private key isn't set on the membership" do
        let(:folder_membership) { create(:folder_membership, user: user, folder: folder, role: "admin", accepted_at: nil, encrypted_private_key: nil, email: "john@example.com") }

        it "doesn't return the related items" do
          get api_v3_items_url, headers: headers

          expect(json_response[:items].count).to eq(0)
        end
      end
    end

    describe "POST create" do
      let(:folder) { create(:folder, single_item: false) }
      let(:folder_id) { folder_membership.folder_id }
      let(:params) { {item: {name: "Name", folder_id: folder_id, encrypted_folder_dek: "blablabla", encryption_version: 2, encrypted_overview: "blablabla", encrypted_details: "blablabla", template: "login/1"}} }

      before { folder_membership }

      it "creates a new item" do
        expect do
          post api_v3_items_path, params: params, headers: headers, as: :json
        end.to change { Item.count }.by(1)

        expect(Item.last.folder_id).to eq(folder.id)
      end

      context "when the folder id is invalid" do
        let(:folder_id) { "e1325bcd-cec7-4ff4-8cad-5a461faad4b8" }

        it "returns an unprocessable entity response" do
          post api_v3_items_path, params: params, headers: headers, as: :json

          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response[:errors]).to include(folder_id: ["is invalid"])
        end
      end
    end

    describe "PATCH /update" do
      let(:folder) { create(:folder, single_item: false) }
      let(:params) { {item: {name: "New Name"}} }

      before { folder_membership }

      it "updates the requested item" do
        expect {
          put api_v3_item_path(group_item), params: params, headers: headers, as: :json
        }.to change { group_item.reload.name }.to("New Name")
      end

      context "when the folder_id is given to a normal item" do
        let(:item) { create(:item, user: user) }
        let(:params) { {item: {name: "New Name", folder_id: folder_id, encrypted_folder_dek: "blablabla"}} }

        it "moves the item from a user to a folder" do
          expect {
            put api_v3_item_path(item), params: params, headers: headers, as: :json
          }.to change { item.reload.folder_id }.from(nil).to(folder.id)
        end
      end
    end
  end
end
