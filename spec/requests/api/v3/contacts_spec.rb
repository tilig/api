require "rails_helper"

describe "Api::V3::Contacts", type: :request, v3: true do
  let(:user) { create(:user, :with_organization, :with_keypair) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let(:headers) { {Authorization: authorization} }

  let!(:organization_user) { create(:user, :with_keypair, organization: user.organization) }
  let!(:contact) { create(:contact, user: user, contact: create(:user, :with_keypair)) }

  context "#index" do
    it_should_behave_like "an authenticated endpoint" do
      let(:path) { api_v3_contacts_path }
      let(:method) { :get }
      let(:params) {}
    end

    it "returns the contacts of the current user" do
      get api_v3_contacts_path, headers: headers, as: :json

      expect(json_response[:contacts]).to include({
        display_name: contact.contact.display_name,
        email: contact.contact.email,
        id: contact.contact.id,
        picture: contact.contact.picture,
        public_key: contact.contact.keypair.public_key,
        source: "Contact",
        organization: nil
      })
      expect(json_response[:contacts]).to include({
        display_name: organization_user.display_name,
        email: organization_user.email,
        id: organization_user.id,
        picture: organization_user.picture,
        public_key: organization_user.keypair.public_key,
        source: "Team",
        organization: {
          id: user.organization.id,
          name: user.organization.name
        }
      })
    end
  end
end
