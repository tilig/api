require "rails_helper"

describe "Api::V3::Track", type: :request, v3: true do
  let(:current_user) { create(:user) }
  let(:access_token) { create(:identity, user: current_user).access_token }
  let(:default_headers) do
    {
      "Authorization" => "Bearer #{access_token}",
      "x-tilig-platform" => "Test Client",
      "x-tilig-version" => "0.1",
      "x-browser-name" => "Safari",
      "x-browser-version" => "15.3",
      "x-os-name" => "Intel Mac OS X 10_15_7"
    }
  end
  let(:params) { {params: {event: "Test Event", properties: {test_property: "yes"}}} }

  describe :create do
    let(:headers) { default_headers }

    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :post }
      let(:path) { api_v3_track_index_path }
      let(:params) { {event: "Test Event", properties: {test_property: "yes"}} }
      let(:expected_status_code) { :ok }
    end

    it "schedules a MixpanelJob with the provided arguments and current_user id" do
      expect do
        post api_v3_track_index_path,
          params: {event: "Test Event", properties: {test_property: "yes"}},
          headers: headers

        expect(response).to have_http_status(:ok)
      end.to(
        have_enqueued_job(MixpanelJob).twice.with do |event, json|
          data = JSON.parse(json)["data"]
          case event
          when :event
            expect(data["event"]).to eq "Test Event"
            expect(data["properties"]["distinct_id"]).to eq current_user.id
            expect(data["properties"]["x-tilig-platform"]).to eq("Test Client")
            expect(data["properties"]["x-tilig-version"]).to eq("0.1")
            expect(data["properties"]["browserName"]).to eq("Safari")
            expect(data["properties"]["browserVersion"]).to eq("15.3")
            expect(data["properties"]["$os"]).to eq("Intel Mac OS X 10_15_7")
            expect(data["properties"]["ip"]).to eq("127.0.0.1")
            expect(data["properties"]["test_property"]).to eq "yes"
          when :profile_update
            expect(data["$distinct_id"]).to eq current_user.id
            expect(data["$set"]["$email"]).to eq current_user.email
            expect(data["$set"]["$first_name"]).to eq current_user.given_name
            expect(data["$set"]["$last_name"]).to eq current_user.family_name
          else
            raise Error
          end
        end
      )
    end

    describe "when the x-nf-client-connection-ip is set" do
      let(:headers) { default_headers.merge({"x-nf-client-connection-ip" => "82.82.82.82"}) }

      it "returns the x-nf-client-connection-ip" do
        expect do
          post api_v3_track_index_path,
            params: {event: "Test Event", properties: {test_property: "yes"}},
            headers: headers

          expect(response).to have_http_status(:ok)
        end.to(have_enqueued_job(MixpanelJob).twice.with do |event, json|
          properties = JSON.parse(json)["data"]["properties"]
          if event == :event
            expect(properties["ip"]).to eq "82.82.82.82"
          end
        end)
      end
    end

    describe "without an event" do
      it "will return a unprocessable_entity" do
        post api_v3_track_index_path,
          params: {event: nil, properties: {test_property: "yes"}},
          headers: headers

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "will not send an event to mixpanel" do
        expect do
          post api_v3_track_index_path,
            params: {event: nil, properties: {test_property: "yes"}},
            headers: headers
        end.to_not have_enqueued_job(MixpanelJob)
      end
    end

    describe "without the os name" do
      let(:headers) do
        {
          "Authorization" => "Bearer #{access_token}",
          "x-tilig-platform" => "iOS",
          "x-tilig-version" => "0.1"
        }
      end

      it "the platform is a fallback when iOS" do
        expect do
          post api_v3_track_index_path,
            params: {event: "Test Event", properties: {test_property: "yes"}},
            headers: headers
        end.to(
          have_enqueued_job(MixpanelJob).twice.with do |event, json|
            data = JSON.parse(json)["data"]
            if event == :event
              expect(data["properties"]["$os"]).to eq("iOS")
            end
          end
        )
      end
    end

    describe "without the os name and without a platform name" do
      let(:headers) do
        {
          "Authorization" => "Bearer #{access_token}",
          "x-tilig-platform" => nil,
          "x-tilig-version" => "0.1"
        }
      end

      it "will fallback to UNKNOWN" do
        expect do
          post api_v3_track_index_path,
            params: {event: "Test Event", properties: {test_property: "yes"}},
            headers: headers
        end.to(
          have_enqueued_job(MixpanelJob).twice.with do |event, json|
            data = JSON.parse(json)["data"]
            if event == :event
              expect(data["properties"]["$os"]).to eq("UNKNOWN")
            end
          end
        )
      end
    end

    describe :without_x_tilig_headers do
      let(:current_user) { create(:user) }
      let(:headers) do
        access_token = create(:identity, user: current_user).access_token
        {"Authorization" => "Bearer #{access_token}"}
      end

      it "it fails silently " do
        post api_v3_track_index_path,
          params: {event: "Test Event", properties: {test_property: "yes"}},
          headers: headers

        expect(response).to have_http_status(:ok)
      end
    end
  end
end
