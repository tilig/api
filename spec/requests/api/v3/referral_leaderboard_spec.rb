require "rails_helper"

describe "Api::V3::ReferralLeaderboard", type: :request, v3: true do
  let(:user) do
    create(:user, :with_keypair).tap { |u| u.profile.update! referral_count: 1 }
  end
  let(:access_token) { create(:identity, user: user).access_token }
  let(:default_headers) { {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"} }
  let(:headers) { {Authorization: "Bearer #{access_token}"}.merge(default_headers) }

  before do
    9.times do |i|
      create(:user, email: "u#{i}@example.com").profile.update! referral_count: 10 - i
    end
  end

  describe "GET /index" do
    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :get }
      let(:path) { api_v3_referral_leaderboard_index_path }
      let(:params) {}
    end

    it "renders the leaderboard" do
      get api_v3_referral_leaderboard_index_path, headers: headers, as: :json

      expect(json_response.dig(:referral_leaderboard, :top_list, 0)).to eq({points: 100, rank: 1, email_short: "u0", is_me: false})
      expect(json_response.dig(:referral_leaderboard, :top_list, 1)).to eq({points: 90, rank: 2, email_short: "u1", is_me: false})
      expect(json_response.dig(:referral_leaderboard, :top_list, 2)).to eq({points: 80, rank: 3, email_short: "u2", is_me: false})
      expect(json_response.dig(:referral_leaderboard, :top_list, 9)).to eq({points: 10, rank: 10, email_short: user.email, is_me: true})

      expect(json_response[:referral_leaderboard][:me]).to eq({email_short: user.email, is_me: true, points: 10, rank: 10})
    end
  end
end
