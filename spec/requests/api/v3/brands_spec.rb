require "rails_helper"
require "fixtures/brandfetch/fixtures"

describe "Api::V3::Brands", type: :request, v3: true do
  let!(:brand) { create(:brand) }
  let(:access_token) { create(:identity).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let(:headers) { {Authorization: authorization} }

  describe "GET /brands?url=:url" do
    before do
      stub_brandfetch
    end

    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :get }
      let(:path) { api_v3_brands_path(url: brand.domain) }
      let(:params) { {} }
    end

    describe "when fetch_date is not set" do
      it "fetches it from brandfetch" do
        get api_v3_brands_path(url: brand.domain), headers: headers
        expect(brand.reload.brandfetch_json).to eq JSON.parse(BrandfetchFixtures::GOOGLE_PLAY)
        expect(brand.reload.fetch_date).to_not be nil
      end

      context "when the status code is not 200" do
        before do
          stub_brandfetch_403
        end

        it "does not update the brand" do
          get api_v3_brands_path(url: brand.domain), headers: headers

          expect(brand.reload.brandfetch_json).to be nil
          expect(brand.reload.fetch_date).to be nil
        end

        it "returns not found" do
          brand.destroy!
          get api_v3_brands_path(url: "play.google.com"), headers: headers
          expect(response).to have_http_status(:not_found)
        end
      end
    end

    describe "when a brand does not yet exist" do
      it "creates a new brand with brandfetch" do
        get api_v3_brands_path(url: "play.google.com"), headers: headers

        expect(response).to have_http_status(:ok)
        expect(Brand.last.brandfetch_json).to eq JSON.parse(BrandfetchFixtures::GOOGLE_PLAY)
        expect(Brand.last.fetch_date).to_not be nil
      end
    end

    describe "when fetch_date is already set" do
      let(:brand) { create(:brand, brandfetch_json: {}, fetch_date: rand(1.year).seconds.ago) }

      it "skip updating the brand" do
        expect do
          get api_v3_brands_path(url: brand.domain), headers: headers
        end.not_to change { brand.reload.updated_at }

        expect(response).to have_http_status(:ok)
      end
    end

    describe "if the saved brand does not return a name" do
      let(:brand) { create(:brand, name: nil, fetch_date: Time.current) }

      it "returns not found" do
        get api_v3_brands_path(url: brand.domain), headers: headers

        expect(response).to have_http_status(:ok)
        expect(json_response[:name]).to eq brand.domain
      end
    end
  end

  def stub_brandfetch_403
    remove_request_stub(@stub)
    stub_request(:get, "https://api.brandfetch.io/v2/brands/play.google.com")
      .to_return(status: 403, body: BrandfetchFixtures::ERROR_403, headers: {"Content-Type" => "application/json"})
  end

  def stub_brandfetch
    @stub = stub_request(:get, "https://api.brandfetch.io/v2/brands/play.google.com")
      .to_return(status: 200, body: BrandfetchFixtures::GOOGLE_PLAY, headers: {"Content-Type" => "application/json"})
  end
end
