require "rails_helper"

describe "Api::V3::Profile", type: :request, v3: true do
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:headers) { {Authorization: "Bearer #{access_token}"} }
  let(:params) { {} }

  describe "GET /show" do
    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :get }
      let(:path) { api_v3_profile_path }
    end

    it "renders the correct information" do
      user.profile.update({
        application_settings: {"disable_legacy_encryption" => true},
        user_settings: {"onboarding" => "finished"}
      })
      get api_v3_profile_path, as: :json, headers: headers
      expect(json_response.as_json).to match(
        {
          display_name: user.given_name,
          email: user.email,
          family_name: user.family_name,
          given_name: user.given_name,
          id: user.id,
          locale: "en",
          picture: nil,
          provider_id: nil,
          public_key: user.public_key,
          created_at: user.created_at.as_json,
          updated_at: user.updated_at.as_json,
          application_settings: user.profile.application_settings.as_json,
          user_settings: user.profile.user_settings.as_json,
          keypair: nil
        }.as_json
      )
    end
  end

  describe "DELETE /destroy" do
    let(:follow_up_instance) { instance_double(Mailchimp::SendCancellationFollowUp) }

    before do
      user # Make sure the user is created before calling the tests below
    end

    subject { delete api_v3_profile_path, headers: headers }

    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :delete }
      let(:path) { api_v3_profile_path }
    end

    it "when not called the user exists" do
      expect(User.find(user.id)).to eq(user)
    end

    it "destroys the current user" do
      expect do
        delete api_v3_profile_path, headers: headers, as: :json
      end.to change { User.count }.by(-1)
    end

    it "after calling the current user cannot be found" do
      subject
      expect { User.find(user.id) }.to raise_exception(ActiveRecord::RecordNotFound)
    end

    it "tracks the 'User Deleted' event" do
      expect_any_instance_of(Trackable).to receive(:track).with("User Deleted")
      subject
    end

    it "deletes the users from mailchimp" do
      expect { subject }.to(
        have_enqueued_job(Mailchimp::DeleteMemberJob).at_least(1).times.with do |s_user|
          expect(s_user["email"]).to eq user.email
        end
      )
    end

    it "deletes the users from intercom" do
      allow(Mailchimp::SendCancellationFollowUp).to receive(:new).and_return(follow_up_instance)
      expect(follow_up_instance).to receive(:call)

      subject
    end
  end

  describe "PATCH /update" do
    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :patch }
      let(:path) { api_v3_profile_path }
      let(:params) { {user: {given_name: "Given"}} }
    end

    context "with valid parameters" do
      let(:attributes) do
        {
          given_name: "Given"
        }
      end

      it "updates the current user" do
        patch api_v3_profile_path,
          params: {user: attributes}, headers: headers, as: :json
        user.reload
        expect(user.given_name).to eq("Given")
      end

      it "returns an ok response" do
        patch api_v3_profile_path,
          params: {user: attributes}, headers: headers, as: :json
        expect(response).to have_http_status(:ok)
      end
    end

    context "with all parameters" do
      let(:attributes) do
        {
          given_name: "Given",
          family_name: "Family",
          public_key: "public_key",
          locale: "nl",
          picture: "https://www.myprettypicture.com",
          display_name: "Gurt Johansson",
          provider_id: "google.com"
        }
      end

      it "updates the current user" do
        patch api_v3_profile_path,
          params: {user: attributes}, headers: headers, as: :json
        user.reload
        expect(user.given_name).to eq("Given")
        expect(user.family_name).to eq("Family")
        expect(user.public_key).to eq("public_key")
        expect(user.display_name).to eq("Given")
        expect(user.provider_id).to eq("google.com")
      end

      it "returns an ok response" do
        patch api_v3_profile_path,
          params: {user: attributes}, headers: headers, as: :json
        expect(response).to have_http_status(:ok)
      end

      it "tracks the 'User Updated' event" do
        expect_any_instance_of(Trackable).to receive(:track).with("User Updated")
        patch api_v3_profile_path, params: {user: attributes}, headers: headers, as: :json
      end
    end

    context "when trying to update the email addres" do
      let(:attributes) do
        {
          email: "changed@email.com",
          given_name: "Given",
          family_name: "Family",
          public_key: "public_key",
          locale: "nl",
          picture: "https://www.myprettypicture.com",
          display_name: "Gurt Johansson",
          provider_id: "google.com"
        }
      end

      it "does not update the email address" do
        old_email = user.email
        patch api_v3_profile_path,
          params: {user: attributes}, headers: headers, as: :json
        user.reload
        expect(user.email).to eq(old_email)
      end

      it "returns an ok response" do
        patch api_v3_profile_path,
          params: {user: attributes}, headers: headers, as: :json
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
