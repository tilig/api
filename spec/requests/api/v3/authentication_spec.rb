require "rails_helper"
require "fixtures/mailchimp/stubs"

describe "Api::V3::Authentication", type: :request, v3: true do
  let(:default_headers) {
    {
      "x-tilig-version" => "1.0",
      "x-tilig-platform" => "Web"
    }
  }
  let(:headers) {
    {"Authorization" => "Token #{token}"}.merge(default_headers)
  }
  let(:token) { nil }
  let(:subject) { post "/api/v3/authenticate", headers: default_headers }

  before do
    stub_mailchimp_request
  end

  describe "when the authorization header is missing" do
    it "returns status unauthorized" do
      post "/api/v3/authenticate", headers: default_headers
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "when the authorization header is invalid" do
    describe "because it does not prefix with 'Token: '" do
      let(:headers) { {"Authorization" => token}.merge(default_headers) }

      it "returns status unauthorized" do
        post "/api/v3/authenticate", headers: headers
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "when the token is invalid" do
    let(:token) { "je-suis-invalid" }

    it "returns status unauthorized" do
      post "/api/v3/authenticate", headers: headers
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "when the token is valid" do
    let(:token) { generate_key_service_token }
    let(:headers) { {"Authorization" => "Token #{token}"}.merge(default_headers) }

    xit "returns status ok" do
      post "/api/v3/authenticate", headers: headers
      expect(response).to have_http_status(:ok)
    end

    xit "returns the expected tokens" do
      stub_token_generator

      post "/api/v3/authenticate", headers: headers

      expect(json_response).to eq({accessToken: "some-access-token", refreshToken: "some-refresh-token"})
    end

    describe "not signing up" do
      let(:email) { Faker::Internet.email }
      let(:token) { generate_key_service_token(email: email) }

      it "returns a 403" do
        post "/api/v3/authenticate", headers: headers
        expect(response).to have_http_status(:forbidden)
      end

      it "does not create a user" do
        expect do
          post "/api/v3/authenticate", headers: headers
        end.not_to change { User.count }
      end
    end

    xdescribe "signing up" do
      let(:email) { Faker::Internet.email }
      let(:token) { generate_key_service_token(email: email) }

      it "creates a user" do
        expect do
          post "/api/v3/authenticate", headers: headers
        end.to change { User.count }.by(1)
      end

      it "assigns the user the correct email address" do
        post "/api/v3/authenticate", headers: headers
        expect(User.last.email).to eq(email)
      end

      it "creates an identity" do
        expect do
          post "/api/v3/authenticate", headers: headers
        end.to change { Identity.count }.by(1)
      end

      it "sends a mail to mailchimp" do
        assert_enqueued_jobs 1, only: Mailchimp::AddMemberJob do
          post "/api/v3/authenticate", headers: headers
        end
      end

      it "returns the tokens of the correct user" do
        post "/api/v3/authenticate", headers: headers

        identity = Identity.find_by(
          access_token: json_response[:accessToken],
          refresh_token: json_response[:refreshToken]
        )

        expect(identity).to be_present
        expect(identity.user.email).to eq(email)
      end

      it "sets the client in the profile meta of the user" do
        post "/api/v3/authenticate", headers: headers

        expect(User.last.profile.application_settings).to include({"client_signed_up" => "Web"})
      end

      it "sends the platform data data to mixpanel" do
        expect { post "/api/v3/authenticate", headers: headers }.to(
          have_enqueued_job(MixpanelJob).at_least(2).times.with do |event, json|
            if event == :profile_update
              data = JSON.parse(json)["data"]
              expect(data["$set"]["sign_up_platform"]).to eq "Web"
            end
          end
        )
      end

      it "disables legacy encryption for the new user" do
        post "/api/v3/authenticate", headers: headers
        expect(User.last.legacy_encryption_disabled?).to be true
      end

      context "when a FolderMembership with the same email address exists" do
        let!(:folder_membership) { create(:folder_membership, email: email, user: nil) }

        it "adds the user to the folder_membership after creation" do
          post "/api/v3/authenticate", headers: headers
          expect(folder_membership.reload.user).to eq(User.last)
          expect(folder_membership.accepted_at).to_not be_nil
        end
      end

      context "when a referral is submitted" do
        let!(:referral_user) { create(:user) }

        it "saves the value in the profile" do
          expect do
            post "/api/v3/authenticate", headers: headers, params: {referral: referral_user.profile.application_settings["referral_link"]}
          end.to change { referral_user.profile.reload.referral_count }.by(1)

          expect(User.last.profile.application_settings["used_referral_link"]).to eq(referral_user.profile.application_settings["referral_link"])
        end

        it "tracks the 'Referral Redeemed'" do
          expect_any_instance_of(Trackable).to receive(:track).with("Referral Redeemed")
          expect_any_instance_of(Trackable).to receive(:track).with("Sign Up")
          expect_any_instance_of(Trackable).to receive(:track).with("Sign In", {trigger: nil})
          post "/api/v3/authenticate", headers: headers, params: {referral: referral_user.profile.application_settings["referral_link"]}
        end

        it "Calls the transaction email job once" do
          expect(Mailchimp::ReferralThankYouTemplate).to receive(:new).once.and_call_original

          post "/api/v3/authenticate", headers: headers, params: {referral: referral_user.profile.application_settings["referral_link"]}
        end

        context "when a referral does not exists with a user" do
          it "Calls the transaction email job once" do
            expect(Mailchimp::ReferralThankYouTemplate).not_to receive(:new)

            post "/api/v3/authenticate", headers: headers, params: {referral: "squirrel-apTGWpStv4"}
          end
        end
      end

      xcontext "when a provider_id is submitted" do
        it "saves the value in the profile" do
          post "/api/v3/authenticate", headers: headers, params: {provider_id: "google.com"}

          expect(User.last.profile.application_settings["provider_id_signed_up"]).to eq("google.com")
          expect(User.last.profile.application_settings["provider_ids_signed_in"][0]).to eq("google.com")
        end
      end
    end

    describe "signing in" do
      let(:user) { create(:user) }
      let(:token) { generate_key_service_token(email: user.email) }

      before do
        user
      end

      it "does not create a user" do
        expect do
          post "/api/v3/authenticate", headers: headers
        end.to_not change { User.count }
      end

      it "does not send a message to mailchimp" do
        expect_any_instance_of(Mailchimp::AddMemberJob).to_not receive(:perform)
      end

      it "creates an identity" do
        expect do
          post "/api/v3/authenticate", headers: headers
        end.to change { Identity.count }.by(1)
      end

      it "returns the tokens of the correct user" do
        post "/api/v3/authenticate", headers: headers
        identity = Identity.find_by(
          access_token: json_response[:accessToken],
          refresh_token: json_response[:refreshToken]
        )
        expect(identity).to be_present
        expect(identity.user).to eq(user)
      end

      it "sets the client in the profile meta of the user" do
        post "/api/v3/authenticate", headers: headers

        expect(User.last.profile.application_settings).to include({"clients_signed_in" => ["Web"]})
      end

      it "sends the platform data to mixpanel" do
        expect { post "/api/v3/authenticate", headers: headers }.to(
          have_enqueued_job(MixpanelJob).at_least(2).times.with do |event, json|
            if event == :profile_update
              data = JSON.parse(json)["data"]
              expect(data["$set"]["sign_in_platforms"]).to eq ["Web"]
            end
          end
        )
      end

      describe "with a trigger" do
        it "adds the trigger to the mixpanel properties" do
          expect { post "/api/v3/authenticate?trigger=refresh", headers: headers }.to(
            have_enqueued_job(MixpanelJob).at_least(2).times.with do |event, json|
              if event == :event
                data = JSON.parse(json)["data"]
                expect(data["properties"]["trigger"]).to eq "refresh"
              end
            end
          )
        end
      end

      context "when a referral is submitted" do
        let!(:referral_user) { create(:user) }

        it "does not save the value in the profile" do
          post "/api/v3/authenticate", headers: headers, params: {referral: user.profile.application_settings["referral_link"]}

          expect(User.last.profile.application_settings["used_referral_link"]).to eq nil
        end
      end

      context "when a provider_id is submitted" do
        it "saves the value in the profile" do
          post "/api/v3/authenticate", headers: headers, params: {provider_id: "google.com"}
          expect(User.last.profile.application_settings["provider_ids_signed_in"]).to eq(["google.com"])
        end
      end
    end
  end

  def stub_token_generator
    allow(Auth::RefreshTokenGenerator).to receive(:generate)
      .and_return("some-refresh-token")
    allow(Auth::AccessTokenGenerator).to receive(:generate)
      .and_return("some-access-token")
  end
end
