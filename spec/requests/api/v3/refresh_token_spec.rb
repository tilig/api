require "rails_helper"

describe "Api::V3::RefreshToken", type: :request, v3: true do
  let(:headers) { {"Authorization" => "Token #{token}"} }
  let(:token) { nil }

  def do_request
    post "/api/v3/refresh", headers: headers
  end

  describe "when the authorization header is missing" do
    let(:headers) { nil }

    it "returns status unauthorized" do
      do_request
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "when the authorization header is invalid" do
    describe 'because it does not prefix with "Token: "' do
      let(:headers) { {"Authorization" => token} }

      it "returns status unauthorized" do
        do_request
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "when the token is invalid" do
    let(:token) { "je-suis-invalid" }

    it "returns status unauthorized" do
      do_request
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "when the refresh token is valid" do
    let(:identity) { create(:identity) }
    let(:token) { identity.refresh_token }

    # mock generators
    before do
      # make sure we have valid tokens before mocking the generators
      headers

      allow(Auth::RefreshTokenGenerator).to receive(:generate)
        .and_return("some-refresh-token")
      allow(Auth::AccessTokenGenerator).to receive(:generate)
        .and_return("some-access-token")
    end

    it "returns status ok" do
      do_request
      expect(response).to have_http_status(:ok)
    end

    it "returns the expected tokens" do
      do_request
      expect(json_response).to eq({
        accessToken: "some-access-token",
        refreshToken: "some-refresh-token"
      })
    end

    it "updates the tokens in the identity" do
      # make sure we have valid tokens before mocking the generators
      headers

      allow(Auth::RefreshTokenGenerator).to receive(:generate)
        .and_return("some-refresh-token")
      allow(Auth::AccessTokenGenerator).to receive(:generate)
        .and_return("some-access-token")

      access_token_before = identity.access_token
      refresh_token_before = identity.refresh_token

      do_request
      identity.reload
      expect(identity.access_token).to_not eq(access_token_before)
      expect(identity.refresh_token).to_not eq(refresh_token_before)
    end

    it "does not create a new identity" do
      expect { do_request }.to_not change { Identity.count }
    end

    it "saves the previous refresh token" do
      refresh_token_before = identity.refresh_token
      do_request
      expect(identity.reload.previous_refresh_token).to eq(refresh_token_before)
    end

    it "saves the previous access token" do
      access_token_before = identity.access_token
      do_request
      expect(identity.reload.previous_access_token).to eq(access_token_before)
    end
  end

  describe "when trying to use the same refresh token twice" do
    let(:token) { create(:identity).refresh_token }

    it "is still valid the second time" do
      do_request
      expect(response).to have_http_status(:ok)
      do_request
      expect(response).to have_http_status(:ok)
    end
  end

  describe "when trying to use the same refresh token three times" do
    let(:token) { create(:identity).refresh_token }

    it "returns status unauthorized" do
      do_request
      expect(response).to have_http_status(:ok)
      do_request
      expect(response).to have_http_status(:ok)
      do_request
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "when trying to use an access token instead of a refresh token" do
    let(:token) { create(:identity).access_token }

    it "returns status unauthorized" do
      do_request
      expect(response).to have_http_status(:unauthorized)
    end
  end
end
