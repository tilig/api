require "rails_helper"

describe "Api::V3::PasswordVersions", type: :request, v3: true do
  let(:secret) { create(:item) }
  let(:access_token) { create(:identity, user: secret.user).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let(:headers) { {Authorization: authorization} }

  before do
    allow_any_instance_of(Item).to receive(:set_brand).and_return true
  end

  it_should_behave_like "an authenticated endpoint" do
    let(:headers) { {Authorization: authorization} }
    let(:method) { :get }
    let(:path) { api_v3_secret_password_versions_path(secret) }
    let(:params) { {} }
  end
end
