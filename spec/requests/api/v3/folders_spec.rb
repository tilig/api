require "rails_helper"

describe "Api::V3::Folders", type: :request, v3: true do
  let(:user) { create(:user, :with_keypair) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:default_headers) { {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"} }
  let(:headers) { {Authorization: "Bearer #{access_token}"}.merge(default_headers) }
  let(:item) { create(:item, user: user) }
  let(:item_params) { {id: item.id, encrypted_folder_dek: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false)} }
  let(:folder_membership_params) { [{user_id: user.id, encrypted_private_key: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false)}] }

  let(:params) {
    {folder: {
      public_key: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false),
      folder_memberships: folder_membership_params,
      items: [item_params]
    }}
  }

  describe "POST /create" do
    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :post }
      let(:path) { api_v3_folders_path }
    end

    context "with valid parameters" do
      it "creates a new folder" do
        expect do
          post api_v3_folders_path, params: params, headers: headers, as: :json
        end.to change { Folder.count }.by(1)
      end

      it "tracks the 'Folder Membership Created' en 'Folder Created' event" do
        expect_any_instance_of(Trackable).to receive(:track).with("Folder Created")
        expect_any_instance_of(Trackable).to receive(:track).with("Folder Membership Created", {count: 1})
        post api_v3_folders_path, params: params, headers: headers, as: :json
      end

      it "returns a created response" do
        post api_v3_folders_path, params: params, headers: headers, as: :json
        expect(response).to have_http_status(:created)
        new_folder = Folder.last
        expect(json_response[:folder]).to match({
          id: new_folder.id,
          name: nil,
          description: nil,
          single_item: true,
          public_key: new_folder.public_key,
          created_at: new_folder.created_at.as_json,
          updated_at: new_folder.updated_at.as_json,
          my_encrypted_private_key: new_folder.folder_memberships.first.encrypted_private_key,
          folder_invitations: [],
          folder_memberships: [
            {
              id: new_folder.folder_memberships.first.id,
              role: new_folder.folder_memberships.first.role,
              accepted_at: new_folder.folder_memberships.first.accepted_at.as_json,
              created_at: new_folder.folder_memberships.first.created_at.as_json,
              updated_at: new_folder.folder_memberships.first.updated_at.as_json,
              accepted: true,
              pending: false,
              missing_private_key: false,
              is_me: true,
              creator: {
                id: new_folder.folder_memberships.first.creator.id,
                email: new_folder.folder_memberships.first.creator.email,
                display_name: new_folder.folder_memberships.first.creator.display_name,
                picture: new_folder.folder_memberships.first.creator.picture
              },
              user: {
                id: user.id,
                email: user.email,
                display_name: user.display_name,
                picture: user.picture,
                public_key: user.keypair.public_key
              }
            }
          ]
        })
      end

      context "when the folder membership only gets an email" do
        let(:folder_membership_params) {
          [{email: "john@example.com"},
            {user_id: user.id, encrypted_private_key: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false)}]
        }

        it "creates a new folder" do
          expect do
            post api_v3_folders_path, params: params, headers: headers, as: :json
          end.to change { Folder.count }.by(1)
        end
      end

      context "with a given item that has a shared link" do
        let(:share) { create(:share_link, item: item, access_token: SecureRandom.hex(16)) }
        let(:encrypted_master_key) { Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false) }
        let(:item_params) {
          {
            id: item.id,
            encrypted_folder_dek: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false), share_link: {encrypted_master_key: encrypted_master_key}
          }
        }

        before { share }

        it "creates a new folder with an item with the share link that has an updated encrypted_master_key" do
          expect do
            post api_v3_folders_path, params: params, headers: headers, as: :json
          end.to change { Folder.count }.by(1)
          expect(Folder.last.items.first.share_link.encrypted_master_key).to eq encrypted_master_key
        end
      end

      context "when the encrypted_master_key is not present in the item_params" do
        let(:share) { create(:share_link, item: item) }
        let(:item_params) {
          {
            id: item.id,
            encrypted_folder_dek: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false), share_link: {encrypted_master_key: nil}
          }
        }

        before { share }

        it "will destroy the share link" do
          expect do
            post api_v3_folders_path, params: params, headers: headers, as: :json
          end.to change { Folder.count }.by(1)

          expect(Folder.last.items.first.share_link).to be_nil
        end
      end

      context "Creator and invety are contacts" do
        let!(:shared_user) { create(:user, :with_keypair) }

        let!(:contact) { create(:contact, user: user, contact: shared_user) }
        let!(:contact_2) { create(:contact, user: shared_user, contact: user) }

        before { stub_request(:post, "https://mandrillapp.com/api/1.0/messages/send-template.json").to_return(status: 200, body: {}.to_json, headers: {}) }

        it "will add the member immediately" do
          params[:folder][:folder_memberships] << {user_id: shared_user.id, encrypted_private_key: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false)}

          post api_v3_folders_path, params: params, headers: headers, as: :json

          expect(response).to have_http_status(:created)
          new_folder = Folder.last
          expect(json_response[:folder]).to match({
            id: new_folder.id,
            name: nil,
            description: nil,
            single_item: true,
            public_key: new_folder.public_key,
            created_at: new_folder.created_at.as_json,
            updated_at: new_folder.updated_at.as_json,
            my_encrypted_private_key: new_folder.folder_memberships.first.encrypted_private_key,
            folder_memberships: [
              {
                id: new_folder.folder_memberships.first.id,
                role: new_folder.folder_memberships.first.role,
                accepted_at: new_folder.folder_memberships.first.accepted_at.as_json,
                created_at: new_folder.folder_memberships.first.created_at.as_json,
                updated_at: new_folder.folder_memberships.first.updated_at.as_json,
                accepted: true,
                pending: false,
                is_me: true,
                user: {
                  id: user.id,
                  email: user.email,
                  display_name: user.display_name,
                  picture: user.picture,
                  public_key: user.keypair.public_key
                },
                missing_private_key: false,
                creator: {
                  id: user.id,
                  email: user.email,
                  display_name: user.display_name,
                  picture: user.picture
                }
              },
              {
                id: new_folder.folder_memberships.last.id,
                role: new_folder.folder_memberships.last.role,
                accepted_at: new_folder.folder_memberships.last.accepted_at.as_json,
                created_at: new_folder.folder_memberships.last.created_at.as_json,
                updated_at: new_folder.folder_memberships.last.updated_at.as_json,
                accepted: true,
                pending: false,
                is_me: false,
                user: {
                  id: shared_user.id,
                  email: shared_user.email,
                  display_name: shared_user.display_name,
                  picture: shared_user.picture,
                  public_key: shared_user.keypair.public_key
                },
                missing_private_key: false,
                creator: {
                  id: user.id,
                  email: user.email,
                  display_name: user.display_name,
                  picture: user.picture
                }
              }
            ],
            folder_invitations: []
          })
        end
      end
    end

    context "with invalid parameters" do
      let(:other_item) { create(:item, user: create(:user)) }

      let(:params) {
        {folder: {
          public_key: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false),
          folder_memberships: [
            {user_id: user.id, encrypted_private_key: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false)}
          ],
          items: [
            {id: item.id, encrypted_folder_dek: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false)}
          ]
        }}
      }

      it "returns a unprocessable_entity without a public key" do
        # No public_key
        params[:folder][:public_key] = nil
        post api_v3_folders_path, params: params, headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors]).to include({public_key: ["can't be blank"]})
      end
      it "returns a unprocessable_entity without an encrypted private key" do
        # No encrypted_private_key
        params[:folder][:folder_memberships][0][:encrypted_private_key] = nil
        post api_v3_folders_path, params: params, headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors]).to include({base: ["Error creating memberships: Encrypted private key can't be blank"]})
      end
      it "returns a unprocessable_entity without an encrypted folder dek" do
        # No item encrypted_folder_dek
        params[:folder][:items][0][:encrypted_folder_dek] = nil
        post api_v3_folders_path, params: params, headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors]).to include({base: ["Could not move items to new folder: Encrypted folder dek can't be blank"]})
      end
      it "returns a unprocessable_entity if the creator is not a member" do
        # ID of a user thet is not the owner
        params[:folder][:folder_memberships][0][:user_id] = create(:user).id
        post api_v3_folders_path, params: params, headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors]).to include({base: ["The creator of the folder should be a member"]})
      end
      it "returns a unprocessable_entity if a user can't be found" do
        # No user_id
        params[:folder][:folder_memberships][0][:user_id] = "empty"
        post api_v3_folders_path, params: params, headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors]).to include({base: ["Error creating memberships: User can't be blank and Email can't be blank"]})
      end
      it "returns a not_found for the item of another user" do
        # Item id of an item of an other user
        params[:folder][:items][0][:id] = other_item.id
        post api_v3_folders_path, params: params, headers: headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response[:errors]).to include({base: ["Could not find item(s)"]})
      end
    end
  end
end
