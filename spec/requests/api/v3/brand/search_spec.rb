require "rails_helper"
require "fixtures/brandfetch/fixtures"

describe "Api::V3::Brands::Search", type: :request, v3: true do
  let(:size) { 3 }
  let!(:stub_brand_size) { stub_const "Brand::DEFAULT_PAGE_SIZE", size }
  let(:brand_instagram) { create(:brand, name: "Instagram", domain: "instagram.com", brandfetch_json: {}, public_suffix_domain: "instagram.com", rank: 10) }
  let(:brand_instacart) { create(:brand, name: "Instacart", domain: "instacart.com", brandfetch_json: {}, public_suffix_domain: "instacart.com", rank: 100) }
  let(:brand_instafamous) { create(:brand, name: "Instafamous", domain: "instafamous.com", brandfetch_json: {}, public_suffix_domain: "instafamous.com", rank: nil) }
  let(:other_brand) { create :brand, name: "google", domain: "play.google.com", brandfetch_json: {}, public_suffix_domain: "play.google.com" }
  let(:nine_almost_the_same_other_brand) { 9.times { |n| create(:brand, name: "Til#{n}ig", domain: "Til#{n}ig.com", brandfetch_json: {}, public_suffix_domain: "Til#{n}ig.com") } }

  let(:prefix) { "insta" }
  let(:params) { {prefix: prefix} }

  let(:access_token) { create(:identity).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let(:headers) { {Authorization: authorization} }

  describe "GET api/v3/brands/search?prefix=:name&page_size=:number" do
    it_should_behave_like "an authenticated endpoint" do
      let(:method) { :get }
      let(:path) { api_v3_brands_search_index_path }
    end

    describe "when the prefix is an emty string like '   '" do
      let(:prefix) { "   " }

      before do
        brand_instagram
      end

      it "does return an empty array of results" do
        get api_v3_brands_search_index_path(params), headers: headers

        expect(response).to have_http_status(:ok)
        expect(json_response).to eq []
      end
    end

    describe "when the prefix is blank like ''" do
      let(:prefix) { "" }

      before do
        brand_instagram
      end

      it "does return an empty array of results" do
        get api_v3_brands_search_index_path(params), headers: headers

        expect(response).to have_http_status(:ok)
        expect(json_response).to eq []
      end
    end

    describe "when the page_size is negetive and the prefix is set" do
      let(:prefix) { "Til" }

      before do
        nine_almost_the_same_other_brand
      end

      it "is limited to return the default size of brands by default" do
        get api_v3_brands_search_index_path(params.merge(page_size: -10)), headers: headers
        expect(json_response.size).to eq 3
      end
    end

    describe "when searching in a large pool of possible matches" do
      let(:prefix) { "Til" }

      before do
        nine_almost_the_same_other_brand
      end

      it "can set the limit of brands to return" do
        get api_v3_brands_search_index_path(params.merge(page_size: 3)), headers: headers
        expect(json_response.size).to eq 3
      end

      it "is limited to return the default size brands by default" do
        get api_v3_brands_search_index_path(params), headers: headers
        expect(json_response.size).to eq 3
      end
    end

    context "with the input prefixing `www.`" do
      let(:prefix) { "www.insta" }

      before do
        brand_instafamous
        brand_instagram
        brand_instacart
      end

      it "matches the domain as if without the prefix" do
        get api_v3_brands_search_index_path(params), headers: headers
        expect(json_response.first).to include({name: "Instagram"})
        expect(json_response.second).to include({name: "Instacart"})
        expect(json_response.any? { |h| h[:name] == "Instafamous" }).to be false
      end
    end

    context "with the prefix `insta` and a brand without rank" do
      let(:prefix) { "insta" }

      before do
        brand_instafamous
        brand_instagram
        brand_instacart
      end

      it "matches the domain with a liking prefix that also have a rank" do
        get api_v3_brands_search_index_path(params), headers: headers
        expect(json_response.any? { |h| h[:name] == "Instafamous" }).to be false
      end
    end

    context "brands without a fetch_date" do
      let(:brand_instagram) { create(:brand, name: "Instagram", brandfetch_json: nil, public_suffix_domain: "instagram.com", fetch_date: nil, rank: 10, domain: "instagram.com") }

      it "start a job to update the brand with brandfetch" do
        brand_instagram

        get api_v3_brands_search_index_path(params), headers: headers

        expect(FetchBrandJob).to have_been_enqueued
      end
    end
  end

  context "with parents" do
    let!(:google_com) { create(:brand, name: :google_com, domain: "google.com", public_suffix_domain: "google.com", brandfetch_json: {}) }
    let!(:google_co_uk) { create(:brand, name: :google_co_uk, domain: "google.co.uk", public_suffix_domain: "google.co.uk", brandfetch_json: {}, parent: google_com) }
    let!(:google_fr) { create(:brand, name: :google_fr, domain: "google.fr", public_suffix_domain: "google.fr", brandfetch_json: {}, parent: google_com, rank: 22) }
    let!(:google_fi) { create(:brand, name: :google_fi, domain: "google.fi", public_suffix_domain: "google.fi", brandfetch_json: {}, parent: google_com, rank: 21) }
    let!(:google_com_mx) { create(:brand, name: :google_com_mx, domain: "google.com.mx", public_suffix_domain: "google.com.mx", brandfetch_json: {}, parent: google_com) }

    describe "with the prefix goog" do
      let(:prefix) { "goog" }

      it "returns only the parent brand for Google" do
        get api_v3_brands_search_index_path(params), headers: headers

        expect(json_response.first).to include({domain: "google.com"})
        expect(json_response.size).to be 1
      end
    end

    describe "with the prefix google.co" do
      let(:prefix) { "google.co" }

      it "returns only the parent for Google" do
        get api_v3_brands_search_index_path(params), headers: headers

        expect(json_response.first).to include({domain: "google.com"})
        expect(json_response.size).to be 1
      end
    end

    describe "with the exact prefix of the domain of the brand google.com" do
      let(:prefix) { "google.com" }

      it "returns only the that brand for Google" do
        get api_v3_brands_search_index_path(params), headers: headers

        expect(json_response.first).to include({domain: "google.com"})
        expect(json_response.size).to be 1
      end
    end

    describe "with the prefix google.co." do
      let(:prefix) { "google.co." }

      it "returns everything that matches the prefix google.co.uk" do
        get api_v3_brands_search_index_path(params), headers: headers

        expect(json_response.first).to include({domain: "google.co.uk"})
        expect(json_response.size).to be 1
      end
    end

    describe "with the prefix google.f" do
      let(:prefix) { "google.f" }

      it "it returns everything that matches the prefix google.f" do
        get api_v3_brands_search_index_path(params), headers: headers

        expect(json_response.first).to include({domain: "google.fi"})
        expect(json_response.second).to include({domain: "google.fr"})
        expect(json_response.size).to be 2
      end

      it "renders the parent brand details" do
        parent_brand = google_com
        get api_v3_brands_search_index_path(params), headers: headers

        expect(json_response.first).to include({
          domain: "google.fi",
          id: google_fi.id,
          public_suffix_domain: google_fi.public_suffix_domain,
          name: google_fi.name,
          totp: parent_brand.totp,
          main_color_hex: parent_brand.main_color_hex,
          main_color_brightness: parent_brand.main_color_brightness,
          logo_source: parent_brand.logo_source,
          logo_icon_source: "https://asset.brandfetch.io/id6O2oGzv-/idNEgS9h8q.jpeg",
          images: {
            icon: {
              png: {
                original: parent_brand.images.dig("icon", "png", "original")

              },
              svg: {
                original: parent_brand.images.dig("icon", "svg", "original")

              },
              theme: "dark"
            }
          },
          is_fetched: parent_brand.fetch_date?
        })
      end
    end

    describe "google.com." do
      let(:prefix) { "google.com." }

      it "it returns everything that matches the prefix 'google.com.'" do
        get api_v3_brands_search_index_path(params), headers: headers

        expect(json_response.first).to include({domain: "google.com.mx"})
        expect(json_response.size).to be 1
      end
    end
  end
end
