require "rails_helper"

describe "Api::V3::Shares", type: :request, v3: true do
  before do
    allow_any_instance_of(Item).to receive(:set_brand).and_return true
  end

  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:default_headers) { {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"} }
  let(:headers) { {Authorization: "Bearer #{access_token}"}.merge(default_headers) }

  let(:secret) { create(:google_play_item, user: user) }
  let(:share_uuid) { SecureRandom.hex(16) }
  let(:share_access_token) { SecureRandom.hex(16) }
  let(:share_encrypted_key) { Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false) }
  let(:share_encrypted_master_key) { Base64.urlsafe_encode64(SecureRandom.hex(18), padding: false) }
  let(:params) do
    {
      uuid: share_uuid,
      access_token: share_access_token,
      encrypted_key: share_encrypted_key,
      encrypted_master_key: share_encrypted_master_key
    }
  end

  let(:other_user) { create(:user) }
  let(:other_secret) { create(:google_play_item, user: other_user) }

  # let(:membership) { create(:membership, user: user) }
  # let(:group) { membership.group }
  # let(:group_secret) { create(:item, user: nil, group: group) }

  # let(:other_group_secret) { create(:item, user: nil, group: create(:membership, user: create(:user)).group) }

  describe "For User Secrets Shares" do
    describe "GET /index for a share" do
      context "valid Authorization header" do
        before do
          create(:share_link, item: create(:item, user: other_user))
          create(:share_link, item: create(:item, user: other_user))
        end

        it "list shares for the User" do
          get api_v3_shares_url, headers: {Authorization: "Bearer #{create(:identity, user: other_user).access_token}"}, as: :json
          expect(response).to be_successful
          expect(json_response.length).to be 2
        end
      end

      context "invalid Authorization header" do
        it "returns a Unauthorization code" do
          get api_v3_shares_url, as: :json
          expect(response).to have_http_status(:unauthorized)
        end
      end

      context "valid Authorization header but has no Shares" do
        it "returns empty array" do
          get api_v3_shares_url, headers: {Authorization: "Bearer #{create(:identity, user: other_user).access_token}"}, as: :json
          expect(response).to be_successful
          expect(json_response).to match []
        end
      end
    end

    describe "POST /create for a share" do
      context "valid Authorization header and params" do
        it "creates a new share" do
          post api_v3_secret_share_index_url(secret.id), params: params, headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to be_successful
          expect(json_response[:id]).to match Item.find(secret.id).share_link.id
        end
      end

      context "invalid Authorization header" do
        it "returns a Unauthorization code" do
          post api_v3_secret_share_index_url(secret.id), params: params, headers: {Authorization: "Bearer #{SecureRandom.hex(16)}"}, as: :json
          expect(response).to have_http_status(:unauthorized)
        end
      end

      context "valid Authorization header and NO params" do
        it "returns a Unprocessable Entry code" do
          post api_v3_secret_share_index_url(secret.id), headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context "valid Authorization header but Share already exists" do
        before do
          create(:share_link, uuid: share_uuid, access_token: SecureRandom.hex(16), item_id: secret.id)
        end

        it "returns the Share" do
          post api_v3_secret_share_index_url(secret.id), params: params, headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to match({base: "Share already created"})
        end
      end

      context "valid Authorization header and secret_id of other User" do
        it "returns a Not Found code" do
          post api_v3_secret_share_index_url(other_secret.id),
            params: {
              uuid: share_uuid,
              access_token: share_access_token,
              encrypted_key: share_encrypted_key,
              encrypted_master_key: share_encrypted_master_key
            },
            headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end

      context "valid Authorization header and missing UUID" do
        it "returns a Unprocessable Entry code" do
          post api_v3_secret_share_index_url(secret.id),
            params: {
              access_token: share_access_token,
              encrypted_key: share_encrypted_key,
              encrypted_master_key: share_encrypted_master_key
            },
            headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to match({uuid: ["is invalid"]})
        end
      end

      context "valid Authorization header and to short UUID" do
        it "returns a Unprocessable Entry code" do
          post api_v3_secret_share_index_url(secret.id),
            params: {
              uuid: SecureRandom.hex(15),
              access_token: share_access_token,
              encrypted_key: share_encrypted_key,
              encrypted_master_key: share_encrypted_master_key
            },
            headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to match({uuid: ["is invalid"]})
        end
      end

      context "valid Authorization header and missing encrypted_key" do
        it "returns a Unprocessable Entry code" do
          post api_v3_secret_share_index_url(secret.id),
            params: {
              uuid: share_uuid,
              access_token: share_access_token,
              encrypted_master_key: share_encrypted_master_key
            },
            headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to match({encrypted_dek: ["can't be blank"]})
        end
      end

      context "valid Authorization header and missing encrypted_master_key" do
        it "returns a Unprocessable Entry code" do
          post api_v3_secret_share_index_url(secret.id),
            params: {
              uuid: share_uuid,
              access_token: share_access_token,
              encrypted_key: share_encrypted_key
            },
            headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to match({encrypted_master_key: ["can't be blank"]})
        end
      end

      context "valid Authorization header and to missing access_token" do
        it "returns a Unprocessable Entry code" do
          post api_v3_secret_share_index_url(secret.id),
            params: {
              uuid: share_uuid,
              encrypted_key: share_encrypted_key,
              encrypted_master_key: share_encrypted_master_key
            },
            headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to match({access_token: ["can't be blank"]})
        end
      end

      context "valid Authorization header and to short access_token" do
        it "returns a Unprocessable Entry code" do
          post api_v3_secret_share_index_url(secret.id),
            params: {
              uuid: share_uuid,
              access_token: SecureRandom.hex(15),
              encrypted_key: share_encrypted_key,
              encrypted_master_key: share_encrypted_master_key
            },
            headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to match({access_token: ["is invalid"]})
        end
      end

      context "valid Authorization header and params but Secret with uuid is already created" do
        before do
          create(:share_link, access_token: share_access_token, uuid: share_uuid)
        end

        it "returns a error 'has already been take' and minimal 3 seconds delay" do
          post api_v3_secret_share_index_url(secret.id), params: {
            uuid: share_uuid,
            access_token: share_access_token,
            encrypted_key: share_encrypted_key,
            encrypted_master_key: share_encrypted_master_key
          }, headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to match({uuid: ["has already been taken"]})
        end
      end
    end

    describe "DELETE /destroy for a share" do
      before { create(:share_link, item: other_secret) }

      context "valid Authorization header" do
        it "Remove the Share for the Secret" do
          delete api_v3_secret_share_url(other_secret.id), headers: {Authorization: "Bearer #{create(:identity, user: other_user).access_token}"}, as: :json
          expect(response).to be_successful
          expect(json_response).to match({status: "deleted"})
          expect(Item.find(response.request.params["secret_id"]).share_link.present?).to be false
        end
      end

      context "valid Authorization header and share is already removed" do
        before { other_secret.share_link.destroy }

        it "returns not_found" do
          delete api_v3_secret_share_url(other_secret.id), headers: {Authorization: "Bearer #{create(:identity, user: other_user).access_token}"}, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end

      context "invalid Authorization header" do
        it "respond with unauthorized" do
          delete api_v3_secret_share_url(other_secret.id), as: :json
          expect(response).to have_http_status(:unauthorized)
          expect(Item.find(response.request.params["secret_id"]).share_link.present?).to_not be false
        end
      end

      context "valid Authorization header and Secret of other User" do
        it "returns not_found" do
          delete api_v3_secret_share_url(other_secret.id), headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to have_http_status(:not_found)
          expect(Item.find(response.request.params["secret_id"]).share_link.present?).to be true
        end
      end

      context "valid Authorization header and random non existing Secret" do
        it "returns not_found" do
          delete api_v3_secret_share_url(SecureRandom.uuid), headers: {Authorization: "Bearer #{create(:identity, user: user).access_token}"}, as: :json
          expect(response).to have_http_status(:not_found)
        end
      end
    end
  end
end
