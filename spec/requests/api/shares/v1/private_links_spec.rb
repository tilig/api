require "rails_helper"

describe "Api::Shares::V1::PrivateLinks", type: :request, v3: true do
  before { allow_any_instance_of(Item).to receive(:set_brand).and_return true }

  let(:secret) { create(:google_play_item, brand: create(:brand), user: create(:user)) }
  let(:access_token) { SecureRandom.hex(16) }
  let(:share) { create(:share_link, access_token: access_token, item_id: secret.id) }
  let(:share_times_viewed) { share.times_viewed }

  describe "GET /show for a Share Private Link" do
    context "with an Access-token" do
      it "shows the shared secret" do
        get api_shares_v1_private_link_url(share.uuid), headers: {"Access-token": access_token}, as: :json
        expect(response).to be_successful
        expect(json_response[:uuid]).to match share.uuid
        expect(json_response[:encrypted_key]).to match share.encrypted_dek
        expect(json_response[:encrypted_dek]).to match share.encrypted_dek
        expect(json_response[:domain]).to match share.item.public_suffix_domain
        expect(json_response[:user_display_name]).to match share.item.user.display_name
      end
    end

    context "with an Access-token" do
      before { share_times_viewed }

      it "times_viewed will be incremented by 1" do
        get api_shares_v1_private_link_url(share.uuid), headers: {"Access-token": access_token}, as: :json
        expect(response).to be_successful
        expect(ShareLink.find_by(uuid: share.uuid).times_viewed).to match share_times_viewed + 1
      end
    end

    context "without an Access-token" do
      it "returns not_found" do
        get api_shares_v1_private_link_url(share.uuid), as: :json
        expect(response).to have_http_status(:not_found)
      end
    end

    context "invallid token but correct length Access-token" do
      it "returns not_found" do
        get api_shares_v1_private_link_url(share.uuid), headers: {"Access-token": SecureRandom.hex(16)}, as: :json
        expect(response).to have_http_status(:not_found)
      end
    end

    context "invallid token and incorrect length Access-token" do
      it "returns not_found" do
        get api_shares_v1_private_link_url(share.uuid), headers: {"Access-token": SecureRandom.hex(15)}, as: :json
        expect(response).to have_http_status(:not_found)
      end
    end

    context "no UUID and 'existing' Access-token" do
      it "returns the defauld root page" do
        expect {
          get api_shares_v1_private_link_url(uuid: ""), headers: {"Access-token": access_token}, as: :json
        }.to raise_error(ActionController::RoutingError)
      end
    end

    context "to short UUID and 'existing' Access-token" do
      it "returns not_found" do
        get api_shares_v1_private_link_url(SecureRandom.hex(15)), headers: {"Access-token": access_token}, as: :json
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe "for folder items" do
    let!(:user) { create(:user, :with_keypair) }
    let(:folder) { create(:folder) }
    let!(:folder_membership) { create(:folder_membership, user: user, folder: folder, role: "admin", accepted_at: Time.zone.now) }
    let!(:folder_item) { create(:item, folder: folder, user: nil, encrypted_folder_dek: "leflef") }

    let(:access_token) { SecureRandom.hex(16) }
    let!(:share_link) { create(:share_link, access_token: access_token, item_id: folder_item.id) }

    describe "GET /show" do
      it "returns a success response" do
        get api_shares_v1_private_link_url(share_link.uuid), headers: {"Access-token": access_token}, as: :json

        expect(json_response[:user_display_name]).to eq "Shared item"
        expect(response).to be_successful
      end
    end
  end
end
