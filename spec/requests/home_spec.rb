require "rails_helper"

describe "Home", type: :request do
  describe "GET  /" do
    it "resolves with {ok: true}" do
      get root_path, as: :json

      expect(response).to have_http_status(:ok)
      expect(json_response).to eq({ok: true})
    end
  end
end
