require "rails_helper"

describe "Mailchimp::TransactionalEmailJob" do
  describe "perform" do
    let(:template) {
      {
        "template_name" => "template_name",
        "template_content" => [{}],
        "message" => {
          subject: "Recipient has shared an item with you on Tilig",
          from_email: "no-reply@tilig.com",
          to: [{
            email: "recipient@email.com",
            name: "Recipient Name"
          }]
        }
      }
    }

    it "sends an API request with template to mailchimp/mandrill" do
      stub_mandrill_request
      Mailchimp::TransactionalEmailJob.perform_now(template)
      assert_requested(:post, "https://mandrillapp.com/api/1.0/messages/send-template.json") { |req| req.body == template.to_json }
    end
  end

  def stub_mandrill_request
    stub_request(:post, "https://mandrillapp.com/api/1.0/messages/send-template.json").and_return(body: {}.to_json)
  end
end
