require "rails_helper"
require "fixtures/brandfetch/fixtures"

RSpec.describe FetchBrandJob, type: :job do
  let(:brand) { create(:brand) }

  before do
    brand
    stub_brandfetch
  end

  describe "runs the FetchBrand Job" do
    it "updates the brand with Brandfetch info" do
      FetchBrandJob.perform_now(brand)
      expect(brand.reload.brandfetch_json).to_not be_nil
    end

    it "is enqueued to perform_later" do
      FetchBrandJob.perform_later(brand)
      expect(FetchBrandJob).to have_been_enqueued.with(brand).at(:no_wait)
    end
  end

  def stub_brandfetch
    @stub = stub_request(:get, "https://api.brandfetch.io/v2/brands/play.google.com")
      .to_return(status: 200, body: BrandfetchFixtures::GOOGLE_PLAY, headers: {"Content-Type" => "application/json"})
  end
end
