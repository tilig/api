require "swagger_helper"

describe "api/v3/folders/{folder_id}/folder_memberships", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user, :with_keypair) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:folder) { create(:folder) }
  let(:folder_membership) { create(:folder_membership, :accepted, user: user, folder: folder) }
  let(:authorization) { "Bearer #{access_token}" }
  let(:folder_id) { folder.id }
  let(:id) { folder_membership.id }

  path "/api/v3/folders/{folder_id}/folder_memberships" do
    post "Create a new Folder Membership" do
      tags "Folders Membership Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      parameter name: :folder_id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "0a048468-fa74-4f20-a72f-7a097d55383",
        description:
          "The ID of the folder."

      parameter name: :folder_membership_params,
        in: :body,
        schema: {
          "$ref" => "#/components/schemas/folder_membership_params"
        }

      let(:folder_id) { folder.id }
      let(:new_member) { create(:user, :with_keypair) }
      let(:folder_membership_params) {
        {
          folder_membership: {
            user_id: new_member.id,
            encrypted_private_key: "123456"
          },
          folder: {
            name: "Updated Folder"
          }
        }
      }

      response "201", "Ok" do
        schema "$ref": "#/components/schemas/folder_membership"
        before { folder_membership }
        before { stub_request(:post, "https://mandrillapp.com/api/1.0/messages/send-template.json").to_return(status: 200, body: {}.to_json, headers: {}) }

        run_test! do |response|
          folder.reload
          new_folder_membership = FolderMembership.last
          expect(json_response).to match({
            folder_membership: {
              id: new_folder_membership.id,
              role: new_folder_membership.role,
              accepted: false,
              pending: true,
              accepted_at: nil,
              is_me: false,
              missing_private_key: false,
              created_at: new_folder_membership.created_at.as_json,
              updated_at: new_folder_membership.updated_at.as_json,
              creator: {
                id: user.id,
                display_name: user.display_name,
                email: user.email,
                picture: user.picture
              },
              user: {
                id: new_member.id,
                display_name: nil,
                email: new_member.email,
                picture: nil,
                public_key: new_member.keypair.public_key
              }
            },
            folder: {
              id: folder.id,
              name: "Updated Folder",
              created_at: folder.created_at.as_json,
              updated_at: folder.updated_at.as_json
            }

          })
        end
      end

      response "201", "Created" do
        schema "$ref": "#/components/schemas/folder_membership"
        before { folder_membership }
        before { stub_request(:post, "https://mandrillapp.com/api/1.0/messages/send-template.json").to_return(status: 200, body: {}.to_json, headers: {}) }

        let(:folder_membership_params) {
          {
            folder_membership: {
              email: "john@example.com"
            }
          }
        }

        run_test! do |response|
          folder.reload
          new_folder_membership = FolderMembership.last
          expect(json_response).to match({
            folder_membership: {
              id: new_folder_membership.id,
              role: new_folder_membership.role,
              accepted: false,
              pending: true,
              accepted_at: nil,
              is_me: false,
              missing_private_key: false,
              created_at: new_folder_membership.created_at.as_json,
              updated_at: new_folder_membership.updated_at.as_json,
              creator: {
                id: user.id,
                display_name: user.display_name,
                email: user.email,
                picture: user.picture
              },
              user: {
                id: nil,
                display_name: nil,
                email: "john@example.com",
                picture: nil,
                public_key: nil
              }
            },
            folder: {
              id: folder.id,
              name: folder.name,
              created_at: folder.created_at.as_json,
              updated_at: folder.updated_at.as_json
            }

          })
        end
      end

      response "404", "Not found" do
        let(:folder_membership_params) {
          {
            folder_membership: {
              user_id: nil,
              encrypted_private_key: "123456"
            }
          }
        }

        run_test!
      end

      response "422", "Invalid request" do
        let(:folder_membership_params) {
          {
            folder_membership: {
              user_id: new_member.id,
              encrypted_private_key: nil
            }
          }
        }
        before do
          folder_membership
        end

        run_test! do |response|
          expect(json_response[:errors]).to include({encrypted_private_key: ["can't be blank"]})
        end
      end

      response "401", "Unauthorized" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end

  path "/api/v3/folders/{folder_id}/folder_memberships/{id}" do
    delete "Delete a Folder Membership" do
      tags "Folders Membership Endpoint"
      produces "application/json"
      security [Bearer: []]

      parameter name: :folder_id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "0a048468-fa74-4f20-a72f-7a097d55383",
        description:
          "The ID of the folder."

      parameter name: :id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "d801aef8-b52e-47f7-a192-64b64b24ec76",
        description:
          "The ID of the folder membership."

      let(:folder_id) { folder.id }
      let(:other_user) { create(:user, :with_keypair) }
      let(:other_folder_membership) { create(:folder_membership, user: other_user, creator: user, folder:) }
      let(:id) { other_folder_membership.id }

      response "200", "Ok" do
        schema "$ref": "#/components/schemas/folder_membership"

        # Make user admin of folder
        before { folder_membership }

        run_test! do |response|
          expect(json_response).to match({
            folder_membership: {
              id: other_folder_membership.id,
              role: other_folder_membership.role,
              accepted: false,
              pending: true,
              accepted_at: other_folder_membership.accepted_at.as_json,
              is_me: false,
              missing_private_key: false,
              created_at: other_folder_membership.created_at.as_json,
              updated_at: other_folder_membership.updated_at.as_json,

              user: {
                id: other_folder_membership.user.id,
                display_name: nil, # No display name
                email: other_folder_membership.user.email,
                picture: nil, # No picture
                public_key: other_folder_membership.user.keypair.public_key
              },

              creator: {
                id: user.id,
                display_name: user.display_name,
                email: user.email,
                picture: user.picture
              }
            },
            folder: {
              id: folder.id,
              name: folder.name,
              created_at: folder.created_at.as_json,
              updated_at: folder.updated_at.as_json

            }
          })
          expect(FolderMembership.find_by(id: other_folder_membership.id)).to be_nil
        end
      end

      response "401", "Unauthorized" do
        let(:authorization) { "Bearer " }
        run_test!
      end

      response "404", "Not found" do
        let(:id) { "empty" }

        run_test!
      end

      response "401", "Unauthorized" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end

    put "Update a Folder Membership" do
      tags "Folders Membership Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      parameter name: :folder_id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "d801aef8-b52e-47f7-a192-64b64b24ec76",
        description: "The ID of the folder."

      parameter name: :id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "d801aef8-b52e-47f7-a192-64b64b24ec76",
        description: "The ID of the folder membership."

      parameter name: :folder_membership_update_params,
        in: :body,
        schema: {
          "$ref" => "#/components/schemas/folder_membership_update_params"
        }

      let(:folder_membership_update_params) { {folder_membership: {encrypted_private_key: "123456"}} }

      response "200", "Ok" do
        # schemfolder_membershipa "$ref": "#/components/schemas/folder_membership"
        before { folder_membership }
        before { stub_request(:post, "https://mandrillapp.com/api/1.0/messages/send-template.json").to_return(status: 200, body: {}.to_json, headers: {}) }

        run_test! do |response|
          expect(json_response).to match({
            folder_membership: {
              id: folder_membership.id,
              role: folder_membership.role,
              accepted: true,
              pending: false,
              accepted_at: folder_membership.accepted_at.as_json,
              is_me: true,
              missing_private_key: false,
              created_at: folder_membership.created_at.as_json,
              updated_at: folder_membership.reload.updated_at.as_json,

              creator: {
                id: folder_membership.creator.id,
                display_name: folder_membership.creator.display_name,
                email: folder_membership.creator.email,
                picture: folder_membership.creator.picture
              },
              user: {
                id: user.id,
                display_name: user.display_name,
                email: user.email,
                picture: nil,
                public_key: user.keypair.public_key
              }
            },
            folder: {
              id: folder.id,
              name: folder.name,
              created_at: folder.created_at.as_json,
              updated_at: folder.updated_at.as_json
            }

          })
          expect(folder_membership.reload.encrypted_private_key).to eq("123456")
        end
      end

      response "401", "Unauthorized" do
        let(:authorization) { "Bearer " }
        run_test!
      end

      response "404", "Not found" do
        let(:id) { "empty" }

        run_test!
      end
    end
  end
end
