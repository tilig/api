require "swagger_helper"

describe "api/v3/folders/{folder_id}/folder_memberships/{folder_membership_id}/acceptance", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user, :with_keypair) }
  let(:folder) { create(:folder) }
  let(:folder_id) { folder.id }
  let(:folder_membership) { create(:folder_membership, :accepted, user: user, folder: folder) }

  let(:acceptance_token) { new_folder_membership.acceptance_token }

  path "/api/v3/folders/{folder_id}/folder_memberships/{folder_membership_id}/acceptance" do
    post "Accept a Folder Membership" do
      tags "Folders Membership Acceptances Endpoint"
      consumes "application/json"
      produces "application/json"

      parameter name: :folder_id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "0a048468-fa74-4f20-a72f-7a097d55383",
        description:
          "The ID of the folder."

      parameter name: :folder_membership_id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "bbbe2fa3-dc48-4984-aeb1-09c0a0c4d814",
        description:
          "The ID of the folder membership."

      parameter name: :acceptance_params,
        in: :body,
        schema: {
          "$ref" => "#/components/schemas/acceptance_params"
        }

      let(:acceptance_params) {
        {
          acceptance: {
            acceptance_token: acceptance_token
          }
        }
      }

      let(:new_folder_membership) { create(:folder_membership, user: create(:user, :with_keypair), folder: folder) }
      let(:folder_membership_id) { new_folder_membership.id }
      let(:other_folder_membership) { create(:folder_membership, user: create(:user, :with_keypair), folder: create(:folder)) }

      response "201", "Ok" do
        schema "$ref": "#/components/schemas/folder_membership"

        run_test! do |response|
          expect(json_response).to match({
            folder_membership: {
              id: new_folder_membership.id,
              role: new_folder_membership.role,
              accepted: true,
              pending: false,
              accepted_at: new_folder_membership.reload.accepted_at.as_json,
              is_me: false,
              created_at: new_folder_membership.created_at.as_json,
              updated_at: new_folder_membership.updated_at.as_json,
              missing_private_key: false,
              creator: {
                id: new_folder_membership.creator.id,
                display_name: new_folder_membership.creator.display_name,
                email: new_folder_membership.creator.email,
                picture: new_folder_membership.creator.picture
              },
              user: {
                id: new_folder_membership.user.id,
                display_name: new_folder_membership.user.display_name,
                email: new_folder_membership.user.email,
                picture: new_folder_membership.user.picture,
                public_key: new_folder_membership.user.keypair.public_key
              }
            }
          })
        end
      end

      response "404", "Not found" do
        let!(:folder_membership_id) { other_folder_membership.id }
        let!(:acceptance_token) { other_folder_membership.acceptance_token }

        run_test!
      end

      response "422", "Invalid request" do
        let!(:acceptance_params) {}
        run_test!
      end
    end

    get "Accept a Folder Membership from mail" do
      tags "Folders Membership Acceptances Endpoint"

      parameter name: :folder_id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "0a048468-fa74-4f20-a72f-7a097d55383",
        description:
          "The ID of the folder."

      parameter name: :folder_membership_id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "bbbe2fa3-dc48-4984-aeb1-09c0a0c4d814",
        description:
          "The ID of the folder membership."

      parameter name: :acceptance_token,
        in: :query,
        required: true,
        schema: {
          type: :string
        },
        example: "aEQgWTVmoQKqMbRcH97wu9wN",
        description:
          "The token that is required to accept the folder membership invite."
      let(:new_folder_membership) { create(:folder_membership, user: create(:user, :with_keypair), folder: folder) }
      let(:folder_membership_id) { new_folder_membership.id }
      let(:other_folder_membership) { create(:folder_membership, user: create(:user, :with_keypair), folder: create(:folder)) }

      response "201", "Ok" do
        run_test! do |response|
          expect(response.body).to include("Your shared item is available!")
        end
      end

      response "200", "Sorry, this link has expired." do
        let(:acceptance_token) { "empty" }
        run_test! do |response|
          expect(response.body).to include("Sorry, this link has expired.")
        end
      end
    end
  end
end
