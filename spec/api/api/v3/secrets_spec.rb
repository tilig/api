require "swagger_helper"

describe "api/v3/secrets", type: :request, v3: true do
  let(:user) { create(:user) }
  let(:user2) { create(:user) }
  let(:brand) { create(:dropbox_brand) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:own_secret) { create(:item, user: user, brand: brand, share_link: create(:share_link)) }
  let(:own_secret_google) do
    create(:google_play_item, brand: create(:brand), user: user)
  end
  let(:other_secret) { create(:item) }
  let(:default_headers) do
    {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"}
  end
  let(:authorization) { "Bearer #{access_token}" }
  let(:secret_params) do
    {
      name: "Name",
      website: "dropbox.com",
      username: "my-personal-username",
      notes: "notes",
      password: "very_secret_string",
      otp: "otp_secret",
      template: "login/1",
      encrypted_dek: "rsaencrypteddek=",
      encrypted_details: "encrypted_details",
      encrypted_overview: "encrypted_overview",
      encryption_version: 2
    }
  end
  let(:include_template) { nil }

  # let(:membership) { create(:membership, user: user) }
  # let(:group) { membership.group }
  # let(:group_id) { group.id }
  # let!(:group_secret) { create(:item, group: group, user: nil, brand: brand) }
  # let!(:other_group_secret) { create(:item, user: nil, group: create(:membership, user: user2).group, brand: brand) }

  path "/api/v3/secrets" do
    get("All secrets of the current user") do
      tags "Secrets Endpoint"
      produces "application/json"
      security [Bearer: []]

      parameter name: :after,
        in: :query,
        type: :string,
        required: false,
        schema: {
          type: :string,
          format: :datetime
        },
        description:
          "When set with a datetime it will only return the secrets updated after this time. It expects a is8601 format",
        example: "Sun, 15 May 2022 21:00:24 +0200"

      parameter name: :include_template,
        in: :query,
        type: :string,
        required: false,
        description:
          "When include is set to `securenote/1` securenotes will be included in the response",
        example: "securenote/1"

      response "200", "Index all secrets" do
        schema "$ref": "#/components/schemas/secrets"
        before do
          own_secret
          own_secret_google
          create(:item, user: user, template: "securenote/1")
        end

        run_test! do |response|
          expect(
            json_response[0][:domain]
          ).to eq own_secret.public_suffix_domain
          expect(json_response[0][:password]).to eq own_secret.password
          expect(json_response[0][:brand][:main_color_hex]).to eq "#0061ff"

          expect(
            json_response[1][:domain]
          ).to eq own_secret_google.public_suffix_domain
          expect(json_response[1][:password]).to eq own_secret_google.password
          expect(json_response[1][:brand][:main_color_hex]).to eq "#4285F4"
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end

    post("Create a new Secret") do
      tags "Secrets Endpoint"
      consumes "application/json"
      security [Bearer: []]

      parameter name: :secret_params,
        in: :body,
        schema: {
          "$ref" => "#/components/schemas/secret_params"
        }

      response "201", "Secret" do
        schema "$ref": "#/components/schemas/secret"

        run_test! do |response|
          expect(json_response).to include(
            {
              name: "Name",
              website: "dropbox.com",
              username: "my-personal-username",
              notes: "notes",
              password: "very_secret_string",
              otp: "otp_secret",
              template: "login/1",
              encrypted_dek: "rsaencrypteddek=",
              encrypted_details: "encrypted_details",
              encrypted_overview: "encrypted_overview",
              encryption_version: 2
            }
          )
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end

  path "/api/v3/secrets/search" do
    let(:domain) { "dropbox.com" }
    post("Search throught the current user's Secrets by domain") do
      tags "Secrets Endpoint"
      produces "application/json"
      security [Bearer: []]

      parameter name: :domain,
        in: :query,
        required: true,
        schema: {
          type: :string,
          format: :uri
        },
        example: "dropbox.com",
        description:
          "A domain to search the secrets by (only exact match on public_suffix_domain)"

      response "200", "Search secrets by domain" do
        schema "$ref": "#/components/schemas/secrets"
        before { own_secret }

        run_test! do |response|
          expect(
            json_response[0][:domain]
          ).to eq own_secret.public_suffix_domain
          expect(json_response[0][:password]).to eq own_secret.password

          expect(json_response[0][:brand][:main_color_hex]).to eq "#0061ff"
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end

  path("/api/v3/secrets/{id}") do
    get("Get one Secret by id") do
      tags "Secrets Endpoint"
      produces "application/json"
      security [Bearer: []]

      parameter name: :id,
        in: :path,
        required: ["id"],
        schema: {
          type: :string,
          format: :uuid
        },
        example: "f076ac78-c750-45f0-b9a0-7faf52d2f715"

      response "200", "Secret" do
        schema "$ref": "#/components/schemas/secret"
        context "with legacy encryption enabled (default)" do # rubocop:disable RSpec/EmptyExampleGroup
          let(:id) { own_secret.id }

          run_test! do |response|
            expect(json_response[:password]).to eq own_secret.password
            expect(json_response[:brand][:name]).to eq "Dropbox"
            expect(json_response[:brand][:main_color_hex]).to eq "#0061ff"
            expect(json_response[:brand][:main_color_brightness]).to eq "88"
            expect(
              json_response[:brand][:logo_source]
            ).to eq "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg"
            expect(
              json_response[:brand][:logo_icon_source]
            ).to eq "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg"
          end
        end

        context "with legacy encryption disabled" do # rubocop:disable RSpec/EmptyExampleGroup
          schema "$ref": "#/components/schemas/secret_encryption_v2"
          before { user.disable_legacy_encryption! }
          after { user.profile.update! application_settings: {} }

          let(:id) { own_secret.id }

          run_test! do |response|
            expect(json_response[:template]).to eq own_secret.template
            expect(json_response[:password]).to be_nil
            expect(json_response[:brand][:name]).to eq "Dropbox"
            expect(json_response[:brand][:main_color_hex]).to eq "#0061ff"
            expect(json_response[:brand][:main_color_brightness]).to eq "88"
            expect(
              json_response[:brand][:logo_source]
            ).to eq "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg"
            expect(
              json_response[:brand][:logo_icon_source]
            ).to eq "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg"
          end
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }
        let(:id) { own_secret.id }

        run_test!
      end

      response "404", "Not found" do
        let(:id) { "wrong-id" }

        run_test!
      end
    end
  end

  path("/api/v3/secrets/{id}") do
    put("Update the Secret by id") do
      tags "Secrets Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      parameter name: :id,
        in: :path,
        required: ["id"],
        schema: {
          type: :string,
          format: :uuid
        },
        example: "f076ac78-c750-45f0-b9a0-7faf52d2f715"
      parameter name: :secret_params,
        in: :body,
        schema: {
          "$ref" => "#/components/schemas/secret_params"
        }

      response "200", "Secret" do
        schema "$ref": "#/components/schemas/secret"
        let(:id) { own_secret.id }

        run_test! do |response|
          expect(json_response[:password]).to eq own_secret.reload.password
        end

        response "401", "Unauthenticated" do
          let(:authorization) { "Bearer " }

          run_test!
        end

        response "404", "Not found" do
          let(:id) { "not_findable" }

          run_test!
        end
      end
    end
  end

  path("/api/v3/secrets/{id}") do
    delete("Delete the Secret by id ") do
      tags "Secrets Endpoint"
      produces "application/json"
      security [Bearer: []]

      parameter name: :id,
        in: :path,
        required: ["id"],
        schema: {
          type: :string,
          format: :uuid
        }

      response "200", "Secret" do
        let(:id) { own_secret.id }

        run_test! do |response|
          expect(Item.active.exists?(id: own_secret.id)).to eq false
        end

        response "401", "Unauthenticated" do
          let(:authorization) { "Bearer " }

          run_test!
        end

        response "404", "Not found" do
          let(:id) { "not_findable" }

          run_test!
        end
      end
    end
  end

  # path "/api/v3/secrets" do
  #   get("All Secrets of the Group") do
  #     tags "Group Secret Endpoint"
  #     produces "application/json"
  #     security [Bearer: []]

  #     parameter name: :group_id,
  #       in: :query,
  #       required: ["group_id"],
  #       schema: {
  #         type: :string,
  #         format: :uuid
  #       },
  #       description: "The UUID of the group",
  #       example: "8e6eec27-8996-42f6-af88-cff689357fcb"

  #     parameter name: :after,
  #       in: :query,
  #       type: :string,
  #       required: false,
  #       schema: {
  #         type: :string,
  #         format: :datetime
  #       },
  #       description:
  #         "When set with a datetime it will only return the secrets updated after this time. It expects a is8601 format",
  #       example: "Sun, 15 May 2022 21:00:24 +0200"

  #     parameter name: :include_template,
  #       in: :query,
  #       type: :string,
  #       required: false,
  #       description:
  #         "When include is set to `securenote/1` securenotes will be included in the response",
  #       example: "securenote/1"

  #     response "200", "Index all secrets of a group" do
  #       schema "$ref": "#/components/schemas/secrets_encryption_v2"

  #       run_test! do |response|
  #         expect(json_response[0][:domain]).to eq group_secret.public_suffix_domain
  #         expect(json_response[0][:encrypted_dek]).to eq group_secret.encrypted_dek
  #       end
  #     end

  #     response "404", "Not found" do
  #       let(:group_id) { other_group_secret.group.id }

  #       run_test!
  #     end

  #     response "401", "Unauthenticated" do
  #       let(:authorization) { "Bearer " }

  #       run_test!
  #     end
  #   end

  #   post("Create a new Secret for the Group") do
  #     tags "Group Secret Endpoint"
  #     consumes "application/json"
  #     security [Bearer: []]

  #     parameter name: :secret_params, in: :body, schema: {
  #       "$ref" => "#/components/schemas/secret_params_encryption_v2"
  #     }

  #     parameter name: :group_id,
  #       in: :query,
  #       required: ["group_id"],
  #       schema: {
  #         type: :string,
  #         format: :uuid
  #       },
  #       description: "The UUID of the group",
  #       example: "8e6eec27-8996-42f6-af88-cff689357fcb"

  #     response "201", "Secret" do
  #       schema "$ref": "#/components/schemas/secret_encryption_v2"

  #       run_test! do |response|
  #         expect(json_response).to include(website: "dropbox.com")
  #         expect(json_response).to include(template: "login/1")
  #         expect(json_response).to include(encrypted_dek: "encrypted_dek")
  #         expect(json_response).to include(encrypted_details: "encrypted_details")
  #         expect(json_response).to include(encrypted_overview: "encrypted_overview")
  #         expect(json_response).to include(encryption_version: 2)
  #       end
  #     end
  #     response "401", "Unauthenticated" do
  #       let(:authorization) { "Bearer " }

  #       run_test!
  #     end

  #     response "404", "Not found" do
  #       let(:group_id) { other_group_secret.group.id }

  #       run_test!
  #     end
  #   end
  # end

  # path("/api/v3/secrets/{id}") do
  #   get("Get one Secret by id for the Group") do
  #     tags "Group Secret Endpoint"
  #     produces "application/json"
  #     security [Bearer: []]

  #     parameter name: :group_id,
  #       in: :query,
  #       required: ["group_id"],
  #       schema: {
  #         type: :string,
  #         format: :uuid
  #       },
  #       description: "The UUID of the group",
  #       example: "8e6eec27-8996-42f6-af88-cff689357fcb"

  #     parameter name: :id,
  #       in: :path,
  #       required: ["id"],
  #       schema: {
  #         type: :string,
  #         format: :uuid
  #       },
  #       example: "f076ac78-c750-45f0-b9a0-7faf52d2f715"

  #     response "200", "Secret" do
  #       let(:id) { group_secret.id }

  #       schema "$ref": "#/components/schemas/secret_encryption_v2"

  #       run_test! do |response|
  #         expect(json_response).to include(website: "dropbox.com")
  #         expect(json_response).to include(template: "login/1")
  #         expect(json_response).to include(encryption_version: 2)

  #         expect(json_response[:encrypted_dek]).to_not be_nil
  #         expect(json_response[:encrypted_details]).to_not be_nil
  #         expect(json_response[:encrypted_overview]).to_not be_nil
  #         expect(json_response[:password]).to be_nil
  #         expect(json_response[:brand][:name]).to eq "Dropbox"
  #         expect(json_response[:brand][:main_color_hex]).to eq "#0061ff"
  #         expect(json_response[:brand][:main_color_brightness]).to eq "88"
  #         expect(
  #           json_response[:brand][:logo_source]
  #         ).to eq "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg"
  #         expect(
  #           json_response[:brand][:logo_icon_source]
  #         ).to eq "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg"
  #       end

  #       response "401", "Unauthenticated" do
  #         let(:authorization) { "Bearer " }

  #         run_test!
  #       end
  #     end

  #     response "404", "Not found" do
  #       let(:id) { other_group_secret.id }

  #       run_test!
  #     end
  #   end
  # end

  # path("/api/v3/secrets/{id}") do
  #   put("Update the Secret by id") do
  #     tags "Group Secret Endpoint"
  #     consumes "application/json"
  #     produces "application/json"
  #     security [Bearer: []]

  #     parameter name: :group_id,
  #       in: :query,
  #       required: ["group_id"],
  #       schema: {
  #         type: :string,
  #         format: :uuid
  #       },
  #       description: "The UUID of the group",
  #       example: "8e6eec27-8996-42f6-af88-cff689357fcb"

  #     parameter name: :id,
  #       in: :path,
  #       required: ["id"],
  #       schema: {
  #         type: :string,
  #         format: :uuid
  #       },
  #       example: "f076ac78-c750-45f0-b9a0-7faf52d2f715"

  #     parameter name: :secret_params, in: :body, schema: {
  #       "$ref" => "#/components/schemas/secret_params_encryption_v2"
  #     }

  #     response "200", "Secret" do
  #       let(:id) { group_secret.id }
  #       let(:secret_params) {
  #         {
  #           template: "login/1",
  #           encrypted_details: "encrypted_details",
  #           encrypted_overview: "encrypted_overview"
  #         }
  #       }

  #       schema "$ref": "#/components/schemas/secret_encryption_v2"

  #       run_test! do |response|
  #         expect(json_response).to include(website: "dropbox.com")
  #         expect(json_response).to include(template: "login/1")
  #         expect(json_response).to include(encrypted_details: "encrypted_details")
  #         expect(json_response).to include(encrypted_overview: "encrypted_overview")
  #       end

  #       response "401", "Unauthenticated" do
  #         let(:authorization) { "Bearer " }

  #         run_test!
  #       end
  #     end

  #     response "404", "Not found" do
  #       let(:id) { other_group_secret.id }

  #       run_test!
  #     end
  #   end
  # end

  # path("/api/v3/secrets/{id}") do
  #   delete("Delete the Secret by id ") do
  #     tags "Group Secret Endpoint"
  #     produces "application/json"
  #     security [Bearer: []]

  #     parameter name: :group_id,
  #       in: :query,
  #       required: ["group_id"],
  #       schema: {
  #         type: :string,
  #         format: :uuid
  #       },
  #       description: "The UUID of the group",
  #       example: "8e6eec27-8996-42f6-af88-cff689357fcb"

  #     parameter name: :id,
  #       in: :path,
  #       required: ["id"],
  #       schema: {
  #         type: :string,
  #         format: :uuid
  #       }

  #     response "200", "Secret" do
  #       let(:id) { group_secret.id }

  #       run_test! do |response|
  #         expect(Secret.active.exists?(id: group_secret.id)).to eq false
  #       end

  #       response "401", "Unauthenticated" do
  #         let(:authorization) { "Bearer " }

  #         run_test!
  #       end
  #     end

  #     response "404", "Not found" do
  #       let(:id) { other_group_secret.id }

  #       run_test!
  #     end
  #   end
  # end
end
