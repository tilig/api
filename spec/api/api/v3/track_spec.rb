require "swagger_helper"

describe "api/v3/track", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:access_token) { create(:identity).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let(:event) { "Test Event" }
  let(:track) { {event: "Test Event", properties: {test_property: "yes"}, userProperties: {variant: "A"}} }
  let(:"x-tilig-platform") { "Test Client" }
  let(:"x-tilig-version") { "0.1" }
  let(:"x-browser-name") { "Safari" }
  let(:"x-browser-version") { "15.3" }
  let(:"x-os-name") { "Intel Mac OS X 10_15_7" }

  path "/api/v3/track" do
    post("Tracks the event with Mixpanel") do
      tags "Track Events Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      parameter name: "x-tilig-platform", in: :header, type: :string, example: "Test client"
      parameter name: "x-tilig-version", in: :header, type: :string, example: "3.1"
      parameter name: "x-browser-name", in: :header, type: :string, example: "Safari"
      parameter name: "x-browser-version", in: :header, type: :string, example: "15.3"
      parameter name: "x-os-name", in: :header, type: :string, example: "Intel Mac OS X 10_15_7"

      parameter in: :body, name: :track, schema: {
        type: :object,
        properties: {
          event: {type: :string, example: "Test Event"},
          properties: {type: :object, example: {test_property: "Yes"}, description: "Extra information can be added to an event "},
          userProperties: {type: :object, example: {variant: "A"}, description: "Properties that will be added to the user on Mixpanel"}
        },
        required: [:event]
      }

      response "200", "Track" do
        run_test! do |response|
          expect(json_response).to eq({msg: "ok"})
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end
end
