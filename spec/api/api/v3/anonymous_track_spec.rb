require "swagger_helper"
require "fixtures/two_f_a_directory/stubs"

describe "api/v3/anonymous_track", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:event) { "Uninstall Survey Answered" }
  let(:track) { {event: event, properties: {test_property: "yes"}} }
  let(:"x-tilig-platform") { "Test Client" }
  let(:"x-tilig-version") { "0.1" }

  path "/api/v3/anonymous_track" do
    post "Track events without having to authenticate a user" do
      tags "Track Anonymous Events Endpoint"
      consumes "application/json"
      produces "application/json"

      parameter name: "x-tilig-platform", in: :header, type: :string
      parameter name: "x-tilig-version", in: :header, type: :string

      parameter in: :body, name: :track, schema: {
        type: :object,
        properties: {
          properties: {type: :object},
          event: {type: :string}
        },
        required: [:properties]
      }

      response "200", "Track" do
        run_test! do |response|
          expect(json_response).to eq({msg: "ok"})

          have_enqueued_job(MixpanelJob)
        end
      end

      response "422", "Unprocessable Entity" do
        let(:event) { "Not allowed" }

        run_test! do |response|
          expect(json_response).to eq({error: "Unprocessable Entity"})
        end
      end
    end
  end
end
