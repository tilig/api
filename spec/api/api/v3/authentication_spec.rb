require "swagger_helper"
require "fixtures/mailchimp/stubs"

describe "api/v3/authenticate", type: :request do
  let(:token) { nil }

  path "/api/v3/authenticate" do
    post "Authenticate the User" do
      tags "Authenticate Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Token: []]

      parameter name: :referral,
        in: :query,
        type: :string,
        required: false,
        description: "String that needs to be submitted at sign up to identify the referral",
        example: "squirrel-8b36dc01d7"

      parameter name: :provider_id,
        in: :query,
        type: :string,
        required: false,
        description: "Indicating the client used to sign in",
        example: "google.com"

      parameter name: :trigger,
        in: :query,
        type: :string,
        required: false,
        description: "The trigger for the authenticate",
        example: "refresh"

      before do
        stub_mailchimp_request
      end

      response "403", "New users" do
        let(:token) { generate_key_service_token }
        let(:Authorization) { "Token #{token}" }

        before do
          stub_token_generator
        end

        after { |example| set_swagger_example(example) }

        run_test!
      end

      response "401", "Unauthorized" do
        examples "application/json" => {error: "Unauthorized"}
        let(:Authorization) { "Token " }
        let(:time_start) { Time.zone.now }

        before do
          stub_token_generator
          time_start
        end

        after { |example| set_swagger_example(example) }

        describe "because it is missing a token" do # rubocop:disable RSpec/EmptyExampleGroup
          run_test!
        end

        describe "invalid token" do # rubocop:disable RSpec/EmptyExampleGroup
          let(:Authorization) { "Token not-working" }

          run_test!
        end

        describe "because token is not prefixed with 'Token: '" do # rubocop:disable RSpec/EmptyExampleGroup
          let(:Authorization) { "Token not-working" }

          run_test!
        end
      end
    end
  end

  def stub_token_generator
    allow(Auth::RefreshTokenGenerator).to receive(:generate)
      .and_return("some-refresh-token")
    allow(Auth::AccessTokenGenerator).to receive(:generate)
      .and_return("some-access-token")
  end

  def set_swagger_example(example)
    example.metadata[:response][:content] = {
      "application/json" => {
        example: JSON.parse(response.body, symbolize_names: true)
      }
    }
  end
end
