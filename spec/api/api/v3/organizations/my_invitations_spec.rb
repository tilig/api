require "swagger_helper"

describe "api/v3/organization/my_invitations", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:organization) { create(:organization) }
  let(:user) { create(:user, :with_keypair) }
  let(:access_token) { create(:identity, user:).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let(:invitation) { create(:invitation, email: user.email, accepted_at: nil) }
  let(:invited_user) { create(:user) }

  let(:id) { invitation.id }

  path "/api/v3/organization/my_invitations" do
    get "List pending invitations" do
      tags "Organization - My Invitations"
      operationId "My Invitations"
      description "List invitations for the current user"
      security [Bearer: []]
      produces "application/json"

      response "200", "pending invitations found" do
        # schema "$ref" => "#/components/schemas/pending_invitations"
        before { invitation }

        run_test! do |response|
          expect(json_response).to match(
            {
              pending_invitations: [
                {
                  id: invitation.id,
                  accepted_at: nil,
                  expire_at: invitation.expire_at.as_json,
                  created_at: invitation.created_at.as_json,
                  updated_at: invitation.updated_at.as_json,
                  creator: {
                    id: invitation.creator.id,
                    email: invitation.creator.email,
                    display_name: invitation.creator.display_name,
                    picture: nil
                  },
                  organization: {
                    id: invitation.organization.id,
                    name: invitation.organization.name
                  }
                }
              ]
            }
          )
        end
      end
      response "401", "unauthorized" do
        let(:authorization) { "Bearer invalid" }

        run_test!
      end
    end
  end

  path "/api/v3/organization/my_invitations/{id}" do
    let(:invitation) { create(:invitation, organization: organization, invitee: user) }
    delete "Decline an invitation" do
      tags "Decline Invitation"
      security [Bearer: []]
      parameter name: :id, in: :path, type: :string, required: true, description: "Invitation ID", example: "ff326819-d4ad-430d-899f-50c026d85141"

      response "200", "Invitation Declined" do
        let(:id) { invitation.id }
        run_test! do
          expect(Invitation).to_not exist
        end
      end

      response "401", "Unauthorized" do
        let(:authorization) { "Bearer invalid" }
        let(:id) { invitation.id }
        run_test!
      end

      response "404", "Not found" do
        let(:id) { "invalid" }
        run_test!
      end
    end

    patch "Accept Organization Invitation" do
      tags "Organization Invitations Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      parameter name: :id,
        in: :path,
        required: ["id"],
        schema: {
          type: :string,
          format: :uuid
        },
        description: "The UUID of the Invitation",
        example: "8e6eec27-8996-42f6-af88-cff689357fcb"

      let(:authorization) { "Bearer #{create(:identity, user: invited_user).access_token}" }
      let!(:invitation) { create(:invitation, creator: user, email: invited_user.email) }
      let(:id) { invitation.id }

      response "200", "Ok" do
        schema "$ref": "#/components/schemas/organization_invitation"

        run_test! do |response|
          expect(json_response[:invitation][:accepted_at]).to_not be nil
        end
      end

      response "404", "Not found" do
        let(:id) { "empty" }

        run_test!
      end

      response "422", "Invalid request" do
        let!(:other_invitation) { create(:invitation, creator: create(:user), email: create(:user).email) }
        let(:id) { other_invitation.id }

        run_test!
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end
end
