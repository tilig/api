require "swagger_helper"

describe "api/v3/organization/invitations", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user, :with_organization) }
  let(:organization) { user.organization }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }

  let(:invited_user) { create(:user) }

  before {
    stub_request(:post, "https://mandrillapp.com/api/1.0/messages/send-template.json").and_return(status: 200, body: {}.to_json, headers: {})
  }

  path "/api/v3/organization/invitations" do
    post "Create Organization Invitations" do
      tags "Organization Invitations Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      parameter name: :organization_invitations_params,
        in: :body,
        schema: {
          "$ref" => "#/components/schemas/organization_invitations_params"
        }

      let(:organization_invitations_params) {
        {
          invitations: {
            emails: ["john.doe@tilig.com", "jane.doe@tilig.com"]
          }
        }
      }

      response "201", "Ok" do
        schema "$ref": "#/components/schemas/organization_invitations"

        run_test!
      end

      response "422", "Invalid request" do
        let(:organization_invitations_params) {
          {
            invitations: {
              emails: ["no-email"]
            }
          }
        }

        run_test! do |response|
          expect(json_response[:errors][0][:errors]).to include({email: ["is invalid"]})
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end

  path "/api/v3/organization/invitations/{id}" do
    get "Show Organization Invitation" do
      tags "Organization Invitations Endpoint"
      produces "application/json"

      parameter name: :id,
        in: :path,
        required: ["id"],
        schema: {
          type: :string,
          format: :uuid
        },
        description: "The UUID of the Invitation",
        example: "8e6eec27-8996-42f6-af88-cff689357fcb"

      parameter in: :query, name: :view_token, schema: {
        type: :object,
        invitation: {
          view_token: {type: :string, example: "q9D3xov5MHNLRZ4rQeRUAp7v", description: "The Token to view the Invitation"}
        },
        required: [:invitation]
      }

      let!(:invitation) { create(:invitation, creator: user, email: invited_user.email) }
      let(:id) { invitation.id }

      let(:view_token) {
        {
          invitation: {view_token: invitation.view_token}
        }
      }

      response "200", "Ok" do
        schema "$ref": "#/components/schemas/organization_invitation_show"

        run_test!
      end

      response "404", "Not found" do
        let(:id) { "empty" }

        run_test!
      end
    end

    patch "Accept Organization Invitation" do
      tags "Organization Invitations Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      parameter name: :id,
        in: :path,
        required: ["id"],
        schema: {
          type: :string,
          format: :uuid
        },
        description: "The UUID of the Invitation",
        example: "8e6eec27-8996-42f6-af88-cff689357fcb"

      let(:authorization) { "Bearer #{create(:identity, user: invited_user).access_token}" }
      let!(:invitation) { create(:invitation, creator: user, email: invited_user.email) }
      let(:id) { invitation.id }

      response "200", "Ok" do
        schema "$ref": "#/components/schemas/organization_invitation"

        run_test! do |response|
          expect(json_response[:invitation][:accepted_at]).to_not be nil
        end
      end

      response "404", "Not found" do
        let(:id) { "empty" }

        run_test!
      end

      response "422", "Invalid request" do
        let!(:other_invitation) { create(:invitation, creator: create(:user), email: create(:user).email) }
        let(:id) { other_invitation.id }

        run_test!
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end

    delete "Delete Organization Invitation" do
      tags "Organization Invitations Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      parameter name: :id,
        in: :path,
        required: ["id"],
        schema: {
          type: :string,
          format: :uuid
        },
        description: "The UUID of the Invitation",
        example: "8e6eec27-8996-42f6-af88-cff689357fcb"

      let!(:invitation) { create(:invitation, creator: user, email: invited_user.email, organization: user.organization) }
      let(:id) { invitation.id }

      response "200", "Deleted Invitation" do
        schema "$ref": "#/components/schemas/organization_invitation"

        run_test! do |response|
          expect(Invitation.exists?(id: id)).to eq false
        end
      end

      response "404", "Not found" do
        let!(:other_invitation) { create(:invitation, creator: create(:user), email: create(:user).email) }
        let(:id) { other_invitation.id }

        run_test!
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end
end
