require "swagger_helper"

describe "api/v3/folders", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user, :with_keypair) }
  let(:item) { create(:item, user:) }
  let(:access_token) { create(:identity, user:).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let(:folder) { create(:folder) }
  let(:folder_membership) { create(:folder_membership, :accepted, user: user, folder: folder) }
  let(:id) { folder.id }
  let(:folder_params) { {folder: {name: "Engineering"}} }

  path "/api/v3/folders" do
    get "Fetch all pending invitations for the creator of the invitaions" do
      tags "Folder Endpoint"
      produces "application/json"
      security [Bearer: []]

      response "200", "Ok" do
        let!(:invitee) { create(:user, :with_keypair) }
        let!(:pending_membership) { create(:folder_membership, user: invitee, creator: user, email: invitee.email, encrypted_private_key: nil) }

        run_test! do |response|
          expect(json_response).to match(
            {
              folders: [],
              pending_memberships: [
                {
                  accepted: false,
                  accepted_at: nil,
                  created_at: pending_membership.created_at.as_json,
                  id: pending_membership.id,
                  is_me: false,
                  missing_private_key: true,
                  pending: true,
                  role: "admin",
                  updated_at: pending_membership.updated_at.as_json,
                  creator: {
                    display_name: user.display_name,
                    email: user.email,
                    id: user.id,
                    picture: user.picture
                  },
                  user: {
                    display_name: nil, # No display name
                    email: invitee.email,
                    id: invitee.id,
                    picture: nil, # No picture
                    public_key: invitee.keypair.public_key
                  }
                }
              ]
            }
          )
        end
      end

      response "401", "Unauthorized" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end

    post "Creates a Folder" do
      tags "Folders Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: {}]

      parameter name: :folder_params,
        in: :body,
        schema: {
          "$ref" => "#/components/schemas/folder_creation_params"
        }

      let(:folder_params) {
        {folder: {
          public_key: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false),
          folder_memberships: [
            {user_id: user.id, encrypted_private_key: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false)}
          ],
          items: [
            {id: item.id, encrypted_folder_dek: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false)}
          ]
        }}
      }

      response "201", "Folder created" do
        schema "$ref": "#/components/schemas/folder"

        run_test! do |response|
          expect(FolderMembership.where(folder: Folder.last, user: user, role: "admin")).to exist
        end
      end

      response "422", "Invalid request" do
        let(:folder_params) {
          {
            folder: {
              public_key: ""
            },
            folder_memberships: [],
            items: []
          }
        }

        run_test! do |response|
          expect(json_response[:errors]).to include({public_key: ["can't be blank"]})
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end

  path "/api/v3/folders/{id}" do
    put "Update a Folder by id" do
      tags "Folders Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: {}]

      parameter name: :id, in: :path,
        required: true,
        schema: {type: :string, format: :uuid},
        example: "123e4567-e89b-12d3-a456-426655440000"
      parameter name: :folder_params, in: :body, schema: {
        "$ref": "#/components/schemas/folder_params"
      }

      before { folder_membership }

      response "200", "Folder updated" do
        schema "$ref": "#/components/schemas/folder"

        run_test! do |response|
          expect(json_response[:folder]).to include({name: "Engineering"})
        end
      end

      response "404", "Folder not found" do
        let(:id) { "123e4567-e89b-12d3-a456-426655440000" }

        run_test! do |response|
          expect(json_response).to eq(msg: "Not found")
        end
      end

      response "422", "Invalid request" do
        let(:folder) { create(:folder, single_item: false) }
        let(:folder_params) { {folder: {name: ""}} }

        run_test! do |response|
          expect(json_response).to eq({errors: {name: ["can't be blank"]}})
        end
      end
    end

    delete "Delete a Folder by id" do
      tags "Folders Endpoint"
      produces "application/json"
      security [Bearer: {}]

      parameter name: :id, in: :path,
        required: true,
        schema: {type: :string, format: :uuid},
        example: "123e4567-e89b-12d3-a456-426655440000"

      before { folder_membership }

      response "200", "Folder deleted" do
        run_test! do |response|
          expect(Folder.active.find_by(id: id)).to be_nil
        end
      end

      response "404", "Folder not found" do
        let(:id) { "123e4567-e89b-12d3-a456-426655440000" }

        run_test! do |response|
          expect(json_response).to eq(msg: "Not found")
        end
      end
    end
  end
end
