require "swagger_helper"

describe "api/v3/shares", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }

  let(:secret) { create(:google_play_item, brand: create(:brand), user: user) }
  let(:share_uuid) { SecureRandom.hex(16) }
  let(:share_access_token) { SecureRandom.hex(16) }
  let(:share_encrypted_dek) { Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false) }
  let(:share_encrypted_master_key) { Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false) }
  let(:share_params) do
    {
      uuid: share_uuid,
      access_token: share_access_token,
      encrypted_key: share_encrypted_dek,
      encrypted_master_key: share_encrypted_master_key
    }
  end
  let(:share) { create(:share_link, access_token: SecureRandom.hex(16), item_id: secret.id) }

  path "/api/v3/shares/" do
    get("List all Shares") do
      tags "Secrets Share Link Endpoint"
      produces "application/json"
      security [Bearer: []]

      before { share }

      response "200", "List Shares" do
        run_test! do |response|
          expect(json_response[0][:id]).to match share.id
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end

  path "/api/v3/secrets/{secret_id}/share" do
    post("Submit a new Share for the current user and certain Secret") do
      tags "Secrets Share Link Endpoint"
      consumes "application/json"
      security [Bearer: []]

      parameter name: :secret_id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "0a048468-fa74-4f20-a72f-7a097d55383",
        description:
          "The ID of the secret."
      parameter name: :share_params,
        in: :body,
        schema: {
          "$ref" => "#/components/schemas/new_share"
        }

      let(:secret_id) { secret.id }

      response "201", "Share created" do
        run_test!
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }
        run_test!
      end
    end
  end

  path "/api/v3/secrets/{secret_id}/share" do
    delete("Remove/destroy the Share") do
      tags "Secrets Share Link Endpoint"
      produces "application/json"
      security [Bearer: []]

      parameter name: :secret_id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "0a048468-fa74-4f20-a72f-7a097d55383",
        description:
          "The ID of the secret."

      let(:secret_id) { share.item.id }

      response "200", "Share disabled" do
        run_test! do |response|
          expect(Item.find(response.request.params["secret_id"]).share_link).to be nil
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test! do |response|
          expect(Item.find(response.request.params["secret_id"]).share_link).to_not be nil
        end
      end

      response "404", "Not found" do
        let(:secret_id) { "empty" }

        run_test!
      end
    end
  end
end
