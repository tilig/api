require "swagger_helper"

describe "api/v3/organization", type: :request, v3: true, bullet: :skip do # rubocop:disable RSpec/EmptyExampleGroup
  let(:organization) { create(:organization) }
  let(:user) { create(:user, :with_keypair, organization: organization) }
  let(:access_token) { create(:identity, user:).access_token }
  let(:authorization) { "Bearer #{access_token}" }

  path "/api/v3/organization" do
    post "Creates an Organization" do
      let(:organization) { nil }
      tags "Organizations Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: {}]

      parameter name: :organization_params,
        in: :body,
        schema: {
          "$ref" => "#/components/schemas/organization_params"
        }

      let(:organization_params) {
        {organization: {name: "Tilig"}}
      }

      response "201", "organization created" do
        schema "$ref": "#/components/schemas/organization"

        run_test! do |response|
          organization = Organization.last

          expect(json_response).to match({
            organization: {
              id: organization.id,
              name: organization.name,
              created_at: organization.created_at.as_json,
              updated_at: organization.updated_at.as_json,
              users: [{
                id: user.id,
                email: user.email,
                display_name: user.display_name,
                picture: user.picture
              }],
              invitations: []

            }
          })
        end
      end

      response "422", "Invalid request" do
        let(:organization_params) {
          {
            organization: {name: nil}
          }
        }

        run_test! do |response|
          expect(json_response[:errors]).to include({name: ["can't be blank"]})
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end

    get "Get Organization" do
      tags "Organizations Endpoint"
      produces "application/json"
      security [Bearer: {}]

      before {
        create(:invitation, creator: user, organization: organization) if organization
      }

      response "200", "Ok" do
        run_test! do |response|
          expect(json_response).to match({
            organization: {
              id: organization.id,
              name: organization.name,
              created_at: organization.created_at.as_json,
              updated_at: organization.updated_at.as_json,
              users: [{
                id: user.id,
                email: user.email,
                display_name: user.display_name,
                picture: user.picture
              }],
              invitations: [{
                accepted_at: nil,
                pending: true,
                expired: false,
                created_at: organization.invitations.first.created_at.as_json,
                creator: {
                  display_name: user.display_name,
                  email: user.email,
                  id: user.id,
                  picture: nil
                },
                email: organization.invitations.first.email,
                expire_at: organization.invitations.first.expire_at.as_json,
                id: organization.invitations.first.id,
                invitee: nil,
                updated_at: organization.invitations.first.updated_at.as_json
              }]
            }
          })
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end

      response "404", "Not Found" do
        let(:organization) { nil }

        run_test!
      end
    end

    put "Update the Organization" do
      tags "Organizations Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: {}]

      parameter name: :organization_params,
        in: :body,
        schema: {
          "$ref" => "#/components/schemas/organization_params"
        }

      let(:organization_params) {
        {organization: {name: "Tilig"}}
      }

      response "200", "Ok" do
        run_test! do |response|
          organization.reload
          expect(json_response).to match({
            organization: {
              id: organization.id,
              name: "Tilig",
              created_at: organization.created_at.as_json,
              updated_at: organization.updated_at.as_json,
              users: [{
                id: user.id,
                email: user.email,
                display_name: user.display_name,
                picture: user.picture
              }],
              invitations: []
            }
          })
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end

      response "404", "Not Found" do
        let(:organization) { nil }

        run_test!
      end
    end
  end
end
