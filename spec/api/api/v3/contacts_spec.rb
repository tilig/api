require "swagger_helper"

describe "api/v3/contacts", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user, :with_organization, :with_keypair) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let!(:organization_user) { create(:user, :with_keypair, organization: user.organization) }
  let!(:contact) { create(:contact, user: user, contact: create(:user, :with_keypair)) }

  path "/api/v3/contacts" do
    get "Get the contacts of the current user" do
      tags "Contacts Endpoint"
      produces "application/json"
      security [Bearer: []]

      response "200", "Ok" do
        schema "$ref": "#/components/schemas/contact"

        run_test! do |response|
          expect(json_response[:contacts]).to include({
            display_name: contact.contact.display_name,
            email: contact.contact.email,
            id: contact.contact.id,
            picture: contact.contact.picture,
            public_key: contact.contact.keypair.public_key,
            source: "Contact",
            organization: nil
          })
          expect(json_response[:contacts]).to include({
            display_name: organization_user.display_name,
            email: organization_user.email,
            id: organization_user.id,
            picture: organization_user.picture,
            public_key: organization_user.keypair.public_key,
            source: "Team",
            organization: {
              id: user.organization.id,
              name: user.organization.name
            }
          })
        end
      end

      response "401", "Unauthorized" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end
end
