require "swagger_helper"

describe "api/v3/items", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }

  let!(:brand) do
    create(
      :brand,
      name: "Dropbox",
      domain: "dropbox.com",
      public_suffix_domain: "dropbox.com",
      logo_source: "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg",
      logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
      main_color_brightness: "88",
      main_color_hex: "#0061ff",
      totp: true,
      brandfetch_json: {filled: true},
      fetch_date: Time.current
    )
  end

  let(:own_item) { create(:item, user: user, brand: brand, share_link: create(:share_link)) }
  let(:own_item_2) { create(:item, user: user, brand: brand, share_link: create(:share_link)) }
  let!(:other_item) { create(:item, user: create(:user)) }
  let(:item_params) do
    {
      name: "Name",
      website: "dropbox.com",
      username: "my-personal-username",
      notes: "notes",
      password: "very_secret_string",
      otp: "otp_secret",
      template: "login/1",
      encrypted_dek: "encrypted_dek",
      encrypted_details: "encrypted_details",
      encrypted_overview: "encrypted_overview",
      encryption_version: 2
    }
  end
  let(:include_template) { nil }

  path "/api/v3/items" do
    get("All Items of the current user") do
      tags "Items Endpoint"
      produces "application/json"
      security [Bearer: []]

      parameter name: :after,
        in: :query,
        type: :string,
        required: false,
        schema: {
          type: :string,
          format: :datetime
        },
        description:
          "When set with a datetime it will only return the items updated after this time. It expects a is8601 format",
        example: "Sun, 15 May 2022 21:00:24 +0200"

      parameter name: :include_template,
        in: :query,
        type: :string,
        required: false,
        description:
          "When include is set to `securenote/1` securenotes will be included in the response",
        example: "securenote/1"

      response "200", "Index all Items" do
        schema "$ref": "#/components/schemas/items"
        # TODO: Fix bullet false positive
        before do
          Bullet.enable = false
          own_item
          own_item_2
          create(:item, user: user, template: "securenote/1")
        end

        after { Bullet.enable = true }

        run_test! do |response|
          expect(
            json_response[:items][0][:domain]
          ).to eq own_item.public_suffix_domain
          expect(json_response[:items][0][:password]).to eq own_item.password
          expect(json_response[:items][0][:brand][:main_color_hex]).to eq "#0061ff"

          expect(
            json_response[:items][1][:domain]
          ).to eq own_item_2.public_suffix_domain
          expect(json_response[:items][1][:password]).to eq own_item_2.password
          expect(json_response[:items][1][:brand][:main_color_hex]).to eq "#0061ff"
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end

    post("Create a new Item") do
      tags "Items Endpoint"
      consumes "application/json"
      security [Bearer: []]

      parameter name: :item_params,
        in: :body,
        schema: {
          "$ref" => "#/components/schemas/item_params"
        }

      response "201", "New Item" do
        schema "$ref": "#/components/schemas/item"

        run_test! do |response|
          expect(
            json_response[:item][:name]
          ).to eq "Name"
          expect(json_response[:item][:website]).to eq "dropbox.com"
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end

    path("/api/v3/items/{id}") do
      get("Get one Item by id") do
        tags "Items Endpoint"
        produces "application/json"
        security [Bearer: []]

        parameter name: :id,
          in: :path,
          required: ["id"],
          schema: {
            type: :string,
            format: :uuid
          },
          example: "f076ac78-c750-45f0-b9a0-7faf52d2f715"

        response "200", "Item" do
          schema "$ref": "#/components/schemas/item"
          context "with legacy encryption enabled (default)" do # rubocop:disable RSpec/EmptyExampleGroup
            let(:id) { own_item.id }

            run_test! do |response|
              expect(json_response[:item][:password]).to eq own_item.password
              expect(json_response[:item][:brand][:name]).to eq "Dropbox"
              expect(json_response[:item][:brand][:main_color_hex]).to eq "#0061ff"
              expect(json_response[:item][:brand][:main_color_brightness]).to eq "88"
              expect(
                json_response[:item][:brand][:logo_source]
              ).to eq "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg"
              expect(
                json_response[:item][:brand][:logo_icon_source]
              ).to eq "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg"
            end
          end

          context "with legacy encryption disabled" do # rubocop:disable RSpec/EmptyExampleGroup
            schema "$ref": "#/components/schemas/item"
            before { user.disable_legacy_encryption! }
            after { user.profile.update! application_settings: {} }

            let(:id) { own_item.id }

            run_test! do |response|
              expect(json_response[:item][:template]).to eq own_item.template
              expect(json_response[:item][:password]).to be_nil
              expect(json_response[:item][:brand][:name]).to eq "Dropbox"
              expect(json_response[:item][:brand][:main_color_hex]).to eq "#0061ff"
              expect(json_response[:item][:brand][:main_color_brightness]).to eq "88"
              expect(
                json_response[:item][:brand][:logo_source]
              ).to eq "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg"
              expect(
                json_response[:item][:brand][:logo_icon_source]
              ).to eq "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg"
            end
          end
        end

        response "401", "Unauthenticated" do
          let(:authorization) { "Bearer " }
          let(:id) { own_item.id }

          run_test!
        end

        response "404", "Not found" do
          let(:id) { other_item.id }

          run_test!
        end
      end

      put("Update the Item by id") do
        tags "Items Endpoint"
        consumes "application/json"
        produces "application/json"
        security [Bearer: []]

        parameter name: :id,
          in: :path,
          required: ["id"],
          schema: {
            type: :string,
            format: :uuid
          },
          example: "f076ac78-c750-45f0-b9a0-7faf52d2f715"

        parameter name: :item_params,
          in: :body,
          schema: {
            "$ref" => "#/components/schemas/item_params"
          }

        response "200", "Updated Item" do
          schema "$ref": "#/components/schemas/item"
          let(:id) { own_item.id }

          run_test! do |response|
            expect(json_response[:item][:password]).to eq own_item.reload.password
          end

          response "401", "Unauthenticated" do
            let(:authorization) { "Bearer " }

            run_test!
          end

          response "404", "Not found" do
            let(:id) { other_item.id }

            run_test!
          end
        end
      end

      delete("Delete the Item by id ") do
        tags "Items Endpoint"
        produces "application/json"
        security [Bearer: []]

        parameter name: :id,
          in: :path,
          required: ["id"],
          schema: {
            type: :string,
            format: :uuid
          }

        response "200", "Deleted Item" do
          let(:id) { own_item.id }

          run_test! do |response|
            expect(Item.active.exists?(id: own_item.id)).to eq false
          end

          response "401", "Unauthenticated" do
            let(:authorization) { "Bearer " }

            run_test!
          end

          response "404", "Not found" do
            let(:id) { other_item.id }

            run_test!
          end
        end
      end
    end
  end
end
