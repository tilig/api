require "swagger_helper"

describe "api/v3/brands/search", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:brand_instagram) { create(:brand, name: "Instagram", domain: "instagram.com", brandfetch_json: {}, public_suffix_domain: "instagram.com", rank: 10) }
  let(:brand_instacart) { create(:brand, name: "Instacart", domain: "instacart.com", brandfetch_json: {}, public_suffix_domain: "instacart.com", rank: 100) }
  let(:brand_instafamous) { create(:brand, name: "Instafamous", domain: "instafamous.com", brandfetch_json: {}, public_suffix_domain: "instafamous.com", rank: nil) }
  let(:other_brand) { create :brand, name: "google", domain: "play.google.com", brandfetch_json: {}, public_suffix_domain: "play.google.com" }
  let(:prefix) { "insta" }

  let(:access_token) { create(:identity).access_token }
  let(:authorization) { "Bearer #{access_token}" }

  path "/api/v3/brands/search" do
    get "Get Brands by name leading substring" do
      tags "Brands Search Endpoint"
      produces "application/json"
      security [Bearer: []]

      parameter name: :prefix, in: :query, type: :string, required: false, example: "insta",
        description: "Returns all Brands that can be autocompleted by the given string"

      parameter name: :page_size, in: :query, type: :integer, required: false, example: 20,
        description: "Sets the max size of the returned array."

      response "200", "Returns all matching brands orderd by rank" do
        schema "$ref": "#/components/schemas/brands"
        before do
          brand_instagram
          brand_instacart
          brand_instafamous
          other_brand
        end

        run_test! do |response|
          expect(json_response.count).to be 2

          expect(json_response.first).to include({name: "Instagram"})
          expect(json_response.second).to include({name: "Instacart"})
        end
      end

      response "401", "Unauthorized" do
        let(:url) { nil }
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end
end
