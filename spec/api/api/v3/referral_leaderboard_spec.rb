require "swagger_helper"

describe "api/v3/referral_leaderboard", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) do
    create(:user, :with_keypair).tap { |u| u.profile.update! referral_count: 1 }
  end
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }

  path "/api/v3/referral_leaderboard" do
    get("Return Referral Leaderboard") do
      tags "Referral Leaderboard Endpoint"
      produces "application/json"
      security [Bearer: []]

      before do
        9.times do |i|
          create(:user, email: "u#{i}@example.com").profile.update! referral_count: 10 - i
        end
      end

      response "200", "Ok" do
        run_test! do |response|
          expect(json_response.dig(:referral_leaderboard, :top_list, 0)).to eq({points: 100, rank: 1, email_short: "u0", is_me: false})
          expect(json_response.dig(:referral_leaderboard, :top_list, 1)).to eq({points: 90, rank: 2, email_short: "u1", is_me: false})
          expect(json_response.dig(:referral_leaderboard, :top_list, 2)).to eq({points: 80, rank: 3, email_short: "u2", is_me: false})
          expect(json_response.dig(:referral_leaderboard, :top_list, 9)).to eq({points: 10, rank: 10, email_short: user.email, is_me: true})

          expect(json_response[:referral_leaderboard][:me]).to eq({email_short: user.email, is_me: true, points: 10, rank: 10})
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end
end
