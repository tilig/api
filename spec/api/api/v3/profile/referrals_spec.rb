require "swagger_helper"
require "fixtures/mailchimp/stubs"

describe "api/v3/profile/referrals", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let!(:referral_user) { create(:user) }

  let(:referrals_params) { {referral: {referral_link: referral_user.profile.application_settings["referral_link"]}} }

  path "/api/v3/profile/referrals" do
    put("Update the current user's referral") do
      tags "Referrals Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      before { stub_mailchimp_request }

      parameter name: :referrals_params, in: :body, schema: {"$ref": "#/components/schemas/referral_params"}

      response "200", "Updated" do
        run_test! do |response|
          expect(json_response.dig(:profile, :application_settings)).to match(
            {
              referral_link: user.profile.application_settings["referral_link"],
              used_referral_link: referral_user.profile.application_settings["referral_link"]
            }
          )
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end
end
