require "swagger_helper"

describe "api/v3/profile/user_settings", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }

  let(:user_settings_params) { {user_settings: {onboarding: {finished: true}}} }

  path "/api/v3/profile/user_settings" do
    put("Update the current user's User Settings") do
      tags "User Profile Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      parameter name: :user_settings_params, in: :body, schema: {"$ref": "#/components/schemas/user_settings_params"}

      response "200", "Updated" do
        run_test! do |response|
          expect(json_response).to match(
            {
              onboarding: {
                finished: true
              }
            }
          )
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end
end
