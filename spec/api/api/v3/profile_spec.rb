require "swagger_helper"

describe "api/v3/profile", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:profile) {
    build(:profile,
      application_settings: {"disable_legacy_encryption" => true},
      user_settings: {"onboarding" => "finished"})
  }
  let(:user) { create(:user, profile: profile) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }

  path "/api/v3/profile" do
    get("Get the current user's Profile") do
      tags "User Profile Endpoint"
      produces "application/json"
      security [Bearer: []]

      response "200", "Profile" do
        run_test! do |response|
          expect(json_response.as_json).to match(
            {
              id: user.id,
              email: user.email,
              public_key: user.public_key,
              display_name: user.display_name,
              given_name: user.given_name,
              family_name: user.family_name,
              locale: user.locale,
              picture: user.picture,
              provider_id: user.provider_id,
              created_at: user.created_at.as_json,
              updated_at: user.updated_at.as_json,
              application_settings: user.profile.application_settings,
              user_settings: user.profile.user_settings,
              keypair: nil
            }.as_json
          )
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end

    patch("Update the current user's Profile") do
      tags "User Profile Endpoint"
      produces "application/json"
      consumes "application/json"
      security [Bearer: []]

      let(:user_params) do
        {
          user: {
            given_name: "Given",
            family_name: "Family",
            locale: "NL",
            picture: "string-to-picture.png"
          }
        }
      end

      parameter name: :user_params,
        in: :body,
        schema: {
          type: :object,
          properties: {
            given_name: {
              type: :string,
              nullable: true
            },
            display_name: {
              type: :string,
              nullable: true
            },
            family_name: {
              type: :string,
              nullable: true
            },
            locale: {
              type: :string,
              nullable: true
            }
          }
        }

      response "200", "Profile" do
        run_test! do |response|
          updated_user = user.reload
          expect(updated_user.given_name).to eq("Given")
          expect(updated_user.family_name).to eq("Family")
          expect(updated_user.display_name).to eq("Given")
          expect(updated_user.locale).to eq("NL")
          expect(updated_user.picture).to eq("string-to-picture.png")
        end

        response "401", "Unauthenticated" do
          let(:authorization) { "Bearer " }

          run_test!
        end
      end
    end

    delete("Delete the current user's Profile and everything belonging to it.") do
      tags "User Profile Endpoint"
      produces "application/json"
      security [Bearer: []]

      response "204", "Succesfully deleted the user profile" do
        run_test! { |response| expect(User.where(id: user.id)).to_not exist }

        response "401", "Unauthenticated" do
          let(:authorization) { "Bearer " }

          run_test!
        end
      end
    end
  end
end
