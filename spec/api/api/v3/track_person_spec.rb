require "swagger_helper"

describe "api/v3/track_person", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:access_token) { create(:identity).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let(:track) { {properties: {variant_survey: "A"}} }

  path "/api/v3/track_person" do
    post("Updates the person profile with the given properties") do
      tags "Track Person Events Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      parameter in: :body, name: :track, schema: {
        type: :object,
        properties: {
          properties: {type: :object, example: {variant_onboarding_survey: "A"}, description: "The properties that are added to the mixpanel profile of a user"}
        },
        required: [:properties]
      }

      response "201", "Track" do
        run_test! do |response|
          expect(json_response).to eq({msg: "ok"})
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end
end
