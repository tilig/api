require "swagger_helper"
require "fixtures/brandfetch/fixtures"

describe "api/v3/polling/items", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let!(:brand) do
    create(
      :brand,
      name: "Dropbox",
      domain: "dropbox.com",
      public_suffix_domain: "dropbox.com",
      logo_source: "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg",
      logo_icon_source: "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
      main_color_brightness: "88",
      main_color_hex: "#0061ff",
      totp: true,
      brandfetch_json: {filled: true},
      fetch_date: Time.current
    )
  end
  let!(:own_item) { create(:item, user: user, brand: brand, share_link: create(:share_link)) }
  let!(:own_item_2) { create(:google_play_item, user: user, brand: create(:brand)) }

  let(:default_headers) do
    {"x-tilig-version" => "1.0", "x-tilig-platform" => "Web"}
  end
  let(:authorization) { "Bearer #{access_token}" }

  path "/api/v3/polling/items" do
    get("All updated and deleted Items of the current user. At max 7 days into the past") do
      tags "Polling Items Endpoint"
      produces "application/json"
      security [Bearer: []]

      parameter name: :after,
        in: :query,
        type: :string,
        required: false,
        schema: {
          type: :string,
          format: :datetime
        },
        description:
          "When set with a datetime it will only return the updated items and a list of ID's with deleted items updated after this time. It expects a is8601 format and is limeted to max 7 days",
        example: "Sun, 15 May 2022 21:00:24 +0200"

      response "200", "Index all items" do
        before { own_item_2.soft_destroy }

        run_test! do |response|
          expect(json_response[:updates][0][:domain]).to eq own_item.public_suffix_domain
          expect(json_response[:deletions][0]).to eq own_item_2.id
          expect(json_response[:fetched_at]).to_not be_nil
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end
end
