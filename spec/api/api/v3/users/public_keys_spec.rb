require "swagger_helper"

describe "api/v3/users/public_key", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }

  let(:user_2) { create(:user, email: "john.doe@tilig.com") }
  let(:email) { user_2.email }
  let!(:user_2_keypair) { create(:keypair, user: user_2) }

  path "/api/v3/users/public_key" do
    get "Get a Public Key of a User based on the email" do
      tags "Users Public Key Endpoint"
      produces "application/json"
      security [Bearer: []]

      response "200", "Ok" do
        parameter name: :email,
          in: :query,
          schema: {type: :string, format: :email},
          required: true,
          description:
            "Email of the user of which the public key is requested",
          example: "john.doe@tilig.com"

        schema "$ref": "#/components/schemas/public_key"

        run_test! do |response|
          expect(json_response[:user]).to match({
            public_key: user_2_keypair.public_key,
            key_type: user_2_keypair.key_type,
            kid: user_2_keypair.kid,
            id: user_2.id,
            email: email
          })
        end

        response "404", "Not found" do
          let(:email) { "does-not-exist@tilig.com" }

          run_test!
        end
      end

      response "401", "Unauthorized" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end
end
