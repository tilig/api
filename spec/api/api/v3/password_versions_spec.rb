require "swagger_helper"

describe "api/v3/secrets/{secret_id}/password_versions", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:own_secret) { create(:item, password: "first password", user: user) }
  let(:secret_id) { own_secret.id }
  let(:authorization) { "Bearer #{access_token}" }

  before do
    allow_any_instance_of(Item).to receive(:set_brand).and_return true
  end

  path "/api/v3/secrets/{secret_id}/password_versions" do
    get("Index all password versions of a Secret") do
      tags "Password Versions Endpoint"
      produces "application/json"
      security [Bearer: []]

      parameter name: :secret_id, in: :path, schema: {type: :string, format: :uuid}

      response "404", "Not found " do
        let(:own_secret) { create(:item, password: "first password") }

        before do
          own_secret.update(password: "second password")
          own_secret.update(password: "thrid password")
        end

        run_test! do |response|
          expect(json_response).to match({msg: "Not found"})
        end
      end

      response "200", "Index all password versions" do
        before do
          own_secret.update(password: "second password")
          own_secret.update(password: "thrid password")
        end

        run_test! do |response|
          expect(json_response.first).to include({password: "first password"})
          expect(json_response.second).to include({password: "second password"})
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end
    end
  end
end
