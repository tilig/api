require "swagger_helper"

describe "api/v3/refresh", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:refresh_token) { create(:identity).refresh_token }
  let(:Authorization) { "Token #{refresh_token}" }

  before do
    refresh_token
    allow(Auth::RefreshTokenGenerator).to receive(:generate)
      .and_return("some-refresh-token")
    allow(Auth::AccessTokenGenerator).to receive(:generate)
      .and_return("some-access-token")
  end

  path "/api/v3/refresh" do
    post("Refresh the authentication tokens") do
      tags "Refresh Token Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Token: []]

      response "200", "Request new tokens" do
        run_test!
      end

      response "401", "Unauthenticated" do
        let(:refresh_token) { nil }

        run_test!
      end
    end
  end
end
