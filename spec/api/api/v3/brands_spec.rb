require "swagger_helper"
require "fixtures/two_f_a_directory/stubs"
require "fixtures/brandfetch/fixtures"

describe "api/v3/brands", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:brand) { create(:brand, breached_at: "2023-03-16T00:00:00.000Z", breach_published_at: "2023-03-16T00:00:00.000Z") }

  let(:other_brand) {
    create :brand, name: "007Names", domain: "007names.com", totp: false
  }
  let(:access_token) { create(:identity).access_token }
  let(:fetched_json) { JSON.parse(TwoFaDirectory::TWO_FA_DIRECTORY_RESPONSE) }
  let(:authorization) { "Bearer #{access_token}" }

  before { stub_brandfetch_google }

  path "/api/v3/brands" do
    get "Get a Brand by url, the url is passed as GET query" do
      tags "Brands Endpoint"
      produces "application/json"
      security [Bearer: []]

      parameter name: :url, in: :query, type: :string, required: false

      response "200", "Brand returned" do
        schema "$ref": "#/components/schemas/brand"
        let(:url) { ERB::Util.url_encode("http://#{brand.domain}") }

        before do
          brand
          other_brand
        end

        run_test! do |response|
          expect(json_response[:name]).to eq "Google play"
          expect(json_response[:domain]).to eq "play.google.com"
          expect(json_response[:public_suffix_domain]).to eq "google.com"
          expect(json_response[:totp]).to be true
          expect(json_response[:main_color_hex]).to eq "#4285F4"
          expect(json_response[:main_color_brightness]).to eq "127"
          expect(json_response[:logo_source]).to eq "https://asset.brandfetch.io/id6O2oGzv-/idP6g0Ep4y.svg"
          expect(json_response[:logo_icon_source]).to eq "https://asset.brandfetch.io/id6O2oGzv-/idNEgS9h8q.jpeg"
          expect(json_response[:breached_at]).to eq "2023-03-16T00:00:00.000Z"
          expect(json_response[:breach_published_at]).to eq "2023-03-16T00:00:00.000Z"

          expect(json_response[:images]).to eq({
            icon: {
              png: {
                original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
              },
              svg: {
                original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
              },
              theme: "light"
            }
          })
        end
      end

      response "401", "Unauthorized" do
        let(:url) { nil }
        let(:authorization) { "Bearer " }

        before { brand }

        run_test!
      end

      response "404", "Not found" do
        let(:url) { "does-not-exist.com" }

        before { stub_brandfetch_error }

        run_test! do
          expect(json_response[:msg]).to eq "Not found"
        end
      end

      response "422", "Unprocesseble entity" do
        let(:url) { ERB::Util.url_encode("http://   #{brand.domain}") }

        run_test! do
          expect(json_response[:msg]).to eq "Unprocessable entity"
        end
      end
    end
  end

  def stub_brandfetch_google
    @brandfetch_stub = stub_request(:get, "https://api.brandfetch.io/v2/brands/play.google.com")
      .to_return(status: 200, body: BrandfetchFixtures::GOOGLE_PLAY, headers: {"Content-Type" => "application/json"})
  end

  def stub_brandfetch_error
    @brandfetch_stub = stub_request(:get, "https://api.brandfetch.io/v2/brands/does-not-exist.com")
      .to_return(status: 503, body: BrandfetchFixtures::NOT_FOUND, headers: {"Content-Type" => "application/json"})
  end
end
