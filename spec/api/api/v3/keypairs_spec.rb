require "swagger_helper"

describe "api/v3/keypairs", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }
  let(:keypair) { create(:keypair, user: user) }

  let(:private_key) do
    '-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIKZ8vLJtU5EzqbMfhRJE1bPhdbIPYC7OBlSOoIjJquS7oAoGCCqGSM49
AwEHoUQDQgAEj3qy5L8FlEMjj0PIvVLbOTSCKR+rizHzVqMsvRKA/4O63XMEEwGd
uv5bl+/B1f1sTI4UyymYZ+jTpnrVgjs+qQ==
-----END EC PRIVATE KEY-----'
  end
  let(:invalid_private_key) do
    '-----BEGIN EC PRIVATE KEY-----
MHcCAQEEINmawoJxtxjYS77CJDRIdDAEsx8q+7sb7HbGAqgYW5TkoAoGCCqGSM49
AwEHoUQDQgAEC8syfrpKh5/b9nzUG3j1+82FmnZ1ughc8LY8Iebc0MTBBLYWAYqm
bz1anI/0CrCwHnyedpFCCh6HakN/Kllwnw==
-----END EC PRIVATE KEY-----'
  end

  let(:token) { JWT.encode(payload, OpenSSL::PKey::EC.new(private_key), "ES256") }
  let(:invalid_token) { JWT.encode(payload, OpenSSL::PKey::EC.new(invalid_private_key), "ES256") }

  let(:payload) {
    {
      kid: "t5DMJYRHJZ4Mp6dF63PfFQi3xO6jfIjKASLSq97jJEM",
      key: {
        encrypted_private_key: "Er4DEoADbjH9BUjxIyxSyPQs5XcVuKOv_1w4fc6Z--nUCiRF2sF4IoArBe4Te_7G7ae52JZjlM0ZdftsxvTI8ckkZEQSPc8CHYmz_xLW1TJq_Mo2pa4AbhY2_bbBzemEeRelpWxee5wYW-2s4-cUB1tq0Z_mLfoMr9LFAfoXlYgg8b9ouUrnrKiJQG-hweACvTsjtyseAsQ4KJrB10byhpOKcdDv8xZS2dGQImZqamAgUNvH3o_0eGXTWlygHYppMjnYn78WjwkPMf_rRRsEw_Mi73_KXqvduTQQ6JFRh2_p6qIagRpajIUkRiLBWl3EvO4-xc77i_U9r3xNhP9YpAo_pfcqubjs-r54f5jAotMUIcQwsCy3ubtgZW8BlPo6xeEVEolWFrJSZcIpvHJoC_51jH44PYp3JkwRDivwQofWqtEQ-QVq8thQpNQacnfAeuu6zVcxalJKWHiIYBtPJcpK_PHT7y3A2STM6_dkrnjMU4-K8JrAvyl3sOugcRdQRC4pl46IGhxpeWlPaWF0ZGowZDVwcEFEa055UVl0azl0T2gxIhV3b3V0ZXJ0aWxpZ0BnbWFpbC5jb20oraeUmwYaRjBEAiAEOb0r20UgeoaFLd2juclyUBTfcESqdJPjHKHodu1xGgIgNjjZE2yyWLRKD5twltphumAs_gGYNTCuLYUAOUCNOEY",
        public_key: "XXAqsNbMz760m_OX2S4e-hhtcoHR5M8uFZCC8wf451w",
        key_type: "x25519"
      },
      meta: {
        app_platform: "platform",
        app_version: "version"
      },
      kind: "encrypted-keypair",
      aud: "tilig-saas",
      iss: "tilig-ks",
      iat: iat,
      sub: "iyiOiatdj0d5ppADkNyQYtk9tOh1#key-t5DMJYRHJZ4Mp6dF63PfFQi3xO6jfIjKASLSq97jJEM"
    }
  }
  let(:iat) { Time.current.to_i }

  path "/api/v3/keypairs" do
    post("Set the Keypair for the current user") do
      tags "Keypairs Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      parameter name: :keypair_params, in: :body, schema: {"$ref" => "#/components/schemas/new_keypair"}

      let(:keypair_params) { {encrypted_keypair: token} }

      response "201", "Keypairs" do
        run_test! do |response|
          expect(json_response).to match(
            {
              encrypted_private_key: user.keypair.encrypted_private_key,
              public_key: user.keypair.public_key,
              key_type: user.keypair.key_type,
              meta_data: {
                app_platform: user.keypair.meta_data.dig("app_platform"),
                app_version: user.keypair.meta_data.dig("app_version")
              },
              kid: user.keypair.kid
            }
          )
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test!
      end

      response "422", "Unprocessable Entity" do
        let(:keypair_params) { {encrypted_keypair: invalid_token} }
        run_test! do
          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to match({base: "JWT payload invalid"})
        end
      end

      response "422", "Unprocessable Entity" do
        before { keypair }

        run_test! do
          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to match({base: "Keypair already created"})
        end
      end
    end
  end
end
