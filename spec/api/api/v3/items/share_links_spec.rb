require "swagger_helper"
describe "api/v3/items/{item_id}/share_link", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:user) { create(:user) }
  let(:access_token) { create(:identity, user: user).access_token }
  let(:authorization) { "Bearer #{access_token}" }

  let!(:item) { create(:item, user: user) }

  let(:share_link_uuid) { SecureRandom.hex(16) }
  let(:share_link_access_token) { SecureRandom.hex(16) }
  let(:share_link_encrypted_dek) { Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false) }
  let(:share_link_encrypted_master_key) { Base64.urlsafe_encode64(SecureRandom.hex(18), padding: false) }
  let(:share_link_params) do
    {
      share_link: {
        uuid: share_link_uuid,
        access_token: share_link_access_token,
        encrypted_dek: share_link_encrypted_dek,
        encrypted_master_key: share_link_encrypted_master_key
      }
    }
  end
  let(:share_link) { create(:share_link, access_token: SecureRandom.hex(16), item_id: item.id) }

  path "/api/v3/items/{item_id}/share_link" do
    post("Submit a new Share link for a Item") do
      tags "Item Share Link Endpoint"
      consumes "application/json"
      security [Bearer: []]

      parameter name: :item_id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "0a048468-fa74-4f20-a72f-7a097d55383",
        description:
          "The ID of the secret."
      parameter name: :share_link_params,
        in: :body,
        schema: {
          "$ref" => "#/components/schemas/new_share"
        }

      let(:item_id) { item.id }

      response "201", "Share link created" do
        run_test!
      end

      response "404", "Not found" do
        let(:item_id) { "empty" }

        run_test!
      end

      response "422", "Unprocessable Entity" do
        before { share_link }

        run_test! do |response|
          expect(json_response[:errors]).to include({base: ["Share link already exists"]})
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }
        run_test!
      end
    end

    delete("Remove/destroy the Share link for a Item") do
      tags "Item Share Link Endpoint"
      produces "application/json"
      security [Bearer: []]

      parameter name: :item_id,
        in: :path,
        required: true,
        schema: {
          type: :string,
          format: :uuid
        },
        example: "0a048468-fa74-4f20-a72f-7a097d55383",
        description:
          "The ID of the secret."

      let(:item_id) { share_link.item.id }

      response "200", "Share link disabled" do
        run_test! do |response|
          expect(Item.find(response.request.params["item_id"]).share_link).to be nil
        end
      end

      response "404", "Not found" do
        let(:item_id) { "empty" }

        run_test!
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }

        run_test! do |response|
          expect(Item.find(response.request.params["item_id"]).share_link).to_not be nil
        end
      end
    end
  end
end
