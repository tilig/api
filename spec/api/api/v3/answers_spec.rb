require "swagger_helper"

describe "api/v3/answers", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:access_token) { create(:identity).access_token }
  let(:authorization) { "Bearer #{access_token}" }

  let(:"x-tilig-platform") { "Test Client" }
  let(:"x-tilig-version") { "0.1" }
  let(:"x-browser-name") { "Safari" }
  let(:"x-browser-version") { "15.3" }
  let(:"x-os-name") { "Intel Mac OS X 10_15_7" }

  let(:default_answer_params) {
    {
      answer: {
        question_attributes: {
          content: "On what devices do you want to have your password and account information available?",
          survey_token: "e2120eb0-428b-471d-ba4a-9b109141acb6",
          token: "b291972e-caa6-4f8c-ad80-5ef91b5775b2",
          answer_options: ["Option A", "Option b", "Option C"]
        },
        chosen_options: ["Option A", "Option C"]
      }
    }
  }

  path "/api/v3/answers" do
    post "The Answers from the questions in the onboarding" do
      tags "Answers Endpoint"
      consumes "application/json"
      produces "application/json"
      security [Bearer: []]

      parameter name: "x-tilig-platform", in: :header, type: :string, example: "Test client"
      parameter name: "x-tilig-version", in: :header, type: :string, example: "3.1"
      parameter name: "x-browser-name", in: :header, type: :string, example: "Safari"
      parameter name: "x-browser-version", in: :header, type: :string, example: "15.3"
      parameter name: "x-os-name", in: :header, type: :string, example: "Intel Mac OS X 10_15_7"
      parameter name: :answer_params, in: :body, schema: {"$ref" => "#/components/schemas/new_answer"}

      response "201", "Create an answer" do
        let(:answer_params) { default_answer_params }
        run_test! do |response|
          expect(Answer).to exist
          expect(Question).to exist
        end
      end

      response "401", "Unauthenticated" do
        let(:authorization) { "Bearer " }
        let(:answer_params) { default_answer_params }

        run_test!
      end

      response "422", "Unprocessible entity" do
        let(:answer_params) {
          default_answer_params[:answer].merge(question_attributes: nil)
        }
        run_test!
      end
    end
  end
end
