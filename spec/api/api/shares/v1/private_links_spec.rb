require "swagger_helper"
require "fixtures/brandfetch/fixtures"

describe "api/shares/v1/private_links", type: :request, v3: true do # rubocop:disable RSpec/EmptyExampleGroup
  let(:secret) { create(:google_play_item, brand: create(:brand), user: create(:user)) }
  let(:access_token) { SecureRandom.hex(16) }
  let(:share_link) { create(:share_link, access_token: access_token, item_id: secret.id) }

  before { stub_brandfetch_google }

  path "/api/shares/v1/private_links/{uuid}" do
    get("Retreive a Share using a combination of uuid and Access-token via a private link") do
      tags "Public Share Link Endpoint"
      produces "application/json"

      parameter name: :uuid,
        in: :path,
        required: true,
        schema: {
          type: :string
        },
        example: "cfdf368e870e2e7f78c77925714d25a2",
        description:
          "The uuid submitted with the post request. This is not the ID of the object. The uuid is derived from the master key generated at the backend."
      parameter name: "Access-token",
        in: :header,
        required: true,
        schema: {
          type: :string
        },
        example: "4e6b3ea5017ffb27180c8b44c496fc55",
        description:
          "The Access-token submitted with the post request. The Access-token is derived from the master key generated at the backend."

      let("Access-token") { access_token }
      let(:uuid) { share_link.uuid }

      response "200", "Show secret" do
        run_test! do |response|
          expect(json_response[:uuid]).to match share_link.uuid
          expect(json_response[:encrypted_key]).to match share_link.encrypted_dek
          expect(json_response[:encrypted_dek]).to match share_link.encrypted_dek
          expect(json_response[:domain]).to match share_link.item.public_suffix_domain
        end
      end

      response "404", "Access-token invalid" do
        let("Access-token") { "" }

        run_test!
      end

      response "404", "uuid invalid/missing" do
        let(:uuid) { "share.uuid" }

        run_test!
      end
    end
  end

  def stub_brandfetch_google
    @brandfetch_stub = stub_request(:get, "https://api.brandfetch.io/v2/brands/play.google.com")
      .to_return(status: 200, body: BrandfetchFixtures::GOOGLE_PLAY, headers: {"Content-Type" => "application/json"})
  end
end
