require "rails_helper"
require "fixtures/brandfetch/fixtures"

describe Brandfetch::AttributesFetcher, type: :service do
  describe "fetch/1" do
    let(:domain) { "play.google.com" }
    let(:parsed_json) { JSON.parse(BrandfetchFixtures::GOOGLE_PLAY) }
    let(:brandfetch_result_json) { BrandfetchFixtures::GOOGLE_PLAY }

    subject { Brandfetch::AttributesFetcher.new(domain).fetch }

    before { stub_brandfetch }

    it "returns the brand" do
      expect(subject.except(:fetch_date)).to match Brandfetch::AttributesParser.new(parsed_json, domain).parse.except(:fetch_date)
    end

    it "returns an empty hash if the brandfetch response status is not 200" do
      stub_brandfetch_403
      expect(subject).to be {}
    end

    context "when the url is invalid" do
      let(:domain) { "this is not valid.com" }

      it "returns an empty hash" do
        expect(subject).to be {}
      end
    end

    context "when the given uri does not contain a tld" do
      let(:domain) { "bank" }

      it "returns an empty hash" do
        expect(subject).to be {}
      end
    end

    context "when a public suffix is private" do
      let(:domain) { "blogspot.com" }

      it "returns an empty hash" do
        stub_brandfetch_blogspot
        expect(subject).to include({domain: "blogspot.com", public_suffix_domain: "blogspot.com"})
      end
    end

    context "when the request raises a timeout" do
      it "returns an empty hash" do
        stub_brandfetch_timeout
        expect(subject).to include({brandfetch_json: {}, domain: "play.google.com"})
      end
    end

    context "when no name is found" do
      let(:domain) { "youtu.be" }
      # This response of youtu.be does not return a name but  has a description
      # and icons.
      let(:brandfetch_result_json) {
        JSON.parse(BrandfetchFixtures::YOUTU_BE).merge({"description" => "A random description without the company name"}).to_json
      }

      it "will create a brand, if there are other fields filled" \
         "name wil be the domain" do
        expect(subject).to include({name: "youtu.be"})
      end
    end
  end

  def stub_brandfetch_403
    remove_request_stub(@stub)
    stub_request(:get, "https://api.brandfetch.io/v2/brands/play.google.com")
      .to_return(status: 403, body: BrandfetchFixtures::ERROR_403, headers: {"Content-Type" => "application/json"})
  end

  def stub_brandfetch_timeout
    remove_request_stub(@stub)
    stub_request(:get, "https://api.brandfetch.io/v2/brands/#{domain}").to_timeout
  end

  def stub_brandfetch
    @stub = stub_request(:get, "https://api.brandfetch.io/v2/brands/#{domain}")
      .to_return(status: 200, body: brandfetch_result_json, headers: {"Content-Type" => "application/json"})
  end

  def stub_brandfetch_blogspot
    stub_request(:get, "https://api.brandfetch.io/v2/brands/blogspot.com")
      .to_return(status: 200, body: BrandfetchFixtures::BLOGSPOT, headers: {"Content-Type" => "application/json"})
  end
end
