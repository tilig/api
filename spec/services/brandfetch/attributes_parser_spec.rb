require "rails_helper"
require "fixtures/brandfetch/fixtures"

describe Brandfetch::AttributesParser, type: :service do
  context "#brandfetch_json_to_brand_hash/2" do
    let(:json) { JSON.parse(BrandfetchFixtures::GOOGLE_PLAY) }
    let(:brand) { build(:brand) }
    let(:url) { "https://play.google.com" }

    subject { Brandfetch::AttributesParser.new(json, url, brand).parse }

    it "parses brandfetch json to a brand hash" do
      expect {
        brand = Brand.create(subject)

        expect(brand.name).to eq "Google play"
        expect(brand.domain).to eq "play.google.com"
        expect(brand.public_suffix_domain).to eq "google.com"
        expect(brand.totp).to be true
        expect(brand.main_color_hex).to eq "#4285F4"
        expect(brand.main_color_brightness).to eq "127"
        expect(brand.images).to match({
          "icon" => {
            "png" => {
              "original" => Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
            },
            "svg" => {
              "original" => Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
            },
            "theme" => "light"
          }
        })
      }.to change(Brand, :count).by(1)
    end

    describe "when the received json has no content" do
      let(:json) { JSON.parse(BrandfetchFixtures::NOT_FOUND) }
      let(:url) { "does-not-exist.com" }

      describe "for empty brands" do
        let(:brand) { Brand.new }

        it "will save" do
          expect(brand.update(subject)).to eq(true)
        end
      end

      describe "for existing brands" do
        let(:json) { JSON.parse(BrandfetchFixtures::NOT_FOUND) }
        let(:brand) { create(:brand, domain: "does-not-exist.com") }

        it "will update existing brands" do
          brand.attributes = subject

          expect(brand.name).to eq "Google play"
          expect(brand.domain).to eq "does-not-exist.com"
          expect(brand.public_suffix_domain).to eq "does-not-exist.com"
          expect(brand.totp).to be true
          expect(brand.main_color_hex).to eq "#1a73e9"
          expect(brand.main_color_brightness).to eq "105"
          expect(brand.images).to eq({
            "icon" => {
              "png" => {
                "original" => nil
              },
              "svg" => {
                "original" => nil
              },
              "theme" => nil

            }
          })

          expect {
            expect(brand.save).to be true
          }.to_not change(Brand, :count)
        end
      end

      describe "without any logos or colors" do
        let(:brand) { Brand.new }
        let(:json) { JSON.parse(BrandfetchFixtures::TEST_COM) }

        it "will still work" do
          brand = Brand.new(subject)

          expect(brand.name).to eq "Test"
          expect(brand.main_color_hex).to be nil
          expect(brand.main_color_brightness).to be nil
        end
      end

      describe "when logos only contain an icon" do
        let(:brand) { Brand.new }
        let(:json) { JSON.parse(BrandfetchFixtures::TEST_COM) }

        it "returns valid attributes" do
          json["logos"] = empty_logo_hash
          brand = Brand.new(subject)

          expect(brand.name).to eq "Test"
          expect(brand.main_color_hex).to be nil
          expect(brand.main_color_brightness).to be nil
        end
      end
    end

    describe "for a Brandfetch where SVG and PNG is availible" do
      let(:brand) { Brand.new }
      let(:json) { JSON.parse(BrandfetchFixtures::GOOGLE_PLAY) }
      let(:url) { "play.google.com" }

      before { brand.attributes = subject }

      it "will use SVG for the SVG source and PNG for PNG" do
        expect(brand.images).to eq({
          "icon" => {
            "png" => {
              "original" => Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
            },
            "svg" => {
              "original" => Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
            },
            "theme" => "light"
          }
        })
      end
      it "will create a new brand" do
        expect {
          expect(brand.save).to be true
        }.to change(Brand, :count).by 1
      end
    end

    describe "for a Brandfetch where only SVG is availible" do
      let(:brand) { Brand.new }
      let(:json) { JSON.parse(BrandfetchFixtures::YAHOO) }
      let(:url) { "yahoo.com" }

      before { brand.attributes = subject }

      it "will use SVG for the SVG source and SVG for PNG" do
        expect(brand.images).to eq({
          "icon" => {
            "png" => {
              "original" => Imgproxy.url_for("https://asset.brandfetch.io/id6AVuB8Pi/idQ9By8eo8.svg", format: "png")
            },
            "svg" => {
              "original" => Imgproxy.url_for("https://asset.brandfetch.io/id6AVuB8Pi/idQ9By8eo8.svg", format: "svg")
            }, "theme" => nil
          }
        })
      end
      it "will create a new brand" do
        expect {
          expect(brand.save).to be true
        }.to change(Brand, :count).by 1
      end
    end

    describe "for a Brandfetch where only SVG is availible" do
      let(:brand) { Brand.new }
      let(:json) { JSON.parse(BrandfetchFixtures::BAIDU) }
      let(:url) { "baidu.com" }

      before { brand.attributes = subject }

      it "will use PNG for PNG logo_source and SVG is nil" do
        expect(brand.images).to eq({
          "icon" => {
            "png" => {
              "original" => Imgproxy.url_for("https://asset.brandfetch.io/idVfYwcuQz/id8hkaZwOR.png", format: "png")
            },
            "svg" => {
              "original" => nil
            },
            "theme" => nil
          }
        })
      end
      it "will create a new brand" do
        expect {
          expect(brand.save).to be true
        }.to change(Brand, :count).by 1
      end
    end
  end

  describe "#fallback_name" do
    let(:brandfetch_hash) { {"description" => "Enjoy the videos and music you love, upload original content, and share it all with friends, family, and the world on YouTube."} }
    let(:domain) { "youtu.be" }

    context "when the sld is in the description" do
      let(:brandfetch_hash) { {"description" => "PayPal is the safer, easier way to pay and get paid online."} }
      let(:domain) { "paypal.com" }

      it "falls back to the name of the description" do
        expect(Brandfetch::AttributesParser.new(brandfetch_hash, domain).parse[:name]).to eq "PayPal"
      end
    end

    context "when the full domain is in the description" do
      it "description" do
        expect(Brandfetch::AttributesParser.new(brandfetch_hash, domain).parse[:name]).to eq "YouTube"
      end
    end
  end

  def empty_logo_hash
    [{"type" => "icon",
      "theme" => nil,
      "formats" =>
    [{"src" => "https://asset.brandfetch.io/idJlutjtyN/id-ruYdPk-.png",
      "background" => nil,
      "format" => "png",
      "height" => 400,
      "width" => 400,
      "size" => 32896}]},
      {"type" => "logo", "theme" => nil, "formats" => []}]
  end
end
