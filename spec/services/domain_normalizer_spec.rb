require "rails_helper"

describe "DomainNormalizer" do
  describe "domain/1" do
    let(:url) { "http://microsoft.com" }

    subject { DomainNormalizer.new(url).domain }

    it "returns the host" do
      expect(subject).to eq("microsoft.com")
    end

    describe "when the url starts with www." do
      let(:url) { "http://www.microsoft.com" }

      it "it will be removed" do
        expect(subject).to eq("microsoft.com")
      end
    end

    describe "when the url has a subdomain before the www." do
      let(:url) { "http://office.www.microsoft.com" }

      it "it keeps the www." do
        expect(subject).to eq("office.www.microsoft.com")
      end
    end

    describe "when there are whitespaces around the url" do
      let(:url) { "   http://microsoft.com   " }

      it "strips the whitespaces" do
        expect(subject).to eq("microsoft.com")
      end
    end

    describe "when a subdomain is given" do
      let(:url) { "http://azure.microsoft.com" }

      it "keeps the subdomain" do
        expect(subject).to eq("azure.microsoft.com")
      end
    end

    describe "when the url is invalid" do
      let(:url) { "this is an invalid url .com" }

      it "returns nil" do
        expect(subject).to be nil
      end
    end

    describe "in case of a private domain" do
      let(:url) { "http://example.blogspot.com" }

      it "includes the private part in the domain" do
        expect(subject).to eq("example.blogspot.com")
      end
    end

    describe "when a given url is an ip_address" do
      let(:url) { "10.0.0.20" }

      it "returns nil" do
        expect(subject).to be nil
      end
    end
  end

  describe "public_suffix_domain/1" do
    let(:url) { "http://microsoft.com" }

    subject { DomainNormalizer.new(url).public_suffix_domain }

    it "returns the host" do
      expect(subject).to eq("microsoft.com")
    end

    describe "when the url starts with www." do
      let(:url) { "http://www.microsoft.com" }

      it "it will be removed" do
        expect(subject).to eq("microsoft.com")
      end
    end

    describe "when there are whitespaces around the url" do
      let(:url) { "   http://microsoft.com   " }

      it "strips the whitespaces" do
        expect(subject).to eq("microsoft.com")
      end
    end

    describe "when the url is invalid" do
      let(:url) { "this is an invalid url .com" }

      it "returns nil" do
        expect(subject).to eq(nil)
      end
    end

    describe "when a subdomain is given" do
      let(:url) { "http://azure.microsoft.com" }

      it "removes the subdomain" do
        expect(subject).to eq("microsoft.com")
      end
    end

    describe "in case of a private domain" do
      let(:url) { "http://example.blogspot.com" }

      it "excludes the private part in the domain" do
        expect(subject).to eq("blogspot.com")
      end
    end

    describe "for sites that don't have a public suffix" do
      let(:url) { "http://test.ing.nl" }

      it "it returns the root domain" do
        expect(subject).to eq("ing.nl")
      end
    end

    describe "when a given url is an ip_address" do
      let(:url) { "10.0.0.20" }

      it "returns nil" do
        expect(subject).to be(nil)
      end
    end
  end

  describe "public_suffix_domain/1" do
    let(:url) { "http://microsoft.com" }

    subject { DomainNormalizer.new(url).public_suffix_domain }

    describe "when a subdomain is given" do
      let(:url) { "http://azure.microsoft.com" }

      it "removes the subdomain" do
        expect(subject).to eq("microsoft.com")
      end
    end

    describe "in case of a private domain" do
      let(:url) { "http://example.blogspot.com" }

      it "excludes the private part by default" do
        expect(subject).to eq("blogspot.com")
      end

      context "when private is explicitly NOT ignored" do
        subject { DomainNormalizer.new(url).public_suffix_domain(ignore_private: false) }

        it "includes the private part of the domain" do
          expect(subject).to eq("example.blogspot.com")
        end
      end

      context "when private IS ignored" do
        subject { DomainNormalizer.new(url).public_suffix_domain }

        it "excludes the private part of the domain and only returns the main domain" do
          expect(subject).to eq("blogspot.com")
        end
      end
    end

    describe "for sites that don't have a public suffix" do
      let(:url) { "http://test.ing.nl" }

      it "it returns the root domain" do
        expect(subject).to eq("ing.nl")
      end
    end
  end
end
