require "rails_helper"

RSpec.describe Auth::AccessTokenGenerator, type: :service do
  let(:user) { create(:user) }
  let(:generated_token) { Auth::AccessTokenGenerator.generate(user) }
  let(:decoded_payload) { decode_payload(generated_token) }

  it "generates an access token for the user" do
    expect(generated_token).to be_present
  end

  it "has the correct issuer" do
    expect(decoded_payload["iss"]).to eq("https://app.tilig.com")
  end

  it "has the correct user id" do
    expect(decoded_payload["uid"]).to eq(user.id)
  end

  it "uses the correct hmac_secret" do
    expect(Auth::AccessTokenGenerator.send(:hmac_secret)).to eq(
      Rails.application.config.access_token_secret
    )
  end

  describe "when generating multiple tokens" do
    let(:first_token) { Auth::AccessTokenGenerator.generate(user) }
    let(:first_decoded_payload) { decode_payload(first_token) }
    let(:second_token) { Auth::AccessTokenGenerator.generate(user) }
    let(:second_decoded_payload) { decode_payload(second_token) }

    it "generates a new jti on each call" do
      expect(first_decoded_payload["jti"]).to_not eq(second_decoded_payload["jti"])
    end
  end

  def decode_payload(token)
    JWT.decode(
      token,
      Rails.application.config.access_token_secret,
      true,
      {algorithm: "HS256"}
    )[0]
  end
end
