require "rails_helper"
require "fixtures/mailchimp/fixtures"

describe "MailchimpService" do
  describe "send_event" do
    let(:event) { "Sign Up" }
    let(:user) { create :user }
    let(:mailchimp_stub) { stub_mailchimp }

    subject { MailchimpService.new(event, user).send_event }

    before do
      mailchimp_stub
    end

    it "returns an active job" do
      ActiveJob::Base.queue_adapter = :test
      expect { subject }.to have_enqueued_job
    end
    it "sends a message to mailchimp" do
      expect(subject.perform_now).to match JSON.parse(Mailchimp::Fixtures::CREATE_MEMBER_RESPONSE)
    end

    describe "when the user name is empty" do
      let(:user) { create :user, given_name: nil, family_name: nil }
      let(:mailchimp_stub) { stub_mailchimp_without_name }

      it "will be omitted in the request" do
        expect(subject.perform_now).to match JSON.parse(Mailchimp::Fixtures::MEMBER_WITHOUT_MERGE_FIELDS)
      end
    end

    context "for the user deleted event" do
      let(:event) { "User Deleted" }
      let(:mailchimp_stub) { stub_mailchimp_user_delete }

      it "sends a message to mailchimp" do
        expect(subject.perform_now).to match JSON.parse({})
      end
    end
  end

  def stub_mailchimp
    stub_request(:post, "https://random-server.api.mailchimp.com/3.0/lists/random-list-id/members")
      .with(body: {"email_address" => user.email,
                   "status" => "subscribed",
                   "language" => user.locale,
                   :merge_fields => {FNAME: user.given_name, LNAME: user.family_name}})
      .to_return(status: 200, body: Mailchimp::Fixtures::CREATE_MEMBER_RESPONSE, headers: {"Content-Type" => "application/json"})
  end

  def stub_mailchimp_without_name
    stub_request(:post, "https://random-server.api.mailchimp.com/3.0/lists/random-list-id/members")
      .with(body: {"email_address" => user.email, "status" => "subscribed", "language" => user.locale})
      .to_return(status: 200, body: Mailchimp::Fixtures::MEMBER_WITHOUT_MERGE_FIELDS, headers: {"Content-Type" => "application/json"})
  end

  def stub_mailchimp_user_delete
    stub_request(:post, "https://random-server.api.mailchimp.com/3.0/lists/random-list-id/members/#{Digest::MD5.hexdigest(user.email)}/actions/delete-permanent")
      .to_return(status: 200, body: {}.to_json, headers: {"Content-Type" => "application/json"})
  end

  def stub_mailchimp_400
    stub_request(:post, "https://random-server.api.mailchimp.com/3.0/lists/random-list-id/members")
      .to_return(status: 404, body: Mailchimp::Fixtures::MEMBER_EXISTS_RESPONSE, headers: {"Content-Type" => "application/json"})
  end
end
