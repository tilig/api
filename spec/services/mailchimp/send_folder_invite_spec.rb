require "rails_helper"

describe "Mailchimp::SendFolderInvite" do
  let(:membership) { build(:folder_membership) }
  let(:inviter) { build(:user) }

  subject { Mailchimp::SendFolderInvite.new(membership: membership, inviter: inviter).call }

  describe "#call" do
    before { stub_request(:post, "https://mandrillapp.com/api/1.0/messages/send-template.json").to_return(status: 200, body: {}.to_json, headers: {}) }

    context "when membership has user" do
      it "calls Mailchimp::SendFolderInvite" do
        expect(Mailchimp::ItemShareInviteTemplate).to receive(:new).with(membership: membership, inviter: inviter)

        subject
      end
    end

    context "when membership has email" do
      let(:membership) { build(:folder_membership, user: nil, email: "john@example.com") }

      it "calls Mailchimp::SendFolderInvite" do
        expect(Mailchimp::ItemShareInviteExternalTemplate).to receive(:new).with(membership: membership, inviter: inviter)
        subject
      end
    end
  end
end
