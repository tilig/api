require "rails_helper"

describe Mailchimp::SendInvitedMemberSignedUpNotice do
  let(:user) { create(:user) }
  let(:folder_memberships) { create_list(:folder_membership, 2, creator: user, creator_id: user.id) }
  let(:template) { Mailchimp::SignUpNoticeForFolderMembershipCreatorTemplate.new(folder_memberships: folder_memberships).build_template }

  subject { Mailchimp::SendInvitedMemberSignedUpNotice.new(folder_memberships: folder_memberships).call }

  describe "#call" do
    before { stub_request(:post, "https://mandrillapp.com/api/1.0/messages/send-template.json").to_return(status: 200, body: {}.to_json, headers: {}) }

    context "a creator only gets one call" do
      it "sends one email " do
        expect(Mailchimp::TransactionalEmailJob).to receive(:perform_later).with(template).once

        subject
      end
    end

    context "without a creator on the membership" do
      let(:folder_memberships) { create_list(:folder_membership, 2) }

      before { folder_memberships.each { |fm| fm.update_column(:creator_id, nil) } }

      it "does  not send the email" do
        expect(Mailchimp::TransactionalEmailJob).to_not receive(:perform_later)

        subject
      end
    end
  end
end
