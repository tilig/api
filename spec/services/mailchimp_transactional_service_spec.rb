require "rails_helper"

describe "Mailchimp::SendFolderInvite" do
  describe "call" do
    let(:membership) { create(:folder_membership) }
    let(:user) { create(:user) }

    it "sends an API request with template to mailchimp/mandrill" do
      template = Mailchimp::ItemShareInviteTemplate.new(membership: membership, inviter: user).build_template
      expect(Mailchimp::TransactionalEmailJob).to receive(:perform_later).with(template)
      Mailchimp::SendFolderInvite.new(membership: membership, inviter: user).call
    end
  end
end
