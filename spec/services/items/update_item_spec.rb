require "rails_helper"

describe "Items::UpdateItem" do
  let(:user) { create(:user) }

  subject { Items::UpdateItem.new(user:, item:, item_params:).call }

  context "creating a user item" do
    let(:item) { user.items.build }

    context "with invalid params" do
      let(:item_params) { {} }

      it { is_expected.to be false }
    end

    context "with valid params" do
      let(:item_params) {
        {
          encrypted_folder_dek: "blablabla",
          encryption_version: 2,
          encrypted_overview: "blablabla",
          encrypted_details: "blablabla",
          template: "login/1"
        }
      }

      it { is_expected.to be true }
      it { expect { subject }.to change { Item.count }.by(1) }
    end
  end

  context "updating a user item" do
    let(:item) { create(:item, user:) }

    context "with invalid params" do
      let(:item_params) { {template: nil} }

      it { is_expected.to be false }
    end

    context "with valid params" do
      let(:item_params) {
        {
          encrypted_folder_dek: "blablabla",
          encryption_version: 2,
          encrypted_overview: "blablabla",
          encrypted_details: "blablabla",
          template: "login/1"
        }
      }

      it { is_expected.to be true }
      it { expect { subject }.to change { Item.count }.by(1) }
    end
  end

  context "updating a folder item" do
    context "when updating a user item" do
      let(:item) { create(:item, user:) }
      let(:item_params) {
        {
          encrypted_folder_dek: "new",
          folder_id: folder_id
        }
      }

      context "moving to a user's folder" do
        let(:folder_membership) { create(:folder_membership, :accepted, user:) }
        let(:folder_id) { folder_membership.folder_id }

        it { is_expected.to be true }
      end

      context "moving to a different folder" do
        let(:folder_id) { create(:folder).id }

        it { is_expected.to be false }
      end

      context "moving to a nonexisting folder" do
        let(:folder_id) { "00000000-00000000-00000000-00000000" }

        it { is_expected.to be false }
      end
    end

    context "when updating a folder item" do
      let(:folder) { create(:folder_membership, :accepted, user:).folder }
      let(:item) { create(:item, :folder_item, folder:) }
      let(:item_params) {
        {
          encrypted_folder_dek: "new",
          folder_id: folder_id
        }
      }

      context "moving to a user's folder" do
        let(:folder_membership) { create(:folder_membership, :accepted, user:) }
        let(:folder_id) { folder_membership.folder_id }

        it { is_expected.to be true }
      end

      context "moving to a different folder" do
        let(:folder_id) { create(:folder).id }

        it { is_expected.to be false }
      end

      context "moving to a nonexisting folder" do
        let(:folder_id) { "00000000-00000000-00000000-00000000" }

        it { is_expected.to be false }
      end

      context "when removing the folder" do
        let(:item_params) { {folder_id: nil, encrypted_dek: "something"} }

        it { is_expected.to be true }
        it { expect { subject }.to change(item, :folder).to(nil) }
      end
    end
  end
end
