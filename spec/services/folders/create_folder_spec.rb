require "rails_helper"

describe "Folders::CreateFolder" do
  let(:creator) { create(:user) }
  let(:item) { create(:item, name: "item-name", user: creator) }

  let(:folder_params) {
    {
      name: item.name,
      public_key: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false),
      folder_memberships: [
        {
          user_id: creator.id,
          encrypted_private_key: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false)
        }, folder_memberships_param
      ],
      items: [
        {
          id: item.id,
          encrypted_folder_dek: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false)
        }
      ]
    }
  }

  subject { Folders::CreateFolder.new(creator: creator, folder_params: folder_params).create }

  describe "#create" do
    context "For a non Tilig user" do
      let(:folder_memberships_param) { {email: "john.doe1@tilig.com"} }

      it "receive an 'ItemShareInviteTemplate' mail" do
        expect(Mailchimp::ItemShareInviteExternalTemplate).to receive(:new)
        subject
      end
    end

    context "For a Tilig user (but not a contact)" do
      let(:folder_memberships_param) {
        {
          user_id: create(:user).id, encrypted_private_key: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false)
        }
      }

      it "receive an 'ItemShareInviteTemplate' mail" do
        expect(Mailchimp::ItemShareInviteTemplate).to receive(:new)
        subject
      end
    end

    context "For a Tilig user which is a contact" do
      before { create(:contact, user: creator, contact: contact) }

      let(:contact) { create(:user) }
      let(:folder_memberships_param) {
        {
          user_id: contact.id, encrypted_private_key: Base64.urlsafe_encode64(SecureRandom.bytes(40), padding: false)
        }
      }

      it "receive an 'ItemShareNotificationTemplate' mail" do
        expect(Mailchimp::ItemShareNotificationTemplate).to receive(:new).once.and_call_original
        subject
      end
    end
  end
end
