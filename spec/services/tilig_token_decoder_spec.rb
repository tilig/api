require "rails_helper"

RSpec.describe Auth::TiligTokenDecoder, type: :service do
  let(:private_key) do
    '-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIKZ8vLJtU5EzqbMfhRJE1bPhdbIPYC7OBlSOoIjJquS7oAoGCCqGSM49
AwEHoUQDQgAEj3qy5L8FlEMjj0PIvVLbOTSCKR+rizHzVqMsvRKA/4O63XMEEwGd
uv5bl+/B1f1sTI4UyymYZ+jTpnrVgjs+qQ==
-----END EC PRIVATE KEY-----'
  end

  let(:token) do
    JWT.encode(payload, OpenSSL::PKey::EC.new(private_key), "ES256")
  end
  let(:payload) { {email: email, exp: exp, iss: iss, iat: iat} }
  let(:email) { Faker::Internet.email }
  let(:iat) { Time.current.to_i }
  let(:exp) { 30.seconds.from_now.to_i }
  let(:iss) { "Tilig Key Service" } # TODO: issuer validation checks!

  describe "#self.decode_email" do
    subject { Auth::TiligTokenDecoder.new.decode_email(token) }

    describe "when the token is valid" do
      it "returns the email address" do
        expect(subject).to eq(email)
      end
    end

    context "invalid tokens" do
      describe "when the token is nil" do
        let(:token) { nil }

        it "should raise a JwtDecodeError" do
          expect { subject }.to raise_error(
            Auth::TiligTokenDecoder::JwtDecodeError
          )
        end
      end

      describe "when the token is an empty string" do
        let(:token) { "" }

        it "should raise a JwtDecodeError" do
          expect { subject }.to raise_error(
            Auth::TiligTokenDecoder::JwtDecodeError
          )
        end
      end

      describe "when the token is a formatted as a non-JWT string" do
        let(:token) { "this_is_not_a_jwt" }

        it "should raise a JwtDecodeError" do
          expect { subject }.to raise_error(
            Auth::TiligTokenDecoder::JwtDecodeError
          )
        end
      end

      describe "when the token is expired" do
        let(:exp) { 1.second.ago.to_i }

        it "should raise a JwtDecodeError error" do
          expect { subject }.to raise_error(
            Auth::TiligTokenDecoder::JwtDecodeError
          )
        end
      end

      describe "when the token is not signed" do
        let(:token) { JWT.encode(payload, nil, "none") }

        it "should raise a JwtDecodeError" do
          expect { subject }.to raise_error(
            Auth::TiligTokenDecoder::JwtDecodeError
          )
        end
      end

      describe "when the token contains a not before claim" do
        let(:payload) do
          {
            email: email,
            exp: exp,
            iss: iss,
            iat: iat,
            nbf: 1.second.from_now.to_i
          }
        end

        it "should raise a JwtDecodeError" do
          expect { subject }.to raise_error(
            Auth::TiligTokenDecoder::JwtDecodeError
          )
        end
      end

      describe "when the token is signed with the wrong algorithm" do
        let(:token) { JWT.encode(payload, "not_so_secret_secret", "HS256") }

        it "should raise a JwtDecodeError" do
          expect { subject }.to raise_error(
            Auth::TiligTokenDecoder::JwtDecodeError
          )
        end
      end

      describe "when the token is signed with the wrong key" do
        let(:private_key) do
          '-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIOfNxEk3dd/Mikv+rtmgs8W1CqkK+dNzUOfqKnp6blPMoAoGCCqGSM49
AwEHoUQDQgAEgHeK68lQEPuJh9ZRfv50Pk9Q3Q0GfYY2B2dkesDfJuGZhLw4JS3E
GOQy1Z+omFomZJN9L4y7JUtGCDAHIAgCWQ==
-----END EC PRIVATE KEY-----'
        end

        it "should raise a JwtDecodeError" do
          expect { subject }.to raise_error(
            Auth::TiligTokenDecoder::JwtDecodeError
          )
        end
      end

      context "when a token is being used in a replay attack" do
        it "raises a JtiExistsError error" do
          expect do
            2.times { Auth::TiligTokenDecoder.new.decode_email(token) }
          end.to raise_error(Auth::TiligTokenDecoder::JtiExistsError)
        end
      end

      context "when the token payload does not contain an email address" do
        let(:payload) { {exp: exp, iss: iss, iat: iat} }

        it "raises an EmailMissingError" do
          expect { subject }.to raise_error(
            Auth::TiligTokenDecoder::EmailMissingError
          )
        end

        it "raises an EmailMissingError that inherits from Auth::KeyServiceTokenDecoder::KeyServiceDecodeError" do
          expect { subject }.to raise_error(
            Auth::TiligTokenDecoder::JwtDecodeError
          )
        end
      end
    end
  end
end
