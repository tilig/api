require "rails_helper"

RSpec.describe Auth::AccessTokenDecoder, type: :service do
  describe "#decode!" do
    let(:user) { create(:user) }
    let(:token) { Auth::AccessTokenGenerator.generate(user) }
    let(:payload) { {"iss" => "Some Evil Government Agency"} }

    # Mock the access token's payload
    before do
      allow(Auth::AccessTokenGenerator).to receive(:payload)
        .and_return(payload)
    end

    subject { Auth::AccessTokenDecoder.new.decode(token) }

    describe "when the token is valid" do
      let(:payload) { {"email" => "henk@test.com", "iss" => "https://app.tilig.com"} }

      it "returns the payload" do
        expect(subject).to eq(payload)
      end
    end

    describe "when the token is invalid" do
      describe "when the issuer does not match" do
        it "raises an JwtDecodeError" do
          expect { subject }.to raise_error(Auth::AccessTokenDecoder::JwtDecodeError)
        end
      end

      describe "when the token is expired" do
        let(:payload) { {"exp" => 1.minute.ago.to_i, "email" => "henk@test.com", "iss" => "https://app.tilig.com"} }

        it "raises a JwtDecodeError" do
          expect { subject }.to raise_error(Auth::AccessTokenDecoder::JwtDecodeError)
        end
      end
    end
  end

  describe "#issuer" do
    it "is the same as the generator's issuer" do
      expect(Auth::AccessTokenDecoder.new.send(:issuer)).to eq(
        Auth::AccessTokenGenerator.send(:issuer)
      )
    end
  end

  describe "#hmac_secret" do
    it "is the same as the generator's hmac_secret" do
      expect(Auth::AccessTokenDecoder.new.send(:hmac_secret)).to eq(
        Auth::AccessTokenGenerator.send(:hmac_secret)
      )
    end
  end
end
