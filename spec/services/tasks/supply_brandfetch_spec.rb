require "rails_helper"
require "fixtures/brandfetch/fixtures"

describe "Tasks::SupplyBrandfetch" do
  let(:subject) { Tasks::SupplyBrandfetch.new.execute }
  let(:stub_google) { google_stub }
  let(:stub_amazon) { amazon_stub }
  let(:stub_facebook) { facebook_stub }
  let(:stub_brands_limit) { stub_const "Tasks::SupplyBrandfetch::BRANDS_INSERT_LIMIT", 2 }
  let(:stub_steps) { stub_const "Tasks::SupplyBrandfetch::STEPS", 1 }

  before { stub_brands_limit }
  before { stub_steps }
  before { stub_google }
  before { stub_amazon }
  before { stub_facebook }

  it "updates all brands without brandfetch_json" do
    create(:brand, rank: 1, brandfetch_json: nil, domain: "play.google.com")
    create(:brand, rank: 2, brandfetch_json: nil, domain: "facebook.com")
    create(:brand, rank: 3, brandfetch_json: nil, domain: "amazon.com")

    expect(subject).to be 1
  end

  it "does not update a brand that already has brandfetch_json set" do
    create(:brand, rank: 1, brandfetch_json: {}, domain: "play.google.com", logo_icon_source: "")

    subject

    expect(Brand).to exist domain: "play.google.com", logo_icon_source: ""
  end

  it "updates lowest rank first" do
    google = create(:brand, rank: 1, brandfetch_json: nil, domain: "play.google.com", logo_icon_source: "")
    facebook = create(:brand, rank: 2, brandfetch_json: nil, domain: "facebook.com")
    amazon = create(:brand, rank: 3, brandfetch_json: nil, domain: "amazon.com")

    subject

    expect(google.reload.brandfetch_json).to_not be_nil
    expect(facebook.reload.brandfetch_json).to_not be_nil

    # Amazon is not within the stubbed brands limit and not updated
    expect(amazon.reload.brandfetch_json).to be_nil
  end

  def google_stub
    stub_request(:get, "https://api.brandfetch.io/v2/brands/play.google.com")
      .to_return(status: 200, body: BrandfetchFixtures::GOOGLE_PLAY, headers: {"Content-Type": "application/json"})
  end

  def amazon_stub
    stub_request(:get, "https://api.brandfetch.io/v2/brands/amazon.com")
      .to_return(status: 200, body: BrandfetchFixtures::AMAZON, headers: {"Content-Type": "application/json"})
  end

  def facebook_stub
    stub_request(:get, "https://api.brandfetch.io/v2/brands/facebook.com")
      .to_return(status: 200, body: BrandfetchFixtures::FACEBOOK, headers: {"Content-Type": "application/json"})
  end
end
