require "rails_helper"
require "fixtures/brandfetch/fixtures"

describe "Tasks::ReparseBrandsBrandfetchjson" do
  let(:subject) { Tasks::ReparseBrandsBrandfetchjson.new.execute }

  it "adds the Imgproxy url to images" do
    google = create(:brand, rank: 1, brandfetch_json: JSON.parse(BrandfetchFixtures::GOOGLE_PLAY), domain: "play.google.com", fetch_date: Time.current, logo_source: nil, logo_icon_source: nil)
    unknown_brand = create(:brand, domain: "does-not-exist.com", name: "does-not-exist", logo_source: nil, logo_icon_source: nil, fetch_date: nil, images: {})

    google.images = {}
    google.save! validate: false

    expect { subject }.to change { {images: google.reload.images, logo_source: google.reload.logo_source, logo_icon_source: google.reload.logo_icon_source} }
      .from({images: {}, logo_source: nil, logo_icon_source: nil})
      .to(
        {
          logo_source: nil,
          logo_icon_source: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png"),
          images: {
            "icon" => {
              "svg" => {
                "original" => Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
              },
              "png" => {
                "original" => Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
              },
              "theme" => "light"
            }
          }
        }
      )
    expect(unknown_brand.reload.logo_source).to eq(nil)
    expect(unknown_brand.reload.logo_icon_source).to eq(nil)
    expect(unknown_brand.reload.images).to eq({})
  end
end
