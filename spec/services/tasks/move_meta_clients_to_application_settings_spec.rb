require "rails_helper"

describe "Tasks::MoveMetaClientsToApplicationSettings" do
  let(:subject) { Tasks::MoveMetaClientsToApplicationSettings.new.execute }
  let(:profile_1) { create(:user).profile }
  let(:profile_2) { create(:user).profile }

  it "moves the values from meta to application_settings" do
    profile_1.update_columns(meta: {client_signed_up: "Web", clients_signed_in: ["Web"]}, application_settings: {legacy_encryption_disabled: true})
    profile_2.update_columns(meta: {client_signed_up: "Android", clients_signed_in: ["Android", "Web", "WebMobile"]}, application_settings: {legacy_encryption_disabled: true})

    subject

    profile_1.reload
    profile_2.reload

    expect(profile_1.application_settings).to eq({"client_signed_up" => "Web", "clients_signed_in" => ["Web"], "legacy_encryption_disabled" => true})
    expect(profile_1.meta).to eq({})
    expect(profile_2.application_settings).to eq({"client_signed_up" => "Android", "clients_signed_in" => ["Android", "Web", "WebMobile"], "legacy_encryption_disabled" => true})
    expect(profile_2.meta).to eq({})
  end

  context "when meta is empty" do
    it "handles empty meta values" do
      profile_1.update_columns(meta: {}, application_settings: {legacy_encryption_disabled: true})

      expect { subject }.to_not change(profile_1, :application_settings)
    end
  end

  context "when application_settings is already set" do
    it "does not change application_settings" do
      profile_1.update_columns(meta: {client_signed_up: "Web", clients_signed_in: ["Web"]}, application_settings: {client_signed_up: "Android", clients_signed_in: ["Android", "WebMobile"], legacy_encryption_disabled: true})

      subject

      profile_1.reload

      expect(profile_1.application_settings).to eq({"client_signed_up" => "Android", "clients_signed_in" => ["Android", "WebMobile"], "legacy_encryption_disabled" => true})
      expect(profile_1.meta).to eq({})
    end
  end
end
