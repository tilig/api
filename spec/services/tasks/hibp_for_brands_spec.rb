require "rails_helper"
require "fixtures/hibp/fixtures"

describe "Tasks::HibpForBrands" do
  let(:unbreached_brands) { Brand.where.not(breached_at: nil) }
  let(:brand) { create(:brand, domain: "zap-hosting.com", breached_at: "2018-10-05", breach_published_at: "2018-03-19T23:48:45Z", hibp_json: "{}") }

  subject { Tasks::HibpForBrands.new.import_breaches }

  before { stub_hibp_request }

  it "updates all breaches for the brands" do
    expect { subject }.to change(unbreached_brands, :count).from(0).to(10)
  end

  it "does not save unverified breaches" do
    expect { subject }.to_not change(Brand.where(domain: "zhenai.com").where.not(breached_at: nil), :exists?)
  end

  it "saves the last breach if a brands is found multiple times" do
    subject

    expect(Brand.find_by(domain: "zoosk.com").breached_at).to eq "2020-01-12 00:00:00.000000000 +0000"
  end

  it "does not save breaches without a domain" do
    # This releates to 'YouveBeenScraped' that does not have a domain name and
    # thus is not saved
    subject

    expect(Brand.where(breached_at: "2018-10-05T00:00:00Z")).to_not exist
  end

  it "touches all related item of brands that have been updated" do
    item = create(:item, website: "zap-hosting.com")

    expect {
      subject
      item.reload
    }.to change(item, :updated_at)
  end

  describe "with an existing breached_at" do
    let(:from_date) { "2018-10-05".to_datetime }
    let(:to_date) { "2021-11-22 00:00:00Z".to_datetime }

    it "updates brands with newer breaches" do
      brand
      expect { subject }.to(
        change {
          Brand.find_by(domain: "zap-hosting.com")
            .reload
            .breached_at
        }.from(from_date).to(to_date)
      )
    end
  end

  def stub_hibp_request
    stub_request(:get, "https://haveibeenpwned.com/api/v3/breaches")
      .to_return(status: 200, body: HibpFixtures::DEFAULT_FIXTURES, headers: {"Content-Type" => "application/json"})
  end
end
