require "rails_helper"
require "fixtures/brandfetch/fixtures"

describe Tasks::AlexaTopDomains, type: :service do
  let(:stub_csv) { stub_const "Tasks::AlexaTopDomains::ALEXA_CSV", "./spec/fixtures/alexa/1_million.csv" }
  let(:stub_total_domains) { stub_const "Tasks::AlexaTopDomains::TOTAL_DOMAINS", 10 }
  let(:stub_domains_per_insert_all) { stub_const "Tasks::AlexaTopDomains::DOMAINS_PER_INSERT_ALL", 2 }

  subject { Tasks::AlexaTopDomains.new.import_brands }

  before { stub_csv }
  before { stub_total_domains }
  before { stub_domains_per_insert_all }

  it "imports all domains" do
    expect { subject }.to change(Brand, :count).from(0).to(10)
    expect(Brand).to exist(domain: "wikipedia.org", rank: "9")
  end

  it "leaves the brandfetch_json empty" do
    expect { subject }.to change(Brand.where(brandfetch_json: nil), :count).from(0).to(10)
  end

  it "updates existing brands with only the rank" do
    brand = create(:brand, name: "Google.com", domain: "google.com", logo_icon_source: "logo-icon-source", logo_source: "logo_source", totp: true)

    subject

    brand.reload
    expect(brand.name).to eq("Google.com")
    expect(brand.totp).to be true
    expect(brand.domain).to eq("google.com")
    expect(brand.logo_icon_source).to eq "logo-icon-source"
    expect(brand.logo_source).to eq("logo_source")
  end

  context "with domains entered twice" do
    let(:stub_csv) { stub_const "Tasks::AlexaTopDomains::ALEXA_CSV", "./spec/fixtures/alexa/double-domains.csv" }

    it "only upserts one" do
      subject
      expect(Brand.where(domain: "wikipedia.org").count).to be 1
    end
  end

  context "with domains that do not have a public_suffix_domain" do
    let(:stub_csv) { stub_const "Tasks::AlexaTopDomains::ALEXA_CSV", "./spec/fixtures/alexa/no-public-suffix-domains.csv" }

    it "only upserts one" do
      expect { subject }.to change(Brand, :count).from(0).to(7)
      expect(Brand.where(domain: "cherkassy.ua")).to_not exist
    end
  end
end
