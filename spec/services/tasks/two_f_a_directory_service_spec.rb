require "rails_helper"
require "fixtures/two_f_a_directory/stubs"

describe Tasks::TwoFADirectoryService, type: :service do
  describe "import_brands" do
    before do
      @stub = stub_request(:get, "https://2fa.directory/api/v3/all.json")
        .to_return(status: 200, body: TwoFADirectory::TWO_F_A_DIRECTORY_RESPONSE, headers: {"Content-Type" => "application/json"})
    end

    subject { Tasks::TwoFADirectoryService.new.import_brands }

    describe "inssert all 2fa.directory services" do
      it "returns a ActiveRecord::Result" do
        expect(subject).to be_kind_of ActiveRecord::Result
      end

      it "has inserted twenty rows" do
        expect(subject.rows.count).to be 110
      end

      it "inserted all services" do
        expect(subject.rows.count).to be Brand.count
      end

      it "inserted all services" do
        subject
        expect(Brand.find_by(domain: "101domain.com").totp).to be true
      end

      it "can find domains by public_suffix_domain" do
        subject
        expect(Brand).to exist public_suffix_domain: "google.com"
      end

      it "can find domains by subdomain" do
        subject
        expect(Brand).to exist domain: "chat.google.com"
      end

      it "allows a private public_suffix_domain to be inserted " do
        subject
        expect(Brand).to exist(domain: "pythonanywhere.com", public_suffix_domain: "pythonanywhere.com")
      end

      it "adds additional domains as standalone brands" do
        subject

        expect(Brand).to exist domain: "amazon.nl"
        expect(Brand).to exist domain: "amazon.com"
        expect(Brand).to exist domain: "amazon.co.uk"
      end
    end

    describe "run 2nd time with updated stubs" do
      it "updates the brands entry (101domain)" do
        subject

        update_101domain_stub

        subject2 = Tasks::TwoFADirectoryService.new.import_brands

        expect(subject2.rows.count).to be 1
      end

      it "changes the totp to false when its missing on update (101domain)" do
        subject

        update_101domain_stub

        Tasks::TwoFADirectoryService.new.import_brands

        expect(Brand.find_by(domain: "101domain.com").totp).to be false
      end
    end
  end

  # This removes the existing stub and adds a stub with 101domain that has an updated
  # tfa
  def update_101domain_stub
    remove_request_stub(@stub)

    stub_request(:get, "https://2fa.directory/api/v3/all.json")
      .to_return(status: 200, body: TwoFADirectory::ONE_O_ONE_DOMAIN_UPDATED, headers: {"Content-Type" => "application/json"})
  end
end
