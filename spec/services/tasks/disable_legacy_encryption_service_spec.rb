require "rails_helper"

describe "Tasks::DisableLegacyEncryption" do
  let(:subject) { Tasks::DisableLegacyEncryption.new.update_users }

  it "updates the legacy_encryption_disabled for the user that has all secrets to v2" do
    user_complete = create(:user, display_name: :complete_migration)
    create(:item, user: user_complete, encryption_version: 2)
    create(:item, user: user_complete, encryption_version: 2)

    subject
    profile = user_complete.profile.reload

    expect(profile.application_settings["legacy_encryption_disabled"]).to be true
  end

  it "does not update the user that has a secret version still on v1" do
    user_incomplete = create(:user, display_name: :partial_migration)
    create(:item, user: user_incomplete, encryption_version: 2)
    create(:item, user: user_incomplete, encryption_version: 1, name: "dup")

    subject
    profile = user_incomplete.profile.reload

    expect(profile.application_settings["legacy_encryption_disabled"]).to be nil
  end

  it "does not change the legacy_encryption_disabled settings for users that have no encryption_version v2 set on secrets" do
    user_not_existing = create(:user, display_name: :no_v2_secrets)
    create(:item, user: user_not_existing, encryption_version: 1, name: "dup")
    create(:item, user: user_not_existing, encryption_version: 1, name: "dup")

    profile = user_not_existing.profile.reload

    expect(profile.application_settings["legacy_encryption_disabled"]).to be nil
  end

  it "it does not update the user that has the setting already set" do
    profile = build(:profile, application_settings: {legacy_encryption_disabled: true})
    create(:user, profile: profile)

    expect { subject }.to_not change(profile, :updated_at)
  end

  it "changes the legacy_encryption_disabled settings for users with deleted old secrets" do
    user_incomplete = create(:user, display_name: :deleted_legacy_secrets)
    create(:item, user: user_incomplete, encryption_version: 2)
    create(:item, user: user_incomplete, encryption_version: 1, name: "dup", deleted_at: Time.current)

    subject
    profile = user_incomplete.profile.reload

    expect(profile.application_settings["legacy_encryption_disabled"]).to be true
  end
end
