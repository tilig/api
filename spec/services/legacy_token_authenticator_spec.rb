require "rails_helper"

RSpec.describe Auth::LegacyTokenAuthenticator, type: :service do
  describe "#decode!" do
    let(:user) { create(:user) }
    let(:token) { Auth::AccessTokenGenerator.generate(user) }
    let(:payload) { {"email" => "henk@test.com", "uid" => "1234", "iss" => "https://app.tilig.com"} }

    # Mock the access token's payload
    before do
      allow(Auth::AccessTokenGenerator).to receive(:payload)
        .and_return(payload)
    end

    subject { Auth::LegacyTokenAuthenticator.new.authenticate(token) }

    describe "when the token is valid" do
      it "returns the payload" do
        allow(User).to receive(:find).with("1234").and_return(true)
        expect(subject).to be true
      end
    end

    describe "when the token is invalid" do
      let(:payload) { {"email" => "henk@test.com", "uid" => "1234", :iss => "Random issuer"} }

      it "raises returns nil" do
        expect(subject).to be_nil
      end
    end

    describe "when the user doesn't exist" do
      it "raises a JwtDecodeError" do
        allow(User).to receive(:find).with("1234").and_raise(ActiveRecord::RecordNotFound)
        expect(subject).to be_nil
      end
    end
  end
end
