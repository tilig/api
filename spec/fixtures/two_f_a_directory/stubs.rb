module TwoFADirectory
  TWO_F_A_DIRECTORY_RESPONSE = <<~JSON.squish
    [
      [
        "(ISC)2",
        {
          "domain": "isc2.org",
          "url": "https://www.isc2.org",
          "tfa": ["sms", "totp"],
          "keywords": ["education"]
        }
      ],
      [
        "000webhost",
        {
          "domain": "000webhost.com",
          "contact": {
            "twitter": "000webhost_com",
            "facebook": "000webhost.Global"
          },
          "keywords": ["hosting"]
        }
      ],
      [
        "007Names",
        {
          "domain": "007names.com",
          "url": "https://www.007names.com",
          "img": "007names.com.png",
          "tfa": ["totp"],
          "documentation": "https://www.007names.com/info/two-step.xhtml",
          "keywords": ["domains"]
        }
      ],
      [
        "1&amp;1 IONOS",
        {
          "domain": "ionos.com",
          "url": "https://www.ionos.com/",
          "tfa": ["totp"],
          "documentation": "https://www.ionos.com/help/index.php?id=3269",
          "keywords": ["domains"]
        }
      ],
      [
        "101domain",
        {
          "domain": "101domain.com",
          "tfa": ["totp"],
          "documentation": "https://help.101domain.com/account-management/account-security/enabling-disabling-two-factor-verification",
          "keywords": ["domains"]
        }
      ],
      [
        "123 Form Builder",
        {
          "domain": "123formbuilder.com",
          "url": "https://www.123formbuilder.com",
          "tfa": ["totp"],
          "documentation": "https://www.123formbuilder.com/docs/multi-factor-authentication-login",
          "keywords": ["developer"]
        }
      ],
      [
        "123 Reg",
        {
          "domain": "123-reg.co.uk",
          "url": "https://www.123-reg.co.uk",
          "contact": { "facebook": "123regfans", "twitter": "123reg" },
          "keywords": ["domains"]
        }
      ],
      [
        "15Five",
        {
          "domain": "15five.com",
          "tfa": ["totp"],
          "documentation": "https://success.15five.com/hc/en-us/articles/360002698811",
          "keywords": ["communication"]
        }
      ],
      [
        "1822direkt",
        {
          "domain": "1822direkt.de",
          "url": "https://www.1822direkt.de/",
          "tfa": ["sms", "custom-software"],
          "documentation": "https://www.1822direkt.de/service/sicherheitsportal/sicheres-online-banking/",
          "keywords": ["banking"],
          "regions": ["de"]
        }
      ],
      [
        "1Password",
        {
          "domain": "1password.com",
          "additional-domains": ["1password.ca", "1password.eu"],
          "tfa": ["totp", "u2f", "custom-hardware", "custom-software"],
          "custom-software": ["Duo"],
          "documentation": "https://support.1password.com/two-factor-authentication/",
          "notes": "TOTP 2FA must be enabled before U2F 2FA can be enabled. Teams using Duo with 1Password can't enable other 2FA methods. Non-U2F Hardware tokens and custom software methods are only available through Duo.",
          "keywords": ["identity"]
        }
      ],
      [
        "20i",
        {
          "domain": "20i.com",
          "url": "https://www.20i.com",
          "tfa": ["totp"],
          "documentation": "https://www.20i.com/support/my-services/two-factor-authentication-my20i",
          "keywords": ["hosting"]
        }
      ],
      [
        "23andMe",
        {
          "domain": "23andme.com",
          "url": "https://www.23andme.com/",
          "tfa": ["totp"],
          "documentation": "https://customercare.23andme.com/hc/en-us/articles/360034119874",
          "keywords": ["health"]
        }
      ],
      [
        "2Checkout",
        {
          "domain": "2checkout.com",
          "url": "https://www.2checkout.com",
          "tfa": ["totp", "email"],
          "documentation": "https://knowledgecenter.2checkout.com/Onboarding/Activate-and-set-up-your-2Checkout-account/Two-factor-authentication",
          "keywords": ["other"]
        }
      ],
      [
        "34SP.com",
        {
          "domain": "34SP.com",
          "tfa": ["totp"],
          "documentation": "https://www.34sp.com/kb/142/how-to-enable-two-factor-authentication-on-your-account",
          "keywords": ["hosting"],
          "regions": ["us"]
        }
      ],
      [
        "3Commas",
        {
          "domain": "3commas.io",
          "tfa": ["totp"],
          "documentation": "https://help.3commas.io/en/articles/3424452",
          "recovery": "https://help.3commas.io/en/articles/3108967",
          "keywords": ["cryptocurrencies"]
        }
      ],
      [
        "500px",
        {
          "domain": "web.500px.com",
          "tfa": ["totp"],
          "documentation": "https://support.500px.com/hc/en-us/articles/205115877",
          "keywords": ["social"]
        }
      ],
      [
        "7Pass",
        {
          "domain": "7pass.de",
          "contact": { "email": "service@7pass.de", "language": "de" },
          "keywords": ["identity"]
        }
      ],
      [
        "A2 Hosting",
        {
          "domain": "a2hosting.com",
          "url": "https://www.a2hosting.com",
          "tfa": ["totp"],
          "documentation": "https://www.a2hosting.com/kb/a2-hosting-customer-portal/account-management/enabling-two-factor-authentication-for-your-a2-hosting-account",
          "keywords": ["hosting"]
        }
      ],
      [
        "AAI@EduHr",
        {
          "domain": "aaiedu.hr",
          "url": "https://www.aaiedu.hr/",
          "contact": { "email": "aai@srce.hr", "language": "hr" },
          "keywords": ["education"],
          "regions": ["hr"]
        }
      ],
      [
        "Abode",
        {
          "domain": "goabode.com",
          "additional-domains": ["goabode.co.uk", "abode.mx"],
          "tfa": ["totp"],
          "documentation": "https://help.goabode.com/hc/en-us/articles/360022414751",
          "keywords": ["iot"],
          "regions": ["us", "ca", "gb", "mx"]
        }
      ],
      [
        "about.me",
        {
          "domain": "about.me",
          "contact": { "twitter": "aboutdotme" },
          "keywords": ["social"]
        }
      ],
      [
        "Above.com",
        {
          "domain": "above.com",
          "url": "https://www.above.com",
          "tfa": ["totp"],
          "documentation": "https://www.above.com/manual/my-account-manual-two-step-auth.html",
          "keywords": ["domains"]
        }
      ],
      [
        "Academic Earth",
        {
          "domain": "academicearth.org",
          "contact": { "facebook": "academicearth", "twitter": "academicearth" },
          "keywords": ["education"]
        }
      ],
      [
        "Accelo",
        {
          "domain": "accelo.com",
          "url": "https://www.accelo.com/",
          "tfa": ["totp"],
          "documentation": "https://www.accelo.com/resources/help/faq/user-permissions-and-settings/two-factor-authentication/",
          "keywords": ["other"]
        }
      ],
      [
        "Acorns",
        {
          "domain": "acorns.com",
          "url": "https://www.acorns.com/",
          "tfa": ["sms"],
          "documentation": "https://www.acorns.com/support/how-do-i-turn-on-two-factor-authentication-on-my-account/",
          "keywords": ["investing"],
          "regions": ["us"]
        }
      ],
      [
        "Acquia",
        {
          "domain": "acquia.com",
          "url": "https://www.acquia.com",
          "tfa": ["sms", "totp"],
          "documentation": "https://docs.acquia.com/cloud-platform/access/signin/",
          "notes": "SMS 2FA is only available for US or Canada phone numbers.",
          "keywords": ["hosting"]
        }
      ],
      [
        "Action Network",
        {
          "domain": "actionnetwork.org",
          "tfa": ["totp", "sms"],
          "documentation": "https://help.actionnetwork.org/hc/en-us/articles/217206826",
          "keywords": ["marketing"]
        }
      ],
      [
        "Actionstep",
        {
          "domain": "actionstep.com",
          "url": "https://www.actionstep.com",
          "tfa": ["totp"],
          "documentation": "https://intercom.help/actionstep/en/articles/4638611",
          "keywords": ["legal"]
        }
      ],
      [
        "ACTIVE 24",
        {
          "domain": "active24.com",
          "url": "https://www.active24.com/",
          "tfa": ["totp"],
          "documentation": "https://faq.active24.com/cz/183996-",
          "keywords": ["hosting"],
          "regions": ["cz", "gb", "nl", "sk", "de", "es"]
        }
      ],
      [
        "ActiveCampaign",
        {
          "domain": "activecampaign.com",
          "url": "https://www.activecampaign.com/",
          "tfa": ["sms", "totp"],
          "documentation": "https://help.activecampaign.com/hc/en-us/articles/360008574740",
          "keywords": ["marketing"]
        }
      ],
      [
        "ActiveCollab",
        {
          "domain": "activecollab.com",
          "contact": { "twitter": "activecollab" },
          "keywords": ["task"]
        }
      ],
      [
        "ActiveState",
        {
          "domain": "activestate.com",
          "url": "https://www.activestate.com",
          "tfa": ["totp"],
          "documentation": "https://docs.activestate.com/platform/user/prefs/twofactor/",
          "keywords": ["developer"]
        }
      ],
      [
        "Adafruit",
        {
          "domain": "adafruit.com",
          "url": "https://www.adafruit.com",
          "tfa": ["sms", "totp"],
          "documentation": "https://learn.adafruit.com/how-to-set-up-2-factor-authentication-on-adafruit",
          "keywords": ["retail"]
        }
      ],
      [
        "Addiko Bank",
        {
          "domain": "addiko.hr",
          "url": "https://www.addiko.hr/",
          "tfa": ["totp", "custom-hardware"],
          "documentation": "https://www.addiko.hr/static/uploads/mtoken-uputa-za-koristenje.pdf",
          "regions": ["hr"],
          "keywords": ["banking"]
        }
      ],
      [
        "AdGuard",
        {
          "domain": "adguard.com",
          "tfa": ["totp"],
          "documentation": "https://kb.adguard.com/en/general/2fa",
          "keywords": ["security"]
        }
      ],
      [
        "Admiral (UK)",
        {
          "domain": "admiral.com",
          "url": "https://www.admiral.com",
          "contact": { "facebook": "admiralUK", "twitter": "AdmiralUK" },
          "keywords": ["finance"],
          "regions": ["gb"]
        }
      ],
      [
        "Adobe ID",
        {
          "domain": "adobe.com",
          "tfa": ["sms", "email", "custom-software"],
          "documentation": "https://helpx.adobe.com/manage-account/using/secure-your-adobe-account.html",
          "keywords": ["creativity"]
        }
      ],
      [
        "ADP",
        {
          "domain": "adp.com",
          "url": "https://www.adp.com",
          "contact": { "facebook": "AutomaticDataProcessing", "twitter": "ADP" },
          "keywords": ["finance"]
        }
      ],
      [
        "ADT Pulse",
        {
          "domain": "adt.com",
          "additional-domains": ["portal.adtpulse.com"],
          "tfa": ["sms", "call", "email"],
          "documentation": "https://www.adt.com/help/pulse/mfa",
          "keywords": ["iot"]
        }
      ],
      [
        "Adyen",
        {
          "domain": "adyen.com",
          "url": "https://www.adyen.com/",
          "tfa": ["totp"],
          "documentation": "https://docs.adyen.com/account/two-factor-authentication",
          "keywords": ["payments"]
        }
      ],
      [
        "Aerolíneas Argentinas",
        {
          "domain": "aerolineas.com.ar",
          "url": "https://www.aerolineas.com.ar/",
          "contact": {
            "facebook": "aerolineas.argentinas",
            "twitter": "Aerolineas_AR",
            "language": "es"
          },
          "keywords": ["transport"],
          "regions": ["ar"]
        }
      ],
      [
        "Aetna",
        {
          "domain": "aetna.com",
          "url": "https://www.aetna.com/",
          "contact": { "facebook": "aetna", "twitter": "Aetna" },
          "keywords": ["finance"],
          "regions": ["us"]
        }
      ],
      [
        "AFIP",
        {
          "domain": "afip.gob.ar",
          "url": "https://www.afip.gob.ar",
          "tfa": ["custom-hardware", "custom-software"],
          "documentation": "https://www.afip.gob.ar/claveFiscal/informacion-basica/solicitud.asp",
          "keywords": ["government"],
          "regions": ["ar"]
        }
      ],
      [
        "AfterPay",
        {
          "domain": "afterpay.com",
          "tfa": ["sms", "email"],
          "keywords": ["payments"],
          "additional-domains": [
            "afterpay.com.au",
            "secure-afterpay.com.au",
            "afterpay.co.nz",
            "clearpay.co.uk"
          ],
          "regions": ["au", "us", "nz", "gb", "ca"]
        }
      ],
      [
        "Agenzia delle Entrate",
        {
          "domain": "agenziaentrate.gov.it",
          "url": "https://www.agenziaentrate.gov.it/",
          "contact": {
            "facebook": "agenziadelleentrate",
            "twitter": "Agenzia_Entrate",
            "language": "it"
          },
          "keywords": ["government"],
          "regions": ["it"]
        }
      ],
      [
        "Aha!",
        {
          "domain": "aha.io",
          "url": "https://www.aha.io/",
          "tfa": ["sms", "call", "custom-software", "u2f"],
          "documentation": "https://support.aha.io/hc/en-us/articles/360031319232#enable-2fa-for-your-account",
          "keywords": ["developer"]
        }
      ],
      [
        "Air Canada",
        {
          "domain": "aircanada.com",
          "url": "https://www.aircanada.com/",
          "tfa": ["email", "sms"],
          "documentation": "https://www.aircanada.com/ca/en/aco/home/customer-profile/frequently-asked-questions.html",
          "keywords": ["transport"],
          "regions": ["ca"]
        }
      ],
      [
        "Air France",
        {
          "domain": "airfrance.com",
          "url": "https://www.airfrance.com",
          "contact": { "facebook": "airfrance", "twitter": "airfrance" },
          "keywords": ["transport"]
        }
      ],
      [
        "AirBank",
        {
          "domain": "airbank.cz",
          "url": "https://www.airbank.cz",
          "tfa": ["sms", "custom-software"],
          "documentation": "https://www.airbank.cz/co-vas-nejvic-zajima/jake-bezpecnostni-prvky-u-nas-mame",
          "regions": ["cz"],
          "keywords": ["banking"]
        }
      ],
      [
        "Airbnb",
        {
          "domain": "airbnb.com",
          "url": "https://www.airbnb.com",
          "tfa": ["email", "sms"],
          "documentation": "https://www.airbnb.com/help/article/501",
          "notes": "Airbnb decides when 2FA is required; user cannot enable 2FA for every login.",
          "keywords": ["hotels"]
        }
      ],
      [
        "Airbrake",
        {
          "domain": "airbrake.io",
          "tfa": ["totp"],
          "documentation": "https://airbrake.io/docs/features/two-factor-authentication/",
          "keywords": ["developer"]
        }
      ],
      [
        "AirDroid",
        {
          "domain": "airdroid.com",
          "url": "https://www.airdroid.com/",
          "contact": { "facebook": "AirDroid", "twitter": "AirDroidTeam" },
          "keywords": ["remote"]
        }
      ],
      [
        "Airship",
        {
          "domain": "airship.com",
          "url": "https://www.airship.com/",
          "tfa": ["totp"],
          "documentation": "https://docs.airship.com/tutorials/security/2fa/",
          "notes": "2FA is only available upon request for customers on their Essential and Comprehensive plans.",
          "keywords": ["marketing"]
        }
      ],
      [
        "Airtable",
        {
          "domain": "airtable.com",
          "tfa": ["sms", "call", "totp"],
          "documentation": "https://support.airtable.com/hc/en-us/articles/219409917",
          "notes": "SMS must be enabled first and software 2FA can be bypassed by SMS.",
          "keywords": ["task"]
        }
      ],
      [
        "Aiven",
        {
          "domain": "aiven.io",
          "tfa": ["totp"],
          "documentation": "https://help.aiven.io/en/articles/947120",
          "keywords": ["cloud"]
        }
      ],
      [
        "AJ Bell Youinvest",
        {
          "domain": "youinvest.co.uk",
          "url": "https://www.youinvest.co.uk/",
          "tfa": ["totp"],
          "documentation": "https://www.youinvest.co.uk/faq/how-do-i-set-two-factor-authentication",
          "keywords": ["investing"],
          "regions": ["gb"]
        }
      ],
      [
        "Alaska Airlines",
        {
          "domain": "alaskaair.com",
          "url": "https://www.alaskaair.com",
          "contact": { "facebook": "alaskaairlines", "twitter": "AlaskaAir" },
          "keywords": ["transport"],
          "regions": ["ca", "cr", "bz", "mx", "us"]
        }
      ],
      [
        "Alchemer",
        {
          "domain": "alchemer.com",
          "url": "https://www.alchemer.com/",
          "tfa": ["totp", "sms"],
          "documentation": "https://help.alchemer.com/help/multi-factor-authentication#user",
          "notes": "SMS 2FA is only available for Enterprise accounts with an SMS 2FA provision.",
          "keywords": ["marketing"]
        }
      ],
      [
        "ALDImobile",
        {
          "domain": "aldimobile.com.au",
          "contact": {
            "twitter": "ALDIMobileAU",
            "email": "feedback@aldimobile.com.au"
          },
          "keywords": ["utilities"],
          "regions": ["au"]
        }
      ],
      [
        "Alectra Utilities",
        {
          "domain": "alectrautilities.com",
          "contact": { "facebook": "alectranews", "twitter": "alectranews" },
          "keywords": ["utilities"],
          "regions": ["ca"]
        }
      ],
      [
        "Algolia",
        {
          "domain": "algolia.com",
          "tfa": ["totp"],
          "documentation": "https://www.algolia.com/doc/security/best-security-practices/#two-factor-authentication",
          "keywords": ["developer"]
        }
      ],
      [
        "Algorithmia",
        {
          "domain": "algorithmia.com",
          "contact": { "twitter": "algorithmia", "facebook": "algorithmia" },
          "keywords": ["developer"]
        }
      ],
      [
        "AliExpress",
        {
          "domain": "aliexpress.com",
          "url": "https://www.aliexpress.com/",
          "contact": { "twitter": "AliExpress_EN" },
          "keywords": ["retail"]
        }
      ],
      [
        "ALL-INKL.COM",
        {
          "domain": "all-inkl.com",
          "tfa": ["totp"],
          "documentation": "https://all-inkl.com/wichtig/faq/#2fa",
          "keywords": ["hosting"]
        }
      ],
      [
        "Allegro",
        {
          "domain": "allegro.pl",
          "tfa": ["sms", "totp"],
          "notes": "SMS 2FA cannot be disabled even when TOTP is enabled.",
          "documentation": "https://allegro.pl/pomoc/dla-kupujacych/logowanie-i-haslo/dwustopniowe-logowanie-najczesciej-zadawane-pytania-dykqg9nMKSZ",
          "keywords": ["retail"],
          "regions": ["pl"]
        }
      ],
      [
        "Alliant Credit Union",
        {
          "domain": "alliantcreditunion.org",
          "url": "https://www.alliantcreditunion.org/",
          "tfa": ["sms"],
          "documentation": "https://www.alliantcreditunion.org/help/how-does-alliant-two-factor-authentication-work",
          "regions": ["us"],
          "keywords": ["banking"]
        }
      ],
      [
        "Allianz",
        {
          "domain": "allianz.de",
          "url": "https://www.allianz.de/",
          "tfa": ["sms", "custom-hardware"],
          "documentation": "https://www.allianz.de/datenschutz/allianz-deutschland-ag/online-service-meine-allianz/",
          "keywords": ["finance"],
          "regions": ["de"]
        }
      ],
      [
        "Ally Bank",
        {
          "domain": "ally.com",
          "url": "https://www.ally.com/",
          "tfa": ["sms", "email"],
          "documentation": "https://www.ally.com/messages/sms/",
          "regions": ["us"],
          "keywords": ["banking"]
        }
      ],
      [
        "AltCoinTrader",
        {
          "domain": "altcointrader.co.za",
          "tfa": ["totp"],
          "documentation": "https://altcointrader.zendesk.com/hc/en-gb/articles/360007179680",
          "recovery": "https://altcointrader.zendesk.com/hc/en-gb/articles/360007225480",
          "keywords": ["cryptocurrencies"]
        }
      ],
      [
        "Alterdice",
        {
          "domain": "alterdice.com",
          "tfa": ["email", "totp"],
          "keywords": ["cryptocurrencies"]
        }
      ],
      [
        "AltoIRA",
        {
          "domain": "altoira.com",
          "url": "https://www.altoira.com",
          "tfa": ["totp"],
          "documentation": "https://help.altoira.com/enabling-/-disabling-two-factor-authentication-2fa",
          "keywords": ["finance"],
          "regions": ["us"]
        }
      ],
      [
        "Altra Federal Credit Union",
        {
          "domain": "altra.org",
          "url": "https://www.altra.org/",
          "tfa": ["sms", "email"],
          "regions": ["us"],
          "keywords": ["banking"]
        }
      ],
      [
        "Amazing Marvin",
        {
          "domain": "amazingmarvin.com",
          "img": "amazingmarvin.com.png",
          "tfa": ["totp"],
          "documentation": "https://help.amazingmarvin.com/en/articles/5019430",
          "keywords": ["task"]
        }
      ],
      [
        "Amazon",
        {
          "domain": "amazon.com",
          "additional-domains": [
            "amazon.ae",
            "amazon.ca",
            "amazon.cn",
            "amazon.co.jp",
            "amazon.co.uk",
            "amazon.com.au",
            "amazon.com.br",
            "amazon.com.mx",
            "amazon.com.tr",
            "amazon.de",
            "amazon.es",
            "amazon.fr",
            "amazon.in",
            "amazon.it",
            "amazon.nl",
            "amazon.sa",
            "amazon.se",
            "amazon.sg"
          ],
          "tfa": ["sms", "call", "totp"],
          "documentation": "https://www.amazon.com/gp/help/customer/display.html?nodeId=G3PWZPU52FKN7PW4",
          "notes": "SMS or phone call required to enable 2FA. Enabling on Amazon.com activates 2FA on other regional Amazon sites, such as UK and DE.",
          "keywords": ["retail"]
        }
      ],
      [
        "Amazon Pay",
        {
          "domain": "pay.amazon.com",
          "tfa": ["sms", "call", "totp"],
          "documentation": "https://www.amazon.com/gp/help/customer/display.html?nodeId=G3PWZPU52FKN7PW4",
          "notes": "SMS or phone call required to enable 2FA. Enabling on Amazon.com activates 2FA on other regional Amazon sites, such as UK and DE.",
          "keywords": ["payments"]
        }
      ],
      [
        "Amazon Web Services",
        {
          "domain": "aws.amazon.com",
          "tfa": ["totp", "custom-hardware", "u2f"],
          "documentation": "https://aws.amazon.com/iam/features/mfa/",
          "keywords": ["cloud"]
        }
      ],
      [
        "America First Credit Union",
        {
          "domain": "americafirst.com",
          "url": "https://www.americafirst.com/",
          "tfa": ["sms", "email"],
          "documentation": "https://www.americafirst.com/online_banking.html",
          "regions": ["us"],
          "keywords": ["banking"]
        }
      ],
      [
        "American Airlines",
        {
          "domain": "aa.com",
          "url": "https://www.aa.com/",
          "contact": { "facebook": "AmericanAirlines", "twitter": "AmericanAir" },
          "keywords": ["transport"]
        }
      ],
      [
        "American Electric Power",
        {
          "domain": "aep.com",
          "url": "https://www.aep.com/",
          "contact": { "facebook": "americanelectricpower", "twitter": "AEPnews" },
          "keywords": ["utilities"],
          "regions": ["us"]
        }
      ],
      [
        "Google Chat",
        {
          "domain": "chat.google.com",
          "tfa": [
            "sms",
            "call",
            "totp",
            "custom-software",
            "u2f"
          ],
          "documentation": "https://www.google.com/intl/en-US/landing/2step/features.html",
          "keywords": [
            "communication"
          ]
        }
      ],
      [
        "Google Cloud Platform",
        {
          "domain": "cloud.google.com",
          "tfa": [
            "sms",
            "call",
            "totp",
            "custom-software",
            "u2f"
          ],
          "documentation": "https://www.google.com/intl/en-US/landing/2step/features.html",
          "keywords": [
            "cloud"
          ]
        }
      ],
      [
        "Google Domains",
        {
          "domain": "domains.google.com",
          "tfa": [
            "sms",
            "call",
            "custom-software",
            "totp",
            "u2f"
          ],
          "documentation": "https://www.google.com/intl/en-US/landing/2step/features.html",
          "keywords": [
            "domains"
          ]
        }
      ],
      [
        "PythonAnywhere",
        {
          "domain": "pythonanywhere.com",
          "url": "https://www.pythonanywhere.com",
          "tfa": [
            "totp"
          ],
          "documentation": "https://help.pythonanywhere.com/pages/SecuringYourAccount/",
          "keywords": [
            "developer"
          ]
        }
      ]
    ]
  JSON

  ONE_O_ONE_DOMAIN_UPDATED = <<~JSON.squish
    [
      [
        "101domain",
        {
          "domain": "101domain.com",
          "documentation": "https://help.101domain.com/account-management/account-security/enabling-disabling-two-factor-verification",
          "keywords": ["domains"]
        }
      ]
    ]
  JSON
end
