require "rails_helper"
require "fixtures/mailchimp/fixtures"

def stub_mailchimp_request
  stub_request(:post, "https://random-server.api.mailchimp.com/3.0/lists/random-list-id/members")
    .to_return(status: 200, body: Mailchimp::Fixtures::CREATE_MEMBER_RESPONSE, headers: {"Content-Type" => "application/json"})
end
