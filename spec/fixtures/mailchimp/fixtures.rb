module Mailchimp
  module Fixtures
    CREATE_MEMBER_RESPONSE = <<~JSON.squish
      {
        "id": "4fa144da6e487e02f72bbf23f8c7a3d2",
        "email_address": "fatboypunk@gmail.com",
        "unique_email_id": "ed7d2b355c",
        "contact_id": "8b1d94d6c51a6efd0bcf85606e8a3bae",
        "full_name": "Marcel Horlings",
        "web_id": 586071796,
        "email_type": "html",
        "status": "subscribed",
        "consents_to_one_to_one_messaging": true,
        "merge_fields": {
          "FNAME": "Marcel",
          "LNAME": "Horlings",
          "ADDRESS": "",
          "PHONE": "",
          "BIRTHDAY": ""
        },
        "stats": {
          "avg_open_rate": 0,
          "avg_click_rate": 0
        },
        "ip_signup": "",
        "timestamp_signup": "",
        "ip_opt": "77.172.76.93",
        "timestamp_opt": "2022-04-06T20:44:23+00:00",
        "member_rating": 2,
        "last_changed": "2022-04-06T20:44:23+00:00",
        "language": "",
        "vip": false,
        "email_client": "",
        "location": {
          "latitude": 0,
          "longitude": 0,
          "gmtoff": 0,
          "dstoff": 0,
          "country_code": "",
          "timezone": "",
          "region": ""
        },
        "source": "API - Generic",
        "tags_count": 0,
        "tags": [

        ],
        "list_id": "c80bd7aa31",
        "_links": [
          {
            "rel": "self",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2",
            "method": "GET",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Response.json"
          },
          {
            "rel": "parent",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members",
            "method": "GET",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/CollectionResponse.json",
            "schema": "https://us14.api.mailchimp.com/schema/3.0/Paths/Lists/Members/Collection.json"
          },
          {
            "rel": "update",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2",
            "method": "PATCH",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Response.json",
            "schema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/PATCH.json"
          },
          {
            "rel": "upsert",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2",
            "method": "PUT",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Response.json",
            "schema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/PUT.json"
          },
          {
            "rel": "delete",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2",
            "method": "DELETE"
          },
          {
            "rel": "activity",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2/activity",
            "method": "GET",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Activity/Response.json"
          },
          {
            "rel": "goals",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2/goals",
            "method": "GET",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Goals/Response.json"
          },
          {
            "rel": "notes",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2/notes",
            "method": "GET",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Notes/CollectionResponse.json"
          },
          {
            "rel": "events",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2/events",
            "method": "POST",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Events/POST.json"
          },
          {
            "rel": "delete_permanent",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2/actions/delete-permanent",
            "method": "POST"
          }
        ]
      }
    JSON

    MEMBER_WITHOUT_MERGE_FIELDS = <<~JSON.squish
      {
        "id": "4fa144da6e487e02f72bbf23f8c7a3d2",
        "email_address": "fatboypunk@gmail.com",
        "unique_email_id": "ed7d2b355c",
        "contact_id": "8b1d94d6c51a6efd0bcf85606e8a3bae",
        "full_name": "Marcel Horlings",
        "web_id": 586071796,
        "email_type": "html",
        "status": "subscribed",
        "consents_to_one_to_one_messaging": true,
        "merge_fields": {
          "FNAME": "",
          "LNAME": "",
          "ADDRESS": "",
          "PHONE": "",
          "BIRTHDAY": ""
        },
        "stats": {
          "avg_open_rate": 0,
          "avg_click_rate": 0
        },
        "ip_signup": "",
        "timestamp_signup": "",
        "ip_opt": "77.172.76.93",
        "timestamp_opt": "2022-04-06T20:44:23+00:00",
        "member_rating": 2,
        "last_changed": "2022-04-06T20:44:23+00:00",
        "language": "",
        "vip": false,
        "email_client": "",
        "location": {
          "latitude": 0,
          "longitude": 0,
          "gmtoff": 0,
          "dstoff": 0,
          "country_code": "",
          "timezone": "",
          "region": ""
        },
        "source": "API - Generic",
        "tags_count": 0,
        "tags": [

        ],
        "list_id": "c80bd7aa31",
        "_links": [
          {
            "rel": "self",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2",
            "method": "GET",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Response.json"
          },
          {
            "rel": "parent",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members",
            "method": "GET",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/CollectionResponse.json",
            "schema": "https://us14.api.mailchimp.com/schema/3.0/Paths/Lists/Members/Collection.json"
          },
          {
            "rel": "update",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2",
            "method": "PATCH",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Response.json",
            "schema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/PATCH.json"
          },
          {
            "rel": "upsert",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2",
            "method": "PUT",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Response.json",
            "schema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/PUT.json"
          },
          {
            "rel": "delete",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2",
            "method": "DELETE"
          },
          {
            "rel": "activity",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2/activity",
            "method": "GET",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Activity/Response.json"
          },
          {
            "rel": "goals",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2/goals",
            "method": "GET",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Goals/Response.json"
          },
          {
            "rel": "notes",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2/notes",
            "method": "GET",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Notes/CollectionResponse.json"
          },
          {
            "rel": "events",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2/events",
            "method": "POST",
            "targetSchema": "https://us14.api.mailchimp.com/schema/3.0/Definitions/Lists/Members/Events/POST.json"
          },
          {
            "rel": "delete_permanent",
            "href": "https://us14.api.mailchimp.com/3.0/lists/c80bd7aa31/members/4fa144da6e487e02f72bbf23f8c7a3d2/actions/delete-permanent",
            "method": "POST"
          }
        ]
      }
    JSON

    MEMBER_EXISTS_RESPONSE = <<~JSON.squish
      {"title": "Member Exists",
       "status": 400,
       "detail":
        "fatboypunk@gmail.com is already a list member. Use PUT to insert or update list members.",
       "instance": 4edbf5db-fdfe-5634-0c53-964c91ded8dd"}
    JSON
  end
end
