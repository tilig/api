module BrandfetchFixtures
  GOOGLE_PLAY = <<~JSON.squish
    {
      "name": "Google",
      "domain": "google.com",
      "claimed": false,
      "description": "Our mission is to organize the world’s information and make it universally accessible and useful.",
      "links": [
        {
          "name": "crunchbase",
          "url": "https://crunchbase.com/organization/google"
        },
        {
          "name": "twitter",
          "url": "https://twitter.com/google"
        },
        {
          "name": "instagram",
          "url": "https://instagram.com/google"
        },
        {
          "name": "linkedin",
          "url": "https://linkedin.com/company/google"
        },
        {
          "name": "facebook",
          "url": "https://facebook.com/Google"
        }
      ],
      "logos": [
        {
          "type": "logo",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/id6O2oGzv-/idP6g0Ep4y.svg",
              "background": "transparent",
              "format": "svg",
              "size": 1933
            },
            {
              "src": "https://asset.brandfetch.io/id6O2oGzv-/idSuJ5ik7i.png",
              "background": "transparent",
              "format": "png",
              "height": 251,
              "width": 800,
              "size": 21577
            }
          ]
        },
        {
          "type": "symbol",
          "theme": "light",
          "formats": [
            {
              "src": "https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg",
              "background": "transparent",
              "format": "svg",
              "size": 851
            },
            {
              "src": "https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png",
              "background": "transparent",
              "format": "png",
              "height": 800,
              "width": 800,
              "size": 29934
            }
          ]
        },
        {
          "type": "icon",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/id6O2oGzv-/idNEgS9h8q.jpeg",
              "background": null,
              "format": "jpeg",
              "height": 400,
              "width": 400,
              "size": 11895
            }
          ]
        }
      ],
      "colors": [
        {
          "hex": "#4285F4",
          "type": "accent",
          "brightness": 127
        },
        {
          "hex": "#FFFFFF",
          "type": "light",
          "brightness": 255
        },
        {
          "hex": "#424242",
          "type": "dark",
          "brightness": 66
        },
        {
          "hex": "#34A853",
          "type": "brand",
          "brightness": 137
        },
        {
          "hex": "#FBBC05",
          "type": "brand",
          "brightness": 188
        },
        {
          "hex": "#EA4335",
          "type": "brand",
          "brightness": 101
        }
      ],
      "fonts": [
        {
          "name": "Google Sans",
          "type": "title",
          "origin": "custom",
          "originId": null,
          "weights": [

          ]
        },
        {
          "name": "Google Sans Text",
          "type": "body",
          "origin": "custom",
          "originId": null,
          "weights": [

          ]
        }
      ],
      "images": [
        {
          "type": "banner",
          "formats": [
            {
              "src": "https://asset.brandfetch.io/id6O2oGzv-/ide1CTsR8v.jpeg",
              "background": null,
              "format": "jpeg",
              "height": 500,
              "width": 1500,
              "size": 46016
            }
          ]
        }
      ]
    }
  JSON

  BLOGSPOT = <<~JSON.squish
    {
      "name": "Blogspot",
      "domain": "blogspot.com",
      "claimed": false,
      "description": "Publish your passions your way. Whether youâd like to share your knowledge, experiences or the latest news, create a unique and beautiful blog for free.",
      "links": [

      ],
      "logos": [

      ],
      "colors": [
        {
          "hex": "#1c4f46",
          "type": "dark",
          "brightness": 68
        },
        {
          "hex": "#ffffff",
          "type": "light",
          "brightness": 255
        },
        {
          "hex": "#398d80",
          "type": "accent",
          "brightness": 122
        }
      ],
      "fonts": [

      ],
      "images": [

      ]
    }
  JSON

  BAIDU = <<~JSON.squish

      {
      "name": "YouTube",
      "domain": "youtube.com",
      "claimed": false,
      "description": "YouTube, a video-sharing platform, allows users to upload, view, and share videos, including movie and music clips and amateur content.",
      "links": [
        {
          "name": "youtube",
          "url": "https://youtube.com/organization/youtube"
        },
        {
          "name": "twitter",
          "url": "https://twitter.com/YouTube"
        }
      ],
      "logos": [
        {
          "type": "logo",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idVfYwcuQz/idjJfigk8Y.svg",
              "background": "transparent",
              "format": "svg",
              "size": 6552
            },
            {
              "src": "https://asset.brandfetch.io/idVfYwcuQz/idDmdVQ6Ym.png",
              "background": "transparent",
              "format": "png",
              "height": 178,
              "width": 800,
              "size": 15889
            }
          ]
        },
        {
          "type": "symbol",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idVfYwcuQz/id8hkaZwOR.png",
              "background": "transparent",
              "format": "png",
              "height": 562,
              "width": 800,
              "size": 22086
            }
          ]
        },
        {
          "type": "icon",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idVfYwcuQz/idmfkl4meA.jpeg",
              "background": null,
              "format": "jpeg",
              "height": 400,
              "width": 400,
              "size": 6987
            }
          ]
        }
      ],
      "colors": [
        {
          "hex": "#212121",
          "type": "dark",
          "brightness": 33
        },
        {
          "hex": "#ffffff",
          "type": "light",
          "brightness": 255
        },
        {
          "hex": "#ff0000",
          "type": "accent",
          "brightness": 54
        }
      ],
      "fonts": [
        {
          "name": "Roboto",
          "type": "title",
          "origin": "google",
          "originId": "Roboto",
          "weights": [

          ]
        },
        {
          "name": "Roboto",
          "type": "body",
          "origin": "google",
          "originId": "Roboto",
          "weights": [

          ]
        }
      ],
      "images": [
        {
          "type": "banner",
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idVfYwcuQz/idUDrr1dt9.png",
              "background": null,
              "format": "png",
              "height": 1000,
              "width": 1500,
              "size": 1179298
            }
          ]
        }
      ]
    }
  JSON
  YAHOO = <<~JSON.squish
    {
      "name": "Yahoo!",
      "domain": "yahoo.com",
      "claimed": false,
      "description": "News, email and search are just the beginning. Discover more every day. Find your yodel.",
      "links": [
        {
          "name": "crunchbase",
          "url": "https://crunchbase.com/organization/yahoo"
        },
        {
          "name": "twitter",
          "url": "https://twitter.com/YahooGroupes"
        },
        {
          "name": "instagram",
          "url": "https://instagram.com/yahoo"
        },
        {
          "name": "linkedin",
          "url": "https://linkedin.com/company/yahoo"
        },
        {
          "name": "facebook",
          "url": "https://facebook.com/yahoo"
        }
      ],
      "logos": [
        {
          "type": "icon",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/id6AVuB8Pi/idQ9By8eo8.svg",
              "background": null,
              "format": "svg",
              "height": 400,
              "width": 400,
              "size": 20007
            }
          ]
        },
        {
          "type": "logo",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/id6AVuB8Pi/idQ9By8eo8.svg",
              "background": "transparent",
              "format": "svg",
              "height": null,
              "width": null,
              "size": 3157
            }
          ]
        }
      ],
      "colors": [
        {
          "hex": "#0f69ff",
          "type": "dark",
          "brightness": 97
        },
        {
          "hex": "#ffffff",
          "type": "light",
          "brightness": 255
        },
        {
          "hex": "#4d00ae",
          "type": "accent",
          "brightness": 29
        }
      ],
      "fonts": [
        {
          "name": "Helvetica Neue",
          "type": "title",
          "origin": "custom",
          "originId": null,
          "weights": [

          ]
        },
        {
          "name": "Helvetica Neue",
          "type": "body",
          "origin": "custom",
          "originId": null,
          "weights": [

          ]
        }
      ],
      "images": [
        {
          "type": "banner",
          "formats": [
            {
              "src": "https://asset.brandfetch.io/id6AVuB8Pi/idW58InF2l.png",
              "background": null,
              "format": "png",
              "height": 500,
              "width": 1500,
              "size": 155539
            }
          ]
        }
      ]
    }
  JSON

  AMAZON = <<~JSON.squish
    {
      "name": "Amazon",
      "domain": "amazon.com",
      "claimed": false,
      "description": "Amazon is an international e-commerce website for consumers, sellers, and content creators.",
      "links": [
        {
          "name": "crunchbase",
          "url": "https://crunchbase.com/organization/amazon"
        },
        {
          "name": "twitter",
          "url": "https://twitter.com/amazon"
        },
        {
          "name": "instagram",
          "url": "https://instagram.com/amazon"
        },
        {
          "name": "linkedin",
          "url": "https://linkedin.com/company/amazon"
        },
        {
          "name": "facebook",
          "url": "https://facebook.com/Amazon"
        }
      ],
      "logos": [
        {
          "type": "symbol",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idawOgYOsG/idjowGqOcI.svg",
              "background": "transparent",
              "format": "svg",
              "size": 3047
            },
            {
              "src": "https://asset.brandfetch.io/idawOgYOsG/idNElpn6qu.png",
              "background": "transparent",
              "format": "png",
              "height": 800,
              "width": 800,
              "size": 25098
            }
          ]
        },
        {
          "type": "logo",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idawOgYOsG/idisSB3V3T.svg",
              "background": "transparent",
              "format": "svg",
              "size": 8721
            },
            {
              "src": "https://asset.brandfetch.io/idawOgYOsG/id-h4P8OVR.png",
              "background": "transparent",
              "format": "png",
              "height": 241,
              "width": 800,
              "size": 17776
            }
          ]
        },
        {
          "type": "icon",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idawOgYOsG/idkXDhRL_u.jpeg",
              "background": null,
              "format": "jpeg",
              "height": 400,
              "width": 400,
              "size": 5561
            }
          ]
        }
      ],
      "colors": [
        {
          "hex": "#000000",
          "type": "dark",
          "brightness": 0
        },
        {
          "hex": "#ffffff",
          "type": "light",
          "brightness": 255
        },
        {
          "hex": "#ff9900",
          "type": "accent",
          "brightness": 164
        }
      ],
      "fonts": [
        {
          "name": "Arial",
          "type": "title",
          "origin": "custom",
          "originId": null,
          "weights": [

          ]
        },
        {
          "name": "Arial",
          "type": "body",
          "origin": "custom",
          "originId": null,
          "weights": [

          ]
        }
      ],
      "images": [
        {
          "type": "banner",
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idawOgYOsG/idDgFxIFL-.jpeg",
              "background": null,
              "format": "jpeg",
              "height": 500,
              "width": 1500,
              "size": 172096
            }
          ]
        }
      ]
    }
  JSON
  MICROSOFT = <<~JSON.squish
    {
      "name": "Microsoft",
      "domain": "microsoft.com",
      "claimed": false,
      "description": "Microsoft is a software corporation that develops, manufactures, licenses, supports, and sells a range of software products and services.",
      "links": [
        {
          "name": "crunchbase",
          "url": "https://crunchbase.com/organization/microsoft"
        },
        {
          "name": "twitter",
          "url": "https://twitter.com/Microsoft"
        },
        {
          "name": "instagram",
          "url": "https://instagram.com/microsoft"
        },
        {
          "name": "linkedin",
          "url": "https://linkedin.com/company/microsoft"
        },
        {
          "name": "facebook",
          "url": "https://facebook.com/Microsoft"
        }
      ],
      "logos": [
        {
          "type": "logo",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idchmboHEZ/idDBL6fNkv.svg",
              "background": "transparent",
              "format": "svg",
              "size": 3277
            },
            {
              "src": "https://asset.brandfetch.io/idchmboHEZ/id-ypZheVL.png",
              "background": "transparent",
              "format": "png",
              "height": 171,
              "width": 800,
              "size": 11523
            }
          ]
        },
        {
          "type": "symbol",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idchmboHEZ/iduap5ndHF.svg",
              "background": "transparent",
              "format": "svg",
              "size": 462
            },
            {
              "src": "https://asset.brandfetch.io/idchmboHEZ/id0K98Gag1.png",
              "background": "transparent",
              "format": "png",
              "height": 800,
              "width": 800,
              "size": 16977
            }
          ]
        },
        {
          "type": "icon",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idchmboHEZ/idtz-2CKRH.jpeg",
              "background": null,
              "format": "jpeg",
              "height": 400,
              "width": 400,
              "size": 6408
            }
          ]
        }
      ],
      "colors": [
        {
          "hex": "#000000",
          "type": "dark",
          "brightness": 0
        },
        {
          "hex": "#f2f2f2",
          "type": "light",
          "brightness": 242
        },
        {
          "hex": "#0067b8",
          "type": "accent",
          "brightness": 87
        }
      ],
      "fonts": [
        {
          "name": "Segoe UI",
          "type": "title",
          "origin": "custom",
          "originId": null,
          "weights": [

          ]
        },
        {
          "name": "Segoe UI",
          "type": "body",
          "origin": "custom",
          "originId": null,
          "weights": [

          ]
        }
      ],
      "images": [
        {
          "type": "banner",
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idchmboHEZ/idxY0uKEip.jpeg",
              "background": null,
              "format": "jpeg",
              "height": 500,
              "width": 1500,
              "size": 673408
            }
          ]
        }
      ]
    }
  JSON

  FACEBOOK = <<~JSON.squish
    {
      "name": "Facebook",
      "domain": "facebook.com",
      "claimed": false,
      "description": "Our mission is to give people the power to build community and bring the world closer together.",
      "links": [
        {
          "name": "crunchbase",
          "url": "https://crunchbase.com/organization/facebook"
        },
        {
          "name": "twitter",
          "url": "https://twitter.com/facebook"
        },
        {
          "name": "instagram",
          "url": "https://instagram.com/facebook"
        },
        {
          "name": "linkedin",
          "url": "https://linkedin.com/company/facebook"
        },
        {
          "name": "facebook",
          "url": "https://facebook.com/facebook"
        }
      ],
      "logos": [
        {
          "type": "logo",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idpKX136kp/idNZ6hqFNO.svg",
              "background": "transparent",
              "format": "svg",
              "size": 2604
            },
            {
              "src": "https://asset.brandfetch.io/idpKX136kp/idA0oZ9v8O.png",
              "background": "transparent",
              "format": "png",
              "height": 156,
              "width": 800,
              "size": 15082
            }
          ]
        },
        {
          "type": "symbol",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idpKX136kp/id4P3q9qSr.svg",
              "background": "transparent",
              "format": "svg",
              "size": 1629
            },
            {
              "src": "https://asset.brandfetch.io/idpKX136kp/iddsfgq1tB.png",
              "background": "transparent",
              "format": "png",
              "height": 800,
              "width": 800,
              "size": 30149
            }
          ]
        },
        {
          "type": "icon",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idpKX136kp/idQBiBTxsm.jpeg",
              "background": null,
              "format": "jpeg",
              "height": 400,
              "width": 400,
              "size": 11801
            }
          ]
        }
      ],
      "colors": [
        {
          "hex": "#f0f2f5",
          "type": "dark",
          "brightness": 242
        },
        {
          "hex": "#ffffff",
          "type": "light",
          "brightness": 255
        },
        {
          "hex": "#1877f2",
          "type": "accent",
          "brightness": 108
        }
      ],
      "fonts": [
        {
          "name": "Segoe UI",
          "type": "title",
          "origin": "custom",
          "originId": null,
          "weights": [
          ]
        },
        {
          "name": "Segoe UI",
          "type": "body",
          "origin": "custom",
          "originId": null,
          "weights": [
          ]
        }
      ],
      "images": [
        {
          "type": "banner",
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idpKX136kp/id5kHV9FuC.png",
              "background": null,
              "format": "png",
              "height": 504,
              "width": 1920,
              "size": 1392697
            }
          ]
        }
      ]
    }
  JSON

  YOUTUBE = <<~JSON
    {
      "name": "YouTube",
      "domain": "youtube.com",
      "claimed": false,
      "description": "YouTube, a video-sharing platform, allows users to upload, view, and share videos, including movie and music clips and amateur content.",
      "links": [
        {
          "name": "youtube",
          "url": "https://youtube.com/organization/youtube"
        },
        {
          "name": "twitter",
          "url": "https://twitter.com/YouTube"
        }
      ],
      "logos": [
        {
          "type": "logo",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idVfYwcuQz/idjJfigk8Y.svg",
              "background": "transparent",
              "format": "svg",
              "size": 6552
            },
            {
              "src": "https://asset.brandfetch.io/idVfYwcuQz/idDmdVQ6Ym.png",
              "background": "transparent",
              "format": "png",
              "height": 178,
              "width": 800,
              "size": 15889
            }
          ]
        },
        {
          "type": "symbol",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idVfYwcuQz/id5E8NIYoL.svg",
              "background": "transparent",
              "format": "svg",
              "size": 1934
            },
            {
              "src": "https://asset.brandfetch.io/idVfYwcuQz/id8hkaZwOR.png",
              "background": "transparent",
              "format": "png",
              "height": 562,
              "width": 800,
              "size": 22086
            }
          ]
        },
        {
          "type": "icon",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idVfYwcuQz/idmfkl4meA.jpeg",
              "background": null,
              "format": "jpeg",
              "height": 400,
              "width": 400,
              "size": 6987
            }
          ]
        }
      ],
      "colors": [
        {
          "hex": "#212121",
          "type": "dark",
          "brightness": 33
        },
        {
          "hex": "#ffffff",
          "type": "light",
          "brightness": 255
        },
        {
          "hex": "#ff0000",
          "type": "accent",
          "brightness": 54
        }
      ],
      "fonts": [
        {
          "name": "Roboto",
          "type": "title",
          "origin": "google",
          "originId": "Roboto",
          "weights": [

          ]
        },
        {
          "name": "Roboto",
          "type": "body",
          "origin": "google",
          "originId": "Roboto",
          "weights": [

          ]
        }
      ],
      "images": [
        {
          "type": "banner",
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idVfYwcuQz/idUDrr1dt9.png",
              "background": null,
              "format": "png",
              "height": 1000,
              "width": 1500,
              "size": 1179298
            }
          ]
        }
      ]
    }
  JSON

  YOUTU_BE = <<~JSON.squish
    {
      "name": null,
      "domain": "youtu.be",
      "claimed": false,
      "description": "Enjoy the videos and music you love, upload original content, and share it all with friends, family, and the world on YouTube.",
      "links": [
        {
          "name": "youtube",
          "url": "https://youtube.com/yt/about"
        }
      ],
      "logos": [
        {
          "type": "logo",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idI4V71PLN/idEFNbvMTV.png",
              "background": "transparent",
              "format": "png",
              "height": 246,
              "width": 820,
              "size": 15823
            },
            {
              "src": "https://asset.brandfetch.io/idI4V71PLN/idBfenk8DP.svg",
              "background": "transparent",
              "format": "svg",
              "height": null,
              "width": null,
              "size": 3984
            }
          ]
        }
      ],
      "colors": [
        {
          "hex": "#282828",
          "type": "dark",
          "brightness": 40
        },
        {
          "hex": "#60dce4",
          "type": "light",
          "brightness": 194
        },
        {
          "hex": "#ff0000",
          "type": "accent",
          "brightness": 54
        }
      ],
      "fonts": [
        {
          "name": "var(--paper-font-title_-_font-family)",
          "type": "title",
          "origin": "custom",
          "originId": null,
          "weights": [

          ]
        }
      ],
      "images": [
      ]
    }
  JSON

  DROPBOX = <<~JSON
    {
      "name": "Dropbox",
      "domain": "dropbox.com",
      "claimed": false,
      "description": "Dropbox provides secure file sharing, collaboration, and storage solutions.",
      "links": [
        {
          "name": "crunchbase",
          "url": "https://crunchbase.com/organization/dropbox"
        },
        {
          "name": "twitter",
          "url": "https://twitter.com/Dropbox"
        },
        {
          "name": "instagram",
          "url": "https://instagram.com/dropbox"
        },
        {
          "name": "linkedin",
          "url": "https://linkedin.com/company/dropbox"
        },
        {
          "name": "facebook",
          "url": "https://facebook.com/Dropbox"
        }
      ],
      "logos": [
        {
          "type": "logo",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg",
              "background": "transparent",
              "format": "svg",
              "size": 2128
            },
            {
              "src": "https://asset.brandfetch.io/idY3kwH_Nx/idIhRD4Bny.png",
              "background": "transparent",
              "format": "png",
              "height": 158,
              "width": 800,
              "size": 13565
            }
          ]
        },
        {
          "type": "symbol",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg",
              "background": "transparent",
              "format": "svg",
              "size": 739
            },
            {
              "src": "https://asset.brandfetch.io/idY3kwH_Nx/iddoB4DghY.png",
              "background": "transparent",
              "format": "png",
              "height": 533,
              "width": 800,
              "size": 11278
            }
          ]
        },
        {
          "type": "icon",
          "theme": null,
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idY3kwH_Nx/idVhCcj8Ev.jpeg",
              "background": null,
              "format": "jpeg",
              "height": 400,
              "width": 400,
              "size": 13939
            }
          ]
        }
      ],
      "colors": [
        {
          "hex": "#0d2f81",
          "type": "dark",
          "brightness": 46
        },
        {
          "hex": "#ffffff",
          "type": "light",
          "brightness": 255
        },
        {
          "hex": "#0061ff",
          "type": "accent",
          "brightness": 88
        }
      ],
      "fonts": [
        {
          "name": "SharpGroteskWide",
          "type": "title",
          "origin": "custom",
          "originId": null,
          "weights": [

          ]
        },
        {
          "name": "SharpGrotesk",
          "type": "body",
          "origin": "custom",
          "originId": null,
          "weights": [

          ]
        }
      ],
      "images": [
        {
          "type": "banner",
          "formats": [
            {
              "src": "https://asset.brandfetch.io/idY3kwH_Nx/idZ4udU76k.png",
              "background": null,
              "format": "png",
              "height": 500,
              "width": 1500,
              "size": 255577
            }
          ]
        }
      ]
    }
  JSON

  TEST_COM = <<~JSON.squish
    {
      "name": "Test",
      "domain": "test.com",
      "claimed": false,
      "description": "Test is a software using multiple data centers, all of which support SSL.",
      "links": [

      ],
      "logos": [

      ],
      "colors": [

      ],
      "fonts": [

      ],
      "images": [

      ]
    }
  JSON

  ERROR_403 = <<~JSON.squish
    {"message": "User is not authorized to access this resource with an explicit deny"}
  JSON

  NOT_FOUND = <<~JSON.squish
    {"name":null,"domain":"does-not-exist.com","claimed":false,"description":null,"links":[],"logos":[],"colors":[{"hex":"#1a73e9","type":"accent","brightness":105},{"hex":"#34a853","type":"dark","brightness":137},{"hex":"#ea4235","type":"light","brightness":101}],"fonts":[{"name":"Roboto","type":"body","origin":"google","originId":"Roboto","weights":[]}],"images":[]}
  JSON
end
