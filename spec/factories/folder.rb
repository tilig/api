FactoryBot.define do
  factory :folder do
    name { "Family account" }
    public_key { Base64.urlsafe_encode64 SecureRandom.bytes(40), padding: false }
    creator factory: :user
  end
end
