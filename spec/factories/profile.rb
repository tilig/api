FactoryBot.define do
  factory :profile, class: Profile do
    meta { {} }
    application_settings { {} }
    user_settings { {} }
    user { build :user }
  end
end
