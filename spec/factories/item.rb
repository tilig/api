def random_base64(bytes, padding: false)
  Base64.urlsafe_encode64 SecureRandom.bytes(bytes), padding:
end

FactoryBot.define do
  factory :item do
    user
    website { "dropbox.com" }
    encryption_version { 2 }
    template { "login/1" }

    encrypted_dek { random_base64(64) }
    rsa_encrypted_dek { random_base64(64, padding: true) }
    encrypted_overview { random_base64(64) }
    encrypted_details { random_base64(64) }

    trait :empty do
      website { nil }
      username { nil }
      notes { nil }
      password { nil }
      otp { nil }
    end

    trait :folder_item do
      user { nil }
      folder
      encryption_version { 2 }
      template { "login/1" }

      encrypted_dek { nil }
      rsa_encrypted_dek { nil }
      encrypted_folder_dek { random_base64(64) }
      encrypted_overview { random_base64(64) }
      encrypted_details { random_base64(64) }
    end

    trait :legacy do
      template { nil }
      encryption_version { 1 }
      encrypted_dek { nil }
      encrypted_overview { nil }
      encrypted_details { nil }

      name { "Dropbox" }
      sequence :username do |n|
        "secret-#{n}@tilig.com"
      end
      notes { "Notes" }
      password { SecureRandom.base64(64) }
      otp { SecureRandom.base64(64) }
    end

    factory :google_play_item do
      user
      website { "play.google.com" }
      name { "Google Play" }
      sequence :username do |n|
        "secret-#{n}@tilig.com"
      end
      notes { "Notes" }
      password { SecureRandom.base64(64) }
      otp { SecureRandom.base64(64) }

      trait :empty do
        website { nil }
        username { nil }
        notes { nil }
        password { nil }
        otp { nil }
      end
    end
  end
end
