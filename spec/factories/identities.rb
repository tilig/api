FactoryBot.define do
  factory :identity do
    user
    refresh_token { Auth::RefreshTokenGenerator.generate(user) }
    access_token { Auth::AccessTokenGenerator.generate(user) }
  end
end

# == Schema Information
#
# Table name: identities
#
#  id                     :integer          not null, primary key
#  access_token           :string(512)
#  previous_access_token  :string
#  previous_refresh_token :string
#  refresh_token          :string(512)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  user_id                :uuid
#
# Indexes
#
#  index_identities_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
