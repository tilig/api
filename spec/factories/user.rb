FactoryBot.define do
  factory :user, class: User do
    sequence :email do |n|
      "user-#{n}@tilig.com"
    end
    family_name { "Doe" }
    given_name { "John" }
    organization { nil }
    trait :with_keypair do
      keypair factory: :keypair
    end

    trait :with_organization do
      organization factory: :organization
    end
  end
end
