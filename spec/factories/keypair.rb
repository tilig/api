FactoryBot.define do
  factory :keypair do
    user
    kid { Base64.urlsafe_encode64(SecureRandom.hex(8), padding: false) }
    encrypted_private_key { Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false) }
    public_key { Base64.urlsafe_encode64(SecureRandom.hex(10), padding: false) }
    key_type { "x25519" }
    meta_data { {"app_version" => "version", "app_platform" => "platform"} }
  end
end
