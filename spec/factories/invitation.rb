FactoryBot.define do
  factory :invitation do
    creator factory: :user
    invitee { nil }
    organization factory: :organization
    accepted_at { nil }
    sequence :email do |n|
      "user-#{n}@tilig.com"
    end

    trait :accepted do
      invitee factory: :user
      accepted_at { Time.zone.now }
    end
  end
end
