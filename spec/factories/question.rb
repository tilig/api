FactoryBot.define do
  factory :question do
    content { "On what devices do you want to have your password and account information available?" }
    survey_token { "e2120eb0-428b-471d-ba4a-9b109141acb6" }
    token { "b291972e-caa6-4f8c-ad80-5ef91b5775b2" }
    answer_options { ["Option A", "Option b", "Option C"] }
  end
end
