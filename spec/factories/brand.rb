FactoryBot.define do
  factory :brand do
    name { "Google play" }
    domain { "play.google.com" }
    public_suffix_domain { "google.com" }
    totp { true }
    json { nil }
    main_color_hex { "#4285F4" }
    main_color_brightness { 127 }
    logo_source { "https://asset.brandfetch.io/id6O2oGzv-/idP6g0Ep4y.svg" }
    logo_icon_source { "https://asset.brandfetch.io/id6O2oGzv-/idNEgS9h8q.jpeg" }
    images {
      {
        icon: {
          png: {
            original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/id-DuOtor_.png", format: "png")
          },
          svg: {
            original: Imgproxy.url_for("https://asset.brandfetch.io/id6O2oGzv-/idvNIQR3p7.svg", format: "svg")
          },
          theme: "dark"
        }
      }
    }
    rank { rand(10000) }
    brandfetch_json { nil }
    fetch_date { false }

    factory :dropbox_brand do
      name { "Dropbox" }
      domain { "dropbox.com" }
      public_suffix_domain { "dropbox.com" }
      logo_source { "https://asset.brandfetch.io/idY3kwH_Nx/idpEif7tzn.svg" }
      logo_icon_source { "https://asset.brandfetch.io/idY3kwH_Nx/idL_rLHtXo.svg" }
      main_color_brightness { "88" }
      main_color_hex { "#0061ff" }
    end
  end
end
