FactoryBot.define do
  factory :folder_membership do
    user
    folder
    encrypted_private_key { Base64.urlsafe_encode64 SecureRandom.bytes(40), padding: false }
    role { "admin" }
    creator factory: :user

    trait :accepted do
      accepted_at { Time.zone.now }
    end
  end
end
