FactoryBot.define do
  factory :answer do
    chosen_options { ["Option A", "Option C"] }
    question { build(:question) }
  end
end
