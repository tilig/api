FactoryBot.define do
  factory :organization do
    name { Faker::Company.name }
    creator factory: :user
  end
end
