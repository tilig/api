FactoryBot.define do
  factory :share_link do
    item
    uuid { SecureRandom.hex(16) }
    access_token { SecureRandom.hex(16) }
    encrypted_dek { Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false) }
    encrypted_master_key { Base64.urlsafe_encode64(SecureRandom.hex(32), padding: false) }
  end
end
