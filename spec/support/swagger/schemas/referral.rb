{
  referral_params: {
    type: :object,
    properties: {
      referral: {
        type: :object,
        properties: {
          referral_link: {
            type: :string,
            nullable: false,
            example: "squirrel-8b36dc01d7",
            description: "String that needs to be submitted to identify the referral"
          }
        }
      }
    }
  }
}
