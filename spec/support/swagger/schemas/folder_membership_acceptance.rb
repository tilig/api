{
  acceptance_params: {
    type: :object,
    properties: {
      acceptance_token: {type: :string, nullable: false, example: "aEQgWTVmoQKqMbRcH97wu9wN", description: "The token that is required to accept the folder membership invite."}
    }
  }
}
