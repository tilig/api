{
  brand: {
    type: :object,
    properties: {
      id: {type: :string, format: :uuid, nullable: false, example: "2e64957e-954e-4385-82b5-5c21926cba48"},
      totp: {type: :boolean, nullable: false, example: true, description: "The totp value of the Brand"},
      main_color_hex: {type: :string, nullable: true, example: "#4285F4", description: "The color hex of the Brand"},
      main_color_brightness: {type: :string, nullable: true, example: "127", description: "The color brightness of the Brand"},
      images: {
        type: :object,
        properties: {
          icon: {
            type: :object,
            properties: {
              png: {
                type: :object,
                properties: {
                  original: {type: :string, nullable: true, example: "https://img.tilig.com/C6RvwxaBbTLYfgCLcm2-FLEmSB6f8tpSZd_NzeHG8ME/aHR0cHM6Ly9hc3Nl/dC5icmFuZGZldGNo/LmlvL2lkNk8yb0d6/di0vaWQtRHVPdG9y/Xy5wbmc.png", description: "The png url of the Brand"}
                }
              },
              svg: {
                type: :object,
                properties: {
                  original: {type: :string, nullable: true, example: "https://img.tilig.com/ytvfCgMTjnvRMFHKxAi8qpELPGiydBA3svabZ9WYUEU/aHR0cHM6Ly9hc3Nl/dC5icmFuZGZldGNo/LmlvL2lkNk8yb0d6/di0vaWR2TklRUjNw/Ny5zdmc.svg", description: "The png url of the Brand"}
                }
              }
            }
          }
        }
      },
      logo_source: {type: :string, nullable: true, example: "https://asset.brandfetch.io/id6O2oGzv-/idP6g0Ep4y.svg", description: "The logo_source url of the Brand"},
      logo_icon_source: {type: :string, nullable: true, example: "https://asset.brandfetch.io/id6O2oGzv-/idP6g0Ep4y.svg", description: "The logo_source url of the Brand"},
      domain: {type: :string, nullable: false, example: "play.google.com", description: "The domain of the Brand"},
      public_suffix_domain: {type: :string, nullable: false, example: "google.com", description: "The public suffix domain of the Brand"},
      name: {type: :string, nullable: false, example: "Google play", description: "The name of the Brand"},
      is_fetched: {type: :boolean, nullable: false, example: true, description: "Indicates if the Brand date is fetched"},
      breached_at: {type: :datetime, nullable: true, example: "2023-03-16T00:00:00.000Z", description: "When the last breach for this brand occurred. Is null when we haven't found breach data for this domain"},
      breach_published_at: {type: :datetime, nullable: true, example: "2023-03-16T14:15:49.880Z", description: "The date that the breach information was published on have I been pwned. Is null when there is no information available for this brand."}

    }
  },

  brands: {
    type: :array,
    items: {
      "$ref": "#/components/schemas/brand"
    }
  }
}
