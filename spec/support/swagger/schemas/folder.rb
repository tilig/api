{
  folder_params: {
    type: :object,
    properties: {
      name: {type: :string, nullable: true, example: "Marketing", description: "The name of the folder"},
      public_key: {type: :string, nullable: false, example: "Marketing", description: "The name of the folder"}
    }
  },

  folder_creation_params: {
    type: :object,
    properties: {
      name: {type: :object, nullable: true, example: "Marketing", description: "The name of the folder"},
      public_key: {type: :string, nullable: false, example: "Marketing", description: "The name of the folder"},
      folder_memberships: {type: :array, items: {"$ref": "#/components/schemas/folder_membership_params"}},
      items: {type: :array, items: {"$ref": "#/components/schemas/folder_item_params"}}
    }
  },

  folder: {
    type: :object,
    properties: {
      id: {type: :string, format: :uuid, nullable: false, example: "e2120eb0-428b-471d-ba4a-9b109141acb6"},
      name: {type: :string, nullable: false, example: "Marketing", description: "The name of the folder"},
      public_key: {type: :string, nullable: false, example: "918vdagjdf9a89fa", description: "The public key of the folder"},
      my_encrypted_private_key: {type: :string, nullable: true, example: "918vdagjdf9a89fa", description: "The public key of the folder"},
      created_at: {type: :string, format: :date_time, example: "2020-01-01T00:00:00.000Z", description: "The datetime of when the folder was created"},
      updated_at: {type: :string, format: :date_time, example: "2020-01-01T00:00:00.000Z", description: "The datetime of when the folder was updated"},
      folder_memberships: {type: :array, items: {"$ref": "#/components/schemas/folder_membership"}}
    }
  },
  folders: {
    type: :array,
    items: {
      "$ref": "#/components/schemas/folders"
    }
  }
}
