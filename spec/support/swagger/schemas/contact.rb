{
  contact: {
    type: :object,
    properties: {
      id: {type: :string, format: :uuid, nullable: false, example: "21a5f97f-5bea-42be-a8ac-3cb5026d18b1"},
      email: {type: :string, nullable: false, example: "johndoe@tilig.com", description: "The email adress of the user"},
      public_key: {type: :string, nullable: false, example: "OGJjOWVkNTVkMzE1YWEzZjFhNzA5ODgzZDdjMDNkNjM4YmM1MWQ0ZWZjODNjYWIxOWJhNzkxZjI4MjJiZWRiNjNhNjM3ODZiOTMxMTQ4M2Q", description: "The public_key of the user"},
      display_name: {type: :string, nullable: false, example: "John", description: "The display_name of the user"},
      picture: {type: :string, example: "https://tilig.com/profile-picture.png", nullable: true, description: "The picture of the user"}
    }
  },
  contacts: {
    type: :object,
    properties: {
      contacts: {
        type: :array,
        items: {
          "$ref": "#/components/schemas/contact"
        }
      }
    }
  }
}
