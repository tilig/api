{
  secret: {
    type: :object,
    properties: {
      id: {type: :string, format: :uuid, nullable: false, example: "8e6eec27-8996-42f6-af88-cff689357fcb"},
      template: {type: :string, nullable: false, example: "login/1", description: "The template of this account"},
      website: {type: :string, nullable: true, example: "facebook.com", description: "The website for this account"},
      domain: {type: :string, nullable: true, example: "facebook.com", description: "The domain for this account"},

      name: {type: :string, nullable: true, example: "Facebook", description: "The name of the secret"},
      notes: {type: :string, nullable: true, example: "This account is only used to work on some facebook pages"},
      username: {type: :string, nullable: true, example: "xQcw", description: "The username the user has chosen"},
      password: {type: :string, format: :password, nullable: true, example: "tH15_15_53Cr37", description: "The encrypted password"},
      otp: {type: :string, nullable: true, example: "tH15_15_53Cr37", description: "This is an encrypted string with the url to generate the one time code for 2fa"},
      android_app_id: {type: "string", format: :uri, nullable: true, example: "com.facebook.blah", description: "The unique identifier for android apps by which we can use autofil for the right apps."},

      encrypted_dek: {type: "string", nullable: false, example: "0oiPlbgDCb6UexzmN3-O0J-w-3cyi06FpE47aoU_kDw", description: "The data encryption key"},
      encrypted_details: {type: "string", nullable: false, example: "Bw7ywCDQAj9u893k6wnpzD2us6nbaZNy8uLG1bL1K5w", description: "The encrypted details for sensitive properties"},
      encrypted_overview: {type: "string", nullable: false, example: "PY7hg3wvgAvVIpiMN1BqmK4sGntTDtOm7f_ed-RbjM0", description: "The encrypted overview for low sensitivity properties"},
      encryption_version: {type: :integer, nullable: false, example: 2, description: "Version of the used encryption (this must be at least 2)"},
      encrypted_key: {type: "string", nullable: true, example: "L3eJCsU-YxR0u2ihds9_lzOA4dU1osFlR3fa5q4Xrl0", description: "The data encryption key"},
      updated_at: {type: :string, format: :date_time, example: "2020-01-01T00:00:00.000Z", description: "The datetime of when the secret was updated"},
      created_at: {type: :string, format: :date_time, example: "2020-01-01T00:00:00.000Z", description: "The datetime of when the secret was created"},
      brand: {
        type: :object,
        "$ref": "#/components/schemas/brand"
      },
      share: {
        type: :object,
        nullable: true,
        "$ref": "#/components/schemas/share"
      },
      legacy_encryption_disabled: {type: :boolean, nullable: false, example: false, description: "Value to indicate if the legacy encryption is disabled"}
    }
  },
  secret_params: {
    type: :object,
    properties: {
      name: {type: :string, nullable: true, example: "Facebook", description: "The name of the secret"},
      website: {type: :string, nullable: true, example: "facebook.com", description: "The website for this account"},
      notes: {type: :string, nullable: true, example: "This account is only used to work on some facebook pages"},
      username: {type: :string, nullable: true, example: "xQcw", description: "The username the user has chosen"},
      password: {type: :string, format: :password, nullable: true, example: "tH15_15_53Cr37", description: "The encrypted password"},
      otp: {type: :string, nullable: true, example: "tH15_15_53Cr37", description: "This is an encrypted string with the url to generate the one time code for 2fa"},
      android_app_id: {type: "string", format: :uri, nullable: true, example: "com.facebook.blah", description: "The unique identifier for android apps by which we can use autofil for the right apps."},
      encrypted_dek: {type: "string", nullable: true, example: "a3eed0237b624ebea24eeab0dc13989a", description: "The data encryption key"},
      encrypted_details: {type: "string", nullable: true, example: "f2ad32403c62405fa4e438143a799d4d", description: "The encrypted details for sensitive properties"},
      encrypted_overview: {type: "string", nullable: true, example: "1fd0d53e48ce4b08a3afde49691e865d", description: "The encrypted overview for low sensitivity properties"},
      encryption_version: {type: :integer, nullable: true, example: 2, description: "Version of the used encryption"}
    }
  },
  secrets: {
    type: :array,
    items: {
      "$ref": "#/components/schemas/secret"
    }
  },

  secret_encryption_v2: {
    type: :object,
    properties: {
      id: {type: :string, format: :uuid, nullable: false, example: "8e6eec27-8996-42f6-af88-cff689357fcb"},
      template: {type: :string, nullable: false, example: "login/1", description: "The template of this account"},
      encrypted_dek: {type: "string", nullable: false, example: "0oiPlbgDCb6UexzmN3-O0J-w-3cyi06FpE47aoU_kDw", description: "The data encryption key"},
      encrypted_details: {type: "string", nullable: false, example: "Bw7ywCDQAj9u893k6wnpzD2us6nbaZNy8uLG1bL1K5w", description: "The encrypted details for sensitive properties"},
      encrypted_overview: {type: "string", nullable: false, example: "PY7hg3wvgAvVIpiMN1BqmK4sGntTDtOm7f_ed-RbjM0", description: "The encrypted overview for low sensitivity properties"},
      encryption_version: {type: :integer, nullable: false, example: 2, description: "Version of the used encryption (this must be at least 2)"},
      website: {type: :string, nullable: true, example: "facebook.com", description: "The website for this account"},
      encrypted_key: {type: "string", nullable: true, example: "L3eJCsU-YxR0u2ihds9_lzOA4dU1osFlR3fa5q4Xrl0", description: "The data encryption key"},
      updated_at: {type: :string, format: :date_time, example: "2020-01-01T00:00:00.000Z", description: "The datetime of when the secret was updated"},
      created_at: {type: :string, format: :date_time, example: "2020-01-01T00:00:00.000Z", description: "The datetime of when the secret was created"},
      domain: {type: :string, nullable: true, example: "facebook.com", description: "The domain for this account"},
      brand: {
        type: :object,
        "$ref": "#/components/schemas/brand"
      },
      share: {
        type: :object,
        nullable: true,
        "$ref": "#/components/schemas/share"
      },
      legacy_encryption_disabled: {type: :boolean, nullable: false, example: true, description: "Value to indicate if the legacy encryption is disabled"}
    }
  },
  secret_params_encryption_v2: {
    type: :object,
    properties: {
      template: {type: :string, nullable: false, example: "login/1", description: "The template of this account"},
      encrypted_dek: {type: "string", nullable: false, example: "0oiPlbgDCb6UexzmN3-O0J-w-3cyi06FpE47aoU_kDw", description: "The data encryption key"},
      encrypted_details: {type: "string", nullable: false, example: "Bw7ywCDQAj9u893k6wnpzD2us6nbaZNy8uLG1bL1K5w", description: "The encrypted details for sensitive properties"},
      encrypted_overview: {type: "string", nullable: false, example: "PY7hg3wvgAvVIpiMN1BqmK4sGntTDtOm7f_ed-RbjM0", description: "The encrypted overview for low sensitivity properties"},
      encryption_version: {type: :integer, nullable: false, example: 2, description: "Version of the used encryption (this must be at least 2)"},
      website: {type: :string, nullable: true, example: "facebook.com", description: "The website for this account"},
      encrypted_key: {type: "string", nullable: true, example: "L3eJCsU-YxR0u2ihds9_lzOA4dU1osFlR3fa5q4Xrl0", description: "The data encryption key"}
    }
  },
  secrets_encryption_v2: {
    type: :array,
    items: {
      "$ref": "#/components/schemas/secret_encryption_v2"
    }
  }
}
