{
  new_answer: {
    type: :object,
    properties: {
      answer: {
        type: :object, nullable: false, properties: {
          skipped: {
            type: :boolean,
            example: false,
            description: "Can be given to notify that the user has skipped the question"
          },
          chosen_options: {
            type: :array, nullable: false, items: {
                                             type: :string
                                           },
            example: ["Option A", "Option B"]
          },
          question_attributes: {
            type: :object, nullable: false, properties: {
              content: {
                type: :string, nullable: false, example: "On what devices do you want to have your password and account information available?"
              },
              survey_token: {
                type: :string, nullable: false, example: "e2120eb0-428b-471d-ba4a-9b109141acb6", description: "This allows the client to bundle questions into 1 susrvey. And with that also change surveys"
              },
              token: {
                type: :string, nullable: false, example: "46e4d763-ad10-4e74-9270-c7769f548082", description: "This is the unique id for each question."
              },
              answer_options: {
                type: :array, nullable: false, items: {
                                                 type: :string
                                               },
                example: ["Option A", "Option B", "Option C"]
              }
            }
          }
        }
      }
    }
  }
}
