{
  public_key: {
    type: :object,
    properties: {
      user: {
        type: :object,
        properties: {
          public_key: {type: :string, format: :public_key},
          key_type: {type: :string, example: "x25519"},
          kid: {type: :string},
          id: {type: :string, format: :uuid, description: "The id of the user", example: "0a048468-fa74-4f20-a72f-7a097d55383"},
          email: {type: :string, format: :email, description: "The email of the user", example: "john@example.com"}
        }

      }
    }
  }
}
