{
  user_settings_params: {
    type: :object,
    required: [:user_settings],
    properties: {
      user_settings: {
        type: :object,
        nullable: false,
        example: {onboarding: {finished: true}},
        description: "The settings/preferences for a user, this is saved as a json blob without validation"
      }
    }
  }
}
