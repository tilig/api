{
  item: {
    type: :object,
    properties: {
      id: {type: :string, format: :uuid, nullable: false, example: "8e6eec27-8996-42f6-af88-cff689357fcb"},
      template: {type: :string, nullable: false, example: "login/1", description: "The template of this account"},
      website: {type: :string, nullable: true, example: "facebook.com", description: "The website for this account"},
      domain: {type: :string, nullable: true, example: "facebook.com", description: "The domain for this account"},

      legacy_encryption_disabled: {type: :boolean, nullable: false, example: false, description: "Value to indicate if the legacy encryption is disabled"},
      android_app_id: {type: "string", format: :uri, nullable: true, example: "com.facebook.blah", description: "The unique identifier for android apps by which we can use autofil for the right apps."},
      name: {type: :string, nullable: true, example: "Facebook", description: "The name of the secret"},
      notes: {type: :string, nullable: true, example: "This account is only used to work on some facebook pages"},
      otp: {type: :string, nullable: true, example: "tH15_15_53Cr37", description: "This is an encrypted string with the url to generate the one time code for 2fa"},
      username: {type: :string, nullable: true, example: "xQcw", description: "The username the user has chosen"},
      password: {type: :string, format: :password, nullable: true, example: "tH15_15_53Cr37", description: "The encrypted password"},

      encrypted_dek: {type: "string", nullable: true, example: "0oiPlbgDCb6UexzmN3-O0J-w-3cyi06FpE47aoU_kDw", description: "The data encryption key"},
      encrypted_folder_dek: {type: "string", nullable: true, example: "0oiPlbgDCb6UexzmN3-O0J-w-3cyi06FpE47aoU_kDw", description: "The data encryption key for folders"},
      rsa_encrypted_dek: {type: "string", nullable: true, example: "0oiPlbgDCb6UexzmN3-O0J-w-3cyi06FpE47aoU_kDw", description: "The data encryption key"},

      encrypted_details: {type: "string", nullable: false, example: "Bw7ywCDQAj9u893k6wnpzD2us6nbaZNy8uLG1bL1K5w", description: "The encrypted details for sensitive properties"},
      encrypted_overview: {type: "string", nullable: false, example: "PY7hg3wvgAvVIpiMN1BqmK4sGntTDtOm7f_ed-RbjM0", description: "The encrypted overview for low sensitivity properties"},
      encryption_version: {type: :integer, nullable: false, example: 2, description: "Version of the used encryption (this must be at least 2)"},

      updated_at: {type: :string, format: :date_time, example: "2020-01-01T00:00:00.000Z", description: "The datetime of when the secret was updated"},
      created_at: {type: :string, format: :date_time, example: "2020-01-01T00:00:00.000Z", description: "The datetime of when the secret was created"},

      folder: {
        type: :object,
        nullable: true,
        "$ref": "#/components/schemas/folder"
      },
      brand: {
        type: :object,
        "$ref": "#/components/schemas/brand"
      },
      share_link: {
        type: :object,
        nullable: true,
        "$ref": "#/components/schemas/share"
      }
    }
  },

  items: {
    type: :object,
    properties: {
      items: {
        type: :array,
        items: {
          "$ref": "#/components/schemas/item"
        }
      }
    }
  },

  item_params: {
    type: :object,
    properties: {
      website: {type: :string, nullable: true, example: "facebook.com", description: "The website for this account"},
      template: {type: :string, nullable: false, example: "login/1", description: "The template of this account"},

      encrypted_dek: {type: :string, nullable: true, example: "a3eed0237b624ebea24eeab0dc13989a", description: "The data encryption key"},
      rsa_encrypted_dek: {type: :string, nullable: true, example: "a3eed0237b624ebea24eeab0dc13989a", description: "The data encryption key"},

      encrypted_details: {type: :string, nullable: true, example: "f2ad32403c62405fa4e438143a799d4d", description: "The encrypted details for sensitive properties"},
      encrypted_overview: {type: :string, nullable: true, example: "1fd0d53e48ce4b08a3afde49691e865d", description: "The encrypted overview for low sensitivity properties"},
      encryption_version: {type: :integer, nullable: true, example: 2, description: "Version of the used encryption"},
      encrypted_folder_dek: {type: :string, nullable: true, example: "a3eed0237b624ebea24eeab0dc13989a", description: "The data encryption key for folders"},
      folder_id: {type: :string, format: :uuid, nullable: true, example: "8e6eec27-8996-42f6-af88-cff689357fcb", description: "The folder id, if it's set to null the item will belong to the current user"}

    }
  },

  folder_item_params: {
    type: :object,
    properties: {
      id: {type: :string, format: :uuid, nullable: false, example: "8e6eec27-8996-42f6-af88-cff689357fcb"},
      encrypted_folder_dek: {type: "string", nullable: true, example: "a3eed0237b624ebea24eeab0dc13989a", description: "The data encryption key"}
    }
  }
}
