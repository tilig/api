{
  folder_membership_params: {
    type: :object,
    properties: {
      user_id: {type: :string, format: :uuid, nullable: false, example: "21a5f97f-5bea-42be-a8ac-3cb5026d18b1", description: "The ID of the user"},
      encrypted_private_key: {type: :string, example: "MjQ0NjM2YTUzNzRhMzYyNTY0Mjk1MzY0ZmRkYzNlZjMxMzNiZmQ1N2I1ZDY", description: "The encrypted_private_key belonging to the group and encrypted using the public key of the user"}
    }
  },

  folder_membership: {
    type: :object,
    properties: {
      id: {type: :string, format: :uuid, nullable: false, example: "e2120eb0-428b-471d-ba4a-9b109141acb6"},
      role: {type: :string, nullable: false, example: "admin", description: "The role of the user in the group"},
      user: {
        type: :object,
        nullable: true,
        properties: {
          id: {type: :string, format: :uuid, nullable: false, example: "21a5f97f-5bea-42be-a8ac-3cb5026d18b1"},
          email: {type: :string, nullable: false, example: "johndoe@tilig.com", description: "The email adress of the user"},
          public_key: {type: :string, nullable: false, example: "OGJjOWVkNTVkMzE1YWEzZjFhNzA5ODgzZDdjMDNkNjM4YmM1MWQ0ZWZjODNjYWIxOWJhNzkxZjI4MjJiZWRiNjNhNjM3ODZiOTMxMTQ4M2Q", description: "The public_key of the user"},
          display_name: {type: :string, nullable: false, example: "John", description: "The display_name of the user"},
          picture: {type: :string, example: "https://tilig.com/profile-picture.png", nullable: true, description: "The picture of the user"}
        }
      },
      accepted: {type: :boolean, example: true, description: "Whether the membership is accepted"},
      pending: {type: :boolean, example: false, description: "Whether the membership is still pending"},
      accepted_at: {type: :string, format: :date_time, example: "2022-01-01T13:37:00.000Z", description: "The datetime of when the membership was accepted", nullable: true},
      created_at: {type: :string, format: :date_time, example: "2020-01-01T00:00:00.000Z", description: "The datetime of when the membership was created"},
      updated_at: {type: :string, format: :date_time, example: "2020-01-01T00:00:00.000Z", description: "The datetime of when the membership was updated"}
    }
  },
  folder_memberships: {
    type: :array,
    items: {
      "$ref": "#/components/schemas/folder_membership"
    }
  }
}
