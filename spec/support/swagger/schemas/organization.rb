{
  organization_params: {
    type: :object,
    properties: {
      organization: {
        type: :object,
        properties: {
          name: {type: :string, example: "Tilig.com", description: "The name of the organization", nullable: false}
        }
      }
    }
  },
  organization: {
    type: :object,
    properties: {
      id: {type: :uuid, format: :uuid, example: "e277265e-13db-485a-a4d3-3260aab0a473", description: "The id of the organization", nullable: false},
      name: {type: :string, example: "Tilig.com", description: "The name of the organization", nullable: false},
      created_at: {type: :string, example: "2019-01-01T00:00:00.000Z", description: "The date the organization was created", nullable: false},
      updated_at: {type: :string, example: "2019-01-01T00:00:00.000Z", description: "The date the organization was updated", nullable: false},
      users: {
        type: :array, items: {
          type: :object, properties: {
            id: {type: :uuid, format: :uuid, example: "ad04d53c-40a4-4707-9218-41532b3c4f52", description: "The id of the user", nullable: false},
            email: {type: :string, example: "john@example.com", description: "The email of the user", nullable: false},
            display_name: {type: :string, example: "John", description: "The display name of the user", nullable: false},
            picture_url: {type: :string, example: "https://example.com/picture.png", description: "The picture url of the user", nullable: false}
          }
        }
      }
    }
  },

  organization_invitations_params: {
    type: :object,
    properties: {
      emails: {type: :array, example: ["john.doe@tilig.com", "jane.doe@tilig.com"], description: "The email adressens of the users that need to be invited.", nullable: false}
    }
  },
  organization_invitation: {
    type: :object,
    properties: {
      id: {type: :uuid, format: :uuid, example: "e44c6dff-0c83-430b-9bef-df12bcd6b93f", description: "The id of the invitation for a organization", nullable: false},
      email: {type: :string, nullable: false, example: "johndoe@tilig.com", description: "The email adress of the invited user"},
      expire_at: {type: :string, format: :date_time, example: "2022-01-01T13:37:00.000Z", description: "The datetime when the invitation will expire", nullable: false},
      accepted_at: {type: :string, format: :date_time, example: "2022-01-01T13:37:00.000Z", description: "The datetime when the invitation for a organization was accepted", nullable: true},
      created_at: {type: :string, example: "2019-01-01T00:00:00.000Z", description: "The date the organization was created", nullable: false},
      updated_at: {type: :string, example: "2019-01-01T00:00:00.000Z", description: "The date the organization was updated", nullable: false},
      creator: {
        type: :object,
        properties: {
          id: {type: :uuid, format: :uuid, example: "47988c89-3306-4a20-86f6-ce175338f525", description: "The id of the creator", nullable: false},
          email: {type: :string, example: "john@example.com", description: "The email of the user", nullable: false},
          display_name: {type: :string, example: "John", description: "The display name of the user", nullable: false},
          picture_url: {type: :string, example: "https://example.com/picture.png", description: "The picture url of the user", nullable: false}
        }
      },
      invitee: {
        type: :object,
        nullable: true,
        properties: {
          id: {type: :uuid, format: :uuid, example: "6344a1c0-922e-4cd0-9ed7-9f4bc8497f4c", description: "The id of the invitee", nullable: false},
          email: {type: :string, example: "john@example.com", description: "The email of the user", nullable: false},
          display_name: {type: :string, example: "John", description: "The display name of the user", nullable: false},
          picture_url: {type: :string, example: "https://example.com/picture.png", description: "The picture url of the user", nullable: false}
        }
      }
    }
  },
  organization_invitations: {
    type: :object,
    properties: {
      invitations: {
        type: :array,
        items: {
          "$ref": "#/components/schemas/organization_invitation"
        }
      }
    }
  },
  organization_invitation_show: {
    type: :object,
    properties: {
      id: {type: :uuid, format: :uuid, example: "e44c6dff-0c83-430b-9bef-df12bcd6b93f", description: "The id of the invitation for a organization", nullable: false},
      created_at: {type: :string, example: "2019-01-01T00:00:00.000Z", description: "The date the organization was created", nullable: false},
      updated_at: {type: :string, example: "2019-01-01T00:00:00.000Z", description: "The date the organization was updated", nullable: false},
      creator: {
        type: :object,
        properties: {
          id: {type: :uuid, format: :uuid, example: "47988c89-3306-4a20-86f6-ce175338f525", description: "The id of the creator", nullable: false},
          display_name: {type: :string, example: "John", description: "The display name of the user", nullable: false},
          picture_url: {type: :string, example: "https://example.com/picture.png", description: "The picture url of the user", nullable: false}
        }
      },
      organization: {
        type: :object,
        properties: {
          id: {type: :uuid, format: :uuid, example: "e277265e-13db-485a-a4d3-3260aab0a473", description: "The id of the Organization", nullable: false},
          name: {type: :string, nullable: false, example: "Chocolate Chip Inc.", description: "The name of the organization"}
        }
      }
    }
  }
}
