{
  share: {
    type: :object,
    properties: {
      id: {type: :string, format: :uuid, nullable: false, example: "a9a94c15-42ef-490d-899d-5c67e7332611"},
      times_viewed: {type: :integer, nullable: false, example: "Zu3DzrFjEYkw1KJvDsCIHkQcuc1Hf9h9fqkNrIRm9zE", description: "The count of how many thimes this share has been requested."},
      encrypted_master_key: {type: :text, nullable: false, example: "Zu3DzrFjEYkw1KJvDsCIHkQcuc1Hf9h9fqkNrIRm9zE", description: "The master key encrypted using the public key of the secret owner."}
    }
  },
  new_share: {
    type: :object,
    properties: {
      uuid: {type: :string, example: "cfdf368e870e2e7f78c77925714d25a2", description: "The derived uuid of the Share based on a master key"},
      access_token: {type: :string, example: "4e6b3ea5017ffb27180c8b44c496fc55", description: "The derived access_token of the Share based on a master key"},
      encrypted_dek: {type: :text, example: "SLYmF07RUDzEH6BOl7nSiS_3swWrq-1WrTXE8oOyjY-_hwvHl_hwJqDjcPZonITSIu2DiP0rMZXEEnhSLeMwlrNhxoI-ArT1c2dJAnBCbbt5hHH6Qz6EI0Y", description: "The encrypted encrypted_dek of the secret, encypted using a key derived from the master key"},
      encrypted_master_key: {type: :text, example: "Zu3DzrFjEYkw1KJvDsCIHkQcuc1Hf9h9fqkNrIRm9zE", description: "The master key encrypted using the public key of the secret owner."}
    }
  }
}
