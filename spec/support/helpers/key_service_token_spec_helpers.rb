module KeyServiceTokenSpecHelpers
  def generate_key_service_token(email: nil)
    private_key = '-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIKZ8vLJtU5EzqbMfhRJE1bPhdbIPYC7OBlSOoIjJquS7oAoGCCqGSM49
AwEHoUQDQgAEj3qy5L8FlEMjj0PIvVLbOTSCKR+rizHzVqMsvRKA/4O63XMEEwGd
uv5bl+/B1f1sTI4UyymYZ+jTpnrVgjs+qQ==
-----END EC PRIVATE KEY-----'

    payload = {
      email: email || Faker::Internet.email,
      exp: 30.seconds.from_now.to_i,
      iss: "https://app.tilig.com",
      iat: Time.current.to_i
    }

    JWT.encode(payload, OpenSSL::PKey::EC.new(private_key), "ES256")
  end
end
