module SpecHelpers
  def json_response
    @json_response ||= JSON.parse(response.body, symbolize_names: true)
  end

  def console_output
    page.driver.browser.manage.logs.get(:browser)
  end

  def generate_swagger_example(example)
    # Check is needed to not obstruct the tests outside of rswag tests
    set_example(example) if example.metadata[:path_item]
  end

  private

  def set_example(example)
    example.metadata[:response][:content] = {
      "application/json" => {
        # When unauthenticated the API doesn't return valid JSON
        # and returning json_response will raise an error saying so.
        example: json_response? ? json_response : response&.body
      }
    }
  end

  def json_response?
    return true if json_response
  rescue
    false
  end
end
