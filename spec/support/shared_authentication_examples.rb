RSpec.shared_examples "an authenticated endpoint" do
  describe "when no bearer token is present in the headers", bullet: :skip do
    it "returns unauthorizated" do
      reset!
      send(method, path, headers: nil)
      expect(response).to have_http_status(:unauthorized)
      expect(response.body).to match(/HTTP Token: Access denied./)
    end
  end

  describe "when an invalid bearer token is present", bullet: :skip do
    let(:headers) { {"Authorization" => "Bearer some-random-value"} }

    it "returns unauthorizated" do
      reset!
      send(method, path, headers: headers)
      expect(response).to have_http_status(:unauthorized)
      expect(response.body).to match(/HTTP Token: Access denied./)
    end
  end

  describe "when a bearer token belonging to a user is present", bullet: :skip do
    it "returns status ok" do
      reset!
      send(method, path, headers: headers, params: params, as: :json)
      expect_valid_response
    end
  end

  def expect_valid_response
    return expected_status_code if respond_to?(:expected_status_code)

    case method
    when :delete
      expect(response).to have_http_status(:no_content)
    when :post
      expect(response).to have_http_status(:created)
    else
      expect(response).to have_http_status(:ok)
    end
  end
end
