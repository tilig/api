# README

## Usefull commands

### Swagger generation

In order to generate the `swagger.json` API specs you need to run the following: `bin/swagger`

### Release

To initiate update `lib/modules/api.rb` with the new version number and run
`bin/release`

After running `bin/release`, all you have to do is, merge the releases branch and deploy
(Manually) to production.

## Changelog

When adding a merge request, please make sure to let the first commit have a
trailer with the following format:

`Changelog: <action>`

where `<action>` is one of the following:

- `added`: New feature
- `fixed`: Bug fix
- `changed`: Feature change
- `deprecated`: New deprecation
- `removed`: Feature removal
- `security`: Security fix
- `performance`: Performance improvement
- `other`: Other
