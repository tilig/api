#!/usr/bin/env bash

if [[ -z "$ACCESS_TOKEN_SECRET" ]]; then
	echo "ERR: No ACCESS_TOKEN_SECRET ENV var; Cannot generate JWT." 1>&2
	exit 1
fi

base64_urlencode() { openssl enc -base64 -A | tr '+/' '-_' | tr -d '='; }

header=$(printf %s "{ \"alg\": \"HS256\"}" | base64_urlencode)

payload_raw="{
    \"jti\": \"f736f779-d09a-47e9-afac-296c52de4e70\",
    \"uid\": \"c09aff96-4e51-44d7-870e-82be7b7a852e\",
    \"exp\": $(($(date +%s) + 86400)),
    \"iss\": \"https://app.tilig.com\",
    \"iat\": $(date +%s)
  }"

payload=$(printf %s "$payload_raw" | base64_urlencode)

signature=$(printf %s "${header}.${payload}" | openssl dgst -binary -sha256 -hmac "$ACCESS_TOKEN_SECRET" | base64_urlencode)

echo "{
  \"headers\" : {
    \"Authorization\" : \"Bearer ${header}.${payload}.${signature}\"
  }
}" >api-fuzzing-overrides.json

cat api-fuzzing-overrides.json
