source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.2.2"

# Rails
gem "rails", "~> 7.0.4"

gem "oj"
gem "multi_json"
gem "jbuilder", "~> 2.11.5"

# DB
gem "pg", "~> 1.4.3", "< 2.0"

# Rack server
gem "puma", "~> 6.2.0"
gem "rack-cors", "~> 2.0.0"
gem "rack-timeout", "~> 0.6.3"

# HTTP requests
gem "faraday", "~> 2.7.0"
gem "parallel", "~> 1.22.1"

gem "jwt", "~> 2.7.0"

# Marketing
gem "mixpanel-ruby", "~> 2.3.0"
gem "MailchimpMarketing", "~> 3.0.78"
gem "MailchimpTransactional"

# Domain parsing
gem "public_suffix", "~> 5.0.0"

# Workers
gem "sidekiq", "~> 7.0.0"
gem "sidekiq_alive", "~> 2.2.0"

# Error reporting
gem "sentry-ruby", "~> 5.8.0"
gem "sentry-rails", "~> 5.8.0"
gem "sentry-sidekiq", "~> 5.8.0"

# Production logging gem
gem "rails_semantic_logger", "~> 4.12.0"

# Hasing using libsodium
gem "rbnacl", "~> 7.1.1"

# Creating urls for the imgproxy (signing, resizing, ect)
gem "imgproxy", "~> 2.1.0"

# rate-limiting middleware
gem "rack-attack", "~> 6.6"
gem "redis", "~> 5.0"

group :development, :test do
  # Check for n+1 query
  gem "bullet", "~> 7.0"

  # ENV var loading
  gem "dotenv-rails", "~> 2.8.1"

  # Debugging on windows
  gem "byebug", platforms: %i[mri mingw x64_mingw]

  # Security testing
  gem "bundle-audit", "~> 0.1.0"
  gem "brakeman", "~> 5.4.0"

  # Automatic spec running
  gem "guard", "~> 2.18.0"
  gem "guard-rspec", "~> 4.7.3", require: false

  # Specs
  gem "rspec-rails", "~> 6.0.0"

  # API Documentation
  gem "rswag", "~> 2.8.0"

  # Linting
  gem "standard", "~> 1.26.0", require: false
  gem "rubocop-rails", "~> 2.18.0", require: false
  gem "rubocop-rspec", "~> 2.19.0", require: false
end

group :development do
  # Table descriptions in model files
  gem "annotate", "~> 3.2.0"

  # Error pages during dev
  gem "listen", ">= 3.0.5", "< 3.8.1"
  gem "web-console", ">= 3.3.0"
end

group :test do
  # Random data generation
  gem "faker", "~> 3.1.0"

  # Model factories
  gem "factory_bot_rails", "~> 6.1"

  # Rspec output
  gem "rspec_junit_formatter", "~> 0.6.0", require: false

  # Coverage
  gem "simplecov", "~> 0.22.0", require: false
  gem "simplecov-cobertura", "~> 2.1.0", require: false

  # Web request mocking
  gem "webmock", "~> 3.18.1"
end
