Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join("tmp", "caching-dev.txt").exist?
    config.action_controller.perform_caching = true
    config.action_controller.enable_fragment_cache_logging = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      "Cache-Control" => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Store uploaded files on the local file system (see config/storage.yml for options).
  # config.active_storage.service = :local

  # Don't care if the mailer can't send.
  # config.action_mailer.raise_delivery_errors = false

  # config.action_mailer.perform_caching = false

  # config.action_mailer.delivery_method = :smtp
  # config.action_mailer.smtp_settings = {
  #   address: "localhost",
  #   port: 1025
  # }

  # To render an HTML page with debugging information:
  config.debug_exception_response_format = :default

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Use sidekiq as job runner
  config.active_job.queue_adapter = :sidekiq

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  # config.assets.debug = true

  # Suppress logger output for asset requests.
  # config.assets.quiet = true

  # Raises error for missing translations.
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  config.mixpanel_token = ENV.fetch("MIXPANEL_TOKEN", nil)
  config.firebase_public_key = ENV.fetch("FIREBASE_PUBLIC_KEY")
  config.refresh_token_secret = ENV.fetch("TILIG_REFRESH_TOKEN_SECRET", "random-tilig-refresh-token-secret")
  config.access_token_secret = ENV.fetch("TILIG_ACCESS_TOKEN_SECRET", "random-tilig-access-token-secret")
  config.access_token_expiration_minutes = ENV.fetch("TILIG_ACCESS_TOKEN_EXPIRATION_MINUTES", 5).to_i
  config.brandfetch_api_token = ENV.fetch("BRANDFETCH_API_TOKEN", "random-brandfetch-api-token")
  config.hibp_api_token = ENV.fetch("HIBP_API_TOKEN", "random-token")

  config.mailchimp_api_key = ENV["MAILCHIMP_API_KEY"]
  config.mailchimp_list_id = ENV.fetch("MAILCHIMP_LIST_ID", "random-list-id")
  config.mailchimp_server = ENV.fetch("MAILCHIMP_SERVER", "random-server")

  config.mandrill_api_key = ENV.fetch("MANDRIlL_API_KEY", "random-key")

  config.webapp_domain = ENV.fetch("WEBAPP_DOMAIN", "app.tilig.com")

  config.accept_share_item_inviation_path = ENV.fetch("ACCEPT_SHARE_ITEM_INVIATION_PATH", "/invite/accept")
  config.accept_share_item_template_slug = ENV.fetch("ACCEPT_SHARE_ITEM_TEMPLATE_SLUG", "invite-share-item")
  config.notification_share_item_template_slug = ENV.fetch("NOTIFICATION_SHARE_ITEM_TEMPLATE_SLUG", "item-share-notification")

  config.accept_organization_invitation_path = ENV.fetch("ACCEPT_ORGANIZATION_INVITATION_PATH", "/accept-invite/")
  config.accept_organization_invitation_template_slug = ENV.fetch("ACCEPT_ORGANIZATION_INVITATION_TEMPLATE_SLUG", "organization-invite")

  config.accept_organization_invitation_confirmation_template_slug = ENV.fetch("ACCEPT_ORGANIZATION_INVITATION_CONFIRMATION_TEMPLATE_SLUG", "organization-invite-confirmation")
  config.sign_up_notice_for_folder_membership_creator_slug = ENV.fetch("SIGN_UP_NOTICE_FOR_FOLDER_MEMBERSHIP_CREATOR_SLUG", "sign-up-notice-for-membership-creator")

  config.cancellation_follow_up_template_slug = ENV.fetch("MAILCHIMP_CANCELLATION_FOLLOW_UP_TEMPLATE_SLUG", "cancellation-follow-up")

  config.referral_thank_you_template_slug = ENV.fetch("REFERRAL_THANK_YOU_TEMPLATE_SLUG", "referral-thank-you")

  config.api_base_domain = ENV["API_BASE_DOMAIN"] || ENV["GITLAB_ENVIRONMENT_URL"]&.delete_prefix("http://") || "api.tilig.com"

  config.hosts << "rails"
  # ngrok allows us to test Sign In With Apple locally (requires https)
  config.hosts << /[a-z0-9-]+\.ngrok.io/
  # localtunnel does the same
  config.hosts << /[a-z0-9-]+\.loca.lt/

  # Allow web console from docker host
  config.web_console.permissions = "172.18.0.1"
end

Rails.application.routes.default_url_options[:host] = Rails.application.config.api_base_domain
