# The test environment is used exclusively to run your application's
# test suite. You never need to work with it otherwise. Remember that
# your test database is "scratch space" for the test suite and is wiped
# and recreated between test runs. Don't rely on the data there!

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Added for testing, therefore only in the Test envirenment file.
  config.middleware.use Rack::MethodOverride

  config.cache_classes = false

  # Do not eager load code on boot. This avoids loading your whole application
  # just for the purpose of running a single test. If you are using a tool that
  # preloads Rails for running tests, you may have to set it to true.
  config.eager_load = false

  # Configure public file server for tests with Cache-Control for performance.
  config.public_file_server.enabled = true
  config.public_file_server.headers = {
    "Cache-Control" => "public, max-age=#{1.hour.to_i}"
  }

  # Show full error reports and disable caching.
  config.consider_all_requests_local = true
  config.action_controller.perform_caching = false
  config.cache_store = :null_store

  # Raise exceptions instead of rendering exception templates.
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment.
  config.action_controller.allow_forgery_protection = false

  # Store uploaded files on the local file system in a temporary directory.
  # config.active_storage.service = :test

  # config.action_mailer.perform_caching = false

  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  # config.action_mailer.delivery_method = :test

  # Print deprecation notices to the stderr.
  # config.active_support.deprecation = :stderr

  # Raises error for missing translations.
  # config.action_view.raise_on_missing_translations = true

  config.active_job.queue_adapter = :test

  config.mixpanel_token = nil

  Rack::Attack.enabled = false

  # The public key we use for testing
  config.firebase_public_key = "-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEj3qy5L8FlEMjj0PIvVLbOTSCKR+r
izHzVqMsvRKA/4O63XMEEwGduv5bl+/B1f1sTI4UyymYZ+jTpnrVgjs+qQ==
-----END PUBLIC KEY-----"
  config.refresh_token_secret = ENV.fetch("TILIG_REFRESH_TOKEN_SECRET", "random-tilig-refresh-token")
  config.access_token_secret = ENV.fetch("TILIG_ACCESS_TOKEN_SECRET", "random-tilig-access-token")
  config.access_token_expiration_minutes = ENV.fetch("TILIG_ACCESS_TOKEN_EXPIRATION_MINUTES", 1440).to_i # 1440 minutes in a day
  config.brandfetch_api_token = ENV.fetch("BRANDFETCH_API_TOKEN", "random-brandfetch-api-token")
  config.hibp_api_token = ENV.fetch("HIBP_API_TOKEN", "random-token")

  config.mailchimp_api_key = ENV.fetch("MAILCHIMP_API_KEY", "random-api-key")
  config.mailchimp_list_id = ENV.fetch("MAILCHIMP_LIST_ID", "random-list-id")
  config.mailchimp_server = ENV.fetch("MAILCHIMP_SERVER", "random-server")
  config.mandrill_api_key = ENV.fetch("MANDRIlL_API_KEY", "random-mandrill-api-key")

  config.webapp_domain = ENV.fetch("WEBAPP_DOMAIN", "app.tilig.com")
  config.accept_share_item_inviation_path = ENV.fetch("ACCEPT_SHARE_ITEM_INVIATION_PATH", "/invite/accept")
  config.accept_share_item_template_slug = ENV.fetch("ACCEPT_SHARE_ITEM_TEMPLATE_SLUG", "group-invite-first-test")
  config.notification_share_item_template_slug = ENV.fetch("NOTIFICATION_SHARE_ITEM_TEMPLATE_SLUG", "item-share-notification")

  config.accept_organization_invitation_path = ENV.fetch("ACCEPT_ORGANIZATION_INVITATION_PATH", "/accept-invite/")
  config.accept_organization_invitation_template_slug = ENV.fetch("ACCEPT_ORGANIZATION_INVITATION_TEMPLATE_SLUG", "organization-invite")

  config.accept_organization_invitation_confirmation_template_slug = ENV.fetch("ACCEPT_ORGANIZATION_INVITATION_CONFIRMATION_TEMPLATE_SLUG", "organization-invite-confirmation")
  config.sign_up_notice_for_folder_membership_creator_slug = ENV.fetch("SIGN_UP_NOTICE_FOR_FOLDER_MEMBERSHIP_CREATOR_SLUG", "sign-up-notice-for-membership-creator")

  config.cancellation_follow_up_template_slug = ENV.fetch("MAILCHIMP_CANCELLATION_FOLLOW_UP_TEMPLATE_SLUG", "cancellation-follow-up")

  config.referral_thank_you_template_slug = ENV.fetch("REFERRAL_THANK_YOU_TEMPLATE_SLUG", "referral-thank-you")

  config.api_base_domain = ENV["API_BASE_DOMAIN"] || ENV["GITLAB_ENVIRONMENT_URL"]&.delete_prefix("http://") || "api.tilig.com"

  config.absolute_url_base = ""

  config.after_initialize do
    Bullet.enable = true
    Bullet.raise = true
  end
end

Rails.application.routes.default_url_options[:host] = Rails.application.config.api_base_domain

RSpec.configure do |config|
  config.swagger_dry_run = false
end
