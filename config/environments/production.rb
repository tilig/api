Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local = false
  config.action_controller.perform_caching = true

  # Ensures that a master key has been made available in either ENV["RAILS_MASTER_KEY"]
  # or in config/master.key. This key is used to decrypt credentials (and other encrypted files).
  # config.require_master_key = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  config.public_file_server.enabled = ENV["RAILS_SERVE_STATIC_FILES"].present?

  # Compress CSS using a preprocessor.
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  # config.assets.compile = false

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Store uploaded files on the local file system (see config/storage.yml for options).
  # config.active_storage.service = :local

  # Mount Action Cable outside main process or domain.
  # config.action_cable.mount_path = nil
  # config.action_cable.url = 'wss://example.com/cable'
  # config.action_cable.allowed_request_origins = [ 'http://example.com', /http:\/\/example.*/ ]

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  config.log_level = :debug

  # Prepend all log lines with the following tags.
  config.log_tags = [:request_id]

  # Use a different cache store in production.
  config.cache_store = :memory_store

  # Use sidekiq as job runner
  # If Redis is not available (such as in review apps), it uses a default backend.
  config.active_job.queue_adapter = ENV["REDIS_URL"] ? :sidekiq : :async

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use semantic logger with JSON output
  $stdout.sync = true
  config.rails_semantic_logger.format = :json
  config.rails_semantic_logger.add_file_appender = false
  config.semantic_logger.add_appender(io: $stdout, formatter: config.rails_semantic_logger.format)

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false

  # Inserts middleware to perform automatic connection switching.
  # The `database_selector` hash is used to pass options to the DatabaseSelector
  # middleware. The `delay` is used to determine how long to wait after a write
  # to send a subsequent read to the primary.
  #
  # The `database_resolver` class is used by the middleware to determine which
  # database is appropriate to use based on the time delay.
  #
  # The `database_resolver_context` class is used by the middleware to set
  # timestamps for the last write to the primary. The resolver uses the context
  # class timestamps to determine how long to wait before reading from the
  # replica.
  #
  # By default Rails will store a last write timestamp in the session. The
  # DatabaseSelector middleware is designed as such you can define your own
  # strategy for connection switching and pass that into the middleware through
  # these configuration options.
  # config.active_record.database_selector = { delay: 2.seconds }
  # config.active_record.database_resolver = ActiveRecord::Middleware::DatabaseSelector::Resolver
  # config.active_record.database_resolver_context = ActiveRecord::Middleware::DatabaseSelector::Resolver::Session

  config.mixpanel_token = ENV.fetch("MIXPANEL_TOKEN", nil)
  config.firebase_public_key = ENV.fetch("FIREBASE_PUBLIC_KEY", nil)
  config.refresh_token_secret = ENV.fetch("TILIG_REFRESH_TOKEN_SECRET", nil)
  config.access_token_secret = ENV.fetch("TILIG_ACCESS_TOKEN_SECRET", nil)
  config.access_token_expiration_minutes = ENV.fetch("TILIG_ACCESS_TOKEN_EXPIRATION_MINUTES", 1440).to_i # 1440 minutes in a day
  config.brandfetch_api_token = ENV.fetch("BRANDFETCH_API_TOKEN", nil)
  config.hibp_api_token = ENV.fetch("HIBP_API_TOKEN", nil)

  config.mailchimp_api_key = ENV["MAILCHIMP_API_KEY"]
  config.mailchimp_list_id = ENV.fetch("MAILCHIMP_LIST_ID", "random-list-id")
  config.mailchimp_server = ENV.fetch("MAILCHIMP_SERVER", "random-server")
  config.mandrill_api_key = ENV.fetch("MANDRIlL_API_KEY")

  config.api_base_domain = ENV["API_BASE_DOMAIN"] || ENV["GITLAB_ENVIRONMENT_URL"]&.delete_prefix("http://") || "api.tilig.com"

  config.webapp_domain = ENV.fetch("WEBAPP_DOMAIN", "app.tilig.com")
  config.accept_share_item_inviation_path = ENV.fetch("ACCEPT_SHARE_ITEM_INVIATION_PATH", "/invite/accept")
  config.accept_share_item_template_slug = ENV.fetch("ACCEPT_SHARE_ITEM_TEMPLATE_SLUG", "group-invite-first-test")
  config.notification_share_item_template_slug = ENV.fetch("NOTIFICATION_SHARE_ITEM_TEMPLATE_SLUG", "item-share-notification")

  config.accept_organization_invitation_path = ENV.fetch("ACCEPT_ORGANIZATION_INVITATION_PATH", "/accept-invite/")
  config.accept_organization_invitation_template_slug = ENV.fetch("ACCEPT_ORGANIZATION_INVITATION_TEMPLATE_SLUG", "organization-invite")

  config.accept_organization_invitation_confirmation_template_slug = ENV.fetch("ACCEPT_ORGANIZATION_INVITATION_CONFIRMATION_TEMPLATE_SLUG", "organization-invite-confirmation")
  config.sign_up_notice_for_folder_membership_creator_slug = ENV.fetch("SIGN_UP_NOTICE_FOR_FOLDER_MEMBERSHIP_CREATOR_SLUG", "sign-up-notice-for-membership-creator")

  config.cancellation_follow_up_template_slug = ENV.fetch("MAILCHIMP_CANCELLATION_FOLLOW_UP_TEMPLATE_SLUG", "cancellation-follow-up")

  config.referral_thank_you_template_slug = ENV.fetch("REFERRAL_THANK_YOU_TEMPLATE_SLUG", "referral-thank-you")

  config.absolute_url_base = ENV.fetch("API_BASE_URL", nil)
end

Rails.application.routes.default_url_options[:host] = Rails.application.config.api_base_domain
