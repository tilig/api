require_relative "boot"

require "rails"
require "active_model/railtie" # Requires neccecery modules.
require "active_record/railtie" # Active Record is the M in MVC. Therefore responsible for representing business data and logic.
require "action_controller/railtie" # Action Controller is the C in MVC. The controller is responsible for making sense of the request and producing the appropriate output.
require "action_view/railtie" # Action View is the V in MVC. Action View is then responsible for compiling the response. Evendo we have an API, we use this to create the JSON response.
require "active_job/railtie" # Responsible for declaring delayed jobs and making them run later.
require_relative "handle_malformed_strings"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TiligApi
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    config.api_only = true

    # List of middlewares that we don't need
    # Documentation on middlewares: https://guides.rubyonrails.org/rails_on_rack.html#internal-middleware-stack
    config.middleware.delete ::Rack::Sendfile # We do not directly send files as download, therefore can be removed.
    config.middleware.delete ::Rack::Runtime # We do not need X-Runtime header (indicating the response time of the request in seconds).

    config.middleware.use ActionDispatch::ContentSecurityPolicy::Middleware # We do need to set a CSP header (evendo we are an API).
    config.middleware.use HandleMalformedStrings

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.autoload_paths += %W[lib/modules/]
  end
end
