Sentry.init do |config|
  config.dsn = ENV.fetch("SENTRY_TILIG_API_DSN", "")

  # Filter sensitive parameters from the event
  filter =
    ActiveSupport::ParameterFilter.new(
      Rails.application.config.filter_parameters
    )
  config.before_send = ->(event, hint) { filter.filter(event.to_hash) }

  config.enabled_environments = %w[review staging production]
  config.breadcrumbs_logger = %i[active_support_logger http_logger]

  # Attach an environment to Sentry messages.
  # review/x/y deploys get set to review.
  environment = ENV["GITLAB_ENVIRONMENT_NAME"].presence || Rails.env
  config.environment = environment.split("/").first

  config.release = ENV["GITLAB_COMMIT_SHORT_SHA"] if ENV[
    "GITLAB_COMMIT_SHORT_SHA"
  ]

  # Uniform sample rate between 0.0 and 1.0
  if ENV["SENTRY_TRACE_SAMPLES"] == "true"
    config.traces_sampler = lambda do |sampling_context|
      # if this is the continuation of a trace, just use that decision (rate controlled by the caller)
      unless sampling_context[:parent_sampled].nil?
        next sampling_context[:parent_sampled]
      end

      # transaction_context is the transaction object in hash form
      # keep in mind that sampling happens right after the transaction is initialized
      # for example, at the beginning of the request
      transaction_context = sampling_context[:transaction_context]

      # transaction_context helps you sample transactions with more sophistication
      # for example, you can provide different sample rates based on the operation or name
      op = transaction_context[:op]
      transaction_name = transaction_context[:name]

      case op
      when /http/
        # for Rails applications, transaction_name would be the request's path (env["PATH_INFO"]) instead of "Controller#action"
        case transaction_name
        when /api\/v3\/polling/
          0.01
        when /api/
          0.1
        else
          0.01
        end
      when /sidekiq/
        0.01 # you may want to set a lower rate for background jobs if the number is large
      else
        0.0 # ignore all other transactions
      end
    end
  end
end
