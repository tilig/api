# We use the SemanticLogger gem to completely replace
# the Rails logger with a JSON logger in production.
SemanticLogger.application = "tilig-server-api"
