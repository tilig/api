Rails.application.config.middleware.insert_before 0, Rack::Cors do
  fallback_origins = Rails.env.development? ? "*" : "app.tilig.com"

  allowed_origins = ENV.fetch("CORS_ORIGINS", fallback_origins).split(",")

  allow do
    origins allowed_origins
    resource "*", headers: :any, methods: %i[get post patch put delete options]
  end

  if ENV["CORS_ALLOW_NETLIFY_PREVIEWS"].present?
    allow do
      origins %r{\Ahttps://[a-z0-9-]+-tilig.vercel.app\z},
        %r{\Ahttps://[a-z0-9-]+--tilig-staging.netlify.app\z},
        %r{\Ahttp://localhost:[0-9]+\z}
      resource "*",
        headers: :any,
        methods: %i[get post patch put delete options]
    end
  end
end
