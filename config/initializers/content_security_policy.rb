# Be sure to restart your server when you modify this file.

# Define an application-wide content security policy
# For further information see the following documentation
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy

Rails.application.config.content_security_policy do |policy|
  policy.default_src :none

  # Specify URI for violation reports
  policy.report_uri "https://o390798.ingest.sentry.io/api/5255022/security/?sentry_key=5b13b3317e8742ae918d9316d9061ac4" if !Rails.env.development?
  policy.style_src "'sha256-rxhvUKfOQ57SBgSxIR3RxOCTCyZKcNMi4xYYQ6kaJTs='"
  policy.img_src "https://app.tilig.com/"
end

# If you are using UJS then enable automatic nonce generation
# Rails.application.config.content_security_policy_nonce_generator = -> request { SecureRandom.base64(16) }

# Set the nonce only to specific directives
# Rails.application.config.content_security_policy_nonce_directives = %w(script-src)

# Report CSP violations to a specified URI
# For further information see the following documentation:
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy-Report-Only
# Rails.application.config.content_security_policy_report_only = true
