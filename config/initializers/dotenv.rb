# Set file location for dotenv to root dir
module Dotenv
  class Railtie < Rails::Railtie
    private

    def dotenv_files
      [root.join("../../.env")].compact
    end
  end
end

Dotenv::Railtie.load if !Rails.env.production?
