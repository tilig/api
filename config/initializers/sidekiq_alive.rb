# SidekiqAlive adds a liveliness check endpoint
# to Sidekiq, allowing Kubernetes to monitor the
# healthiness of the pod.
SidekiqAlive.setup do |config|
  # Kubernetes expects liveliness checks on port :5000
  config.port = ENV.fetch("SIDEKIQ_ALIVE_PORT", 5000)

  # Use puma, since we have it in the Gemfile
  config.server = ENV.fetch("SIDEKIQ_ALIVE_SERVER", "puma")
end
