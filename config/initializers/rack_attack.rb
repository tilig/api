### Configure Cache ###

# Note: The store is only used for throttling (not blocklisting and
# safelisting). It must implement .increment and .write like
# ActiveSupport::Cache::Store
Rack::Attack.cache.store = if !ENV["RACK_ATTACK_REDIS_URL"] || Rails.env.test?
  ActiveSupport::Cache::MemoryStore.new
else
  ActiveSupport::Cache::RedisCacheStore.new(url: ENV["RACK_ATTACK_REDIS_URL"])
end

### Throttle Spammy Clients ###

# If any single client IP is making tons of requests, then they're
# probably malicious or a poorly-configured scraper. Either way, they
# don't deserve to hog all of the app server's CPU. Cut them off!

# Throttle all requests by IP
#
# Key: "rack::attack:#{Time.now.to_i/:period}:req/ip:#{req.ip}"
Rack::Attack.throttle("req/ip", limit: 1000, period: 1.minute) do |req|
  req.ip
end

### Prevent Brute-Force Login Attacks ###

# Throttle POST requests to /api/v3/authenticate by IP address
#
# Key: "rack::attack:#{Time.now.to_i/:period}:logins/ip:#{req.ip}"
Rack::Attack.throttle("logins/ip", limit: 10, period: 30.seconds) do |req|
  if req.path == "/api/v3/authenticate" && req.post?
    req.ip
  end
end

Rack::Attack.throttle("req/ip/public-keys-tries", limit: 30, period: 30.seconds) do |req|
  req.ip if req.path == "/api/v3/users/public_key"
end

ActiveSupport::Notifications.subscribe(/rack_attack/) do |name, start, finish, request_id, payload|
  if payload[:request].env["rack.attack.matched"] == "req/ip/public-keys-tries"
    Sentry.set_user(
      id: get_user_uid(payload[:request]),
      ip_address: payload[:request].env["action_dispatch.remote_ip"].to_s
    )
    Sentry.capture_message "Rack::Attack throttle req/ip/public-keys-tries"
  end
end

# Get the UID of the JWT payload, rescue for any errors with return nil
def get_user_uid(request)
  # Check if authorization header exists
  authorization_header = request.env["HTTP_AUTHORIZATION"]
  return if authorization_header.blank?

  # Check if authorization header is present
  jwt_token = authorization_header.match(/Bearer\s+([^\s]+)/)
  return authorization_header if jwt_token.blank?

  begin
    # Decode the JWT payload
    payload, _header = JWT.decode(jwt_token[1], nil, false)
    # Check for a UID
    payload.dig("uid") || jwt_token[1]
  rescue
    jwt_token[1]
  end
end

# Block suspicious requests for '/etc/password', admin and wordpress specific paths.
# After 3 blocked requests in 10 minutes, block all requests from that IP for 30 minutes.
Rack::Attack.blocklist("fail2ban autoscanners") do |req|
  # `filter` returns truthy value if request fails, or if it's from a previously banned IP
  # so the request is blocked
  Rack::Attack::Fail2Ban.filter("autoscanners-#{req.ip}", maxretry: 3, findtime: 10.minutes, bantime: 30.minutes) do
    # The count for the IP is incremented if the return value is truthy
    req.path.include?("/etc/passwd") ||
      req.path.include?("admin") ||
      req.path.include?("wp-content") ||
      req.path.include?("wp-admin") ||
      req.path.include?("wp-login")
  end
end
