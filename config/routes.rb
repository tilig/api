Rails.application.routes.draw do
  root to: "home#index"

  if Rails.env.development?
    mount Rswag::Ui::Engine => "/api-docs"
    mount Rswag::Api::Engine => "/api-docs"
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api, defaults: {format: :json} do
    namespace :v3 do
      post :authenticate, to: "authentication#authenticate"
      post :refresh, to: "refresh_token#refresh"

      resources :secrets do
        resources :password_versions, only: %i[index]
        post :search, on: :collection
        # resource :share, only: %i[create destroy]
        resources :share, only: %i[create], controller: "shares"
        delete "share", action: :destroy, controller: "shares"
      end

      resources :items do
        resource :share_link, only: %i[create destroy], module: :items
      end

      resources :contacts, only: :index

      resources :folders, only: %i[index create update destroy] do
        resources :folder_memberships, only: %i[create update destroy], module: :folders do
          resource :acceptance, only: :create
          get :acceptance, controller: "acceptances", action: :accept, as: "get_acceptance"
        end
      end

      resource :organization, only: %i[create show update] do
        resources :invitations, only: %i[show create update destroy], module: :organizations
        resources :my_invitations, only: %i[index update destroy], module: :organizations
      end

      resources :shares, only: %i[index]
      resource :profile, controller: :profile, except: :create do
        resource :user_settings, only: %i[update], module: :profile
        resource :referrals, only: %i[update], module: :profile
      end
      namespace :users do
        resource :public_key, only: %i[show]
      end
      resource :keypairs, only: :create

      resources :brands, only: [:index]

      namespace :brands do
        resources :search, only: [:index], on: :collection
      end

      resource :answers, only: [:create]

      resources :track, only: %i[create]
      resources :track_person, only: %i[create]
      resources :anonymous_track, only: %i[create]

      namespace :polling do
        resources :secrets, only: %i[index]
        resources :items, only: %i[index]
      end

      resources :referral_leaderboard, only: [:index]
    end

    namespace :shares do
      namespace :v1 do
        resources :private_links, only: :show, param: :uuid
      end
    end
  end
end
